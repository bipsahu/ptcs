<div class="container">

  <center><h3>Search Form</h3></center>

      <table class="table" id="table_search">

        <form method="post" id="customer_form" action="<?php echo base_url();?>index.php/cts_search_controller/search" >
         
			
			<tr>
	
			<?php $data = $this->session->userdata('data');?>
               <td><input type="text" class="form-control" name="regd_no" placeholder= "Task" value="<?php if(isset($$data['regd_no'])) echo $data['regd_no']; ?>"></td>

			  <td><input type="text" class="form-control" name="class" placeholder="Project" value="<?php if(isset($$data['class'])) echo $data['class']; ?>" ></td>
			   <td><input type="text" class="form-control" name="insure" placeholder ="Branch" value="<?php if(isset($$data['insure'])) echo $data['insure']; ?>" ></td>

                 <td><select name="department">
                              <option value="">Select Department</option>
                             <?php 
                            
                             foreach($departments as $key=>$dep){?>
                                <option value="<?php echo $key;?>"><?php echo $dep->dep_name;?></option>
                             <?php }?>
                
                 </select></td>
			<tr>
			<td><button type="submit" name="search" class="btn btn-primary" ><div class='glyphicon glyphicon-search'></div></button>
			<a href= "<?php echo base_url()?>index.php/cts_search_controller/clear_session" class="btn btn-primary"> <div class='glyphicon glyphicon-refresh'></div></a></td>
			</tr>
        </form>
    </table>
			    
            
<?php 	if (isset ($result))
{
?>
<h1>Search Results</h1>
<table class="table table-striped datatable">
<thead><tr id="new_customer_table_row"><th>Task No.</th><th>Task Name</th><th>Project</th><th>Branch</th><th>Department</th><th>status</th><th>Detail</th></tr></thead>

<tbody>
<?php
$offset =  $offset+1;
foreach($results as $row){
?>
<tr>
<td><?php echo $row['regd_no'];?></td>
	<td><?php echo $row['claim_no'];?></td>
   	<td><?php echo $row['class'];?></td>
    <td><?php if($row['vehicle_no']!='0') echo $row['vehicle_no'];?></td>
	<td><?php echo $row['insure'];?></td>

    <td><?php if ($row->status == 0) echo '<span class="green">Complete</span>';
        if ($row->status == 1) echo '<span class="red">Held For Approval</span>';
        if ($row->status == 2) echo '<span class="green">Complete</span>';
        if ($row->status == 3) echo '<span class="yellow">On Going</span>';
        if ($row->status == 4) echo '<span class="red">Held For Approval</span>';
        if ($row->status == 5) echo '<span class="yello">On Going</span>';
        if ($row->status == 6) echo '<span class="green">Complete</span>';
        ?></td>
	

<td> <a class="btn btn-primary glyphicon glyphicon-list" alt="<?php echo $row['claim_id'];?>" data-toggle='modal' data-target='#insure_details_<?php echo $row['claim_id'];?>'></a></td>
    <div class="modal fade" id="insure_details_<?php echo $row['claim_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Task Detail</h4>
      </div>
      <div class="modal-body">
        <?php $details['row']=(object)$row;$this->load->view('surveyor_deportation/cts_sd_show_all_claim_details',$details);?>
      </div>
    </div>
  </div>
</div>

</tr>

<?php 
$offset++;
}
?>
</tbody>
</table>
<?php 
}
if(isset($links))
echo $links;
?>
</div><!---end of container-->

 </body>
 </html>