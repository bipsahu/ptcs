<div class="container">
<div class='alert alert-danger' role='alert' id="species_type_registration_alert"'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Species Type Already Exists</b></div>
  <h3>Species Type Registration Form</h3><br>
      <table class="table" id="table">

        <form method="post" role="form" enctype="multipart/form-data" id="species_type_form">
    
			
			<tr>
              <td><label>Species Type </label></td>
              <td><input type="text" class="form-control" name="species_type" id="species_type" required> </td>
            </tr>

            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary" id="species_type_submit">Submit</button></td>
            </tr>
          
        </form>
    </table>
</div><!---end of container-->