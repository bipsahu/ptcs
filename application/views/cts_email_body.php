<strong>Date: 17/11/2014 <br>
Mr Sata Eng<br><br> 
Ref.  : 	Survey of Vehicle loss under our
		Policy No: POL/1000/31/MOT-PRD-003/2014/005806
		Claim No: 011/02/04/0101/486
		Insured: M/s Himalayan Bank/ Big Suppliers Pv. Ltd
		Vehicle No: Ba 3 Kha 3862
</strong><br><br>
Dear sir,<br><br>

With reference to the above, we would like to request you to carry out a survey under vehicle insurance policy for the damage of vehicle. The contact person of insured on the following number can be contacted to carry out the survey work as well as to find the exact location of loss.<br><br>

<strong>Bishal Kuwar : 9841746746</strong><br><br><br>

Kindly submit the survery report within 15days as per insurance rules, 2049.<br><br>

Thanks and best regards,<br>
Shailesh Khanal<br><br>

01/08/2071