<div class="container">
	 <div class="table row" id="approve_table">
        <form method="post" role="form" class="form-horizontal" enctype="multipart/form-data" id="dv_insert_details" >
         
			<div class="form-group">
              <label class="col-sm-2 control-label">Cheque Received Date</label>
              
              <input type="text" class="col-sm-8 form-control" name="cheque_received_date" id="cheque_received_date" value="<?php echo date('Y/m/d');?>" required />
          </div>
          <div class="form-group">
			<label class="col-sm-2 control-label">Cheque Type</label>
             <select class="form-control" name="cheque_type" required>
             	<option value="">Select Type</option>
             	<option value="0">Advance</option>
                <option value="1">Final</option>
             </select>
           
          </div>
           <div class="form-group pull-left">
            <button type="submit" name="submit" class="btn btn-primary" id="discharge_voucher_insert_submit" >Save</button>
           </div>
     </div>
        </form>
   

</div><!---end of container-->
</body>
<script type="text/javascript">
$(document).ready(function(){
	$("#cheque_received_date").datepicker({ dateFormat: 'yy/mm/dd'});
});
</script>
</html>