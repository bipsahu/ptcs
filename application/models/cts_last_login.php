<?php 

Class cts_last_login extends CI_model
{
	public function last_login()
	{
		date_default_timezone_set('UTC'); 
		$data = array(
		
		'user_last_login_date' => date('m-d-y')
		);
		$this->db->where('user_email', $this->session->userdata('email'));
		$result = $this->db->update('user', $data);
	}

}