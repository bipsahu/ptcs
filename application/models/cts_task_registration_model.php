<?php

/*
 * This is task model class that deals with the database logic for task registration
*/

class cts_task_registration_model extends CI_Model
{
    /*
     *insert the posted data from form thorugh controller
     * @return : boolean
     * */
    public function insert_task_registration_details($task_id = '')
    {
        $data = $this->input->post('task');

        $data["indicators"] = json_encode($data["indicators"]);
        $data["members"] = json_encode($data["members"]);
        $data['assigned_by'] = $this->session->userdata['email'];
        $data['updated_by'] = $this->session->userdata['email'];
        $this->db->select('achieved');
        $task_detail = $this->db->get_where('task_registration',array('task_id' => $task_id))->result_array();
        if( isset($task_detail[0]['achieved']) && $task_detail[0]['achieved'] != '')
        {

            $data['percent_complete'] = $this->calculate_percent($data['indicators'],$task_detail[0]['achieved']);
        }
          else
        {
            $data['percent_complete'] = '';
        }

        unset($data['indicator']);
        unset($data['member']);
        //update if submitted from edit request else insert
        if (isset($task_id) && $task_id != NULL) 
        {
            $data["updated_at"] = date("Y-m-d H:i:s");
            // echo $data["updated_at"];die;
            //$data['percent_complete'] = $this->calculate_percent($data['indicators'],$data['achieved']); //calculate ach

            $this->db->where("task_id", $task_id);
            $result = $this->db->update('task_registration', $data);

            $this->db->delete('notification', array('task_id' => $task_id));
        } 
        else 
        {
            $data["created_at"] = date("Y-m-d H:i:s");
            if ($data["percent_complete"] == 100) {
                $data["status"] = "2";
            }
            $data["updated_at"] = $data["created_at"];
            // echo "<pre>";
            // print_r($data);die;
            $result = $this->db->insert('task_registration', $data);
            // print_r($this->db->last_query());die;

            $task_id = $this->db->insert_id();
        }

        if(isset($data['deadline_date']) and isset($data['alert_day'] ) and $data['deadline_date'] != '' and $data['alert_day'] != '')
        {
            $deadline_date = $data['deadline_date'];
            $alert_date = date("Y-m-d",strtotime($deadline_date . ' -'.$data['alert_day'].' day'));
            $notification['deadline_date'] = $deadline_date;
            $notification['alert_date'] = $alert_date;
            $notification['task_id'] = $task_id;
            $notification['user_id'] = $this->session->userdata['user_id'];
            $notification['status'] = 0;
            $this->db->insert('notification', $notification);
            $minedata = json_decode($data['members'],true);
            if(!empty($minedata))
            {
                foreach ($minedata as $key => $value) 
                {
                    if($key != $this->session->userdata['user_id'])
                    {
                        $notification['user_id'] = $key;
                        $this->db->insert('notification', $notification);
                    }
                }
            }
        }
        
        return $result;
    }


    public function update_task_completion($task_id = '')
    {
        // echo "hello";die;
        $data = $this->db->query("SELECT * FROM task_registration WHERE task_id = '$task_id'")->result_array();
        // print_r($data);die;
        return $data;
    }

    /*
     * Calculates the achieved completed percent
     * @param array $indicator
     * @param array $achieved
     * @return int
     */
    public function calculate_percent($indicators,$achieved)
    {

        $indicators = json_decode($indicators,true);
        $achieved = json_decode($achieved,true);
        $finalValue = 0;
        foreach($indicators as $key => $value) {
            $finalValue += $achieved[$key]/100 * $value;

        }
        return $finalValue;

    }
    /*
     * Get all tables from database task_registration
     * @return : boolean
     * */
    public function show_all_task()
    {
        // echo 'i am here';die;

        //$this->db->limit($limit, $start);
        $this->db->select("*");
        $this->db->from("task_registration");
        // $this->db->join("department", "task_registration.department = department.dep_id", "inner");
        $this->db->join("project", "task_registration.project = project.project_id", "inner");
        $this->db->join("task_category", "task_registration.task_category_id = task_category.t_category_id", "left");
        $this->db->join("department", "task_registration.department = department.dep_id", "left");
        $this->db->order_by("regd_no", "desc");
        $result = $this->db->get();
        // echo "<pre>";
        // print_r($result->result_array());die;
        return $result;
    }

    /*
     *get all the task from task_registration table by project_id
     * @param : ajax POST project_id
     * @return : array
     * */
    public function get_all_task_by_project_id($project_id)
    {
        $this->db->select("task_registration.task_id,task_registration.task_name");
        $result = $this->db->get_where('task_registration', array('project' => $project_id));
        return $result->result_array();
    }

    public function get_all_task_category_by_project_id($project_id)
    {
        // echo "hello";die;
        $this->db->select("*");
        $result = $this->db->get_where('project', array('project_id' => $project_id))->result_array();
        if($result[0]['project_taskcategory'] != '')
        {
            $task_category = json_decode($result[0]['project_taskcategory'], true);
        }
        foreach ($task_category as $key => $value) {
            # code...
            $list_task_category = $this->db->query("SELECT * FROM task_category WHERE t_category_id = '$key'")->result_array();
            $final_result[] = $list_task_category[0];
        }
        // print_r($final_result);die;
        return $final_result;
    }

    public function get_details_of_task_by_id($task_id)
    {
        $result = $this->db->get_where('task_registration', array('task_id' => $task_id));
        return $result->result_array();
    }

    public function edit_task_by_id($task_id)
    {
        
        $vehicle_no = $this->input->post('vehicle_no');
        $data = array(
            //'customer_id' => $this->input->post('task_customer_id'),
            //'insure_id' => $this->input->post('task_insure_id'),
            'assign_date' => $this->input->post('assign_date'),
            'accident_date' => $this->input->post('accident_date'),
            //'customer_name' => $customer_name,
            'task_no' => $this->input->post('task_no'),
            'insure' => $this->input->post('insure_name'),
            'class' => $this->input->post('class'),

            'vehicle_no' => $vehicle_no,
            'applicant_name' => $this->input->post('applicant_name'),
            'applicant_contact' => $this->input->post('applicant_contact'),
            'task_branch' => $this->input->post('task_branch'),
            'task_amount' => $this->input->post('task_amount'),
            'sum_insured' => $this->input->post('sum_insured'),
            'marketting_code' => $this->input->post('marketting_code'),
            'policy_no' => $this->input->post('policy_no'),
            'receipt_no' => $this->input->post('receipt_no'),
            'premium_paid_date' => $this->input->post('premium_paid_date'),
            'policy_from' => $this->input->post('policy_from'),
            'policy_to' => $this->input->post('policy_to'),
            //'animal_type'=>$this->input->post('animal_type'),
            //'investigation_requested' => $investigation,
            //'task_type' => $this->input->post('task_type'),
            //'short_history' => $this->input->post('short_history'),
            //'no_of_task' => $this->input->post('no_of_task'),
            //'surveyor' => $this->input->post('surveyor'),
            //'refrence_no' => $this->input->post('refrence_no'),
            //'paid_amount' => $this->input->post('paid_amount'),
            'remarks' => $this->input->post('remarks')
        );
        $task_id = $this->input->post('task_id');
        $this->db->where('task_id', $task_id);
        $result = $this->db->update('task_registration', $data);

        $last_inserted_task_id = $task_id;

        return $result;
    }

    public function is_editable($task_id)
    {


        $query = $this->db->get_where('department', array('dep_id' => $task_id));

        foreach ($query->result() as $row) {
            //echo $row->dep_name;
            if ($row->dep_name == "evaluation" || $row->dep_name == "verification" || $row->dep_name == "molecular_legal") {

                //$this->db->select('registered');
                $dep_name = explode("_", $row->dep_name);
                if (count($dep_name) == 1)
                    $dep_name = $dep_name[0];
                else
                    $dep_name = $dep_name[1];
                $query1 = $this->db->get_where($dep_name, array($dep_name . ".task_id" => $task_id));

                foreach ($query1->result() as $row1) {
                    if ($row1->registered > 1)
                        return 1;
                    else
                        return 0;

                }
            }
            /* else
            {
                return $row1->registered;
            }  */
        }
    }

    public function delete_task_by_id($task_id)
    {
        $result = $this->db->delete('task_registration', array('task_id' => $task_id));
        $this->db->delete('notification', array('task_id =>$task_id'));
        $this->db->delete('comment', array('task_id =>$task_id'));
        return $result;
    }

    public function get_max_count()
    {
        $this->db->select_max('regd_no');
        $result = $this->db->get('task_registration');
        return $result;
    }

    public function get_department_of_customer_task($task_id)
    {
        $this->db->select('dep_id');
        $result = $this->db->get_where('task_department', array('task_id' => $task_id));
        return $result->result_array();
    }

}
