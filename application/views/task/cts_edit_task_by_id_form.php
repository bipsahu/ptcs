<?php
if(isset($result))
{
	if($result==1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Update Succesful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Update Failed!</b></div>";
}
if(isset($sample_type)){
foreach($sample_type as $row)
{
$sid = $row['sid'];
$sample_type = $row['sample_type'];
}
}
?>

<div class="container">
    <h3>Edit Sample Detail Form</h3><br>
           <table class="table table-striped" id="table">

        <form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/lims_sampl_controller/edit_sample_by_id/<?php echo $sid;?>">

			
			<tr>
              <td><label>Sample Type</label></td>
              <td><input type="text" class="form-control" name="sample_type" value="<?php echo $sample_type;?>" required></td>
			  <td></td>
            </tr> 
		
		  <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary">Submit</button></td>
			   <td></td>
            </tr>
          
        </form>
    </table>
	
</div><!---end of container-->
 </body>
 </html>
 
			

