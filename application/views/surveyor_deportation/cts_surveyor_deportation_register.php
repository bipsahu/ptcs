<div class="sd_success alert alert-success">Saved Successfully!</div>
 <form method="post" id="surveyor_deportation_form" class="surveyor_deportation_form">   
       <div class="row">
            <div class="form-group col-sm-4">
              <div><label>Deputed Date</label></div>
              <div><input type="text" class="form-control datepicker" name="appointment_date" value="<?php echo date('Y/m/d');?>" ></div>
            </div>
           <div class="form-group col-sm-4">
              <div><label>Survey Type</label></div>
              <div><select class="form-control" id="survey_type"  name="survey_type" >
              			<option value="">Select Type</option>
                        <option value="0">Spot</option>
                        <option value="1">Final</option>
						<option value="2">Legal Investigation</option>
						<option value="3">N/S</option>

              		</select></div>
            </div>
		  <div class="form-group col-sm-4">
              <div><label>Surveyor</label></div>
              <div><input type="text" class="form-control surveyor" tabindex="5" name="surveyor"  required></div>
			<div class="species_msg msg">Surveyor Does Not Exist</div>
			</div>
          
			<div class="form-group col-sm-4">
                  <div><label>Report Received Date</label></div>
                  <div><input type="text" class="datepicker form-control" id="result_received_date" name="result_received_date" value="<?php echo date('Y/m/d');?>"></div>
            </div>
			
        	 <div class="form-group col-sm-4">
                  <div><label>Assessed Amount</label></div>
                  <div><input type="text" class="form-control" name="surveyor_estimated_loss" ></div>
            </div>
			
            
			<div class="form-group col-sm-4">
                  <div><label>Remarks</label></div>
                  <div><textarea class="form-control" name="surveyor_remarks"></textarea></div>
            </div>
		
			
<?php //new surveyor entry
for($count=1;$count<=10;$count++){?>

<div class="table new_surveyor mar-top-20 new_surveyor_<?php echo $count;?>" id="new_surveyor_<?php echo $count;?>"> 
         <hr>
          
            <div class="form-group col-sm-4">
              <div><label>Deputed Date</label></div>
              <div><input type="text" class="form-control datepicker" name="appointment_date_<?php echo $count;?>" value="<?php echo date('Y/m/d');?>" ></div>
            </div>
           <div class="form-group col-sm-4">
              <div><label>Survey Type</label></div>
              <div><select class="form-control" id="survey_type" name="survey_type_<?php echo $count;?>" >
              			<option value="">Select Type</option>
                        <option value="0">Spot</option>
                        <option value="1">Final</option>
						<option value="2">Legal Investigation</option>
						<option value="3">N/S</option>
              		</select>
               </div>
            </div>
		  <div class="form-group col-sm-4">
              <div><label>Surveyor</label></div>
              <div><input type="text" class="form-control surveyor" tabindex="5" name="surveyor_<?php echo $count;?>" ></div>
			
			<div class="msg species_msg">Surveyor Does Not Exist</div>
		</div>
        
        
			<div class="form-group col-sm-4">
                  <div><label>Report Received Date</label></div>
                  <div><input type="text" class="form-control datepicker" name="result_received_date_<?php echo $count;?>" value="<?php echo date('Y/m/d');?>"></div>
            </div>
			
        	 <div class="form-group col-sm-4">
                  <div><label>Assess Amount</label></div>
                  <div><input type="text" class="form-control" name="surveyor_estimated_loss_<?php echo $count;?>" ></div>
            </div>
			
            
			<div class="form-group col-sm-4">
                  <div><label>Remarks</label></div>
                  <div><textarea class="form-control" name="surveyor_remarks_<?php echo $count;?>" ></textarea></div>
            </div>
	
		</div>
<?php } //new surveyor ends?>
			<div class="form-group">
				<input type="hidden" class="sd_id" id="sd_id" name="sd_id" value="<?php if(isset($result)&&$result!=NULL) echo $result->sd_id;?>">
             </div>
			
        		   <div class="col-sm-2"><input type="submit" name="submit" class="btn btn-primary" id="surveyor_deportation_submit"  value="save"/></div>	
                   <div class="col-sm-2  col-sm-offset-6"> <a href="#" class="btn btn-primary add_new_surveyor" id="add_new_surveyor">Add More Surveyor</a></div>
          </div>
         </form>

</body>
<script type="application/javascript">
$(document).ready(function(){
			var count=1;
			$(".datepicker").datepicker({ dateFormat: 'yy/mm/dd'});
			$(".add_new_surveyor").click(function(){
								$(".new_surveyor_"+count).show();
								count++;
			});
	
});
</script>
</html>