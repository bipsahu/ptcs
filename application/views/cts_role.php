<div class="container">
	<div id="img-icon">
      <img src="<?php echo base_url();?>resource/images/add-role.jpg" align="left"/>
    </div>
    <div id="breadcumb-text">
      <ol class="breadcrumb">
        <li><strong>Role</strong></li>&nbsp;&nbsp;
        <li class="vertical-divider"></li>
        <li class="active">Add Department Role</li>
      </ol>
    </div>
    <hr>
<?php
if(!empty($result_indicator))
{
	if($result_indicator == 1)
	{
		echo "<div class='alert alert-info' role='alert' id='role-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Roles Successfully Assigned!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='role-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Role Assignment Failed!</b></div>";
}
?>
<form action="<?php echo base_url();?>index.php/cts_role_controller/add_role" method="post">
<table class="table table-striped" id="role_table">

<thead><th>Position</th><th>Add Report</th><th>Edit Report</th><th>Delete Report</th><th>Generate Report</th><th>Approve Report</th>
<?php 
$num_of_roles_assigned = count($roles_info);
$i=0;
foreach($positions->result() as $key=>$row)
{
?>
</thead>
<td><?php echo $row->position_name;?></td>
<?php $row->position_name = str_replace(" ","_",$row->position_name);?>
<?php if($i!=$num_of_roles_assigned){?>
<td class="onoffswitch"> <input type="checkbox" class="onoffswitch-checkbox" id="myonoffswitch-add-<?php echo $key?>" name="<?php echo $row->position_name;?>_add" value="1" <?php if($roles_info[$i]['add_role']) echo "checked";?>>
	<label class="onoffswitch-label" for="myonoffswitch-add-<?php echo $key?>">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
 </td>
<td class="onoffswitch"> <input type="checkbox" class="onoffswitch-checkbox" id="myonoffswitch-edit-<?php echo $key?>" name="<?php echo $row->position_name;?>_edit" value="1" <?php if($roles_info[$i]['edit_role']) echo "checked";?>>
<label class="onoffswitch-label" for="myonoffswitch-edit-<?php echo $key;?>">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</td>
<td class="onoffswitch"> <input type="checkbox" class="onoffswitch-checkbox" id="myonoffswitch-delete-<?php echo $key?>" name="<?php echo $row->position_name;?>_delete" value="1" <?php if($roles_info[$i]['delete_role']) echo "checked";?>>
<label class="onoffswitch-label" for="myonoffswitch-delete-<?php echo $key?>">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</td>
<td class="onoffswitch"> <input type="checkbox" class="onoffswitch-checkbox" id="myonoffswitch-generate-<?php echo $key?>" name="<?php echo $row->position_name;?>_report" value="1" <?php if($roles_info[$i]['report_generate_role']) echo "checked";?>>
<label class="onoffswitch-label" for="myonoffswitch-generate-<?php echo $key?>">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
    </td>
<td class="onoffswitch"> <input type="checkbox" class="onoffswitch-checkbox" id="myonoffswitch-approve-<?php echo $key?>" name="<?php echo $row->position_name;?>_approve" value="1" <?php if($roles_info[$i]['approve_role']) echo "checked";?>>
<label class="onoffswitch-label" for="myonoffswitch-approve-<?php echo $key;?>">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
   </label>
</td>
<?php
$i++;
}
else
{
?>
<td> <input type="checkbox" class="role-check" name="<?php echo $row->position_name;?>_add" value="1"></td>
<td> <input type="checkbox" class="role-check" name="<?php echo $row->position_name;?>_edit" value="1"></td>
<td> <input type="checkbox" class="role-check" name="<?php echo $row->position_name;?>_delete" value="1"></td>
<td> <input type="checkbox" class="role-check" name="<?php echo $row->position_name;?>_report" value="1"></td>
<td> <input type="checkbox" class="role-check" name="<?php echo $row->position_name;?>_view" value="1"></td>
</tr>
<?php
}
echo "</tr>";
}
?>
<tr><td colspan="6"> <input type="submit" name="position" value="Submit" class="btn btn-primary" style="float:right;" name="submit"></td></tr>
</table>
</form>