<?php 
$role= array();
$modules= array();
if(isset($roles_info))
{
$i=0;
foreach($roles_info->result_array() as $key=>$value)
{
$role[] = $value;
$module[] = $role[$i]['module_name'];
}
}
?>



<div class="container">
<!--	<div id="img-icon">
      <img src="<?php echo base_url();?>" align="left"/>
    </div>-->
    <div id="breadcumb-text">
      <ul class="nav nav-tabs">
        <li><a href=""><strong>Task Category</strong></a></li>
        <!-- <li class="vertical-divider"></li>&nbsp; -->
    <?php if(strtolower($this->session->userdata('user_position'))=="admin" || $role[2]['add']){?>
        <li class="active"><a href="">Add New Task Category</a></li>
        <!-- <li class="vertical-divider"></li>&nbsp; -->
    <?php } ?>
        <li><a href="<?php echo base_url();?>index.php/cts_controller/show_all_task_category">List Task Category</a></li>
      </ul>
    </div>
    <hr>
<?php
if(isset($result))
{
	if($result == 1)
	{
	echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Task Category Added!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Task Category Addition Failed!</b></div>";
}
?>
<table class="table" id="table">
<form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/cts_controller/insert_task_category">
<tr><td class="first_td">Task Category</td><td><input type="text" class="form-control" name="task_category" required></tr>

<tr><td colspan="2"><input type="submit" name="submit" value="Submit" class="btn btn-primary"></td></tr>
</form>
</table>
</div>
</body>
</html>