<div class="container">
  <h3>Firm Registration Form</h3><br>
  <?php

if(isset($result))
{
	if($result==1){
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Successfully Registered!</b></div>";
		}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Registration Failed!</b></div>";
}  
  ?>
      <table class="table" id="table">

        <form method="post" action="<?php echo base_url();?>index.php/lims_customer_controller/insert_new_firm1">
            <tr>
              <td><label>Firm Name</label></td>
              <td><input type="text" class="form-control" name="firm_name" required></td>
            </tr>
            <tr>
              <td colspan="2"><label><b>Address</b></label></td>
               <!--- <td><input type="text" class="form-control" name="address" required></td> --->
            </tr>
          <tr>
              <td><label>Zone</label></td>
              <td><input type="text" class="form-control" name="firm_zone" ></td>
            </tr>
          <tr>
              <td><label>District</label></td>
              <td><input type="text" class="form-control" name="firm_district" required></td>
            </tr>
          <tr>
              <td><label>VDC/Municipality</label></td>
              <td><input type="text" class="form-control" name="firm_vdc/municipality" ></td>
            </tr>
			
			<tr>
              <td><label>VDC No./Ward No</label></td>
              <td><input type="text" class="form-control" name="firm_vdc/ward_no" ></td>
            </tr>
			
			<tr>
              <td><label>Street</label></td>
              <td><input type="text" class="form-control" name="firm_street" ></td>
            </tr>
			
			<tr>
	          <td><label>Mobile Number </label></td>
              <td><input type="text" class="form-control" name="firm_mobile" ></td>
            </tr>
          
            <tr>
              <td><label>Firm Phone No</label></td>
              <td><input type="text" class="form-control" name="firm_phone"  ></td>
            </tr> 
			
			<tr>
              <td><label>Email</label></td>
              <td><input type="text" class="form-control" name="firm_email" ></td>
            </tr>
   
            <tr>
        	   <td colspan="2"><button type="submit" id="firm_submit" name="submit" class="btn btn-primary">Submit</button></td>
            </tr>
          <input type="hidden" name="customer_id" id="customer_id" value=""/>
        </form>
    </table>
</div><!---end of container-->
 </body>
 </html>