<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class cts_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
// echo "i am constructor";die;
        $this->load->model('cts_user_model');
        $this->load->model('cts_department_model');
        $this->load->model('cts_role_model');
        $this->load->model('cts_project_model');
        $this->load->model('cts_indicator_model');
        $this->load->model('cts_login');
        $this->load->model('cts_navigation_model');
        $this->load->model('cts_last_login');
        $this->load->model('cts_task_model');
        $this->load->model('cts_entry_person');
        $this->load->model('cts_task_registration_model');
        $this->load->model('cts_task_category_model');
        $this->load->model('cts_report_model');
        $this->load->model('cts_notification_model');

    }

    public function index($params = NULL)
    {

        if ($this->session->userdata('is_logged_in')) 
        {
            // echo "This is how we f**king party";die;
            $offset = 0;
            $data1['offset'] = $offset;
            $data1['departments'] = $this->cts_department_model->getDepartment();
            $data1['offset'] = $offset;
            if (isset($_POST['dash-submit'])) 
            {
                //print_r($_POST);
                $from_date = $this->input->post('date_from');
                $to_date = $this->input->post('date_to');
                $result = $this->cts_task_model->count_all_task($from_date, $to_date);

                $data1['total_task'] = $result['total'];
                $data1['average_task'] = $result['avg'];
                $data1['average_completed'] = $result['avg_completed'];
                $data1['total_paid'] = $result['total_paid'];
                $data1['pending_task'] = $result['pending_task'];
                $data1['paid_amount'] = $result['paid_amount'];
            } 
            else 
            {
                $result = $this->cts_task_model->count_all_task();
                // echo "<pre>";
                // print_r($result);die;
                $data1['total_task'] = $result['total'];
                $data1['average_task'] = $result['avg'];
                $data1['average_completed'] = $result['avg_completed'];
                $data1['total_paid'] = $result['total_paid'];
                $data1['pending_task'] = $result['pending_task'];
                $data1['paid_amount'] = $result['paid_amount'];
            }

            $data1['projects'] = $this->cts_task_model->get_all_projects();

            foreach ($data1['projects'] as $project):
                $project_count[$project['project_name']] = $this->cts_task_model->count_project_by_name($project['project_name']);
            endforeach;
            $data1['project_count'] = $project_count;

            $data1['today_entries'] = $this->cts_entry_person->get_all_today_entries($offset);
            $data1['positions'] = $this->cts_role_model->show_all_position_role();
            $data1['departments'] = $this->cts_department_model->getDepartment();
            $data1['updates'] = $this->cts_task_model->getUpdates();
            // echo "<pre>";
            // print_r($data1['updates']);die;
            $data1['new_message'] = $this->cts_login->check_new_message();
            $this->load->view("cts_header");
            $this->load->view('cts_navigation', $data1);

            $position = $this->session->userdata('position_id');

            $data1['users'] = $this->cts_user_model->get_all_user();
            $data1['meeting_info'] = $this->cts_notification_model->get_meeting_info();
            if ($params == "home")
            {
                //$this->load->view("cts_dashboard",$data1);
                // print_r($data1['users']);die;
                $this->load->view("cts_dashboard", $data1);
            }

            elseif ($params != "home" && $position == '1' || $position == '2')
                $this->load->view("cts_dashboard", $data1);

            else
                $this->load->view("cts_dashboard", $data1);

            $this->load->view("cts_footer");
        } 
        else 
        {
            $this->load->view("login_header");
            $this->load->view('cts_login');
        }
    }

    public function get_unknown_meeting()
    {
        $id = $_POST['id'];
        $data = $this->db->query("SELECT * FROM meeting where status = 2 and member_id = $id")->result_array();
        // echo $id;die;

        // print_r($data);die;
        echo json_encode($data);
    }

    public function change_status($status, $unique_id, $member_id)
    {
        // echo $status.'/'.$unique..
        // print_r($_POST);die;
        // echo 'helo';die; 
        $this->db->query("UPDATE `meeting` SET `status`= $status WHERE `unique_id` = $unique_id and `member_id` = $member_id");
        redirect(base_url() . 'index.php/cts_controller/index/home');
    }

     public function insert_meeting_detail()
    {
        // echo 'hello';die;
        // echo "<pre>";
        if ($this->session->userdata('is_logged_in')) 
        {

            if (isset($_POST)) 
            {
                // print_r($_POST);die;

                $data['result'] = $this->cts_notification_model->insert_meeting_detail();
                redirect(base_url() . 'index.php/cts_controller/index/home');
            } 
            else 
            {
                redirect('404');
            }

        } 
        else 
        {
            $this->load->view("cts_header");
            $this->load->view('cts_login');
        }

    }


    public function cancel_meeting($id)
    {
        $this->db->query("DELETE FROM meeting WHERE unique_id = $id");
        redirect(base_url() . 'index.php/cts_controller/index/home');
    }

    public function add_new_customer()

    {
        $this->load->view('cts_header');
        $this->load->view('cts_navigation');
        $this->load->view('cts_registration');
    }

    public function insert_customer_details()
    {

        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin") {

            if (isset($_POST['submit'])) {
                $data['result'] = $this->cts_customer_model->insert_customer_details();
                $this->load->view("cts_header");
                $this->load->view("cts_registration", $data);
                $this->load->view("cts_footer");
            } else {
                redirect('404');
            }

        } else {
            $this->load->view("cts_header");
            $this->load->view('cts_login');
        }

    }

   

    //User
    public function add_new_user_form($id = 0)
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[1]['add']) {
            $data['positions'] = $this->cts_role_model->show_all_position_role();
            $data['new_message'] = $this->cts_login->check_new_message();
            $data['departments'] = $this->cts_department_model->getDepartment();
            $data['department_list'] = $this->cts_department_model->getDepartment();
            $data['position_name'] = $this->cts_role_model->show_all_position_role();
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('user/cts_user_add_form', $data);
            $this->load->view("cts_footer");

        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }


    public function add_new_user()
    {

        $data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data1['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[1]['add'] == 1) {
            $data1['new_message'] = $this->cts_login->check_new_message();
            $data1['departments'] = $this->cts_department_model->getDepartment();
            $data1['positions'] = $this->cts_role_model->show_all_position_role();
            $data['result'] = $this->cts_user_model->add_new_user();
            $last_recorded_user_id = $this->cts_user_model->get_last_recorded_user();
            $dep_chk_box = $this->input->post('dep_chk_group');
            $this->cts_department_model->insert_the_department_of_user($last_recorded_user_id);
            $data['department_list'] = $this->cts_department_model->getDepartment();//extract all of the departments
            $data['position_name'] = $this->cts_role_model->show_all_position_role();
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data1);
            $this->load->view("user/cts_user_add_form", $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }


    public function show_all_user($offset = 0)
    {
        $data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data1['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        if ($this->session->userdata('is_logged_in') || strtolower($this->session->userdata('user_position')) == "admin" || ($role[1]['edit'] == 1 || $role[1]['delete'] == 1)) {
            $data1['positions'] = $this->cts_role_model->show_all_position_role();
            $data1['departments'] = $this->cts_department_model->getDepartment();
            $data1['new_message'] = $this->cts_login->check_new_message();
            //$data['result'] = $this->cts_assignment_model->check_assignment(0);
            $data['offset'] = $offset;
            $config = array();
            $config["base_url"] = base_url() . "index.php/cts_controller/show_all_user";
            $config["total_rows"] = $this->db->count_all("user");
            $config["per_page"] = 20;
            $config["uri_segment"] = 3;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["results"] = $this->cts_user_model->show_all_user($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();

            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data1);
            $this->load->view('user/cts_show_all_user', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view('cts_header');
            $this->load->view('cts_login'); */
        }
    }


    public function show_all_user_desc($offset = 0)
    {
        $data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data1['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || ($role[1]['edit'] == 1 || $role[1]['delete'] == 1)) {

            $data['offset'] = $offset;
            $data1['positions'] = $this->cts_role_model->show_all_position_role();
            $data1['departments'] = $this->cts_department_model->getDepartment();
            $data1['new_message'] = $this->cts_login->check_new_message();
            $config = array();
            $config["base_url"] = base_url() . "index.php/cts_controller/show_all_user_desc";
            $config["total_rows"] = $this->db->count_all("user");
            $config["per_page"] = 20;
            $config["uri_segment"] = 3;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["results"] = $this->cts_user_model->show_all_user_desc($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();

            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data1);
            $this->load->view('user/cts_show_all_user', $data);
            $this->load->view("cts_footer");

        } else {
            redirect(base_url());
            /* $this->load->view('cts_header');
            $this->load->view('cts_login'); */
        }
    }

    public function show_all_position_asc($offset = 0)
    {
        $data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data1['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || ($role[1]['edit'] == 1 || $role[1]['delete'] == 1)) {

            $data['offset'] = $offset;
            $data1['new_message'] = $this->cts_login->check_new_message();
            $data1['positions'] = $this->cts_role_model->show_all_position_role();
            $data1['departments'] = $this->cts_department_model->getDepartment();

            $config = array();
            $config["base_url"] = base_url() . "index.php/cts_controller/show_all_position_assc";
            $config["total_rows"] = $this->db->count_all("user");
            $config["per_page"] = 20;
            $config["uri_segment"] = 3;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["results"] = $this->cts_user_model->show_all_position_asc($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();

            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data1);
            $this->load->view('user/cts_show_all_user', $data);
            $this->load->view("cts_footer");

        } else {
            redirect(base_url());
            /* $this->load->view('cts_header');
            $this->load->view('cts_login'); */
        }
    }

    public function show_all_position_desc($offset = 0)
    {
        $data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data1['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || ($role[1]['edit'] == 1 || $role[1]['delete'] == 1)) {

            $data['offset'] = $offset;
            $data1['new_message'] = $this->cts_login->check_new_message();
            $data1['positions'] = $this->cts_role_model->show_all_position_role();
            $data1['departments'] = $this->cts_department_model->getDepartment();

            $config = array();
            $config["base_url"] = base_url() . "index.php/cts_controller/show_all_position_desc";
            $config["total_rows"] = $this->db->count_all("user");
            $config["per_page"] = 20;
            $config["uri_segment"] = 3;
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["results"] = $this->cts_user_model->show_all_position_desc($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();

            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data1);
            $this->load->view('user/cts_show_all_user', $data);
            $this->load->view("cts_footer");

        } else {
            redirect(base_url());
            /* $this->load->view('cts_header');
            $this->load->view('cts_login'); */
        }
    }


    public function delete_user_by_id($user_id)
    {
        $data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data1['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[1]['delete'] == 1) {
            $data['result_indicator'] = $this->cts_user_model->delete_user_by_id($user_id);
            $this->cts_notification_model->delete_by_user_id($user_id);

            redirect(base_url() . 'index.php/cts_controller/show_all_user');
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }


    public function edit_user_by_id_form($user_id)
    {
        $data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data1['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || ($role[1]['edit'] == 1)) {
            $data['user_details'] = $this->cts_user_model->get_details_of_user_by_id($user_id);
            $data['position_name'] = $this->cts_role_model->show_all_position_role();
            $data['department_list'] = $this->cts_department_model->getDepartment();
            $data['department_of_user'] = $this->cts_department_model->get_department_of_user($user_id);
            $data1['positions'] = $this->cts_role_model->show_all_position_role();
            $data1['departments'] = $this->cts_department_model->getDepartment();
            $data1['new_message'] = $this->cts_login->check_new_message();
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data1);
            $this->load->view('user/cts_user_edit_form', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }

    }

    public function edit_user_by_id($user_id)
    {
        $data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data1['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || ($role[1]['edit'] == 1)) {
            $data['result'] = $this->cts_user_model->edit_user_by_id($user_id);
            $this->cts_department_model->edit_the_department_of_user($user_id);
            redirect(base_url() . 'index.php/cts_controller/show_all_user');
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }


    //Department
    public function add_department_form($result = "")
    {
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['departments'] = $this->cts_department_model->getDepartment();
        $data['new_message'] = $this->cts_login->check_new_message();
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $this->load->helper('form');
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['add']) {
            $data['department_parent_is_null'] = $this->cts_department_model->get_null_parent_department();
            $data['all_departments'] = $this->cts_department_model->getDepartment();
            $department_index = 0;

            foreach ($data['department_parent_is_null'] as $row) {
                $departments[$department_index]['department'] = $row;
                $departments[$department_index]['sub_departments'] = $this->get_sub_department($row['dep_id']);
                $department_index++;
            }
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            if (isset($departments))
                $data['departments'] = $departments;
            $this->load->view('department/cts_add_department', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
        }
    }


    public function get_sub_department($dep_id)
    {
        $sub_department = array();
        $result = $this->cts_department_model->get_sub_departments($dep_id);
        $index = 0;
        foreach ($result as $row) {
            $sub_department[$index]['departments'] = $row;
            $sub_department[$index]['sub_departments'] = $this->get_sub_department($row['dep_id']);
            $index++;
        }
        return $sub_department;
    }

    /*
     * insert new department to the repective table
     * @params : none
     * @return : view
     * */
    public function insert_department_details()
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['departments'] = $this->cts_department_model->getDepartment();
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['add']) {
            $data['result'] = $this->cts_department_model->insert_department_details();

            $data['all_departments'] = $this->cts_department_model->getDepartment();
            $data['new_message'] = $this->cts_login->check_new_message();
            $department_index = 0;

            $data['positions'] = $this->cts_role_model->show_all_position_role();

            //if called by ajax add new form
            if ($this->input->is_ajax_request()) {
                return $data["result"];
            }
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $data['departments'] = $departments;
            $this->load->view('department/cts_add_department', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());

        }
    }

    /*
     * insert new department to the repective table
     * @params : none
     * @return : view
     * */

    //projects

    public function add_project_form($result = "")
    {
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['departments'] = $this->cts_project_model->getProject();
        $data['new_message'] = $this->cts_login->check_new_message();
        $data['task_category'] = $this->cts_task_category_model->get_all_task_category();

        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $this->load->helper('form');
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['add']) {
//            $data['department_parent_is_null'] = $this->cts_department_model->get_null_parent_department();
//            $data['all_departments'] = $this->cts_department_model->getDepartment();
//            $department_index = 0;
// echo "<pre>";
            // print_r($data);die;
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);

            $this->load->view('projects/cts_add_project', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
        }
    }


    public function insert_project_details()
    {
        // echo "<pre>";
        // print_r($_POST);die;
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['projects'] = $this->cts_project_model->getProject();
        $data['task_category'] = $this->cts_task_category_model->get_all_task_category();
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['add']) 
        {
            $data['result'] = $this->cts_project_model->insert_project_details();

            $data['all_projects'] = $this->cts_project_model->getproject();
            $data['new_message'] = $this->cts_login->check_new_message();
            $project_index = 0;

            $data['positions'] = $this->cts_role_model->show_all_position_role();

            //if called by ajax add new form
            if ($this->input->is_ajax_request()) {
                return $data["result"];
            }
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
//            $data['projects'] = $projects;
            $this->load->view('projects/cts_add_project', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());

        }
    }

    public function show_all_project($result_indicator = "")
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['edit'] || $role[2]['delete']) {
            $data['result_indicator'] = $result_indicator;
            $data['all_department_with_parent'] = $this->cts_project_model->get_all_project();
            $data['new_message'] = $this->cts_login->check_new_message();
            $data['project'] = $this->cts_project_model->getProject();
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('projects/pts_show_all_project', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }

    public function edit_project_form($project_id)
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['projects'] = $this->cts_project_model->getProject();
        $data['task_category'] = $this->cts_task_category_model->get_all_task_category();
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) 
        {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['edit']) {
            $data['project_details'] = $this->cts_project_model->get_the_project_by_id($project_id);
            $data['all_project'] = $this->cts_project_model->getProject();
            $data['new_message'] = $this->cts_login->check_new_message();
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('projects/pts_edit_project', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }

    public function edit_the_project($project_id)
    {
        // echo "<pre>";
        // print_r($_POST);die;
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));

        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['edit']) {
            $data['result'] = $this->cts_project_model->edit_the_project($project_id);
            redirect(base_url() . 'index.php/cts_controller/show_all_project');
        } else {
            redirect(base_url());
//            $this->load->view("cts_header");
//            $this->load->view('cts_login');
        }
    }


    public function delete_the_project($project_id)
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['delete']) {
            $data['result_indicator'] = $this->cts_project_model->delete_project_by_id($project_id);
            $this->show_all_project($data['result_indicator']);
            // $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }




    

    //project ends here

    //Indicator starts here
    public function add_indicator_form($result = "")
    {
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['departments'] = $this->cts_indicator_model->getIndicator();
        $data['new_message'] = $this->cts_login->check_new_message();
        $data['task_category'] = $this->cts_task_category_model->get_all_task_category();
        // print_r($data['task_category']);die;
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $this->load->helper('form');
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['add']) {
//            $data['department_parent_is_null'] = $this->cts_department_model->get_null_parent_department();
//            $data['all_departments'] = $this->cts_department_model->getDepartment();
//            $department_index = 0;


            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);

            $this->load->view('indicator/pts_add_indicator', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
        }
    }


    public function insert_indicator_details()
    {
        // echo 'hello';die;

        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['indicator'] = $this->cts_indicator_model->getIndicator();
        $i = 0;
        $data['task_category'] = $this->cts_task_category_model->get_all_task_category();
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['add']) {
        // echo 'hello';die;s
            $data['result'] = $this->cts_indicator_model->insert_indicator_details();

            $data['indicator'] = $this->cts_indicator_model->getIndicator();
            $data['new_message'] = $this->cts_login->check_new_message();
            $project_index = 0;

            $data['positions'] = $this->cts_role_model->show_all_position_role();

            //if called by ajax add new form
            if ($this->input->is_ajax_request()) {
                   
                return $data["result"];
            }
            // echo 'inserted';die;
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('indicator/pts_add_indicator', $data);
            $this->load->view("cts_footer");
        } 
        else 
        {
            redirect(base_url());

        }
    }

    public function show_all_indicator($result_indicator = "")
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['edit'] || $role[2]['delete']) {
            $data['result_indicator'] = $result_indicator;
            $data['all_indicator_with_parent'] = $this->cts_indicator_model->get_all_indicator();
            $data['new_message'] = $this->cts_login->check_new_message();
            $data['project'] = $this->cts_indicator_model->getIndicator();
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('indicator/pts_show_all_indicator', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }

    public function edit_indicator_form($project_id)
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['projects'] = $this->cts_indicator_model->getIndicator();
        $data['task_category'] = $this->cts_task_category_model->get_all_task_category();
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) 
        {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }

        $data['positions'] = $this->cts_role_model->show_all_position_role();

        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['edit']) {
            $data['project_details'] = $this->cts_indicator_model->get_the_indicator_by_id($project_id);
            $data['all_project'] = $this->cts_indicator_model->getIndicator();
            $data['new_message'] = $this->cts_login->check_new_message();
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('indicator/pts_edit_indicator', $data);
            $this->load->view("cts_footer");
        } 
        else 
        {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }

    public function edit_the_indicator($project_id)
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));

        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['edit']) {
            $data['result'] = $this->cts_indicator_model->editIndicator($project_id);

            redirect(base_url() . 'index.php/cts_controller/show_all_indicator');
            return $data['result'];
        } else {
            redirect(base_url());
//            $this->load->view("cts_header");
//            $this->load->view('cts_login');
        }
    }

    public function deleteIndicator($project_id)
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['delete']) {
            $data['result_indicator'] = $this->cts_indicator_model->delete_indicator_by_id($project_id);
            $this->show_all_indicator($data['result_indicator']);
//            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }

    //Indicator ends here

    public function show_all_department($result_indicator = "")
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['edit'] || $role[2]['delete']) {
            $data['result_indicator'] = $result_indicator;
            $data['all_department_with_parent'] = $this->cts_department_model->get_all_department_with_parent();
            $data['new_message'] = $this->cts_login->check_new_message();
            $data['departments'] = $this->cts_department_model->getDepartment();
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('department/cts_show_all_department', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }

    public function delete_the_department($dep_id)
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['delete']) {
            $data['result_indicator'] = $this->cts_department_model->delete_the_department($dep_id);
            $this->show_all_department($data['result_indicator']);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }

    public function edit_the_department_form($dep_id)
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['departments'] = $this->cts_department_model->getDepartment();
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['edit']) {
            $data['department_details'] = $this->cts_department_model->get_the_department_by_id($dep_id);
            $data['all_departments'] = $this->cts_department_model->getDepartment();
            $data['new_message'] = $this->cts_login->check_new_message();
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('department/cts_edit_department', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }

    public function edit_the_department($dep_id)
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));

        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['edit']) {
            $data['result'] = $this->cts_department_model->edit_the_department($dep_id);
            redirect(base_url() . 'index.php/cts_controller/show_all_department');
        } else {
            redirect(base_url());
            $this->load->view("cts_header");
            $this->load->view('cts_login');
        }
    }

//others
    public function check_email()
    {
        if ($this->session->userdata('is_logged_in')) {
            if (!$this->input->is_ajax_request()) {
                redirect(404);
            } else {
                $result = $this->cts_user_model->check_email($this->input->post('email'));
            }
        } else {
            $this->load->view("cts_header");
            $this->load->view('cts_login');
            $this->load->view("cts_footer");
        }
    }


    public function dashboard()
    {

        if ($this->session->userdata('is_logged_in')) {
            $this->load->view('cts_header');
            $this->load->view('cts_navigation');
            $this->load->view('cts_dashboard');
            $this->load->view("cts_footer");
        } else {
            $this->load->view("cts_header");
            $this->load->view('cts_login');
            //$this->load->view("cts_footer");

        }
    }


    public function check_new_messages()
    {
        $new_message = $this->cts_login->check_new_message1();
        echo json_encode($new_message);
    }

    public function validate_credentials($offset = 0, $params = false)
    {
        if ($this->session->userdata('is_logged_in')) {
            redirect(base_url());
        } 
        else 
        {
            if ($this->cts_login->can_log_in()) {
                //print_r($this->session);
                $data1['offset'] = $offset;
                $data1['departments'] = $this->cts_department_model->getDepartment();
                $position_id = $this->cts_login->get_position_id($this->input->post('user_email'));
                $user_id = $this->cts_login->get_user_id($this->input->post('user_email'));
                $user_position = $this->cts_login->get_position($this->input->post('user_email'));
                $user_name = $this->cts_login->get_user_name($this->input->post('user_email'));

                $data1['projects'] = $this->cts_task_model->get_all_projects();
                $data1['users'] = $this->cts_user_model->get_all_user();
                $data1['meeting_info'] = $this->cts_notification_model->get_meeting_info();
                foreach ($data1['projects'] as $project):
                    $project_count[$project['project_name']] = $this->cts_task_model->count_project_by_name($project['project_name']);
                endforeach;
                $data1['project_count'] = $project_count;


                $data1['positions'] = $this->cts_role_model->show_all_position_role();
                $data1['today_entries'] = $this->cts_entry_person->get_all_today_entries($offset);


                $data = array(
                    'email' => $this->input->post('user_email'),
                    'is_logged_in' => 1,
                    'position_id' => $position_id,
                    'user_id' => $user_id,
                    'user_position' => $user_position,
                    'user_name' => $user_name
                );
                $user_departments = $this->cts_navigation_model->validated_user_data_for_navigation($data);
                $department = array();
                foreach ($user_departments->result() as $row) {
                    $department[] = strtolower($row->dep_name);
                }
                $data['department'] = $department;
                $this->session->set_userdata($data);
                // $data1['ar_data'] = $this->cts_approve_recommend_model->count_ar_details($this->session->userdata('user_id'));
                $data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
                $data1['new_message'] = $this->cts_login->check_new_message();
                $data1['updates'] = $this->cts_task_model->getUpdates();
                // redirect(base_url());
                $this->load->view('cts_header');
                $this->load->view('cts_navigation', $data1);
                $this->cts_last_login->last_login();
                $position = $this->session->userdata('position_id');
                $data1['users'] = $this->cts_user_model->get_all_user();
                $data1['meeting_info'] = $this->cts_notification_model->get_meeting_info();
                if ($params == "home")
                    $this->load->view("cts_dashboard", $data1);

                elseif ($params != "home" && $position == '1' || $position == '2')
                    //$this->load->view("cts_graphical_dashboard",$data1);
                    $this->load->view("cts_dashboard", $data1);

                else
                    $this->load->view("cts_dashboard", $data1);
                $this->load->view("cts_footer");
            } 
            else 
            {
                $data['error'] = "Invalid Email/Password";
                $this->load->view("login_header");
                $this->load->view('cts_login', $data);
                //$this->load->view("cts_footer");
            }

        }
    }

    public function logout()
    {
        $data = array(
            'email' => '',
            'email' => '',
            'is_logged_in' => '',
            'position_id' => '',
            'user_id' => '',
            'user_position' => '',
            'department' => ''
        );
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        $this->load->view('login_header');
        $this->load->view('cts_login');


    }


    /*
     * Add indicator type
     * @return void
     */
    public function add_task_category()
    {
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['departments'] = $this->cts_indicator_model->getIndicator();
        $data['new_message'] = $this->cts_login->check_new_message();
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $this->load->helper('form');
        // echo 'task_category';die;
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['add']) {
//            $data['department_parent_is_null'] = $this->cts_department_model->get_null_parent_department();
//            $data['all_departments'] = $this->cts_department_model->getDepartment();
//            $department_index = 0;


            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);

            $this->load->view('task_category/pts_add_task_category', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
        }
    }


    /*
     * Insert indicator
     * @return void
     */
    public function insert_task_category()
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['indicator'] = $this->cts_indicator_model->getIndicator();
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['add']) 
        {
            $data['result'] = $this->cts_task_category_model->insert_task_category();

            $data['indicator'] = $this->cts_indicator_model->getIndicator();
            $data['new_message'] = $this->cts_login->check_new_message();
            $project_index = 0;

            $data['positions'] = $this->cts_role_model->show_all_position_role();

            //if called by ajax add new form
            if ($this->input->is_ajax_request()) {
                return $data["result"];
            }
            // echo "<pre>";
            // print_r($data);die;
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('task_category/pts_add_task_category', $data);
            $this->load->view("cts_footer");
        } 
        else {
            redirect(base_url());

        }
    }


    public function show_all_task_category($result_indicator = "")
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['edit'] || $role[2]['delete']) 
        {
            $data['result_indicator'] = $result_indicator;
            // print_r($result_indicator);die
            $data['all_task_category'] = $this->cts_task_category_model->getTaskCategory();
            $data['new_message'] = $this->cts_login->check_new_message();
            $data['project'] = $this->cts_indicator_model->getIndicator();
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('task_category/pts_show_all_task_category', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }



    /*
     * Edit indicator type
     * @return void
     */
    public function edit_task_category($taskCategoryId)
    {
        // echo "<pre>";

        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['projects'] = $this->cts_indicator_model->getIndicator();
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['edit']) 
        {
            $data['task_category_detail'] = $this->cts_task_category_model->get_task_category_by_id($taskCategoryId);
            // print_r($data['task_category_detail']);die;
            $data['all_project'] = $this->cts_indicator_model->getIndicator();
            $data['new_message'] = $this->cts_login->check_new_message();
            // print_r($data);die;
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('task_category/pts_edit_task_category', $data);
            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }

    }



    /*
     * Update the indicator
     */
    public function update_task_category($taskCategoryId)
    {
        // echo 
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));

        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        // echo "hello";
        // echo $taskCategoryId;die;
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['edit']) {
            $data['result'] = $this->cts_task_category_model->editTaskCategory($taskCategoryId);

            redirect(base_url() . 'index.php/cts_controller/show_all_task_category');
            return $data['result'];
        } else {
            redirect(base_url());
//            $this->load->view("cts_header");
//            $this->load->view('cts_login');
        }
    }


    /*
     * Delete indicator type
     * @return void
     */
    public function delete_task_category($taskCategoryId)
    {
// echo "deleted";die;
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['delete']) {
            $data['result_indicator'] = $this->cts_task_category_model->delete_task_category_by_id($taskCategoryId);
            // echo "hello";die;
            $this->show_all_task_category($data['result_indicator']);
//            $this->load->view("cts_footer");
        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }

    }


    /*
     * show taskcatagory related to project
     * @return view (with dashboard details)
     */
    public function ajax_get_task_category_by_project($project_id)
    {
        $result = $this->db->select('*')->from('project')->where('project_id', $project_id)->get()->result_array();
        foreach($result as $results)
        {
            if(isset($results['project_taskcategory']) && $results['project_taskcategory'] != '')
            {
                $taskcategory = json_decode($results['project_taskcategory']);
            // print_r($taskcategory);die;
                foreach($taskcategory as $key => $category)
                {
                    $db_data = $this->db->query("select name from task_category where t_category_id = $key")->result_array();
                    $data[$key] = $db_data[0]['name'];
                }
            }
            else 
                $data = '';
        }
        // print_r($data);die;

        echo json_encode($data);
    }

    public function ajax_get_indicator_by_task_category($task_cat_id)
    {
        $result = $this->db->select('*')->from('indicators')->where('task_category_id', $task_cat_id)->get()->result_array();
        // print_r($result);die;
        foreach($result as $results)
        {
            if(isset($results['indicator_id']) && $results['indicator_id'] != '')
            {
                $data[$results['indicator_id']] = $results['indicator_name'];
            }
            else 
                $data = '';
        }
        // print_r($data);die;

        echo json_encode($data);
    }


    public function get_backup(){
         // echo 'hello';die;
         $prefs = array(     
                'format'      => 'zip',             
                //'filename'    => 'my_db_backup.sql'
              );
         $this->load->database();
        // Load the DB utility class
        $this->load->dbutil();
        // Backup your entire database and assign it to a variable
        $backup =& $this->dbutil->backup($prefs); 
        // Load the file helper and write the file to your server
        $db_name = 'cts_backup_'.date('y_m_d').'_'.time().'.zip';
        $path = FCPATH."downloads/".$db_name;
        //echo $path;die();
        $this->load->helper('file');
        write_file($path, $backup); 
        // Load the download helper and send the file to your desktop
        $this->load->helper('download');
        //$this->load->library('zip');
        //$this->zip->archive($path);
        //$this->zip->download($db_name);
        force_download($db_name, $backup);
   
    }


    /*
     * Details for dashboard
     * @return view (with dashboard details)
     */
    public function dashboard_info()
    {
        // echo "<pre>";
        $data["results"] = $this->cts_report_model->show_all_project_details();
        $data["monthreports"] = $this->cts_report_model->project_month_report();
//         echo "<pre>";
// print_r($data["results"]);
// print_r($data["monthreports"]);

// die;
        foreach ($data['monthreports'] as $key => $value) {
            if (preg_match('/[\'\/02\\\]/', $value['status'], $matches)) {
                // echo $key;
                unset($data['monthreports'][$key]);

            }
        }
        $this->load->view('cts_header');
        $this->load->view('cts_navigation');
        $this->load->view('cts_dashboard_info', $data);

        $this->load->view('cts_footer');

    }

    

    


    

    

    



    
}
