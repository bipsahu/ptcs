<?php 
class cts_clam_controller extends CI_Controller 
{
	public function __construct()
	{
	parent::__construct();
		$this->load->model('cts_claim_model');
		$this->load->model('cts_sampl_model');
		$this->load->model('cts_role_model');
		$this->load->model('cts_department_model');
		$this->load->model('cts_login');
	}
	public function index()
	{
	$data1['positions'] = $this->cts_role_model->show_all_position_role();
	$data1['departments'] = $this->cts_department_model->getDepartment();
	$data1['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data1);
		$this->load->view('claim/cts_new_claim_registration');
	}
	
	public function insert_sampl_details()
	{
		$data['result'] = $this->cts_sampl_model->insert_sampl_details();
		if($data['result'] == false)
			{
			echo " 0";
			}
			else
			{
				foreach($data['result']->result() as $row)
		{
			$sampl_details = array(
				'claim_type' => $row->claim_type
				);
		}
		echo json_encode($sampl_details);
		}
	}
	
	public function add_new_claim()
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'])
	{
		$data['positions'] = $this->cts_role_model->show_all_position_role();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data);
		$this->load->view('claim/cts_new_claim_registration1');
		$this->load->view('cts_footer');
	
	}
	else
	{
	redirect(base_url());
	}
	}
	
	public function insert_new_claim()
	{	
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
			if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'])
			{
				$data['result'] = $this->cts_sampl_model->insert_new_claim_details();
				$data['positions'] = $this->cts_role_model->show_all_position_role();
				$this->load->view('cts_header');
				$this->load->view('cts_navigation',$data);
				$this->load->view('claim/cts_new_claim_registration1',$data);
				$this->load->view('cts_footer');
			}
		else
		{
		redirect(base_url());
		}
	}
	
	public function edit_claim_by_id_form($sid)
	{	
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit'])
			{
				$data['positions'] = $this->cts_role_model->show_all_position_role();
				$data['claim_type'] = $this->cts_claim_model->get_details_of_claim_by_id($sid);
				$this->load->view('cts_header');
				$this->load->view('cts_navigation',$data);
				$this->load->view('claim/cts_edit_claim_by_id_form',$data);
			}
		else
		{
		redirect(base_url());
		}
	}
	
	public function count_all_claim($offset = 0)
	{
		
		$total = $this->db->count_all("claim_registration");
		return $result;
	}

	public function show_all_claim($offset = 0)
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		$data['positions'] = $this->cts_role_model->show_all_position_role();
		$data['offset'] = $offset;
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" ||$this->session->userdata('user_position') == "Manager") {
		$config = array();
		$config["base_url"] = base_url() . "index.php/cts_sampl_controller/show_all_claim";
		$config["total_rows"] = $this->db->count_all("claim");
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		;
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["results"] = $this->cts_claim_model->show_all_claim($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();
		$data1['positions'] = $this->cts_role_model->show_all_position_role();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data);
		$this->load->view('claim/cts_show_all_claim',$data);
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function edit_claim_by_id($sid)
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit'])
		{
		$data['result'] = $this->cts_claim_model->edit_claim_by_id($sid);
		redirect(base_url().'index.php/cts_sampl_controller/show_all_claim/'.$data[result]);
		}
		else
		{
		redirect(base_url());
		}

	
	}
	public function delete_claim_by_id($sid)
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['new_message'] = $this->cts_login->check_new_message();
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['delete'])
		{
			$data['result'] = $this->cts_claim_model->delete_claim_by_id($sid);
			redirect(base_url().'index.php/cts_sampl_controller/show_all_claim');
		}
		else
		{
		redirect(base_url());
		}
	}
	
	
	
	public function search_sampl_detail($key)
	{
	$this->cts_sampl_model->search_sampl_detail($key);
	}
	public function check_claim_type()
	{
	$result = $this->cts_sampl_model->check_claim_type();
		if($result)
		echo " 1";
		else
		echo " 0";
	}

}