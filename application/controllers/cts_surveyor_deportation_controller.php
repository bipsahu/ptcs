<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class cts_surveyor_deportation_controller extends CI_controller{
	
	public function __construct()
	{
			parent::__construct();
			{
				$this->load->model('cts_surveyor_deportation_model');
				$this->load->model('cts_login');
				$this->load->model('cts_user_model');
				$this->load->model('cts_department_model');
				$this->load->model('cts_role_model');
			}
	}
	
	public function index($offset=0)
	{
		if($this->session->userdata('is_logged_in') && in_array("surveyor deputation",$this->session->userdata('department')))
		{
			$data1['departments'] = $this->cts_department_model->getDepartment();
			$data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
			$data1['positions'] = $this->cts_role_model->show_all_position_role();
			$data1['new_message'] = $this->cts_login->check_new_message();
			$data['offset'] = $offset;
			
			$data['result'] = $this->cts_surveyor_deportation_model->show_all_surveyor_deportation_details($offset);
			$data['all_users'] = $this->cts_user_model->get_all_user();
			$data['departments'] = $this->cts_department_model->getDepartment();
			$this->load->view('cts_header');
			$this->load->view('cts_navigation',$data1);
			$this->load->view('surveyor_deportation/cts_show_all_surveyor_deportation_details',$data);
			$this->load->view("cts_footer");
		}
		else{
			redirect(base_url());
		}
	}
	public function assigned_surveyors($claim_id){
		$result = $this->cts_surveyor_deportation_model->get_assigned_surveyors($claim_id);
		return $result;
	}
	public function add_new_surveyor_deportation_details($claim_id)
	{
		if($this->session->userdata('is_logged_in') && in_array("surveyor_deputation",$this->session->userdata('department')))
		{
			$data['claim_id'] = $claim_id; 
			$this->load->view('cts_header');
			$this->load->view('surveyor_deportation/cts_surveyor_deportation_register',$data);
			$this->load->view("cts_footer");
		}
		else
		{
		redirect(base_url());
		}
	}
	
	public function edit_surveyor_deportation_by_id()
	{
	if($this->input->is_ajax_request())
	{
		
		$surveyor_deportation_id = $this->input->post('pk');
		$data['edit_result'] = $this->cts_surveyor_deportation_model->edit_surveyor_deportation_by_id($surveyor_deportation_id);
		echo $data['edit_result'];
	}
	else
		{
			redirect(base_url(404));
		}
	}
	
	public function insert_surveyor_deportation_details($surveyor_deportation_id)
	{
	
		if($this->input->is_ajax_request())
		{
			$data['result'] = $this->cts_surveyor_deportation_model->insert_surveyor_deportation_details($surveyor_deportation_id);
			if ($data['result'])
			{
				/* redirect(base_url().'index.php/cts_surveyor_deportation_controller',refresh); */
				echo "1";
			}
			
		}		
		else
		{
		redirect(404);
		}		
			
	}

	public function edit_surveyor_deportation_by_id_form($surveyor_deportation_id)
	{
	if(!$this->input->is_ajax_request())
		{
			redirect(404);
		}
		else
		{
			$data['surveyor_deportation_details'] = $this->cts_surveyor_deportation_model->get_details_of_surveyor_deportation_by_id($surveyor_deportation_id);
			foreach($data['surveyor_deportation_details'] as $row)
			{
			$surveyor_deportation_details = array
				(
				'surveyor_deportation_id' => $row['surveyor_deportation_id'],
				'test_method' => $row['test_method'],
				'result' => $row['result'],
				'remark' => $row['remark'],
				'total_sample_tested' => $row['total_sample_tested'],
				'sample_collection_date' => $row['sample_collection_date'],
				'tested_date' => $row['tested_date'],
				'approved_date' => $row['approved_date']
				
				);
			}
			echo json_encode($surveyor_deportation_details);
		}
/* 	$this->load->view('cts_header');
	$this->load->view('cts_navigation');
	$this->load->view('surveyor_deportation/cts_edit_surveyor_deportation_by_id_form',$data); */
	}
	public function delete_surveyor_deportation_by_id($surveyor_deportation_id)
	{
	$data['deletion_result'] = $this->cts_surveyor_deportation_model->delete_surveyor_deportation_by_id($surveyor_deportation_id);
	if($data['deletion_result'])
	 echo "0";
	}
	public function check_new_customer()
	{
	if($this->input->is_ajax_request())
	{
	$this->cts_surveyor_deportation_model->check_new_customer();
	}
	else
	{
	redirect(404);
	}
	}
	public function set_accept_on($surveyor_deportation_id)
	{
	if(!$this->input->is_ajax_request())
	redirect(404);
	else
	{
	$result = $this->cts_surveyor_deportation_model->set_accept_on($surveyor_deportation_id);
	echo $result;
	}
	}
	public function search_pending_sample($offset)
	{

		$query_result = $this->cts_surveyor_deportation_model->search_pending_sample($offset);
		echo "<thead><th>surveyor_deportation No.</th> <th>Regd. No.</th><th>Customer Name</th><th>Firm Name</th><th>Animal</th><th>Age</th><th>Sex</th><th>Species</th><th>Species Type</th><th>Sample Type</th><th>Sample No.</th><th>Investigation Requested</th><th>Sample Collection Date</th><th>Publish</th></thead>";
		foreach($query_result['result']->result() as $row)

		{
			print_R($row);
		echo "<tr><td>".$row->surveyor_deportation_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->animal."</td><td>".$row->age."</td><td>".$row->sex."</td><td>".$row->animal_type."</td><td>".$row->species_type."</td><td>".$row->sample_type."</td><td>".$row->sample_no."</td><td>".$row->investigation_requested."</td><td>".$row->sample_collection_date."</td><td><button class='btn btn-info set_publish_btn' id='' alt='".$row->surveyor_deportation_id."'>Publish</button></td></tr>";
		}
		echo "<tr><td colspan='14'>";
				if($offset == 0)
				echo "<span class='glyphicon glyphicon-backward'></span>&nbsp;";
				else
				echo "<a href='javascript:surveyor_deportation_pending_pagination(".($offset-1).")'> <span class='glyphicon glyphicon-backward'></span>&nbsp;</a>";
			for($i=1;$i<=ceil($query_result['count']/6);$i++)
			{
				if($i == $offset+1)
				{
				echo $i;
				}
				else
				{
				echo "<a href='javascript:surveyor_deportation_pending_pagination(".($i-1).")'> ".$i." </a>";
				}
			}
				if($offset == ceil($query_result['count']/6)-1)
				echo "&nbsp;<span class='glyphicon glyphicon-forward'></span>";
				else 
				echo "<a href='javascript:surveyor_deportation_pending_pagination(".($offset+1).")'>&nbsp;<span class='glyphicon glyphicon-forward'>&nbsp;</span></a>";
			echo "</td></tr>";
	}
	public function set_publish_on($surveyor_deportation_id)
	{
		if(!$this->input->is_ajax_request())
		redirect(404);
		else
		{
		$result = $this->cts_surveyor_deportation_model->set_publish_on($surveyor_deportation_id);
		echo $result;
		}
	}
	public function search_all_sample($offset)
	{
		$result = $this->cts_login->get_row_by_position_id($this->session->userdata('position_id'));
		foreach($result->result_array() as $row)
		{
		$add_role = $row['add_role'];
		$edit_role = $row['edit_role'];
		$delete_role = $row['delete_role'];
		$report_generate_role = $row['report_generate_role'];
		}
		$query_result = $this->cts_surveyor_deportation_model->search_on_progress_sample($offset);


		echo "<thead><th>surveyor_deportation No.</th><th>Regd. No.</th><th>Customer Name</th><th>Firm Name</th><th>Animal</th><th>Age</th><th>Sex</th><th>Species</th><th>Species Type</th><th>Sample Type</th><th>Sample No.</th><th>Investigation Requested</th><th>Sample Collection Date</th><th>Test Method</th><th>Result</th><th>Remarks</th><th>Tested Date</th><th>Approved Date</th>".(($add_role==1)?'<th>Add</th>':'').(($edit_role==1)?'<th>Edit</th>':'').(($delete_role==1)?'<th>Delete</th>':'')."<th>Completed</th></thead>";

		foreach($query_result['result']->result() as $row)
		{

		echo "<tr><td>".$row->surveyor_deportation_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->animal."</td><td>".$row->age."</td><td>".$row->sex."</td><td>".$row->animal_type."</td><td>".$row->species_type."</td><td>".$row->sample_type."</td><td>".$row->sample_no."</td><td>".$row->investigation_requested."</td><td>".$row->sample_collection_date."</td><td>".$row->test_method."</td><td>".$row->result."</td><td>".$row->remark."</td><td>".$row->tested_date."</td><td>".$row->approved_date."</td>";

	
		if($add_role == 1)
		{
		echo "<td><a href='#' class='btn btn-primary glyphicon-plus' alt='".$row->surveyor_deportation_id."'  data-toggle='modal' data-target='#register_surveyor_deportation'>Report Entry</a></td>";
		}
		if($edit_role == 1)
		{
		echo "<td><a href='#'class='btn btn-primary glyphicon-edit' data-toggle='modal' data-target='#edit_surveyor_deportation' alt='".$row->surveyor_deportation_id."'><div class='glyphicon' ></div>Report Edit</a></td>";
		}
		if($delete_role == 1){
		echo "<td><a href='#' class='btn btn-primary glyphicon-trash' alt='".$row->surveyor_deportation_id."'><div class='glyphicon' ></div>Delete Report</a></td>";
		}
		echo "<td><button class='btn btn-primary set_completed_button' id='' alt='".$row->surveyor_deportation_id."'>Completed</button></td></tr>";
		}
		echo "<tr><td colspan='23'>";
				if($offset == 0)
				echo "<span class='glyphicon glyphicon-backward'></span>&nbsp;";
				else
				echo "<a href='javascript:surveyor_deportation_pagination(".($offset-1).")'> <span class='glyphicon glyphicon-backward'></span>&nbsp;</a>";
			for($i=1;$i<=ceil($query_result['count']/6);$i++)
			{
				if($i == $offset+1)
				{
				echo $i;
				}
				else
				{
				echo "<a href='javascript:surveyor_deportation_pagination(".($i-1).")'> ".$i." </a>";
				}
			}
				if($offset == ceil($query_result['count']/6)-1)
				echo "&nbsp;<span class='glyphicon glyphicon-forward'></span>";
				else 
				echo "<a href='javascript:surveyor_deportation_pagination(".($offset+1).")'>&nbsp;<span class='glyphicon glyphicon-forward'>&nbsp;</span></a>";
			echo "</td></tr>";
	}
	public function set_completed_on($surveyor_deportation_id)
	{
	$result = $this->cts_surveyor_deportation_model->set_completed_on($surveyor_deportation_id);
	echo $result;
	}
	public function search_completed_sample($offset)
	{
		$result = $this->cts_login->get_row_by_position_id($this->session->userdata('position_id'));
		foreach($result->result_array() as $row)
		{
		$report_generate_role = $row['report_generate_role'];
		$approve_report_role = $row['approve_role'];
		}
/* <<<<<<< .mine
		$query_result = $this->cts_surveyor_deportation_model->search_completed_sample();
		echo "<tr><th>surveyor_deportation No.</th><th>Regd. No.</th><th>Customer Name</th><th>Firm Name</th><th>Animal</th><th>Age</th><th>Sex</th><th>Species</th><th>Species Type</th><th>Sample Type</th><th>No of Sample</th><th>Sample Collection Date</th><th>Investigation Requested </th><th>Test Method</th><th>Result</th><th>Remarks</th><th>Total Sample Tested</th><th>Approved Remarks</th><th>Tested Date</th><th>Approved Date</th>".(($report_generate_role == 1)?'<th>Generate Report</th>':'')."<th>Approve</th></tr>";
		foreach($query_result->result() as $row)
======= */
		$query_result = $this->cts_surveyor_deportation_model->search_completed_sample($offset);
		
		echo "<thead><th>surveyor_deportation No.</th><th>Regd. No.</th><th>Customer Name</th><th>Firm Name</th><th>Animal</th><th>Age</th><th>Sex</th><th>Species</th><th>Species Type</th><th>Sample Type</th><th>Sample No.</th><th>Investigation Requested </th><th>Sample Collection Date</th><th>Test Method</th><th>Result</th><th>Remarks</th><th>Approved Remarks</th><th>Tested Date</th><th>Approved Date</th>".(($report_generate_role == 1)?'<th>Generate Report</th>':'')."<th>Approved</th></thead>";
		foreach($query_result['result']->result() as $row)

		{
		/* echo "<tr><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->animal."</td><td>".$row->sample_type."</td><td>".$row->no_of_sample."</td><td>".$row->investigation_requested."</td><td>".$row->test_method."</td><td>".$row->result."</td><td>".$row->remark."</td><td>".$row->approved_remark."</td><td>".$row->tested_date."</td><td>".$row->approved_date."</td>"; */

		 echo "<tr><td>".$row->surveyor_deportation_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->animal."</td><td>".$row->age."</td><td>".$row->sex."</td><td>".$row->animal_type."</td><td>".$row->species_type."</td><td>".$row->sample_type."</td><td>".$row->sample_no."</td><td>".$row->investigation_requested."</td><td>".$row->sample_collection_date."</td><td>".$row->test_method."</td><td>".$row->result."</td><td>".$row->remark."</td><td>".$row->approved_remarks."</td><td>".$row->tested_date."</td><td>".$row->approved_date."</td>"; 
			if($report_generate_role == 1 && $row->registered == 5 && $row->publish_bit == 1)
			{
			echo "<td><a href='".base_url()."index.php/cts_surveyor_deportation_controller/generate_report/".$row->sample_id."' target='_blank'><button class='btn btn-primary'>Generate Report</button></a></td>";
			}
			elseif($report_generate_role != 1 )
			{
			echo "";
			}
		else
			{
			echo "<td>Report Pending</td>";
			}
			if($row->registered != 5 && $approve_report_role == 1)
			{
				echo "<td><button class='btn btn-primary set_approved_btn'  data-toggle='modal' data-target='#approve_surveyor_deportation ' id='' alt='".$row->surveyor_deportation_id."'>Approve</button></td></tr>";
			}
			else if($row->registered == 5)
			{
			echo "<td><div class='glyphicon glyphicon-check'></div></td>"; 
			}
			else
			{
			echo "<td>&nbsp;</td>";
			}
		}
		echo "<tr><td colspan='22'>";
				if($offset == 0)
				echo "<span class='glyphicon glyphicon-backward'></span>&nbsp;";
				else
				echo "<a href='javascript:surveyor_deportation_completed_pagination(".($offset-1).")'> <span class='glyphicon glyphicon-backward'></span>&nbsp;</a>";
			for($i=1;$i<=ceil($query_result['count']/6);$i++)
			{
				if($i == $offset+1)
				{
				echo $i;
				}
				else
				{
				echo "<a href='javascript:surveyor_deportation_completed_pagination(".($i-1).")'> ".$i." </a>";
				}
			}
				if($offset == ceil($query_result['count']/6)-1)
				echo "&nbsp;<span class='glyphicon glyphicon-forward'></span>";
				else 
				echo "<a href='javascript:surveyor_deportation_completed_pagination(".($offset+1).")'>&nbsp;<span class='glyphicon glyphicon-forward'>&nbsp;</span></a>";
			echo "</td></tr>";
		}
		
		public function send_to_dept($claim_id)
		{
		$sendValues = $this->input->post('send_values');
		if(isset($sendValues) && $sendValues!=NULL){
			$result = $this->cts_surveyor_deportation_model->send_to_dept($claim_id,$sendValues);
			echo json_encode(true);
			}
		else {
		echo json_encode(false);
		}
		//$this->load->view('cts_header');
		//$this->load->view('report/generate_surveyor_deportation_report',$data);
		}
		
		public function generate_report($sample_id)
		{
		$data['result'] = $this->cts_surveyor_deportation_model->generate_report($sample_id);
		$this->load->view('cts_header');
		$this->load->view('report/generate_surveyor_deportation_report',$data);
		}
		/* public function generate_entry_report($sample_id)
		{
		$data['result'] = $this->cts_surveyor_deportation_model->generate_entry_report($sample_id);
		$this->load->view('cts_header');
		$this->load->view('report/generate_surveyor_deportation_report',$data);
		} */	
}