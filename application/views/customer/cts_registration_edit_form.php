<?php
if(isset($result))
{
	if($result==1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Update Succesful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Update Failed!</b></div>";
}
if(isset($customer_details)){
foreach($customer_details as $row)
{
$cid = $row['cid'];
$first_name = $row['customer_first_name'];
$middle_name = $row['customer_middle_name'];
$last_name = $row['customer_last_name'];
$zone = $row['customer_zone'];
$district = $row['customer_district'];
$vdc_municipality = $row['customer_vdc_municipality'];
$vdc_ward_no = $row['customer_vdc_ward_no'];
$street = $row['customer_street'];
$mobile = $row['customer_mobile'];
$phone = $row['customer_phone'];
$email = $row['customer_email'];
}
}
?>



  <div class="container">
    <h3>Edit Customer Detail Form</h3><br>
      <table class="table" id="table">

        <form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/lims_customer_controller/edit_customer_by_id/<?php echo $cid;?>">
          
         
          
            <tr>
              <td><label>First Name</label></td>
              <td><input type="text" class="form-control" name="first_name" value="<?php echo $first_name;?>" required></td>
            </tr>
			
			<tr>
              <td><label>Middle Name</label></td>
              <td><input type="text" class="form-control" name="middle_name" value="<?php echo $middle_name;?>"></td>
            </tr>
			
			<tr>
              <td><label>Last Name</label></td>
              <td><input type="text" class="form-control" name="last_name" value="<?php echo $last_name;?>"required></td>
            </tr>
			
			 <tr>
              <td><label>Phone No</label></td>
              <td><input type="text" class="form-control" name="phone" value="<?php echo $phone;?>" required></td>
            </tr> 
			
			
          
            <tr>
              <td colspan="2"><label><b>Address</b></label></td>
              
            </tr>
          <tr>
              <td><label>Zone</label></td>
              <td><input type="text" class="form-control" name="zone" value="<?php echo $zone;?>"></td>
            </tr>
          <tr>
              <td><label>District</label></td>
              <td><input type="text" class="form-control" name="district" value="<?php echo $district;?>" required ></td>
            </tr>
          <tr>
              <td><label>VDC/Municipality</label></td>
              <td><input type="text" class="form-control" name="vdc/municipality" value="<?php echo $vdc_municipality;?>"></td>
            </tr>
			
			<tr>
              <td><label>VDC No./Ward No</label></td>
              <td><input type="text" class="form-control" name="vdc/ward_no" value="<?php echo $vdc_ward_no;?>"></td>
            </tr>
			
			<tr>
              <td><label>Street</label></td>
              <td><input type="text" class="form-control" name="street" value="<?php echo $street;?>" ></td>
            </tr>
			
			<tr>
	          <td><label>Mobile Number </label></td>
              <td><input type="text" class="form-control" name="mobile" value="<?php echo $mobile;?>" ></td>
            </tr>
          
           
			
			<tr>
              <td><label>Email</label></td>
              <td><input type="text" class="form-control" name="email" value="<?php echo $email;?>" ></td>
            </tr>
   

            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Submit</button></td>
            </tr>
          
        </form>
    </table>
	
</div><!---end of container-->
 </body>
 </html>
 