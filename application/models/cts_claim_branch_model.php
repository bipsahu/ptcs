<?php 
class cts_claim_branch_model extends CI_Model 
{
//This is the main customer model
	public function insert_branch_details()
	{
	//print_r($_POST);die();
		$data = array(
		'branch_name' => $this->input->post('branch_name'),	
		'address' =>$this->input->post('address')	
		);
		$result = $this->db->get_where('branches',array('branch_name'=>$data['branch_name']))->num_rows;
		/* print_r($result);
		echo $this->db->last_query(); */
		if($result >= 1)
		{
			return false;
		}
		else
		{
		$result = $this->db->insert('branches', $data);
		$result = $this->db->get_where('branches',array('branch_id'=>$this->db->insert_id()));
		return $result;
		}
	}
		
	public function insert_new_branch()
	{
		$data = array(
			'branch_name' => $this->input->post('branch_name')	
		);
		$result = $this->db->get_where('branches',array('branch_name'=>$data['branch_name']));
		if($result->num_rows == 1)
		{
			return false;
		}
		else
		{
			$result = $this->db->insert('branches', $data);
			return $result;
		}
	}
		
	public function show_all_branches()
	{
	$this->db->order_by("branch_id","asc");
	//$this->db->limit($limit, $start);
	$query = $this->db->get('branches')->result_array();
	return $query;

	}
	
	public function getBranches()
	{
		$query = $this->db->get('branches');
		return $query->result_array();

	}
	
	
	public function get_details_of_branch_by_id($cid)
	{
	$result = $this->db->get_where('branches', array('branch_id' => $cid));
	return $result->result_array();
	}
	
	public function edit_branch_by_id($bid)
	{
		$data = array(
		'branch_id' => $this->input->post('branch_id'),
		'branch_name' => $this->input->post('branch_name'),
		'address' => $this->input->post('address'),
		);
		
		$result = $this->db->get_where('branches',array('branch_id'=>$bid));
		if($result->num_rows == 1)
		{
		$this->db->where('branch_id', $bid);
		$result = $this->db->update('branches', $data);
		return $result;
		}
		else
		{
			return false;
		}
	}
	
	public function get_branch_by_id($bid)
	{
	$result = $this->db->get_where('branches', array('branch_id' => $bid)); 
	return $result->result_array();
	}
	
		public function delete_branch_by_id($bid)
	{
	$result = $this->db->delete('branches', array('branch_id' => $bid)); 
	return $result->result_array();
	}
	
	public function search_branch_detail($key)
	{
		$result = $this->db->query("select * from branch where branch_name like '".$key."%'");
		$branchs= array();
		foreach($result->result() as $row)
		{
			$branch="";
			$branch.=$row->branch_name;
						
			$branchs[]=$branch;
		}
	echo json_encode($branchs);
	}
	public function check_branch_name()
	{
	$branch_name = $this->input->post('branch_name');
	$result = $this->db->get_where('branch',array('branch_name'=>$branch_name));
		if($result->num_rows()>=1)
		{
		return true;
		}
		else
		{
		return false;
		}
	}
}