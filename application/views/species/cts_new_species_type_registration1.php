<div class="container">
  <h3>Species Type Registration Form</h3><br>
      <?php

if(isset($result))
{
	if($result==1){
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Successfully Registered!</b></div>";
		}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Registration Failed!</b></div>";
}  
  ?>
  
      <table class="table" id="table">

        <form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/lims_species_type_controller/insert_new_species_type">
    
			
			<tr>
              <td><label>Species Type </label></td>
              <td><input type="text" class="form-control" name="species_type" id="species_type" required> </td>
            </tr>

            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary" id="species_type_submit">Submit</button></td>
            </tr>
          
        </form>
    </table>
</div><!---end of container-->
 </body>
 </html>