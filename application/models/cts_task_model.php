<?php

class cts_task_model extends CI_Model
{
//This is the main task model
    public function insert_task_details()
    {
        $data = array(

            'task_type' => $this->input->post('task_type'),
            'total_task' => $this->input->post('total_task')

        );
        $result = $this->db->insert('task', $data);
        if ($this->db->_error_number() == 1062) {
            return 0;
        } else {
            return $result;
        }
    }

    /*
     * get the recent 10 updates for the tasks
     * @params : NULL
     * @return : result array
     * */
    public function getUpdates()
    {
        $this->db->select("*");
        $this->db->limit(10, 0);
        $this->db->order_by("updated_at", "DESC");
        return $this->db->get("task_registration")->result_array();

    }

    public function count_all_task($from = NULL, $to = NULL)
    {
        date_default_timezone_set('Asia/Kathmandu');
        if (isset($from) && $from != NULL && isset($to) && $to != NULL) {
            $from = str_replace("/", "-", $from);
            $to = str_replace("/", "-", $to);
            $days = date_diff(date_create($from), date_create($to));
            $days = $days->days;
            if ($days == '0')
                $days = 1;
            $this->db->where('task_registration.assign_date >=', $from);
            $this->db->where('task_registration.assign_date <=', $to);
        } else {
            $from = date("y-m-01");
            $to = date("y-m-d");

            $days = date_diff(date_create($from), date_create($to));
            $days = $days->days;
            if ($days == '0')
                $days = 1;
            $this->db->where('task_registration.assign_date >=', $from);
            $this->db->where('task_registration.assign_date <=', $to);

        }
        $result['total'] = $this->db->get("task_registration")->num_rows();
        //avg task perday

        if (isset($days))
            $result['avg'] = $result['total'] / ($days);
        else $result['avg'] = '0';


        //pending  task task
        $this->db->where('task_registration.status !=', "6");
        if (isset($days) && $days != '0') {
            $this->db->where('task_registration.assign_date >=', $from);
            $this->db->where('task_registration.assign_date <=', $to);
        }

        $pending_task = $this->db->get("task_registration")->num_rows();
        //echo $this->db->last_query();

        $result['pending_task'] = $pending_task;

        //paid amount
        $this->db->where("status", '6');

        //$this->db->join("discharge_voucher_generation", "discharge_voucher_generation.task_id = task_registration.task_id");
        $paid_amount = $this->db->get("task_registration")->num_rows();
        $result['paid_amount'] = $paid_amount;

        //average completed task
        $this->db->where("status", '6');
        if (isset($days) && $days != '0') {
            $this->db->where('task_registration.assign_date >=', $from);
            $this->db->where('task_registration.assign_date <=', $to);
        }

        $completed_task = $this->db->get("task_registration")->num_rows();
        //echo $this->db->last_query();

        if (isset($days))
            $result['avg_completed'] = $completed_task / $days;

        $this->db->select('*');
        $this->db->from('task_registration');

        $total = $this->db->get()->num_rows();
        //echo $this->db->last_query();
        $result['total_paid'] = $total;

        return $result;
    }

    public function count_project_by_name($name)
    {
        $this->db->where('task_registration.project', $name);
        $this->db->where('task_registration.project !=', "6");
        $query = $this->db->get('task_registration')->num_rows();

        return $query;


    }

    public function get_all_projects()
    {

        $query = $this->db->get('project')->result_array();
        return $query;


    }

    public function show_all_task($limit, $start)
    {
        $this->db->order_by("sid", "asc");
        $this->db->limit($limit, $start);
        $query = $this->db->get('task');
        return $query;


    }


    public function edit_task_by_id($sid)
    {
        $data = array(
            'task_type' => $this->input->post('task_type'),
            'total_task' => $this->input->post('total_task')
        );
        $result = $this->db->get_where('task', array('task_type' => $data['task_type']));
        if ($result->num_rows >= 1) {
            return false;
        } else {
            $this->db->where('sid', $sid);
            $result = $this->db->update('task', $data);
            return $result;
        }
    }


    public function get_details_of_task_by_id($sid)
    {
        $result = $this->db->get_where('task', array('sid' => $sid));
        return $result->result_array();
    }

    public function delete_task_by_id($sid)
    {
        $result = $this->db->delete('task_registration', array('task_id' => $sid));
        return $result;
    }
}