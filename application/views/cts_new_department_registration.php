<div class="container">
    <div class='alert alert-danger' role='alert' id="department_alert_error">
        <div class='glyphicon glyphicon-remove'></div>
        &nbsp;<b>Department Already Exists</b>
    </div>
    <div class='alert alert-success' role='alert' id="department_alert_success">
        <div class='glyphicon glyphicon-remove'></div>
        &nbsp;<b>Department Added Successfully !!!</b>
    </div>
    <h3>Add New Department</h3><br>

    <form method="post" role="form" enctype="multipart/form-data" id="department_form">
        <table class="table" id="table">

            <tr>
                <td><label>Department Name </label></td>
                <td><input type="text" class="form-control" name="dep_name" required></td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="submit" class="btn btn-primary" id="department_submit" value="Save">
                </td>
            </tr>
        </table>
    </form>

</div><!---end of container-->