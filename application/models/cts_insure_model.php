<?php 
class cts_insure_model extends CI_Model 
{
//This is the main customer model
	public function insert_insure_details()
	{
	$data = array(
	//'cid' =>$this->input->post('customer_id'),
	'insure_name' => $this->input->post('insure_name'),
	//'insure_zone' => $this->input->post('insure_zone'),
	'insure_address' => $this->input->post('address'),
	//'insure_vdc_municipality' => $this->input->post('insure_vdc/municipality'),
	//'insure_vdc_ward_no' => $this->input->post('insure_vdc/ward_no'),
	//'insure_street' => $this->input->post('insure_street'),
	'insure_mobile' => $this->input->post('insure_mobile'),
	'insure_phone' => $this->input->post('insure_phone'),
	'insure_email' => $this->input->post('insure_email')
	);
	$result = $this->db->insert('insure', $data);
		$result = $this->db->query("select insure_name,iid from insure where iid=".$this->db->insert_id());
			foreach($result->result() as $row)
			{
			$insure_details = array
			(
			'insure_name' => $row->insure_name,
			'insure_id' => $row->iid
			);
			}
			echo json_encode($insure_details);
	}
	
	public function insert_insure_details1()
	{
	$data = array(
	
	'insure_name' => $this->input->post('insure_name'),
	'insure_zone' => $this->input->post('insure_zone'),
	'insure_district' => $this->input->post('insure_district'),
	'insure_vdc_municipality' => $this->input->post('insure_vdc/municipality'),
	'insure_vdc_ward_no' => $this->input->post('insure_vdc/ward_no'),
	'insure_street' => $this->input->post('insure_street'),
	'insure_mobile' => $this->input->post('insure_mobile'),
	'insure_phone' => $this->input->post('insure_phone'),
	'insure_email' => $this->input->post('insure_email'),
	);
	$result = $this->db->insert('insure', $data);
	return $result;
	}
	
	public function show_all_insure()
	{
	$this->db->order_by("iid","desc");
	//$this->db->limit($limit, $start);
	$query = $this->db->get('insure');
	return $query;

	}
	
	public function edit_insure_by_id($iid)
	{
	
	$data = array(
	//'cid' =>$this->input->post('customer_id'),
	'insure_name' => $this->input->post('insure_name'),
	//'insure_zone' => $this->input->post('insure_zone'),
	'insure_address' => $this->input->post('address'),
	//'insure_vdc_municipality' => $this->input->post('insure_vdc/municipality'),
	//'insure_vdc_ward_no' => $this->input->post('insure_vdc/ward_no'),
	//'insure_street' => $this->input->post('insure_street'),
	'insure_mobile' => $this->input->post('insure_mobile'),
	'insure_phone' => $this->input->post('insure_phone'),
	'insure_email' => $this->input->post('insure_email')
	);
	$this->db->where('iid', $iid);
	$result = $this->db->update('insure', $data);
	return $result;
	}
	
	public function get_details_of_insure_by_id($iid)
	{
	$result = $this->db->get_where('insure', array('iid' => $iid));
	return $result->result_array();
	}
	
	public function get_insure_by_id($iid)
	{
	$result = $this->db->get_where('insure', array('iid' => $iid)); 
	return $result;
	}
	
	public function delete_insure_by_id($iid)
	{
	$result = $this->db->delete('insure', array('iid' => $iid)); 
	return $result;
	}
	
	public function search_insure($key)
	{
	
	$result = $this->db->query("select * from insure where insure_name like '".$key."%'");
	$insures= array();
		foreach($result->result() as $row)
		{
			//$insure="";
			//$insure.=$row->iid;
			//$insure.="-".$row->insure_name;	
			$insure=$row->insure_name;			
			$insures[]=$insure;
		}
	echo json_encode($insures);
	}
	public function check_insure()
	{
		$insure_name = $this->input->post('insure_name');
		$insure_name_arr = explode("-",$insure_name);
		if(count($insure_name_arr) == 2)
		$insure_name = $insure_name_arr[1];
		$result = $this->db->get_where('insure',array('insure_name'=>$insure_name));
		if($result->num_rows()>=1)
		{
		return true;
		}
		else
		{
		return false;
		}
	}
	
}