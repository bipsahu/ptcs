
<div class="container">

        <!--main dashboard starts here -->
        <div class="row">
            <div class="col-sm-12">
                <div id="bargraph" style="height: 400px; width: 100%;"></div>
            </div>
            <div class="col-sm-12">
                <div id="doughnut" style="height: 300px; width: 100%;"></div>
            </div>
        </div>
    <br/>
    <div class="col-sm-12">
        <div id="linegraph" style="height: 300px; width: 100%;"></div>
    </div>
</div>


<?php
// just maping the array into one for completed projects reports for min and max values
// print_r($monthreports);die;
$numbers = array_map(function($monthreports) {
    return $monthreports['completed_projects'];
}, $monthreports);
// print_r($monthreports);die;
$maxNum = max($numbers);
$minNum = min($numbers);
?>
<script type="text/javascript">

    window.onload = function () {
        var bargraph = new CanvasJS.Chart("bargraph", {
            theme: "theme2",//theme1
            title:{
                text: "Completion (%) Of Current Projects"
            },
            axisX: {
                // labelAngle: -75
            },
            animationEnabled: false,   // change to true
            data: [
                {
                    // Change type to "bar", "area", "spline", "pie",etc.
                    type: "column",
                    dataPoints: [
                        <?php foreach($results as $result):?>
                        { label: "<?php echo $result['project_name'];?>",  y: <?php echo $result['task_based_percent_complete'];?>  },
                        <?php endforeach;?>
                    ]
                }
            ]
        });

        var chart = new CanvasJS.Chart("doughnut",
            {
                title:{
                    text: "Completion (%) Of Current Projects"
                },
                data: [
                    {
                        type: "pie",
                        dataPoints: [
                            <?php foreach($results as $result):?>
                            { label: "<?php echo $result['project_name'];?>",  y: <?php echo $result['task_based_percent_complete'];?>  },
                            <?php endforeach;?>
                        ]
                    }
                ]
            });


        var linegraph = new CanvasJS.Chart("linegraph",
            {
                theme: "theme2",
                title:{
                    text: "Total Projects Completed"
                },
                animationEnabled: true,
                axisX: {
                    valueFormatString: "MMM",
                    interval:1,
                    intervalType: "month"

                },
                axisY:{
                    includeZero: false

                },
                data: [
                    {
                        type: "line",
                        //lineThickness: 3,
                        dataPoints: [
                            <?php foreach($monthreports as $monthreport):
                            if($monthreport['completed_projects'] == $maxNum):?>
                            { x: new Date(<?php echo $monthreport['year'];?>, <?php echo date_parse($monthreport['month'])['month'];?>,0), y: <?php echo $maxNum;?>, indexLabel: "highest",markerColor: "red", markerType: "triangle"},

                            <?php elseif($monthreport['completed_projects'] == $minNum):?>

                            { x:  new Date(<?php echo $monthreport['year'];?>,<?php echo date_parse($monthreport['month'])['month'];?>,0), y: <?php echo $minNum;?> , indexLabel: "lowest",markerColor: "DarkSlateGrey", markerType: "cross"},
                          <?php else:?>
                            { x:  new Date(<?php echo $monthreport['year'];?>,<?php echo date_parse($monthreport['month'])['month'];?>,0), y: <?php echo $monthreport['completed_projects'];?> },
                            <?php endif;?>
                            <?php endforeach;?>

                        ]
                    }


                ]
            });
        bargraph.render();
        chart.render();
        linegraph.render();
    }
</script>