<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Lims_approval_model extends CI_model
{
	public function approve_sample()
	{
		date_default_timezone_set('UTC');
		$publish_check_box_id = $this->input->post('publish_check_box');
		$table_name = $this->input->post('tbl');
		$id = $this->input->post($table_name.'_id');
		$data = array
		(
		'approved_remarks' => $this->input->post('approve_remarks'),
		'approved_date' => date('m/d/Y'),
		'registered' => 5
		);
		$this->db->where($table_name.'_id',$id);
		$this->db->update($table_name,$data);
				if($publish_check_box_id)
				{
				$this->db->where($table_name.'_id',$id);
				$this->db->update($table_name,array('publish_bit'=>1));
				}
				else
				{
				$this->db->where($table_name.'_id',$id);
				$this->db->update($table_name,array('publish_bit'=>0));
				}
			$this->db->select('*');
			$this->db->from('sample_registration');
			$this->db->join($table_name, 'sample_registration.sample_id ='. $table_name.'.sample_id');
			$this->db->where($table_name.'_id',$id);
			$result_1 = $this->db->get();
			$row_array = array();
				foreach($result_1->result_array() as $row)
				{
				$row_array = $row;
				}
			$data1['send_to'] = $this->input->post('user_assign');
			if($data1['send_to'] != 0)
			{
			$data1['send_from'] = $this->session->userdata('user_id');
			$data1['assigned_date'] = date('m/d/Y');
			$data1['lab_no'] = $row_array['labno'];
			$data1['dep_name'] = $table_name;
			$data1['message'] = $this->input->post('approve_remarks');
			$result = $this->db->insert('assignment_table', $data1);
			}
			
				$investigation = "";
				$in = $this->input->post('investigation');
				if(!empty($in))
				{
					foreach($in as $key => $val)
					{
						$investigation_requested = str_replace("_"," ",$key);
						$investigation.=$investigation_requested.",";	
					}
				}
				
				$dep = array();
				$dep = $this->input->post('dep');
				if(!empty($dep))
				{
					$departments="";
					
					$this->db->insert('sample_registration',array('customer_id'=>$row_array['customer_id'],'firm_id'=>$row_array['firm_id'],'labno' =>$row_array['labno'],'date' => $row_array['date'],'customer_name' => $row_array['customer_name'],'firm_name' =>$row_array['firm_name'],'animal'=>$row_array['animal'],'sample_type' => $row_array['sample_type'],'no_of_sample' => 1,'remarks'=>$row_array['remarks'],'animal_type'=>$row_array['animal_type'],'investigation_requested'=>$investigation,'sex'=>$row_array['sex'],'refrence_no'=>$row_array['refrence_no'],'paid_amount'=>$row_array['paid_amount'],'species_type'=>$row_array['species_type']));
					$last_inserted_sample_id = $this->db->insert_id();
					
					if(!empty($in))
					{
						foreach ($in as $key => $val)
						{
							$this->db->insert('sample_investigation',array('sample_id'=>$last_inserted_sample_id,'investigation_id'=>$val,'investigation_name'=>$key));
						} 
					}
					foreach($dep as $key => $val)
					{
						$department_name = ucfirst(str_replace("_"," ",$key));
						$departments.=",{$department_name} ";
					if(strtolower($key)=='serology')
					{
							$result = $this->db->insert('serology',array('sample_id'=>$last_inserted_sample_id ,'cid'=>$row_array['cid'],'sample_no'=>1));
							$last_serology_id = $this->db->insert_id();
							$this->db->insert('sample_department',array('sample_registration_id'=>$row_array['sample_id'],'dep_id'=>$val,'lab_reg_id'=>$last_serology_id,'dep_name'=>$key));
					}
					else if(strtolower($key) == 'virology')
					{
							$result = $this->db->insert('virology',array('sample_id'=>$last_inserted_sample_id ,'cid'=>$row_array['cid'],'sample_no'=>1));
							$last_virology_id = $this->db->insert_id();
							$this->db->insert('sample_department',array('sample_registration_id'=>$row_array['sample_id'],'dep_id'=>$val,'lab_reg_id'=>$last_virology_id,'dep_name'=>$key));
					}
					else if(strtolower($key) == 'molecular_biology')
					{
							$result = $this->db->insert('biology',array('sample_id'=>$last_inserted_sample_id ,'cid'=>$row_array['cid'],'sample_no'=>1));
							$last_biology_id = $this->db->insert_id();
							$this->db->insert('sample_department',array('sample_registration_id'=>$row_array['sample_id'],'dep_id'=>$val,'lab_reg_id'=>$last_biology_id,'dep_name'=>$key));
					}
					else
					{
							$this->db->insert('sample_department',array('sample_registration_id'=>$row_array['sample_id'],'dep_id'=>$val,'lab_reg_id'=>0,'dep_name'=>$key));
					}
					$departments = trim(trim($departments,","));
				}
				$this->db->where('sample_id',$last_inserted_sample_id);
				$this->db->update('sample_registration',array('departments'=>$departments));
				}
	}
}
