  <?php
  if(isset($roles_info))
  {
  $i=0;
  $role = array();
  foreach($roles_info->result_array() as $key=>$value)
  {
  $role[] = $value;
  $module[] = $role[$i]['module_name'];
  $i++;
  }
  }
  ?>
  <div class="container">
  	<!-- <div id="img-icon">
        <img src="<?php echo base_url();?>resource/images/department-icon.jpg" align="left"/>
      </div> -->
      <div id="breadcumb-text">
        <ul class="nav nav-tabs">
          <li><a href=""><strong>Projects</strong></a></li>
          <!-- <li class="vertical-divider"></li> -->
  		<?php if(strtolower($this->session->userdata('user_position'))=="admin" || $role[2]['add']){?>
          <li><a href="<?php echo base_url();?>index.php/cts_controller/add_project_form">Add New Project</a></li>
          <!-- <li class="vertical-divider"></li> -->
  		<?php } ?>
          <li class="active"><a href="">List Projects</a></li>
        </ul>
      </div>
      <hr>
  <?php
  if(!empty($result_indicator))
  {
  	if($result_indicator == 1)
  	{
                    
  		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Deletion Successful!</b></div>";
  	}
  	else
  	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Deletion Failed!</b></div>";
  }
  ?>
  <table class="table table-striped" id="department_table">
  <thead>
    <th>S NO.</th>
    <th>Project Name</th>
    <?php
    if(strtolower($this->session->userdata('user_position')) == "admin")
    {
      echo "<th>Edit</th>";
    } 
    elseif($role[2]['edit'])
    {?>
      <th>Edit</th>
    <?php  } ?>
    <?php if(strtolower($this->session->userdata('user_position')) == "admin")
    {
      echo "<th>Delete</th>";
    } 
    elseif($role[2]['delete']){?>
    <th>Delete</th>
    <?php  } $count = 1;?>
  </thead>
  <tbody style="text-align: center;">
    
  <?php
  foreach($all_department_with_parent as $row){
  ?>
  <tr>
    <td><?php echo $count;$count++;?></td>

  <td><?php echo $row['child_name'];?></td> 
  <!--<td><?php echo $row['parent_name'];?></td>-->
  <?php 
  if(strtolower($this->session->userdata('user_position')) == "admin")
  {
    echo "<td><a href='".base_url()."index.php/cts_controller/edit_project_form/".$row['child_project_id']."'><span class='glyphicon glyphicon-edit'></span></a></td>";
  }
  elseif($role[2]['edit'])
  {
    echo "<td><a href='".base_url()."index.php/cts_controller/edit_project_form/".$row['child_project_id']."'><span class='glyphicon glyphicon-edit'></span></a></td>";
  }
  if(strtolower($this->session->userdata('user_position')) == "admin")
  {
    echo "<td><a href='#'><span class='glyphicon glyphicon-trash' alt='".$row['child_project_id']."'></span></a></td>";
  }
  elseif($role[2]['delete'])
  {
    echo "<td><a href='#'><span class='glyphicon glyphicon-trash' alt='".$row['child_project_id']."'></span></a></td>";
  }
  ?>
  </tr>
  <?php
  }
  ?> 
  </tbody>

  </table>
  </div>

  <div id="dialog-confirm" title="Are you sure to delete this project?">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p>
  </div>

  </body>
  <script type="text/javascript">
  $(document).ready(function(){
    $( "#dialog-confirm" ).hide();
    $('.glyphicon-trash').click(function(){
      var project_id = $(this).attr('alt');
      $("#dialog-confirm" ).dialog({
        resizable: false,
        height:160,
        modal: true,
        show: {
        effect: "blind",
        duration: 300
        },
        hide: {
        effect: "blind",
        duration: 300
        },
        buttons: {
          "Delete": function() {
            // alert('hello');exit();
            window.location.assign('<?php echo base_url();?>'+'index.php/cts_controller/delete_the_project/'+project_id);
          },
          Cancel: function() {
            $( this ).dialog( "close" );
          }
        }
      });
    });
  });
  </script>
  </html>