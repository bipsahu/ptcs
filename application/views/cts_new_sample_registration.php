<div class="container">
  <h3>Sample Registration Form</h3><br>
  <?php

if(isset($result))
{
	if($result==1){
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Successfully Registered!</b></div>";
		}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Registration Failed!</b></div>";
}  
  ?>
      <table class="table" id="table">

        <form method="post" role="form" enctype="multipart/form-data" id="sample_form">
    
			
			<tr>
              <td><label>Sample Name </label></td>
              <td><input type="text" class="form-control" name="sample_type" required> </td>
            </tr

            <tr>
        	   <td colspan="2"><button type="button" name="submit" class="btn btn-primary" id="sample_submit">Submit</button></td>
            </tr>
          
        </form>
    </table>
</div><!---end of container-->
 </body>
 </html>