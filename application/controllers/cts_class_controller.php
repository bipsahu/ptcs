<?php 
class cts_class_controller extends CI_Controller 
{
	public function __construct()
	{
	parent::__construct();
		$this->load->model('cts_class_model');
		$this->load->model('cts_login');
	}
	public function index()
	{
		$data1['positions'] = $this->cts_role_model->show_all_position_role();
		$data1['departments'] = $this->cts_department_model->getDepartment();
		$data1['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data1);
		$this->load->view('classtype/cts_new_class_registration');
	}
	
	public function insert_class_details()
	{
		$data['result'] = $this->cts_class_model->insert_class_details();
		if($data['result'] == false)
		{
		echo " 0";
		}
		else
		{
			foreach($data['result']->result() as $row)
			{
			$class_details = array(
				'class_name' => $row->class_name
				);
			}
			echo json_encode($class_details);
		}
	
	}
	public function search_class_detail($key)
	{
	$this->cts_class_model->search_class_detail($key);
	}
	public function check_class_name()
	{
	$result = $this->cts_class_model->check_class_name();
	if($result)
	echo " 1";
	else
	echo " 0";
	}
}