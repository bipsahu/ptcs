    <?php
    if(isset($task_detail[0]["task_category_id"]) && $task_detail[0]["task_category_id"] != '')
    {
        $data = $task_detail[0]['project'];
        // echo $data;die;
        $result = $this->db->query("select project_taskcategory from project where project_id = $data")->result_array();
        $display_task_category = json_decode($result[0]['project_taskcategory']);
        foreach ($display_task_category as $key => $val) {
            $task_category_name = $this->db->query("SELECT name FROM task_category WHERE t_category_id = $key")->result_array();
            // echo $key;
            $myarray[$key] = $task_category_name[0]['name'];
        }
    // print_r($myarray);die;
    }
    ?>
<div class="container">
    <div id="breadcumb-text">
      <ul class="nav nav-tabs">
        <li><a href=""><strong>Task</strong></a></li>
        <!-- <li class="vertical-divider"></li>&nbsp; -->
    <?php if(strtolower($this->session->userdata('user_position'))=="admin" || $role[2]['add']){?>
        <li class="active"><a href="">Add Task</a></li>
        <!-- <li class="vertical-divider"></li>&nbsp; -->
    <?php } ?>
        <li><a href="<?php echo base_url();?>index.php/cts_task_controller/showTask">Update Task</a></li>
        <li><a href="<?php echo base_url();?>index.php/cts_task_controller/show_all_task">List Task</a></li>
      </ul>
    </div>
    <hr>
    <!-- <h3><?php
     //   if (isset($edit_id) && $edit_id != NULL) echo "Edit Task <span class='red'>(" . $task_detail[0]['task_name'] . ")</span>  Completed " . $task_detail[0]['percent_complete']. "%"; else echo "Task Assign Form" ?></h3> -->
    <br>
    <?php
    if (isset($success)) {
        if ($success == "ok") {
            echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Successfully Registered!</b></div>";
        } else
            echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Registration Failed!</b></div>";
    }
    ?>
    <div class="modal fade" id="customer_model" tabindex="101" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Client</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('cts_registration'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="insure_model" tabindex="102" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Insure</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('cts_new_insure_registration'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="task_model" tabindex="103" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New task</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('task/cts_new_task_registration'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="task_department_modal" tabindex="104" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Department</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('cts_new_department_registration'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="project_model" tabindex="104" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Project</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('cts_new_project_registration'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="indicator_model" tabindex="104" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Indicator</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('cts_new_indicator_registration'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="species_type_modal" tabindex="106" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Species Type</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('species/cts_new_species_type_registration'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="investigation_modal" tabindex="107" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Investigation Requested</h4>
                </div>
                <div class="modal-body">
                    <?php
                    $data['department_list'] = $department_list;
                    $this->load->view('investigation/cts_investigation_registration', $data); ?>
                </div>
            </div>
        </div>
    </div>

    <form method="post" role="form" autocomplete="off" enctype="multipart/form-data"
          action="<?php if (isset($edit_id) && $edit_id != NULL) echo base_url() . 'index.php/cts_task_controller/insert_task_registration_details/' . $edit_id; else echo base_url() . 'index.php/cts_task_controller/insert_task_registration_details' ?>"
          id=" task_registration_form">


        <table class="table" id="task_table">
            <?php date_default_timezone_set("UTC"); ?>
            <tr>
                <td class="first_td"><label>Task Assign Date*</label></td>
                <td><input type="text" class="form-control" tabindex="0" name="task[assign_date]" required id="assign_date" value="<?php if (isset($edit_id) && $edit_id != NULL) echo $task_detail[0]['assign_date']; else echo date('Y-m-d'); ?>">
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="first_td"><label>Task Name.*</label></td>
                <td><input type="text" class="form-control" tabindex="1" name="task[task_name]" required
                           value="<?php if (isset($edit_id) && $edit_id != NULL) echo $task_detail[0]['task_name']; ?>">
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="first_td"><label>Task No.*</label></td>
                <td><input type="text" class="form-control" tabindex="1" name="task[regd_no]" required value="<?php if (isset($edit_id) && $edit_id != NULL) echo $task_detail[0]['regd_no']; else echo $regd_no + 1; ?>">
                </td>
                <td></td>
            </tr>

            <tr>
                <td class="first_td"><label>Deadline</label></td>
                <td><input type="text" class="form-control" tabindex="0" name="task[deadline_date]" id="datepicker" value="<?php if (isset($edit_id) && $edit_id != NULL) echo $task_detail[0]['deadline_date']; ?>">
                </td>
                <td></td>
            </tr>

            <tr>
                <td class="first_td"><label>Alert Before</label></td>
                <td><input type="number" min="0"  class="form-control" tabindex="0" name="task[alert_day]" id="alert_day" value="<?php if (isset($edit_id) && $edit_id != NULL) echo $task_detail[0]['alert_day']; ?>">
                </td>
                <td>Days</td>
            </tr>


            <tr>
                <td class="first_td"><label>Departments</label></td>
                <td><select type="text" class="form-control" tabindex="4" name="task[department]" id="department">
                        <option value="">Select Department</option>
                        <?php

                        if (isset($departments)):
                            foreach ($departments as $department) {
                                //selected for editing task
                                $selected = $department->dep_id == $task_detail[0]["department"] ? "selected" : "";

                                echo "<option value='" . $department->dep_id . "'" . $selected . " >" . $department->dep_name . "</option>";
                            }
                        endif; ?>
                    </select></td>
                <td>
                    <button type="button" name="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#task_department_modal" id="new_class">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>

                </td>
            </tr>
            <tr>
                <td class="first_td"><label>Projects*</label></td>
                <td>
                    <select type="text" class="form-control" tabindex="4" name="task[project]" id="project" required>
                        <option value="">Select Project</option>
                        <?php if (isset($projects)):

                            foreach ($projects as $project) {

                                //selected for editing task
                                $selected = $project['project_id'] == $task_detail[0]["project"] ? "selected" : "";
                                echo "<option value='" . $project['project_id'] . "' " . $selected . ">" . $project['project_name'] . "</option>";
                            }
                        endif; ?>
                    </select>
                </td>
                <td>
                    <button type="button" name="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#project_model" id="new_class">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>

                </td>
            </tr>

            <tr>
                <td class="first_td"><label>Task Category*</label></td>
                <td>
                    <select type="text" class="form-control" tabindex="4" name="task[task_category_id]" id="task_cat" required>
                        <option value="">Select Task Category</option>
                        <?php 
                        if($myarray)
                        {
                            foreach ($myarray as $key => $value) 
                            {
                            ?>
                            <option value="<?php echo $key; ?>" <?php if($task_detail[0]['task_category_id'] == $key){ echo "selected";} ?>><?php echo $value; ?></option>

                      <?php 
                            }
                        } ?>
                    </select>
                </td>
            </tr>

            <!-- <tr>
                <td><label>Task Category Completed(%)</label></td>
                <td><input type="number" class="form-control" tabindex="1" name="task[task_category_completed]" 
                           value="<?php if (isset($edit_id) && $edit_id != NULL) echo $task_detail[0]['task_category_completed']; ?>">
                </td>
                <td></td>
            </tr> -->

            <tr>
                <td class="first_td"><label>Indicators*</label></td>
                <td><select type="text" id="multiple-select-indicator" class="indicator" name="task[indicator]" required  multiple="multiple">
                        <?php if (isset($indicators)):

                            //convert json encoded data to array and check if key of array and indicator id matches
                            $editIndicators = json_decode($task_detail[0]["indicators"], true);
//                            print_r($editIndicators);
                            $editachieved = json_decode($task_detail[0]["achieved"], true);


                            foreach ($indicators as $indicator) {

                                //assign key-value pair to indicators id and name respectively
                                $keyValueIndicators[$indicator['indicator_id']] = $indicator['indicator_name'];

                                //check for selection for indicators
                                $selected = array_key_exists($indicator['indicator_id'], $editIndicators) ? "selected" : "";

                                echo "<option value='" . $indicator['indicator_id'] . "' " . $selected . " >" . $indicator['indicator_name'] ."</option>";
                            }
                        endif; ?>
                    </select>
                </td>
                <td>
                    <button type="button" name="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#indicator_model" id="new_class">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>

                </td>
                <!-- user table added through juery               -->
                <td>
                    <table class="table-bordered alert alert-warning">
                        <tbody id="assigned_indicators">
                        <tr class="indicator_title">
                            <td><strong><em>Indicators Selected</em></strong></td>
                            <td><strong><em>Assign Weightage(%)</em></strong></td>
                            <!-- <td style="color:#0F4C8F"><strong><em>Achievement 100% per <br/>indicator(%)</em></strong> -->
                            <!-- </td> -->
                        </tr>

                        <!--list of indicators and weightage for edit task only-->
                        <!-- Used achieved array in same loop with indicator key-->
                        <?php if (isset($edit_id) && $edit_id != NULL):
//                            print_r($editIndicators);
                            foreach ($editIndicators as $eKey => $eIndicators):
                                ?>

                                <tr id="indicator_<?php echo $eKey; ?>" class="indicator_body">
                                    <td><?php echo $keyValueIndicators[$eKey];?></td>
                                    <td><input type="number" name="task[indicators][<?php echo $eKey; ?>]"
                                               value="<?php echo $eIndicators; ?>">
                                    </td>
                                   <!--  <td style="color:#0F4C8F"><input type="number"
                                                                     name="task[achieved][<?php echo $eKey;?>]"
                                                                     value="<?php echo $editachieved[$eKey]; ?>"></td> -->
                                    <!--                                               value=""></td>-->
                                </tr>
                            <?php endforeach;endif; ?>
                        <!--list of indicators ends here-->

                        <tr class="indicator_total">
                            <td><em><strong>Total: </strong></em></td>
                            <td id="indicator_total_value">
                            </td>
                            <!-- <td style="color:#0F4C8F" id="achievement_total_value"></td> -->
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td><label>Team Members*</label></td>
                <td><select type="text" id="multiple-select-team" multiple="multiple" name="task[member]" required multiple="multiple">

                        <?php if (isset($users)):
                            //convert json encoded data to array and check if key of array and user id matches
                            $editUsers = json_decode($task_detail[0]["members"], true);

                            foreach ($users->result_array() as $user) {

                                //assign key-value pair to users/members id and name respectively
                                $keyValueUsers[$user['user_id']] = $user['user_name'];

                                //check for selection for users/members
                                $selected = array_key_exists($user['user_id'], $editUsers) ? "selected" : "";
                                echo "<option value='" . $user['user_id'] . "'" . $selected . ">" . $user['user_name'] . "</option>";
                            }
                        endif; ?>
                    </select>
                </td>
                <td>
                   <!--  <button type="button" name="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#class_model" id="new_class">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button> -->

                </td>
                <!-- user table added through juery               -->
                <td>
                    <table class="table-bordered alert alert-warning">
                        <tbody id="assigned_users">
                        <tr class="user_title">
                            <td><strong><em>Users Selected</strong></em></td>
                            <td><strong><em>Assign Hours(hrs)</em></strong></td>
                           <!--  <td><strong><em>Salary Per Hrs</em></strong></td>
                            <td><strong><em>Total Salary</em></strong></td> -->
                        </tr>
                        <!--list of indicators and weightage for edit task only-->
                        <?php if (isset($edit_id) && $edit_id != NULL):
                        // print_r($editUsers);die;
                            foreach ($editUsers as $eKey => $eUsers):
                            $user_salary = $this->db->query("SELECT hourly_salary FROM user WHERE user_id = $eKey")->result_array();
                                ?>
                                <tr id="user_<?php echo $eKey; ?>" class="user_body">
                                    <td><?php echo $keyValueUsers[$eKey]; ?></td>
                                    <td><input type="number" name="task[members][<?php echo $eKey; ?>]"
                                               value="<?php echo $eUsers; ?>">
                                    </td>
                                    <!-- <td><?php// echo $user_salary[0]['hourly_salary'] ?></td> -->
                                </tr>
                            <?php endforeach;endif; ?>
                        <!--list of indicators ends here-->
                        <tr class="hour_total">
                            <td><em><strong>Total: </strong></em></td>
                            <td id="hour_total_value"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>

            <tr>
                <td><label>Cost Allocated</label></td>
                <td><input type="number" min="0" class="form-control" tabindex="1" name="task[cost_allocated]"
                           value="<?php if (isset($edit_id) && $edit_id != NULL) echo $task_detail[0]['cost_allocated']; ?>">
                </td>
                <td></td>
            </tr>
         
            <tr>
                <td><label>Remarks</label></td>
                <td><textarea class="form-control" tabindex="0" name="task[remarks]"
                              id="task_remarks"><?php if (isset($edit_id) && $edit_id != NULL) echo $task_detail[0]['remarks']; ?></textarea>
                </td>
                <td></td>
            </tr>
            <tr>
                <td><label>Assigned By</label></td>
                <td>
                    <span><?php if (isset($edit_id) && $edit_id != NULL) echo $task_detail[0]['assigned_by']; else echo $this->session->userdata('user_name') . ' (' . $this->session->userdata('email') . ')'; ?></span>
                </td>

            </tr>

            <tr>
                <td colspan="2">
                    <button type="submit" name="submit" class="btn btn-primary" id="submit">Submit</button>
                </td>
                <td></td>
            </tr>
        </table>
    </form>
</div><!---end of container-->
<script>
    $(document).ready(function () {

        

      //flash messages display/hide
        $("#department_alert_error").hide();
        $("#department_alert_success").hide();
        $("#project_alert_error").hide();
        $("#project_alert_success").hide();
        $("#indicator_alert_error").hide();
        $("#indicator_alert_success").hide();

        //show the indicators and member titles for editing task request
        <?php if (isset($edit_id) && $edit_id != NULL): ?>
        $("tr.indicator_title").show();
        $("tr.indicator_total").show();
        $("tr.hour_title").show();
        $("tr.user_title").show();
        $("tr.hour_total").show();
        $("#indicator_total_value").text(<?php echo array_sum($editIndicators); ?> +" %");
        $("#hour_total_value").text(<?php echo array_sum($editUsers); ?> +" hrs");
        <?php endif; ?>
        <?php if(isset($edit_id)&&$edit_id!=NULL):?>
        $("tr.indicator_title").show();
        $("tr.indicator_total").show();
        $("tr.hour_title").show();
        $("tr.hour_total").show();
        $("#indicator_total_value").text(<?php echo array_sum($editIndicators);?> +" %");
        $("#achievement_total_value").text(<?php echo $task_detail[0]['percent_complete'];?> +" %");
        $("#hour_total_value").text(<?php echo array_sum($editUsers);?> +" hrs");
        <?php endif;?>

        //multiple select indicators

        indicatorMultiple();
        function indicatorMultiple()
        {

        $("#multiple-select-indicator").multipleSelect({
            placeholder: "Select Indicators",
            width: "100%",
            filter: true,
            onCheckAll: function () {

                $("tr.indicator_title").show();
                $("tr.indicator_total").show();
                //clear first
                $(".indicator_body").each(function () {
                    $(this).remove();
                });
                var checkAllValues = $("#multiple-select-indicator").multipleSelect("getSelects");

                var checkAllUsers = $("#multiple-select-indicator").multipleSelect("getSelects", "text");
                $.each(checkAllValues, function (index, value) {
                    $("#assigned_indicators tr:first-child").after(" <tr class='indicator_body' id='indicator_" + value + "'><td>" + checkAllUsers[index] + "</td><td><input type='number' name='task[indicators][" + value + "]'></td></tr>");
                });
            },
            onUncheckAll: function () {
                $("tr.indicator_title").hide();
                $("tr.indicator_total").hide();
                $(".indicator_body").each(function () {
                    $(this).remove();
                });
            },
            onClick: function (view) {
                // alert('single');
                if (view.checked == true) {
                    $("tr.indicator_title").show();
                    $("tr.indicator_total").show();
                    $("#assigned_indicators tr:first-child").after("<tr class='indicator_body' id='indicator_" + view.value + "'><td>" + view.label + "</td><td><input type='number' name='task[indicators][" + view.value + "]'></td></tr>");
                }
                else {
                    var removeCheck = "#assigned_indicators tr#indicator_" + view.value;
                    $(removeCheck).remove();
                }

            },
        });

        }



        //multiple select members
        $("#multiple-select-team").multipleSelect({
            placeholder: "Select Members",
            width: "100%",
            onCheckAll: function () {

                $("tr.user_title").show();
                $("tr.hour_total").show();
                //clear first
                $(".user_body").each(function () {
                    $(this).remove();
                });

                var checkAllValues = $("#multiple-select-team").multipleSelect("getSelects");
                var checkAllUsers = $("#multiple-select-team").multipleSelect("getSelects", "text");
                console.log(checkAllUsers);
                $.each(checkAllValues, function (index, value) {
                    $("#assigned_users tr:first-child").after(" <tr class='user_body' id='user_" + value + "'><td>" + checkAllUsers[index] + "</td><td><input type='number' name='task[members][" + value + "]'> </td></tr>");
                });
            },
            onUncheckAll: function () {
                $("tr.user_title").hide();
                $("tr.hour_total").hide();
                $(".user_body").each(function () {
                    $(this).remove();
                });
            },
            onClick: function (view) {
                // alert(view);
                console.log(view);
                if (view.checked == true) {
                    $("tr.user_title").show();
                    $("tr.hour_total").show();
                    $("#assigned_users tr:first-child").after(" <tr class='user_body' id='user_" + view.value + "'><td>" + view.label + "</td><td><input type='number' name='task[members][" + view.value + "]'> </td></tr>");
                }
                else {
                    var removeCheck = "#assigned_users tr#user_" + view.value;
                    $(removeCheck).remove();
                }

            },
        });


    //calculate total for indicators weightages

    $("#assigned_indicators ").on("keyup", "input[type='number']", function (event, value) {
        var totalWeightage = {};
        $('#assigned_indicators tr td input[type=number]').each(function () {
            var colIndex = $(this).parent().index();
            if (!(colIndex in totalWeightage)) {
                totalWeightage[colIndex] = 0;
            }
            var input = parseInt($(this).val(), 10);
            if (input) {

                    totalWeightage[colIndex] += input;

            }
        });
        var weight = {
            '1': $("input[name='task[indicators][1]']").val(),
            '2': $("input[name='task[indicators][2]']").val()
        };

        var achieved = {
            '1': $("input[name='task[achieved][1]']").val(),
            '2': $("input[name='task[achieved][2]']").val()
        };

        totalWeightage['2'] = Math.round((achieved['2']/100*weight['2'])+(achieved['1']/100*weight['1']));
        $.each(totalWeightage, function (i, total) {
            if (total > 100) {
                $('.indicator_total td').eq(i).html("<span class='red'>error: percent greater than 100</span>");
            }
            else if(i == 1 && total < 100) {
                $('.indicator_total td').eq(i).html("<span class='red'>error: percent less than 100</span>");
            }
            else {
                $('.indicator_total td').eq(i).text(total + ' %');
            }
        });
    });

    //calculate total for member hours

    $("#assigned_users").on("keyup", "input[type='number']", function (event, value) {

        var totalHours = 0;
        $("#assigned_users input[type='number']").each(function () {
            // alert(1);
            if ($(this).val() != "")
                totalHours += parseInt($(this).val());
        });

        //add the sum values
        $("#hour_total_value").text(totalHours + " hrs");
    });


    //add new department from plus icon in the "add new task " form
    $("form#department_form").submit(function (e) {
        e.preventDefault();
        var formData = $(this).serializeArray();
        $.ajax({
            url: '<?php echo base_url("index.php/cts_controller/insert_department_details")?>',
            type: "POST",
            data: {"department": formData},
            dataType: "json",
            success: function (insertId) {
                if (insertId == false) {
                    $("#department_alert_success").hide();
                    $("#department_alert_error").show();
                }
                else {
                    $("#department_alert_error").hide();
                    $("#department_alert_success").show();
                    $("#department").append("<option value=" + insertId + ">" + $('form#department_form input').val() + "</option>");
                }
            }
        });
    });

    //add new project from plus icon in the "add new task " form
    $("form#project_form").submit(function (e) {
        e.preventDefault();
        var formData = $(this).serializeArray();
        $.ajax({
            url: '<?php echo base_url("index.php/cts_controller/insert_project_details")?>',
            type: "POST",
            data: {"project": formData},
            dataType: "json",
            success: function (insertId) {
                if (insertId == false) {
                    $("#project_alert_success").hide();
                    $("#project_alert_error").show();
                }
                else {
                    $("#project_alert_error").hide();
                    $("#project_alert_success").show();
                    $("#project").append("<option value=" + insertId + ">" + $('form#project_form input').val() + "</option>");
                }
            }
        });
    });


    $("form#indicator_form").submit(function (e) {
        // alert('hello');
        e.preventDefault();
        var formData = $(this).serializeArray();
        $.ajax({
            url: '<?php echo base_url("index.php/cts_controller/insert_indicator_details")?>',
            type: "POST",
            data: {"indicator": formData},
            dataType: "json",
            success: function (insertId) {
                if (insertId == false) {
                    $("#indicator_alert_success").hide();
                    $("#indicator_alert_error").show();
                    // alert(1);
                }
                else 
                {
                    $("#indicator_alert_error").hide();
                    $("#indicator_alert_success").show();
                    // alert('true');
                    $("#multiple-select-indicator").append("<option value=" + insertId + ">" + $('form#indicator_form input').val() + "</option>");

                    indicatorMultiple();
                }
                // alert(2);
            }
        });
    });

    $('#project').change(function(){

        var project_id = $(this).val();

        $.ajax({
            url: "<?php echo site_url()?>/cts_controller/ajax_get_task_category_by_project/"+project_id,
            type: 'GET',
            dataType: 'json',
            success:function(response)
            {
                if (response == null || response == '') 
                {
                // console.log(response);
                $('#task_cat').html('<option value="">Select Task Category</option>');
                }
                else
                {
                    $('#task_cat').html('<option>Select Task Category</option>');
                    
                    $.each(response, function (index, item) 
                    {
                   
                        $('#task_cat').append(
                            $('<option></option>').val(index).html(item)
                         );
                    });
                }

            }
        })
    });
});


</script>
<style>
</style>
</body>
</html>
