<?php
foreach($roles_info->result_array() as $key=>$value)
{
$module[] = $value;
}
?>
<div class="container">
  <div id="img-icon">
    <img src="<?php echo base_url();?>resource/images/add-role.jpg" align="left"/>
  </div>
  <div id="breadcumb-text">
    <ol class="breadcrumb">
      <li><strong>Role</strong></li>&nbsp;&nbsp;
      <li class="vertical-divider"></li>
      <li><a href="<?php echo base_url();?>index.php/cts_role_controller/add_new_role_display">Add Department Role</a></li>
      <li class="vertical-divider"></li>
      <li class="active">Add Module Role</li>
    </ol>
  </div>
  <hr>
  <?php
if(isset($result)){
	if($result== 1)
	{
		echo "<div class='alert alert-info' role='alert' id='role-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Roles Successfully Assigned!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='role-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Role Assignment Failed!</b></div>";
	}
?>
  <table class="table table-striped" id="table_module_role">
    <form method="post" action="<?php echo base_url();?>index.php/cts_role_controller/add_module_role/<?php echo $position_id;?>">
	<thead>
        <th>Module</th>
        <th>Add</th>
        <th>Edit</th>
        <th>Delete</th>
        <th>View</th>
		</thead>
      <tbody>
        <?php 
		$i = 0;
		foreach($modules->result() as $row)
		{
		?>
		<tr>
		<td><?php echo $row->modules;?></td>
		<?php $row->modules = strtolower(str_replace(" ","_",$row->modules));?>
		<?php if(isset($module[$i]['add']) && $module[$i]['add'])
		{
		?>
		<td><input type='checkbox' class="role-check" name="<?php echo $row->modules;?>_add_role" checked value="1" /></td>
		<?php 
		}
		else
		{
		?>
		<td><input type='checkbox' class="role-check" name="<?php echo $row->modules;?>_add_role"  value="1" /></td>
		<?php } 
		if(isset($module[$i]['edit']) && $module[$i]['edit'])
		{
		?>
		<td><input type='checkbox' class="role-check" name="<?php echo $row->modules;?>_edit_role" checked  value="1" /></td>
		<?php 
		}
		else
		{
		?>
		<td><input type='checkbox' class="role-check" name="<?php echo $row->modules;?>_edit_role"  value="1" /></td>
		<?php 
		}
		if(isset($module[$i]['delete']) && $module[$i]['delete'])
		{
		?>
		<td><input type='checkbox' class="role-check" name="<?php echo $row->modules;?>_delete_role" checked  value="1" /></td>
		<?php 
		} 
		else
		{
		?>
		<td><input type='checkbox' class="role-check" name="<?php echo $row->modules;?>_delete_role"  value="1" /></td>
		<?php 
		}
		if(isset($module[$i]['view']) && $module[$i]['view'])
		{
		?>
		<td><input type='checkbox' class="role-check" name="<?php echo $row->modules;?>_view_role" checked  value="1" /></td>
		<?php 
		}
		else
		{
		?>
		<td><input type='checkbox' class="role-check" name="<?php echo $row->modules;?>_view_role"  value="1" /></td>
		<?php 
		} 
		?>
		</tr>
		<?php 
		$i++;
		} ?>
        <tr><td colspan="5"><input type="submit" class="role-check" name="submit" value="Submit" class="btn btn-primary"/></td></tr>
		</tbody>
    </form>
  </table>
<?php //} ?>
</div><!--end of container-->
</body>