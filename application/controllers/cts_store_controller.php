<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cts_store_controller extends CI_controller
{
	public function __construct()
	{
	parent::__construct();
	
		$this->load->model('cts_store_model');
		$this->load->model('cts_login');
		$this->load->model('cts_user_model');
		$this->load->model('cts_department_model');
		$this->load->model('cts_role_model');
	}
	
	
	public function index($data="")
	{
		if($this->session->userdata('is_logged_in') && in_array("store",$this->session->userdata('department')))
		{
		$data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data1['departments'] = $this->cts_department_model->getDepartment();
		$data1['new_message'] = $this->cts_login->check_new_message();
		$data1['positions'] = $this->cts_role_model->show_all_position_role();
		$data['result'] = $this->cts_store_model->show_all_store_details();
		$data['all_users'] = $this->cts_user_model->get_all_user();
		$data['all_departments'] = $this->cts_department_model->getDepartment();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data1);
		$this->load->view('store/cts_show_all_store_details',$data);
		}
		else
		{
		redirect(base_url());
		}
	}
	
	public function add_new_store($store_id)
	{
		$data['store_id'] = $store_id; 
		$this->load->view('cts_header');
		$this->load->view('store/cts_store_register', $data);
	}
	
	public function insert_store_details($store_id)
	{
		if($this->input->is_ajax_request())
		{
		$data['result'] = $this->cts_store_model->insert_store_details($store_id);
		if ($data['result'])
		{
			echo $data['result'];
		}
		else
		{
			$this->load->view('cts_registration');
		}
		}
		else
		{
		redirect(404);
		}
	}
	
	public function store_approve($claim_id)
	{
		if($this->input->is_ajax_request())
		{
			$check_value = $this->input->post('store_approve');
			$data['result'] = $this->cts_store_model->store_approve($claim_id,$check_value);
		if ($data['result'])
		{
			echo $data['result'];
		}
		else
		{
			$this->load->view('cts_registration');
		}
		}
		else
		{
		redirect(404);
		}
	}
	
	public function edit_store_by_id_form($store_id)
	{
	if($this->input->is_ajax_request())
	{
	$data['store_details'] = $this->cts_store_model->get_details_of_store_by_id($store_id);
	foreach($data['store_details'] as $row)
	{
	$store_details = array
				(
				'store_id' => $row['store_id'],
				'collection_date' => $row['collection_date'],
				'received_date' => $row['received_date'],
				'extraction_date' => $row['extraction_date'],
				'sample_submitted_by' => $row['sample_submitted_by'],
				'flock_size' => $row['flock_size'],
				'mortality' => $row['mortality'],
				'pcr_m_gene_result' => $row['pcr_m_gene'],
				'pcr_h5_result' => $row['pcr_h5'],
				'pcr_n1_result' => $row['pcr_n1'],
				'pcr_n9_result' => $row['pcr_n9'],
				'pcr_h9_result' => $row['pcr_h9'],
				'pcr_h7_result' => $row['pcr_h7'],
				'pcr_ibd_result' => $row['pcr_ibd'],
				'pcr_nd_result' => $row['pcr_nd'],
				'ct_value' => $row['ct_value'],
				'remarks' => $row['remarks'],
				'tested_date' => $row['tested_date'],
				'approved_date' => $row['approved_date'],
				'others' =>$row['others']
				);
	}
	echo json_encode($store_details);
	/* $this->load->view('cts_header');
	$this->load->view('cts_navigation');
	$this->load->view('store/cts_edit_store_by_id_form',$data); */
	}
	else
	{
	redirect(404);
	}
	}
	
	public function edit_store_by_id($store_id)
	{
	if($this->input->is_ajax_request())
	{
	$data['edit_result'] = $this->cts_store_model->edit_store_by_id($store_id);
	echo $data['edit_result'];
	}
	else
	{
	redirect(404);
	}
	}
	public function delete_store_by_id($store_id)
	{
	if($this->input->is_ajax_request())
	{
	$data['deletion_result'] = $this->cts_store_model->delete_store_by_id($store_id);
	echo $data['deletion_result'];
	}
	else
	{
	redirect(404);
	}
	}
	public function check_new_customer()
	{
	if($this->input->is_ajax_request())
	{
	$this->cts_store_model->check_new_customer();
	}
	else
	{
	redirect(404);
	}
	}
	public function set_accept_on($sample_id)
	{
	if(!$this->input->is_ajax_request())
	redirect(404);
	else
	{
	$result = $this->cts_store_model->set_accept_on($sample_id);
	echo $result;
	}
	}
	public function search_pending_sample($offset)
	{
		$query_result = $this->cts_store_model->search_pending_sample($offset);
		echo "<thead><th>Biology No.</th><th>Regd. No.</th><th>Customer Name</th><th>Firm Name</th><th>Animal</th><th>Age</th><th>Sex</th><th>Species</th><th>Species Type </th><th>Sample Type</th><th>Sample No.</th><th>Investigation Requested</th><th>Publish</th></thead>";
		foreach($query_result['result']->result() as $row)
		{
		echo "<tr><td>".$row->store_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->animal."</td><td>".$row->age."</td><td>".$row->sex."</td><td>".$row->animal_type."</td><td>".$row->species_type."</td><td>".$row->sample_type."</td><td>".$row->sample_no."</td><td>".$row->investigation_requested."</td><td><button class='btn btn-info set_publish_btn' id='' alt='".$row->store_id."'>Publish</button></td></tr>";
/* =======
		echo "<tr><td>".$row->store_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->animal."</td><td>".$row->animal_type."</td><td>".$row->species_type."</td><td>".$row->sample_type."</td><td>".$row->no_of_sample."</td><td>".$row->investigation_requested."</td><td><button class='btn btn-info set_publish_btn' id='' alt='".$row->store_id."'>Publish</button></td></tr>";
>>>>>>> .r392 */

/* 		echo "<tr><td>".$row->store_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->animal."</td><td>".$row->animal_type."</td><td>".$row->species_type."</td><td>".$row->sample_type."</td><td>".$row->no_of_sample."</td><td>".$row->investigation_requested."</td><td><button class='btn btn-info set_publish_btn' id='' alt='".$row->store_id."'>Publish</button></td></tr>"; */
		}
		echo "<tr><td colspan='13'>";
				if($offset == 0)
				echo "<span class='glyphicon glyphicon-backward'></span>&nbsp;";
				else
				echo "<a href='javascript:store_pending_pagination(".($offset-1).")'> <span class='glyphicon glyphicon-backward'></span>&nbsp;</a>";
			for($i=1;$i<=ceil($query_result['count']/6);$i++)
			{
				if($i == $offset+1)
				{
				echo $i;
				}
				else
				{
				echo "<a href='javascript:store_pending_pagination(".($i-1).")'> ".$i." </a>";
				}
			}
				if($offset == ceil($query_result['count']/6)-1)
				echo "&nbsp;<span class='glyphicon glyphicon-forward'></span>";
				else 
				echo "<a href='javascript:store_pending_pagination(".($offset+1).")'>&nbsp;<span class='glyphicon glyphicon-forward'>&nbsp;</span></a>";
			echo "</td></tr>";
		
	}
	public function set_publish_on($store_id)
	{
		if(!$this->input->is_ajax_request())
		redirect(404);
		else
		{
		$result = $this->cts_store_model->set_publish_on($store_id);
		echo $result;
		}
	}
	public function search_on_progress_sample($offset)
	{
		$result = $this->cts_login->get_row_by_position_id($this->session->userdata('position_id'));
		foreach($result->result_array() as $row)
		{
		$add_role = $row['add_role'];
		$edit_role = $row['edit_role'];
		$delete_role = $row['delete_role'];
		$report_generate_role = $row['report_generate_role'];
		}
		//$query_result = $this->cts_store_model->search_on_progress_sample($offset);
		echo "<tr style='background-color:#2395D2; color:#fff'><th rowspan='2'>Biology No.</th><th rowspan='2'>Regd. No</th><th rowspan='2'>Customer Name</th><th rowspan='2'>Firm Name</th><th rowspan='2'>Animal</th><th rowspan='2'>Age</th><th rowspan='2'>Sex</th><th rowspan='2'>Species</th><th rowspan='2'>Species Type</th><th rowspan='2'>Sample Type</th><th rowspan='2'>Sample No.</th><th  rowspan='2'>Investigation Requested</th><th rowspan='2'>Sample Submitted by</th><th rowspan='2'>Flock Size</th><th rowspan='2'>Mortality</th><th rowspan='2'>Collection Date</th><th rowspan='2'>Recived Date</th><th rowspan='2'>Extraction Date</th><th colspan='8'>PCR Result</th><th rowspan='2'>CT Value of Positive</th><th rowspan='2'>Others</th><th rowspan='2'>Remarks</th><th rowspan='2'>Tested Date</th><th rowspan='2'>Approved Date</th>";
		if($add_role ==1)
		echo "<th rowspan='2'>Add</th>";
		if($edit_role ==1)
		echo "<th rowspan='2'>Edit</th>";
		if($delete_role == 1)
		echo "<th rowspan='2'>Delete</th>";
		echo "<th rowspan='2'>Completed</th></tr>
			<tr id='store_table_row' style='background-color:#2395D2; color:#fff'><th>M Gene</th><th>H5</th><th>N1</th><th>N9</th><th>H9</th><th>H7</th><th>IBD</th><th>ND</th></tr>";
	
		foreach($query_result['result']->result() as $row)
		{
				echo "<tr><td>".$row->store_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->animal."</td><td>".$row->age."</td><td>".$row->sex."</td><td>".$row->animal_type."</td><td>".$row->species_type."</td><td>".$row->sample_type."</td><td>".$row->sample_no."</td><td>".$row->investigation_requested."</td><td>".$row->sample_submitted_by."</td><td>".$row->flock_size."</td><td>".$row->mortality."</td><td>".$row->collection_date."</td><td>".$row->received_date."</td><td>".$row->extraction_date."</td><td>".$row->pcr_m_gene."</td><td>".$row->pcr_h5."</td><td>".$row->pcr_n1."</td><td>".$row->pcr_n9."</td><td>".$row->pcr_h9."</td><td>".$row->pcr_h7."</td><td>".$row->pcr_ibd."</td><td>".$row->pcr_nd."</td><td>".$row->ct_value."</td><td>".$row->others."</td><td>".$row->remarks."</td><td>".$row->tested_date."</td><td>".$row->approved_date."</td>";
				if($add_role == 1)
				{	
					echo "<td><a href='#' class='btn btn-primary glyphicon-plus' data-toggle='modal' data-target='#register_store' alt='".$row->store_id."'><div class='glyphicon' ></div>Report Entry</a></td>";
		
					/* echo "<td><a href='#'><div class='glyphicon glyphicon-edit' data-toggle='modal' data-target='#edit_store' alt='".$row->store_id."'></div></a></td><td><a href='#'><div class='glyphicon glyphicon-trash' alt='".$row->store_id."'></div></a></td><td><button class='btn btn-success' id='set_completed_button' alt='".$row->store_id."'>Completed</button></td></tr>"; */

				}
				if($edit_role == 1)
				{
					echo "<td><a href='#' class='btn btn-primary glyphicon-edit' data-toggle='modal' data-target='#edit_store' alt='".$row->store_id."'><div class='glyphicon'></div>Report Edit</a></td>";
				}
				if($delete_role == 1)
				{
					echo "<td><a href='#' class='btn btn-primary glyphicon-trash' alt='".$row->store_id."'><div class='glyphicon'></div>Delete Report </a></td>";
				}
				echo "<td><button class='btn btn-primary set_completed_button'  alt='".$row->store_id."'>Completed</button></td></tr>";
		
		}
		echo "<tr><td colspan='33'>";
				if($offset == 0)
				echo "<span class='glyphicon glyphicon-backward'></span>&nbsp;";
				else
				echo "<a href='javascript:store_on_progess_pagination(".($offset-1).")'> <span class='glyphicon glyphicon-backward'></span>&nbsp;</a>";
			for($i=1;$i<=ceil($query_result['count']/6);$i++)
			{
				if($i == $offset+1)
				{
				echo $i;
				}
				else
				{
				echo "<a href='javascript:store_on_progess_pagination(".($i-1).")'> ".$i." </a>";
				}
			}
				if($offset == ceil($query_result['count']/6)-1)
				echo "&nbsp;<span class='glyphicon glyphicon-forward'></span>";
				else 
				echo "<a href='javascript:store_on_progess_pagination(".($offset+1).")'>&nbsp;<span class='glyphicon glyphicon-forward'>&nbsp;</span></a>";
			echo "</td></tr>";
	}
	
	public function set_completed_on($store_id)
	{
	$result = $this->cts_store_model->set_completed_on($store_id);
	echo $result;
	}
	public function search_completed_sample($offset)
	{
		$result = $this->cts_login->get_row_by_position_id($this->session->userdata('position_id'));
		foreach($result->result_array() as $row)
		{
		$report_generate_role = $row['report_generate_role'];
		$approve_report_role = $row['approve_role'];
		}
		$query_result = $this->cts_store_model->search_completed_sample($offset);

		echo "<tr style='background-color:#2395D2; color:#fff'><th rowspan='2'>Biology No</th><th rowspan='2'>Regd. No.</th><th rowspan='2'>Customer Name</th><th rowspan='2'>Firm Name</th><th rowspan='2'>Animal</th><th rowspan='2'>Age</th><th rowspan='2'>Sex</th><th rowspan='2'>Species</th><th rowspan='2'>Species Type</th><th rowspan='2'>Sample Type</th><th rowspan='2'>Sample No.</th><th rowspan='2'>Investigation Requested</th><th rowspan='2'>Sample Submitted By</th><<th rowspan='2'>Flock Size</th><th rowspan='2'>Mortality</th><th rowspan='2'>Collection Date</th><th rowspan='2'>Recived Date</th><th rowspan='2'>Extraction Date</th><th colspan='8'>PCR Result</th><th rowspan='2'>CT Value of Positive</th><th rowspan='2'>Others</th><th rowspan='2'>Remarks</th><th rowspan='2'>Tested Date</th><th rowspan='2'>Approved Date</th>".(($report_generate_role == 1)?'<th rowspan="2">Generate Report</th>':'')."<th rowspan='2'>Approved</th></thead>
		<tr style='background-color:#2395D2; color:#fff' id='store_table_row'><th>M Gene</th><th>H5</th><th>N1</th><th>N9</th><th>H9</th><th>H7</th><th>IBD</th><th>ND</th></tr>";

		foreach($query_result['result']->result() as $row)
		{
		echo "<tr><td>".$row->store_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->animal."</td><td>".$row->age."</td><td>".$row->sex."</td><td>".$row->animal_type."</td><td>".$row->species_type."</td><td>".$row->sample_type."</td><td>".$row->sample_no."</td><td>".$row->investigation_requested."</td><td>".$row->sample_submitted_by."</td><td>".$row->flock_size."</td><td>".$row->mortality."</td><td>".$row->collection_date."</td><td>".$row->received_date."</td><td>".$row->extraction_date."</td><td>".$row->pcr_m_gene."</td><td>".$row->pcr_h5."</td><td>".$row->pcr_n1."</td><td>".$row->pcr_n9."</td><td>".$row->pcr_h9."</td><td>".$row->pcr_h7."</td><td>".$row->pcr_ibd."</td><td>".$row->pcr_nd."</td><td>".$row->ct_value."</td><td>".$row->others."</td><td>".$row->remarks."</td><td>".$row->tested_date."</td><td>".$row->approved_date."</td>";
		if($report_generate_role == 1 && $row->registered == 5 && $row->publish_bit == 1)
		{
		echo "<td><a href='".base_url()."index.php/cts_store_controller/generate_report/".$row->sample_id."' target='_blank'><button class='btn btn-primary' >Generate Report</button></a></td>";
		}
		elseif($report_generate_role !=1)
		{
		echo "";
		}
		else
			{
			echo "<td>Report Pending</td>";
			}
			if($row->registered != 5 && $approve_report_role == 1)
			{
				echo "<td><button class='btn btn-primary set_approved_btn'  data-toggle='modal' data-target='#approve_store' id='' alt='".$row->store_id."'>Approve</button></td>";
			}
			elseif($row->registered == 5)
			{
			echo "<td><div class='glyphicon glyphicon-check'></div></td>"; 
			}
			else
			{
			echo "<td>&nbsp;</td>";
			}
			echo "</tr>";
		}
		echo "<tr><td colspan='32'>";
				if($offset == 0)
				echo "<span class='glyphicon glyphicon-backward'></span>&nbsp;";
				else
				echo "<a href='javascript:store_completed_pagination(".($offset-1).")'> <span class='glyphicon glyphicon-backward'></span>&nbsp;</a>";
			for($i=1;$i<=ceil($query_result['count']/6);$i++)
			{
				if($i == $offset+1)
				{
				echo $i;
				}
				else
				{
				echo "<a href='javascript:store_completed_pagination(".($i-1).")'> ".$i." </a>";
				}
			}
				if($offset == ceil($query_result['count']/6)-1)
				echo "&nbsp;<span class='glyphicon glyphicon-forward'></span>";
				else 
				echo "<a href='javascript:store_completed_pagination(".($offset+1).")'>&nbsp;<span class='glyphicon glyphicon-forward'>&nbsp;</span></a>";
			echo "</td></tr>";
		}
	public function generate_report($sample_id)
	{
	$data['result'] = $this->cts_store_model->generate_report($sample_id);
	$this->load->view('cts_header');
	$this->load->view('report/generate_store_report',$data);
	}
	public function generate_entry_report($customer_id)
	{
		$data['result'] = $this->cts_store_model->generate_entry_report($customer_id);
		$this->load->view('cts_header');
		$this->load->view('report/generate_store_report',$data);
	}
}