<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cts_approve_recommend_process_controller extends CI_controller {
	public function __construct()
	{
	parent::__construct();
	$this->load->model('cts_approve_recommend_model');
	$this->load->model('cts_login');
	$this->load->model('cts_user_model');
	$this->load->model('cts_department_model');
	$this->load->model('cts_role_model');
	}
	
	public function index($data= "")
	{
	
	if($this->session->userdata('is_logged_in') && in_array("approve/recommend process",$this->session->userdata('department')))
	{
	
	$data1['departments'] = $this->cts_department_model->getDepartment();
	$data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data1['positions'] = $this->cts_role_model->show_all_position_role();
	$data1['user_detail'] = $this->cts_user_model->get_user_details($this->session->userdata('user_id'));
	$data1['access_users'] = $this->cts_user_model->get_all_user();
	$data1['approve_amount'] = $this->cts_approve_recommend_model->get_all_approval_amount();
	$data['result'] = $this->cts_approve_recommend_model->show_all_approve_recommend_details($data1['user_detail']);
	$data1['new_message'] = $this->cts_login->check_new_message();
	$data['all_users'] = $this->cts_user_model->get_all_user();
	$data['all_departments'] = $this->cts_department_model->getDepartment();
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data1);
	$this->load->view('approve_recommend/cts_show_all_approve_recommend_details',$data);
	$this->load->view('cts_footer');
	}
	else
	{
	redirect(base_url());
	}
	}
	public function add_approve_recommend_details ($approve_recommend_id)
	{
		$data['approve_recommend_id'] = $approve_recommend_id;
		$this->cts_approve_recommend_model->insert_approve_recommend_details($approve_recommend_id);
		$this->load->view('cts_header');
		$this->load->view('approve_recommend/cts_approve_recommend_register', $data);
	}
	public function insure_send_email(){
	
		if($this->input->is_ajax_request())
		{
		
			$data['result'] = $this->cts_approve_recommend_model->insure_send_email();
			if ($data['result'])
				{
					echo $data['result'];
				}
	
	}
		else
			{
			redirect(404);
			}
	
	}
	public function insert_approve_recommend($approve_recommend_id)
	{
	if($this->input->is_ajax_request())
	{
		
		$data['result'] = $this->cts_approve_recommend_model->insert_approve_recommend_details($approve_recommend_id);
		if ($data['result'])
		{
		echo "1";
		}
	else
	{
	//$this->load->view('cts_registration');
	}
	}
	else
	{
	redirect(404);
	}
	}
	
	public function cheque_received($approve_recommend_id)
	{
	if($this->input->is_ajax_request())
	{
		$cn_status = $this->input->post('cheque_received');
		$data['result'] = $this->cts_approve_recommend_model->cheque_received($approve_recommend_id,$cn_status);
		if ($data['result'])
		{
		echo $data['result'];
		}
	else
	{
	//$this->load->view('cts_registration');
	}
	}
	else
	{
	redirect(404);
	}
	}
	
	 public function show_all_approve_recommend_details()
	{
	/* 	if($this->session->userdata('is_logged_in')) 
		{
		$data['result'] = $this->cts_approve_recommend_model->show_all_approve_recommend_details();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation');
		$this->load->view('approve_recommend/cts_show_all_approve_recommend_details',$data);
		}
		else
		{
			$this->load->view("cts_header");
			$this->load->view('cts_login');
		} */
	}
	
	public function edit_approve_recommend_by_id_form($approve_recommend_id)
	{
		if(!$this->input->is_ajax_request())
		{
			redirect(404);
		}
		else
		{
			$data['approve_recommend_details'] = $this->cts_approve_recommend_model->get_details_of_approve_recommend_by_id($approve_recommend_id);

			foreach($data['approve_recommend_details'] as $row)
			{
				$approve_recommend_details = array
				(
				'approve_recommend_id' => $row['approve_recommend_id'],
				'egg_inoculation' => $row['egg_inoculation'],
				'pa_result' => $row['plate_aggluination'],
				'rapid_anitgen_test' => $row['rapid_anitgen_test'],
				'ha' => $row['ha'],
				'hi' => $row['hi'],
				'penside_test' => $row['penside_test'],
				'negri_body' => $row['negri_body'],
				'cft_result' => $row['cft'],
				'agid_result' => $row['agid'],
				'elisa_result' => $row['elisa'],
				'cd_result' => $row['confirmative_diagnosis'],
				'fat' => $row['fat']
				);
			}
			echo json_encode($approve_recommend_details);
		}
	}
	
	public function delete_approve_recommend_by_id($approve_recommend_id)
	{
		if($this->input->is_ajax_request())
		{
		$data['deletion_result'] = $this->cts_approve_recommend_model->delete_approve_recommend_by_id($approve_recommend_id);
		echo $data['deletion_result'];
		}
		else
		{
		redirect(404);
		}
	}
	
	public function edit_approve_recommend_by_id($approve_recommend_id)
	{
		if($this->input->is_ajax_request())
		{
		$data['edit_result'] = $this->cts_approve_recommend_model->edit_approve_recommend_by_id($approve_recommend_id);
		echo $data['edit_result'];
		}
		else
		{
		redirect(404);
		}
	}
	
	public function check_new_customer()
	{
	if($this->input->is_ajax_request())
	{
	$this->cts_approve_recommend_model->check_new_customer();
	}
	else
	{
	redirect(404);
	}
	}
	
	public function set_accept_on($approve_recommend_id)
	{
	if(!$this->input->is_ajax_request())
	redirect(404);
	else
	{
	$result = $this->cts_approve_recommend_model->set_accept_on($approve_recommend_id);
	echo $result;
	}
	}
	public function search_pending_claim($offset)
	{
		$query_result = $this->cts_approve_recommend_model->search_pending_claim($offset);
		echo "<thead><th>Virology No.</th><th>Regd No.</th><th>Customer Name</th><th>Firm Name</th><th>Animal</th><th>Age</th><th>Sex</th><th>Species</th><th>Species Type</th><th>Sample Type</th><th>Sample No.</th> <th>Investigation Requested<th>Publish</th></thead>";
		foreach($query_result['result']->result() as $row)
		{
/* <<<<<<< .mine
		echo "<tr><td>".$row->approve_recommend_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->animal."</td><td>".$row->animal_type."</td><td>".$row->species_type."</td><td>".$row->claim_type."</td><td>".$row->no_of_claim."</td><td>".$row->investigation_requested."</td><td><button class='btn btn-info set_publish_btn' id='' alt='".$row->approve_recommend_id."'>Publish</button></td></tr>";
======= */
		echo "<tr><td>".$row->approve_recommend_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->animal."</td><td>".$row->age."</td><td>".$row->sex."</td><td>".$row->animal_type."</td><td>".$row->species_type."</td><td>".$row->claim_type."</td><td>".$row->claim_no."</td><td>".$row->investigation_requested."</td><td><button class='btn btn-info set_publish_btn' id='' alt='".$row->approve_recommend_id."'>Publish</button></td></tr>";
		}
		echo "<tr><td colspan='13'>";
				if($offset == 0)
				echo "<span class='glyphicon glyphicon-backward'></span>&nbsp;";
				else
				echo "<a href='javascript:approve_recommend_pending_pagination(".($offset-1).")'> <span class='glyphicon glyphicon-backward'></span>&nbsp;</a>";
			for($i=1;$i<=ceil($query_result['count']/6);$i++)
			{
				if($i == $offset+1)
				{
				echo $i;
				}
				else
				{
				echo "<a href='javascript:approve_recommend_pending_pagination(".($i-1).")'> ".$i." </a>";
				}
			}
				if($offset == ceil($query_result['count']/6)-1)
				echo "&nbsp;<span class='glyphicon glyphicon-forward'></span>";
				else 
				echo "<a href='javascript:approve_recommend_pending_pagination(".($offset+1).")'>&nbsp;<span class='glyphicon glyphicon-forward'>&nbsp;</span></a>";
			echo "</td></tr>";
	}
	public function set_publish_on($approve_recommend_id)
	{
		if(!$this->input->is_ajax_request())
		redirect(404);
		else
		{
		$result = $this->cts_approve_recommend_model->set_publish_on($approve_recommend_id);
		echo $result;
		}
	}
	public function search_on_progress_claim($offset)
	{
		$result = $this->cts_login->get_row_by_position_id($this->session->userdata('position_id'));
		foreach($result->result_array() as $row)
		{
		$add_role = $row['add_role'];
		$edit_role = $row['edit_role'];
		$delete_role = $row['delete_role'];
		$report_generate_role = $row['report_generate_role'];
		}
		$query_result = $this->cts_approve_recommend_model->search_on_progress_claim($offset);

		echo "<thead><th>Virology No.</th><th>Regd No.</th><th>Customer Name</th><th>Firm Name</th><th>Animal</th><th>Age</th><th>Sex</th><th>Species</th><th>Species Type</th><th>Sample Type</th><th>Sample No.</th><th>Investigation Requested</th><th>Egg Inoculation</th><th>Plate Aggluination</th><th>Rapid Anitgen Test </th><th>HA</th><th>HI</th><th>Penside Test for PPR</th><th>Negri Body Test</th><th>CFT</th><th>AGID</th><th>ELISA </th><th>Confirmative Diagnosis</th><th>FAT</th>".(($add_role == 1)?'<th>Add</th>':'').(($edit_role == 1)?'<th>Edit</th>':'').(($delete_role == 1)?'<th>Delete</th>':'')."<th>Completed</th></thead>";
		foreach($query_result['result']->result() as $row)
		{
		$egg_inoculation=$row->egg_inoculation;
		echo "<tr><td>".$row->approve_recommend_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->animal."</td><td>".$row->age."</td><td>".$row->sex."</td><td>".$row->animal_type."</td><td>".$row->species_type."</td><td>".$row->claim_type."</td><td>".$row->claim_no."</td><td>".$row->investigation_requested."</td>";
		if($egg_inoculation == 1){
		echo "<td><div class='glyphicon glyphicon-ok'></div></td>";
		
		}
		else {
		echo "<td></td>";
		
		}
		
		
		echo "<td>".$row->plate_aggluination."</td><td>".$row->rapid_anitgen_test."</td><td>".$row->ha."</td><td>".$row->hi."</td><td>".$row->penside_test."</td><td>".$row->negri_body."</td><td>".$row->cft."</td><td>".$row->agid."</td><td>".$row->elisa."</td><td>".$row->confirmative_diagnosis."</td><td>".$row->fat."</td>";
		if($add_role == 1)
		{
			echo "<td><a href='#' class='btn btn-primary glyphicon-plus' data-toggle='modal' data-target='#register_approve_recommend' alt='".$row->approve_recommend_id."'><div class='glyphicon'></div>Report Entry</a></td>";
		}
		if($edit_role == 1)
		{
		echo "<td><a href='#' class='btn btn-primary glyphicon-edit' data-toggle='modal' data-target='#edit_approve_recommend' alt='".$row->approve_recommend_id."'><div class='glyphicon' ></div>Report Edit</a></td>"; 
		}
		if($delete_role == 1)
		{
		echo "<td><a href='#' class='btn btn-primary glyphicon-trash' alt='".$row->approve_recommend_id."'><div class='glyphicon'></div>Delete Report</a></td>";
		}
		echo "<td><button class='btn btn-primary set_completed_button' id='' alt='".$row->approve_recommend_id."'>Completed</button></td></tr>";
		}
		echo "<tr><td colspan='26'>";
				if($offset == 0)
				echo "<span class='glyphicon glyphicon-backward'></span>&nbsp;";
				else
				echo "<a href='javascript:approve_recommend_pagination(".($offset-1).")'> <span class='glyphicon glyphicon-backward'></span>&nbsp;</a>";
			for($i=1;$i<=ceil($query_result['count']/6);$i++)
			{
				if($i == $offset+1)
				{
				echo $i;
				}
				else
				{
				echo "<a href='javascript:approve_recommend_pagination(".($i-1).")'> ".$i." </a>";
				}
			}
				if($offset == ceil($query_result['count']/6)-1)
				echo "&nbsp;<span class='glyphicon glyphicon-forward'></span>";
				else 
				echo "<a href='javascript:approve_recommend_pagination(".($offset+1).")'>&nbsp;<span class='glyphicon glyphicon-forward'>&nbsp;</span></a>";
			echo "</td></tr>";
	}
	public function set_completed_on($approve_recommend_id)
	{
	$result = $this->cts_approve_recommend_model->set_completed_on($approve_recommend_id);
	echo $result;
	}
	public function search_completed_claim($offset)
	{
		$result = $this->cts_login->get_row_by_position_id($this->session->userdata('position_id'));
		foreach($result->result_array() as $row)
		{
		$report_generate_role = $row['report_generate_role'];
		$approve_report_role = $row['approve_role'];
		}
		$query_result = $this->cts_approve_recommend_model->search_completed_claim($offset);
		echo "<thead><th>Virology No.</th><th>Regd No.</th><th>Customer Name</th><th>Firm Name</th><th>Animal</th><th>Age</th><th>Sex</th><th>Species</th><th>Species Type</th><th>Sample Type</th><th>Sample No.</th><th>Investigation Requested</th><th>Egg Inoculation</th><th>Plate Aggluination</th><th>Rapid Anitgen Test </th><th>HA</th><th>HI</th><th>Penside Test for PPR</th><th>Negri Body Test</th><th>CFT</th><th>AGID</th><th>Confirmative Diagnosis</th><th>FAT</th><th>Approve Remark</th>".(($report_generate_role == 1)?'<th>Generate Report</th>':'<th>Generate</th>')."<th>Approved</th></thead>";
		foreach($query_result['result']->result() as $row)
		{
			echo "<tr><td>".$row->approve_recommend_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->animal."</td><td>".$row->age."</td><td>".$row->sex."</td><td>".$row->animal_type."</td><td>".$row->species_type."</td><td>".$row->claim_type."</td><td>".$row->claim_no."</td><td>".$row->investigation_requested."</td><td>".$row->egg_inoculation."</td><td>".$row->plate_aggluination."</td><td>".$row->rapid_anitgen_test."</td><td>".$row->ha."</td><td>".$row->hi."</td><td>".$row->penside_test."</td><td>".$row->negri_body."</td><td>".$row->cft."</td><td>".$row->agid."</td><td>".$row->confirmative_diagnosis."</td><td>".$row->fat."</td><td>".$row->approved_remarks."</td>";
			if($report_generate_role == 1 && $row->registered == 5 && $row->publish_bit == 1)
			{
			echo "<td><a href='".base_url()."index.php/cts_approve_recommend_controller/generate_report/".$row->approve_recommend_id."' target='_blank'><button class='btn btn-primary'>Generate Report</button></a></td>";
			}
			else
			{
			echo "<td>Report Pending</td>";
			}
			if($row->registered != 5 && $approve_report_role == 1)
			{
			echo "<td><button class='btn btn-primary set_approved_btn'  data-toggle='modal' data-target='#approve_approve_recommend' id='' alt='".$row->approve_recommend_id."'>Approve</button></td></tr>";
			}
			else if($row->registered == 5)
			{
			echo "<td><div class='glyphicon glyphicon-check'></div></td>"; 
			}else
			{
			echo "<td>&nbsp;</td>";
			}
		}
		echo "<tr><td colspan='24'>";
				if($offset == 0)
				echo "<span class='glyphicon glyphicon-backward'></span>&nbsp;";
				else
				echo "<a href='javascript:approve_recommend_completed_pagination(".($offset-1).")'> <span class='glyphicon glyphicon-backward'></span>&nbsp;</a>";
			for($i=1;$i<=ceil($query_result['count']/6);$i++)
			{
				if($i == $offset+1)
				{
				echo $i;
				}
				else
				{
				echo "<a href='javascript:approve_recommend_completed_pagination(".($i-1).")'> ".$i." </a>";
				}
			}
				if($offset == ceil($query_result['count']/6)-1)
				echo "&nbsp;<span class='glyphicon glyphicon-forward'></span>";
				else 
				echo "<a href='javascript:approve_recommend_completed_pagination(".($offset+1).")'>&nbsp;<span class='glyphicon glyphicon-forward'>&nbsp;</span></a>";
			echo "</td></tr>";
	}
	public function generate_report($approve_recommend_id)
	{
	$data['result'] = $this->cts_approve_recommend_model->generate_report($approve_recommend_id);
	$this->load->view('cts_header');
	$this->load->view('report/generate_approve_recommend_report',$data);
	}
}