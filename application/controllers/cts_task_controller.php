<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * controller that deals with business logic of task
 * */

class cts_task_controller extends CI_controller
{
    public function __construct()
    {
        parent::__construct();

        //load required models
        $this->load->model('cts_department_model');
        $this->load->model('cts_project_model');
        $this->load->model('cts_task_registration_model');
        $this->load->model('cts_indicator_model');
        $this->load->model('cts_user_model');
        $this->load->model('cts_login');
        $this->load->model('cts_department_model');
        $this->load->model('cts_role_model');
        $this->load->model('cts_task_model');
        $this->load->model('cts_notification_model');
    }

    /*
     * index method for CTS
     * @params:
     * @return: registration view
     * */
    public function index($success = NULL)
    {
//         $datetime1 = new DateTime('2009-10-11');
// $datetime2 = new DateTime('2009-10-13');
// $interval = $datetime1->diff($datetime2);
// echo $interval->format('%R%a');die;
        
        //get data from different tables and assign it to data array
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));

        $data['new_message'] = $this->cts_login->check_new_message();
        $i = 0;

        //set roles to array
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $i++;
        }

        //check if user is logged in and send datas top view
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('position_id') == "5" || $this->session->userdata('position_id') == "2" || $this->session->userdata('position_id') == "1" || $this->session->userdata('position_id') == "4") {
            $data['positions'] = $this->cts_role_model->show_all_position_role();

            $data['departments'] = $this->cts_department_model->getDepartment();
            $data['indicators'] = $this->cts_indicator_model->get_indicator_with_type();
            $data['users'] = $this->cts_user_model->get_all_user();
            $projects = $this->cts_project_model->show_all_project(10000, 0);
            $data['projects'] = $projects->result_array();
            $data['regd_no'] = $this->cts_task_registration_model->get_max_count();
            // echo "<pre>";
            $labno = 0;
// print_r($data);die;
            foreach ($data['regd_no']->result() as $row) {
                $regd_no = $row->regd_no;
            }
            if(isset($success) && ($success != NULL)){
                $data['success'] = $success;
            }
            // echo "<pre>";
            $data['regd_no'] = $regd_no;
            // print_r($data['users']->result_array());die;
            // echo "<pre>";
            // print_r($data);die;
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('taskregistration/cts_task_registration', $data);
            $this->load->view("cts_footer");

        } else {
            redirect(base_url());
        }
    }

    /*
     * Insert Form Data from task registration form
     * @params :
     * @return : form view
     * */
    public function insert_task_registration_details($task_id = NULL)
    {
       
        // echo $task_id;die;
        // echo "<pre>";
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['departments'] = $this->cts_department_model->getDepartment();
        $data['new_message'] = $this->cts_login->check_new_message();
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('position_id') == 5 || $this->session->userdata('position_id') == 1 || $role[4]['add'] || $this->session->userdata('position_id') == "4") 
        {
            $data['positions'] = $this->cts_role_model->show_all_position_role();
            $data['department_list'] = $this->cts_department_model->getDepartment();
            $data['departments'] = $this->cts_department_model->getDepartment();
            $data['indicators'] = $this->cts_indicator_model->getIndicators();
            $data['users'] = $this->cts_user_model->get_all_user();
            $projects = $this->cts_project_model->show_all_project(10000, 0);
            $data['projects'] = $projects->result_array();
            // $data['investigation_requested'] = $this->cts_investigation_model->getInvestigationRequested();
            $data['result'] = $this->cts_task_registration_model->insert_task_registration_details($task_id);
            $data['regd_no'] = $this->cts_task_registration_model->get_max_count();
            $regd_no = 0;

            foreach ($data['regd_no']->result() as $row) {
                $regd_no = $row->regd_no;
            }
            $data['regd_no'] = $regd_no;

            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            if (isset($task_id) && $task_id != NULL)
            {
                redirect(base_url() . "index.php/cts_task_controller/show_all_task");
            }
            else
            {
                redirect(base_url() . "index.php/cts_task_controller/index/ok");
            }
            // $this->load->view("cts_footer");

        } 
        else {
            redirect(base_url());
        }
    }


    /*
     * Get all task from task registration table
     * @return : view
     * */
    public function show_all_task($task = '')
    {
        // echo 'hello';die;
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['new_message'] = $this->cts_login->check_new_message();
        $data['departments'] = $this->cts_department_model->getDepartment();

        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $i++;
        }

        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" || $this->session->userdata('user_position') == "Admin" || $this->session->userdata('user_position') == "Manager" || $role[4]['edit'] || $role[4]['delete'] || $this->session->userdata('position_id') == "4") {

            $data['positions'] = $this->cts_role_model->show_all_position_role();
            $data["results"] = $this->cts_task_registration_model->show_all_task();
            // echo "<pre>";
            // print_r($data["results"]);die;
            $taskIndicator = array();
            $taskMember = array();

            //convert json params from database into array and get the indicator name and weightage to parse into show_all_task_registration view
            foreach ($data["results"]->result() as $resultKey => $result) {

                foreach (json_decode($result->indicators, true) as $key => $indicator) {

                    $taskIndicator[$resultKey][$key]["indicator"] = $this->cts_indicator_model->get_details_of_indicator_by_id($key);
                    $taskIndicator[$resultKey][$key]["weightage"] = $indicator;
                }
            }

            //convert json params from database into array and get the members name and hours assigned to parse into show_all_task_registration view
            foreach ($data["results"]->result() as $resultKey => $result) {

                foreach (json_decode($result->members, true) as $key => $member) {

                    $taskMember[$resultKey][$key]["member"] = $this->cts_user_model->get_user_details($key);
                    $taskMember[$resultKey][$key]["hour"] = $member;
                }
            }

            $data["indicators"] = $taskIndicator;

            $data["members"] = $taskMember;

            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('taskregistration/cts_show_all_task_registration', $data);
            $this->load->view("cts_footer");

        } else {
            redirect(base_url());
        }
    }

    /*
     * Get all task related to user
     * @return : view
     * */
    public function showTask($task = '')
    {
        // echo 'hello';die;
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['new_message'] = $this->cts_login->check_new_message();
        $data['departments'] = $this->cts_department_model->getDepartment();

        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $i++;
        }

        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" || $this->session->userdata('user_position') == "Admin" || $this->session->userdata('user_position') == "Manager" || $role[4]['edit'] || $role[4]['delete'] || $this->session->userdata('position_id') == "4") 
        {

            $data['positions'] = $this->cts_role_model->show_all_position_role();
            $data["results"] = $this->cts_task_registration_model->show_all_task();
            // echo "<pre>";
            // print_r($data["results"]);die;
            $taskIndicator = array();
            $taskMember = array();

            //convert json params from database into array and get the indicator name and weightage to parse into show_all_task_registration view
            foreach ($data["results"]->result() as $resultKey => $result) {

                foreach (json_decode($result->indicators, true) as $key => $indicator) {

                    $taskIndicator[$resultKey][$key]["indicator"] = $this->cts_indicator_model->get_details_of_indicator_by_id($key);
                    $taskIndicator[$resultKey][$key]["weightage"] = $indicator;
                }
            }

            //convert json params from database into array and get the members name and hours assigned to parse into show_all_task_registration view
            foreach ($data["results"]->result() as $resultKey => $result) {

                foreach (json_decode($result->members, true) as $key => $member) {

                    $taskMember[$resultKey][$key]["member"] = $this->cts_user_model->get_user_details($key);
                    $taskMember[$resultKey][$key]["hour"] = $member;
                }
            }

            $data["indicators"] = $taskIndicator;

            $data["members"] = $taskMember;

            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('taskregistration/cts_list_of_task_for_user', $data);
            $this->load->view("cts_footer");
            
        } 
        else 
        {
            redirect(base_url());
        }
    }

    public function insert_comment_task($task_id = '')
    {
        // echo "<pre>";
        // echo $task_id;
        $data = $this->cts_notification_model->insert_comment_task($task_id);
        unset($_POST);
        $this->update_task_completion($task_id);
        
        // print_r($_POST);
        // die;
    }


    public function update_task_completion($task_id = '', $check_type = '')
    {
        // print_r($this->session->userdata('user_position'));die;
        // $this->session->userdata('is_logged_in') 
         // $this->session->userdata('user_position')
        if($_POST)
        {
        // echo "<pre>";
        // print_r($_POST);
        // die;
            if($check_type == 'task_status')
            {
                $indicators = $this->db->query("SELECT indicators FROM task_registration where task_id = $task_id")->result_array();
                $data['indicators'] = $indicators[0]['indicators'];
                $data['achieved'] = json_encode($_POST['achieved']);
                $count_indicator_data = count($_POST['achieved']);
                $sum_indicator_data = array_sum($_POST['achieved']);
                $sum_by_count = $sum_indicator_data/$count_indicator_data;
                if($sum_by_count == 100)
                {
                    $data['status'] = "2";
                }
                else
                {
                    $data["status"] = "1";
                }
                $data['percent_complete'] = $this->cts_task_registration_model->calculate_percent($data['indicators'],$data['achieved']);
            }

            if($check_type == 'hour_status')
            { 
                // print_r($_POST);
                $data['completed_hour'] = json_encode($_POST['completed_hour']);
                // echo "<pre>";
                // print_r($completed_hour);
                // die;
                // echo "this is hour update page";die;
            }
            // echo $sum_indicator_data.'/'.$count_indicator_data.'/'.$sum_by_count;die;
            $data["updated_at"] = date("Y-m-d H:i:s");
            $data['updated_by'] = $this->session->userdata['email'];
            // print_r($data);die;
            $this->db->where('task_id', $task_id);
            if($this->db->update('task_registration', $data))
            {
                $data['success'] = 'ok';
            }
        }

        $task_detail = $this->cts_task_registration_model->update_task_completion($task_id);
        // print_r($task_detail);die;
        $data['task_id'] = $task_id;
        if(!empty($task_detail))
        {
            // print_r($task_detail);die;

            $comment_detail = $this->db->query("SELECT * FROM comment WHERE task_id = '$task_id'")->result_array();
            // print_r($comment_detail);die;
            $data['comment_detail'] = $comment_detail;
            $data['task_name'] = $task_detail[0]['task_name'];
            $project_id = $task_detail[0]['project'];
            $project_detail = $this->db->query("SELECT project_name FROM project where project_id = $project_id")->result_array();
            $data['project_name'] = $project_detail[0]['project_name'];

            if($task_detail[0]['achieved'] != '')
            {
                $indicator_achieved = json_decode($task_detail[0]['achieved'],true);   
            }


            if($task_detail[0]['indicators'] != '')
            {
                $indicator_task = json_decode($task_detail[0]['indicators'],true);   
                foreach ($indicator_task as $key => $value) 
                {
                    # code...
                    $indicator_name = $this->db->query("SELECT indicator_name FROM indicators where indicator_id = '$key'")->result_array();
                    $data['task_detail'][$key]['indicator_id'] = $indicator_name[0]['indicator_name'];
                    $data['task_detail'][$key]['weightage'] = $value;
                    if(isset($indicator_achieved[$key]) && $indicator_achieved[$key] != '')
                    {
                    // print_r($key);die;
                        $data['task_detail'][$key]['achieved'] = $indicator_achieved[$key];
                    }
                    else
                    {
                        $data['task_detail'][$key]['achieved'] = '';
                    }
                }
            }
            else
            {
                $data['error_message']['message'] = 'Indicator not available';
            }

                // echo "<pre>";
// print_r($task_detail);die;
            if($task_detail[0]['members'] != '')
            {
                if($task_detail[0]['completed_hour'] != '')
                {
                    $completed_hour = json_decode(($task_detail[0]['completed_hour']),true); 
                }
                // print_r($completed_hour);die;               }
                $member = json_decode(($task_detail[0]['members']),true);
                foreach ($member as $key => $value) 
                {
                    $member_name = $this->db->query("SELECT user_name,hourly_salary FROM user WHERE user_id = '$key'")->result_array();
                    // print_r($member_name);die;
                    $data['member'][$key]['assigned_hour'] = $value;
                    $data['member'][$key]['member_name'] = $member_name[0]['user_name'];
                    $data['member'][$key]['hourly_salary'] = $member_name[0]['hourly_salary'];
                // print_r($completed_hour);
                    if(isset($completed_hour[$key]))
                    {
                        $data['member'][$key]['completed_hour'] = $completed_hour[$key];
                    }
                    else
                        $data['member'][$key]['completed_hour'] = '';
                    # code...
                }
                // die;
            // print_r($data);die;
            }

        }
        else
        {
            // echo "i am here";die;
            $data['error_message']['message'] = 'Task not available';
        }

        
        // print_r($indicator_task);die;
        // print_r($data);die;
        // $data['task_detail'] = 
        // echo "<pre>";
        // print_r($data);die;
        $this->load->view('cts_header');
        $this->load->view('cts_navigation', $data);
        $this->load->view('taskregistration/cts_update_task_completion', $data);
        $this->load->view("cts_footer");
        // echo "update ";die;
    }

    public function delete_the_task($task_id)
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data['positions'] = $this->cts_role_model->show_all_position_role();
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[2]['delete']) {
            $data['result_indicator'] = $this->cts_task_model->delete_task_by_id($task_id);
            $this->show_all_task($data['result_indicator']);
            // $this->load->view("cts_footer");
        } 
        else 
        {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }

    /*
     * Get all task from task registration table by project id
     * @return : json
     * */
    public function get_all_task_by_project_id()
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['new_message'] = $this->cts_login->check_new_message();
        $data['departments'] = $this->cts_department_model->getDepartment();

        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $i++;
        }

        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" || $this->session->userdata('user_position') == "Admin" || $this->session->userdata('user_position') == "Manager" || $role[4]['edit'] || $role[4]['delete'] || $this->session->userdata('position_id') == "4") {

            $data['positions'] = $this->cts_role_model->show_all_position_role();
            if ($this->input->is_ajax_request()) {
                $result = $this->cts_task_registration_model->get_all_task_by_project_id($this->input->post("project"));
                echo json_encode($result);
            }

        } else {
            redirect(base_url());
        }
    }


    public function get_all_task_category_by_project_id()
    {
        // echo "hello";die;
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['new_message'] = $this->cts_login->check_new_message();
        $data['departments'] = $this->cts_department_model->getDepartment();

        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $i++;
        }

        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" || $this->session->userdata('user_position') == "Admin" || $this->session->userdata('user_position') == "Manager" || $role[4]['edit'] || $role[4]['delete'] || $this->session->userdata('position_id') == "4") {

            $data['positions'] = $this->cts_role_model->show_all_position_role();
            if ($this->input->is_ajax_request()) {
                $result = $this->cts_task_registration_model->get_all_task_category_by_project_id($this->input->post("project"));
                // print_r($result);die;
                echo json_encode($result);
            }

        } else {
            redirect(base_url());
        }
    }

    /*
     * edit the task with the respective task id
     * @param : task_id
     * @return : task view
     * */
    public function edit_task_by_id_form($task_id)
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['departments'] = $this->cts_department_model->getDepartment();
        $data['new_message'] = $this->cts_login->check_new_message();

        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('position_id') == "1" || $this->session->userdata('position_id') == "2" || $role[4]['edit'] == 1 || $this->session->userdata('position_id') == "3") {
            //$data['department_list'] = $this->cts_department_model->getDepartment();//

            $data['task_detail'] = $this->cts_task_registration_model->get_details_of_task_by_id($task_id);
            $data['departments'] = $this->cts_department_model->getDepartment();
            $data['indicators'] = $this->cts_indicator_model->get_indicator_with_type();
            $data['users'] = $this->cts_user_model->get_all_user();
            $projects = $this->cts_project_model->show_all_project(10000, 0);
            $data['projects'] = $projects->result_array();
            $data["edit_id"] = $task_id;
// echo "hel";die;
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);

            $this->load->view('taskregistration/cts_task_registration', $data);

        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }

    public function edit_task_by_id($task_id)
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['departments'] = $this->cts_department_model->getDepartment();

        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('position_id') == 2 || $this->session->userdata('position_id') == 1 || $this->session->userdata('position_id') == 3) {

            $data['result'] = $this->cts_task_registration_model->edit_task_by_id($task_id);

            /* $this->cts_task_registration_model->edit_the_department_of_task_registered($task_id); */

            redirect(base_url() . 'index.php/cts_task_controller/show_all_task');

        } else {
            redirect(base_url());
            /* $this->load->view("cts_header");
            $this->load->view('cts_login'); */
        }
    }

    public function delete_task_by_id($task_id)
    {
        $data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $data['departments'] = $this->cts_department_model->getDepartment();
        $i = 0;
        foreach ($data['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $i++;
        }
        if ($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" || $this->session->userdata('user_position') == "Admin" || $role[4]['delete'] == 1 || $this->session->userdata('position_id') == "4") {
            $data['result'] = $this->cts_task_registration_model->delete_task_by_id($task_id);
            redirect(base_url() . 'index.php/cts_task_controller/show_all_task');
        } else {
            redirect(base_url());
        }
    }

 /*
     * edit the task with the respective task id
     * @param : task_id
     * @return : task view
     * */
    public function getTodayMeeting()
    {
        // echo "<pre>";
        $date = date('Y-m-d');
        // echo $date;die;
        if(strtolower($this->session->userdata['user_position']) == 'admin')
        {
            $data['meeting_records'] = $this->db->query("SELECT * FROM notification left join task_registration on notification.task_id = task_registration.task_id where notification.deadline_date = '$date' group by notification.task_id")->result_array();
        }
        else
        {
            $user_id = $this->session->userdata['user_id'];
            $data['meeting_records'] = $this->db->query("SELECT * FROM notification left join task_registration on notification.task_id = task_registration.task_id where notification.deadline_date = '$date' AND notification.user_id = $user_id")->result_array();

        }
        // print_r($data);die;
        $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data);
            $this->load->view('notification/cts_list_of_today_task', $data);
            $this->load->view("cts_footer");
            
    }

}