<?php 

class cts_role_model extends CI_Model 
{
	public function show_all_position_role() 
	{
		$result = $this->db->get('position');
		return $result;
	}
	public function show_all_modules()
	{
	$result = $this->db->get('modules');
	return $result;
	}
	public function insert_role()
	{
		$this->db->empty_table('role');
		$result = $this->show_all_position_role();
			foreach($result->result() as $row)
			{
			//echo $this->input->post(str_replace(' ','_',$row->position_name."_edit"));
			 $result = $this->db->insert('role',array('position_id'=>$row->position_id,'add_role'=>$this->input->post(str_replace(' ','_',$row->position_name."_add")),'edit_role'=>$this->input->post(str_replace(' ','_',$row->position_name."_edit")),
			'delete_role' => $this->input->post(str_replace(' ','_',$row->position_name."_delete")),
			'approve_role' => $this->input->post(str_replace(' ','_',$row->position_name."_approve")),
			'report_generate_role' => $this->input->post(str_replace(' ','_',$row->position_name."_report"))
			));
			}
		return $result;
	}
	public function get_roles_data()
	{
	$result = $this->db->get('role');
	return $result->result_array();
	}
	public function show_module_role_by_position($position_id)
	{
		$result = $this->db->get_where('master_role',array('position_id'=>$position_id));
		return $result;
	}
	public function add_module_role($position_id)
	{
	$result = $this->db->get('modules');
	$this->db->delete('master_role',array('position_id'=>$position_id));
	foreach($result->result() as $row)
	{
	$module_name = str_replace(' ','_',$row->modules);
	$module_id = $row->module_id;
	$add= strtolower($module_name.'_add_role');
	$edit = strtolower($module_name.'_edit_role');
	$delete = strtolower($module_name.'_delete_role');
	$view = strtolower($module_name.'_view_role');
	
	if($this->input->post($add) == 1)
	$data['add'] = $this->input->post($add);
	else
	$data['add'] = 0;
	if($this->input->post($edit) == 1)
	$data['edit'] = $this->input->post($edit);
	else
	$data['edit'] = 0;
	if($this->input->post($delete) == 1)
	$data['delete'] = $this->input->post($delete);
	else
	$data['delete'] = 0;
	if($this->input->post($view) == 1)
	$data['view'] = $this->input->post($view);
	else
	$data['view'] = 0;
	$data['position_id'] = $position_id;
	$data['module_id'] = $row->module_id;
	$data['module_name'] = $row->modules;
	$this->db->where('position_id',$position_id);
	$result_insert = $this->db->insert('master_role',$data);
	}
	return $result_insert;
	}
	public function get_modules()
	{
	$result = $this->db->get('modules');
	return $result;
	}
	
}
