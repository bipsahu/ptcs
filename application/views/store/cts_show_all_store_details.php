<div class="container">
<center><h1 id="store_header">All Stored Data</h1></center>
<div id="tabs">
  <?php 
  foreach($departments as $department){
		$listDepartments[$department->dep_id] = $department->dep_name;
	}
  /*?><ul>
    <li><a href="#tabs-1" id="head_tab1" alt="_all"><span class="glyphicon glyphicon-list-alt">&nbsp;</span>New Customer Sample</a></li>
	<li><a href="#tabs-3" id="head_tab3" alt="2"><span class="glyphicon glyphicon-refresh">&nbsp;</span>On Progess</a></li>
    <li><a href="#tabs-4" id="head_tab4" alt="3"><span class="glyphicon glyphicon-ok">&nbsp;</span>Completed</a></li>
	<li><a href="#tabs-2" id="head_tab2" alt="3"><span class="glyphicon glyphicon-ok">&nbsp;</span>Report Pending</a></li>
   
  </ul><?php */?>
<div id="tabs-1">
<div class="dataTables_select">
	<select id="filter_send_to_dept" name="send_to_dept">
			<option value=""> Transfer Files To</option>
            <?php foreach($listDepartments as $key=>$dep):
				?>
            <option value="<?php echo $key;?>"><?php echo $dep;?></option>
            <?php endforeach;?>
    	</select>
<button type="button" id="apply_filter_button">Ok</button>
</div>
    <table class="table table-bordered datatable" id="new_customer_tab">
<!--<<<<<<< .mine
	<tr id="new_customer_table_row"><th>verification No.</th><th>Regd No.</th><th>Customer Name</th><th>Firm Name</th><th>Animal</th><th>Age</th><th>Sex</th><th>Species</th><th>Species Type</th><th>Sample Type</th><th>No of Sample</th><th>Accept</td></tr>
=======-->
	<thead>
	<tr id="new_customer_table_row"><th><input type="checkbox" id="checkall" data-type="check"/></th><th>Regd No.</th><th>Claim No.</th><th>Class</th><th>Insure</th><th>Surveyor</th><th>Estimated Loss</th><th>Survey Remarks</th><th>View Detail</th><th>Send To</th><th></th></tr>
	</thead>
	<?php 
	
	//print_r($result);die();
	foreach($result->result() as $row){
	?>
	<tr>
	<td><input type="checkbox" id="<?php echo $row->claim_id;?>" class="input-checkbox"/></td>
	<td><?php echo $row->regd_no;?></td>
	<td><?php echo $row->claim_no;?></td>
   	<td><?php echo $row->class;?></td>
   
	<td><?php echo $row->insure;?></td>
	<td><?php if(($row->surveyor)!= '0') echo $row->surveyor;?></td>
	<td><?php echo $row->surveyor_estimated_loss;?></td>
    <td><?php echo $row->surveyor_remarks;?></td>
     <td> <a class="btn btn-primary glyphicon glyphicon-list" alt="<?php echo $row->claim_id;?>" data-toggle='modal' data-target='#insure_details-<?php echo $row->claim_id;?>'></a></td>
    <div class="modal fade" id="insure_details-<?php echo $row->claim_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Claim Details</h4>
      </div>
      <div class="modal-body">
        <?php $details['row']=$row;$this->load->view('claim_note_preparation/cts_claim_note_preparation_details',$details);?>
      </div>
    </div>
  </div>
</div>
<td class="sumoSelect"><select id="send_to_dept_<?php echo $row->claim_id;?>" name="send_to_dept" alt="<?php echo $row->claim_id;?>">
			<option value=""> Select Department</option>
            <?php foreach($listDepartments as $key=>$dep):
			
				if($key!=3):?>
            <option value="<?php echo $key;?>"><?php echo $dep;?></option>
            <?php endif;endforeach;?>
    	</select>
      
    </td>
     <td> <a class="btn btn-primary glyphicon glyphicon-forward" alt="<?php echo $row->claim_id;?>">Send</a></td>
     <div id="confirm-dialog"></div>
     <?php /*?><td> <a class="btn btn-primary glyphicon glyphicon-forward" alt="<?php echo $row->claim_id;?>">Send</a></td><?php */?>
	</tr>
	<?php
	}
	?>
	
	</table>
  </div>
  <div id="tabs-2">
   <table class="table table-bordered" id="accepted_store_table"></table>
  </div>
    <div id="tabs-3">
   <table class="table table-bordered" id="on_progess_store_table"></table>
  </div>
   <div id="tabs-4">
   <table class="table table-bordered" id="completed_store_table"></table>
  </div>
</div>
<div class="modal fade" id="register_store" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Molecular Biology Details</h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('store/cts_store_register');?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="edit_store" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Molecular Biology Details</h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('store/cts_edit_store_by_id_form');?>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="approve_store" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Approve Molecular Biology Details</h4>
      </div>
      <div class="modal-body">
        <h3>Approve Molecular Biology Details Form</h3>
		<form id="approve_form" method="post">
		<table class="table">
		
		<input type="hidden" name="tbl" value="store">
		<input type="hidden" name="store_id" id="store_id">
		<tr><td><label>Approved Remarks</label></td>
		<td><textarea class="form-control" name="approve_remarks"></textarea></td></tr>
		<tr><td><label>Message To:</label></td>
		<td><select name="user_assign" class="form-control"><option value="0">--Message To Specific User--</option><?php foreach($all_users->result() as $row)
		echo "<option value='".$row->user_id."'>".$row->user_name." / ".$row->user_position."</option>";
		?>
		</select></td></tr>
		<tr><td>Report</td><td><input type="radio" class="radio-inline" id="publish_check_box" name="publish_check_box" value="1"/>&nbsp;&nbsp;<label>Publish</label>&nbsp;&nbsp;
		<input type="radio" class="radio-inline" id="donot_publish_check_box" name="publish_check_box" value="0"/>&nbsp;&nbsp;<label> Donot Publish</label></td></tr>
		<tr><td><label>Transfer To</label></td>
		<td>
		<?php foreach($all_departments as $row)
		{
		$dep_name = str_replace(" ","_",$row->dep_name);
		echo "<input class='lab' alt ='".$dep_name."' type='checkbox' name=\"dep[$dep_name]\" value='".$row->dep_id."'/> &nbsp;".$row->dep_name."&nbsp;&nbsp;";
		}
		?></td></tr>
		<tr><td> <label> Investigation Requested for <label> </td>
		<td>
				<div id="investigation_requested_td">
			<ul class="select_lab">No Lab Selected </ul>
			</div>
			</td></tr>
		<tr><td colspan="2"><input type="submit" class="btn btn-primary" value="Submit" id="approve_store_submit"/></td></tr>
		
		</table>
		</form>
	  </div>
    </div>
  </div>
</div>

</div>
<script type="text/javascript">
var selected_id = 0;
var selected_edit_id = 0;
$(document).ready(function(){
$( "#tabs" ).tabs();

$("#checkall").click(function(){
	$(".input-checkbox").prop("checked", $("#checkall").prop("checked"))
});


//apply filter to transfer multiple files using multiple checkboxes
$("#apply_filter_button").click(function(){
	$(".input-checkbox").each(function(){
		
		if(($(this).is(":checked"))==true){
			var claim_id = $(this).attr('id');
			filter_send_to_dept(claim_id);
		}
	});
});


var send_forward = $('.glyphicon-forward');
$(send_forward).click(function(){
send_forward = $(this);
$("#confirm-dialog").html("Are You Sure?");
$("#confirm-dialog").dialog({
						resizable: false,
						height:140,
						modal: true,
						 show: {
						effect: "blind",
						duration: 300
						},
						hide: {
						effect: "blind",
						duration: 300
						},
						buttons: {
        "Yes": function() {
			
          glyphicon_forward_click(send_forward);
		 
        },
        Cancel: function() {
          $( this ).dialog( "close" );
		 
        }
      }
						});
});



$(".glyphicon-edit").bind("click",function(){glyphicon_edit_func($(this));});
$(".glyphicon-plus").bind('click',function(){glyphicon_plus_func($(this));});
$("#set_accept_btn").bind('click',function(){set_accept_function($(this))});
$("#head_tab2").bind('click',function(){head_tab2_click_function(0);});
$("#head_tab3").bind('click',function(){head_tab3_click_function(0);});
$("#head_tab4").bind('click',function(){head_tab4_click_function(0);});

$("#approve_form").submit(function(event)
{
if(!$("#approve_form")[0].checkValidity())
			{
			event.preventDefault();
			}
			else
			{
				$.ajax({
				url:'<?php  echo base_url();?>'+'index.php/cts_approval_controller/approve_sample/',
				type:'POST',
				data:$('#approve_form').serialize(),
				error :function(xhr, ajaxOptions, thrownError)
				{
					console.log(xhr);
					console.log(ajaxOptions);
					console.log(thrownError);
					head_tab3_click_function();
				},
				success:function(msg)
				{
					//if(parseInt(msg)==1)
					//{
					head_tab4_click_function();
					$("#approve_store").modal("hide");
					$("#approve_form")[0].reset();
					//}
				} 
			});
			}
			return false;
});
function glyphicon_forward_click(obj)
{

claim_id =obj.attr("alt");
send_to_dept(claim_id);
}
function send_to_dept(obj)
{
	var id =obj;
	var send_to_select = $("#send_to_dept_"+id).val();
	
	
	alert(send_to_select);
		$.ajax({
		type: 'POST',
		url:'<?php echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/send_to_dept/'+id,
		data:{'send_values':send_to_select,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
		dataType:'json',
		error:function(xhr, ajaxOptions, thrownError)
										{
										console.log(xhr);
											console.log(ajaxOptions);
											//console.log(thrownError);
										},
		success:function(msg)
			{
			
				window.location.assign("<?php echo base_url()?>"+"index.php/cts_store_controller");
				
			}
		});
}

function filter_send_to_dept(claim_id,status)
{
	var id =claim_id;
	var send_to_select = $("#filter_send_to_dept").val();
		$.ajax({
		type: 'POST',
		url:'<?php echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/send_to_dept/'+id,
		data:{'send_values':send_to_select,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
		dataType:'json',
		error:function(xhr, ajaxOptions, thrownError)
										{
										console.log(xhr);
											console.log(ajaxOptions);
											//console.log(thrownError);
										},
		success:function()
			{
			
				 window.location.href = ("<?php echo base_url()?>"+"index.php/cts_store_controller");
			}
		});
}

function check_status()
{
$.ajax({
	url:'<?php  echo base_url();?>'+'index.php/cts_store_controller/check_new_customer',
	dataType:'json',
	success:function(msg)
	{
		console.log(msg);
		for(i=0; i<msg.length;i++)
		{
		$("#new_customer_table_row").after("<tr><td>"+msg[i].labno+"</td><td>"+msg[i].customer_name+"</td><td>"+msg[i].firm_name+"</td><td>"+msg[i].animal+"</td><td>"+msg[i].age+"</td><td>"+msg[i].sex+"</td><td>"+msg[i].animal_type+"</td><td>"+msg[i].sample_type+"</td><td>"+msg[i].no_of_sample+"</td><td>"+msg[i].investigation_requested+"</td><td><button class='btn btn-primary set_accept_btn' alt='"+msg[i].sample_id+"'><span class='glyphicon glyphicon-saved'></span>&nbsp;Accept</button></td></tr>");
		$(".set_accept_btn").bind('click',function(){set_accept_function($(this))});
		
		
		}
	}
});
}
setInterval(check_status,20000);
$(".onoffswitch-checkbox").click(function(event)
			{
			var claim_id = $(this).attr('alt');
			var check_value = $(this).val();
				if(check_value==0)
					var final_value = 1;
				if(check_value==1)
					var final_value = 0;
				$.ajax({
				url:'<?php  echo base_url();?>'+'index.php/cts_store_controller/store_approve/'+claim_id,
				type:'POST',
				data:{'store_approve':final_value},
				error :function(xhr, ajaxOptions, thrownError)
				{
				console.log(xhr);
				console.log(ajaxOptions);
				console.log(thrownError);
				head_tab3_click_function();
				},
				success:function(msg)
				{
					return true;
				}
			});
		});
	$("#edit_store_form").submit(function(event){
		if(!$("#edit_store_form")[0].checkValidity())
		{
			event.preventDefault();
		}
		else
		{
			$.ajax
			({
				url : '<?php echo base_url();?>'+'index.php/cts_store_controller/edit_store_by_id/'+selected_edit_id,
				type: 'POST',
				data: $("#edit_store_form").serialize(),
				success:function(msg)
				{
					if(parseInt(msg)==1)
					{
					head_tab3_click_function();
					$("#edit_store").modal("hide");
					}
					else
					{
					$("store_header").after("<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Deletion Failed!</b></div>");
					$("$edit_store").modal("toggle");
					}
				}
			});
		}
		return false;
	});	
	
});
function head_tab2_click_function(offset)
{
	$.ajax
	({
	url:'<?php echo base_url();?>'+'index.php/cts_store_controller/search_pending_sample/'+offset,
	type: 'POST',
		success:function(msg)
		{
		$('#accepted_store_table').html(msg);
		$(".set_publish_btn").bind('click',function(){set_publish_function($(this))});
		}
	});
}
function head_tab3_click_function(offset)
{
$.ajax
	({
	url:'<?php echo base_url();?>'+'index.php/cts_store_controller/search_on_progress_sample/'+offset,
	type: 'POST',
		success:function(msg)
		{
		$('#on_progess_store_table').html(msg);
		$(".glyphicon-plus").bind('click',function(){glyphicon_plus_click($(this));});
		$(".glyphicon-edit").bind('click',function(){glyphicon_edit_click($(this));});
		$(".glyphicon-trash").bind('click',function(){glyphicon_trash_click($(this));});
		$(".set_completed_button").bind('click',function(){set_completed_function($(this));});
		
		}
	});
}
function head_tab4_click_function(offset)
{
$.ajax
	({
	url:'<?php echo base_url();?>'+'index.php/cts_store_controller/search_completed_sample/'+offset,
	type: 'POST',
		success:function(msg)
		{
		$('#completed_store_table').html(msg);
		$('.set_approved_btn').bind('click',function(){set_approved_btn($(this));});
		}
	});
}
function set_approved_btn(obj)
{
var id = obj.attr("alt");
$("#investigation_requested_td").empty();
$("#store_id").val(id);
}
function set_completed_function(obj)
{
var id = obj.attr("alt");
$.ajax({
		url:'<?php echo base_url();?>'+'index.php/cts_store_controller/set_completed_on/'+id,
		type: 'POST',
			success:function(msg)
			{
				if(parseInt(msg) == 1)
				{
				head_tab3_click_function();
				}
			}
		});
}
function set_accept_function(obj)
{
	var id =obj.attr("alt");
		$.ajax({
		url:'<?php echo base_url();?>'+'index.php/cts_store_controller/set_accept_on/'+id,
		type: 'POST',
		error :function(xhr, ajaxOptions, thrownError)
				{
				console.log(xhr);
				console.log(ajaxOptions);
				console.log(thrownError);
				head_tab3_click_function();
				},
			success:function(msg)
			{
				if(parseInt(msg) == 1)
				{
				window.location.assign("<?php echo base_url();?>"+"index.php/cts_store_controller");
				}
			}
		});
}
function set_publish_function(obj)
{
var id= obj.attr("alt");
	$.ajax({
	url:'<?php echo base_url();?>'+'index.php/cts_store_controller/set_publish_on/'+id,
	type:'Post',
		success:function(msg)
		{
			if(parseInt(msg) == 1)
			{
			head_tab2_click_function();
			}
		}
	});
}
function glyphicon_trash_click(obj)
{
selected_delete_id = obj.attr("alt");
$.ajax
		({
			url : '<?php echo base_url();?>'+'index.php/cts_store_controller/delete_store_by_id/'+selected_delete_id,
			type: 'POST',
			error:function(xhr, ajaxOptions, thrownError)
			{
			console.log(xhr);
				console.log(ajaxOptions);
				console.log(thrownError);
			},
				success:function(msg)
				{
					if(parseInt(msg)==1)
					{
					head_tab3_click_function();
					}
				}
			});
}
function glyphicon_edit_click(obj)
{
		selected_edit_id = obj.attr("alt");
		$.ajax
		({
			url : '<?php echo base_url();?>'+'index.php/cts_store_controller/edit_store_by_id_form/'+selected_edit_id,
			type: 'POST',
			dataType:'json',
			success:function(msg)
			{
			console.log(msg);

			if(msg.pcr_h5_result=="positive")
			$("#pcr_h5_positive").attr("checked",true);
			if (msg.pcr_h5_result=='negative')
			$("#pcr_h5_negative").attr("checked", true);
			
			if(msg.pcr_n1_result=="positive")
			$("#pcr_n1_positive").attr("checked",true);
			if (msg.pcr_n1_result=='negative')
			$("#pcr_n1_negative").attr("checked", true);
			
			if(msg.pcr_n9_result=="positive")
			$("#pcr_n9_positive").attr("checked",true);
			if (msg.pcr_n9_result=='negative')
			$("#pcr_n9_negative").attr("checked", true);
			
			if(msg.pcr_h9_result=="positive")
			$("#pcr_h9_positive").attr("checked",true);
			if (msg.pcr_h9_result=='negative')
			$("#pcr_h9_negative").attr("checked", true);
			if(msg.pcr_h7_result=="positive")
			$("#pcr_h7_positive").attr("checked",true);
			if (msg.pcr_h7_result=='negative')
			$("#pcr_h7_negative").attr("checked", true);
			
			
			if(msg.pcr_ibd_result=="positive")
			$("#pcr_ibd_positive").attr("checked",true);
			if (msg.pcr_ibd_result=='negative')
			$("#pcr_ibd_negative").attr("checked", true);
			
			
			if(msg.pcr_nd_result=="positive")
			$("#pcr_nd_positive").attr("checked",true);
			if (msg.pcr_nd_result=='negative')
			$("#pcr_nd_negative").attr("checked", true);
			
			
			if(msg.pcr_m_gene_result=="positive")
			$("#pcr_m_gene_positive").attr("checked",true);
			if (msg.pcr_m_gene_result=='negative')
			$("#pcr_m_gene_negative").attr("checked", true);
			
			
			$("#ct_value").val(msg.ct_value);
			$("#remarks").val(msg.remarks);
			$("#tested_date").val(msg.tested_date);
			$("#approved_date").val(msg.approved_date);
			$("#collection_dat").attr("value",msg.collection_date);
			$("#received_dat").val(msg.received_date);
			$("#extraction_dat").val(msg.extraction_date);
			$("#sample_submitted").val(msg.sample_submitted_by);
			$("#flock_siz").val(msg.flock_size);
			$("#mortalit").val(msg.mortality);
			$("#others").val(msg.others);
			
				$("#collection_dat").datepicker();
				$("#received_dat").datepicker();
				$("#extraction_dat").datepicker();
			}
		});
	}
function glyphicon_plus_click(obj)
{
	selected_id = obj.attr("alt");
}
function store_on_progess_pagination(offset)
{
head_tab3_click_function(offset);
}
function store_completed_pagination(offset)
{
head_tab4_click_function(offset);
}
function store_pending_pagination(offset)
{
head_tab2_click_function(offset);
}
</script>
</body>
</html>