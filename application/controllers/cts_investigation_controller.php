<?php 
class lims_investigation_controller extends CI_Controller 
{
	public function __construct()
	{
	parent::__construct();
		$this->load->model('lims_investigation_model');
		$this->load->model('lims_role_model');
		$this->load->model('lims_department_model');
		$this->load->model('lims_login');
	}
	public function index()
	{
		$data['positions'] = $this->lims_role_model->show_all_position_role();
		$data['departments'] = $this->lims_department_model->getDepartment();
		$data['new_message'] = $this->lims_login->check_new_message();
		$this->load->view('lims_header');
		$this->load->view('lims_navigation',$data);
		$this->load->view('investigation/lims_investigation_registration');
		$this->load->view('lims_footer');
	}
	
	public function add_new_investigation_requested()
	{
		$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['new_message'] = $this->lims_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'])
		{
			$data['departments'] = $this->lims_department_model->getDepartment();
			$data['positions'] = $this->lims_role_model->show_all_position_role();
			$this->load->view('lims_header');
			$this->load->view('lims_navigation',$data);
			$this->load->view('investigation/lims_investigation_registration1',$data);
			$this->load->view('lims_footer');
		}
		else
		{
		redirect(base_url());
		}
	
	}
	public function insert_investigation_details ()
	{
		$data['result'] = $this->lims_investigation_model->insert_investigation_details();
			if($data['result'] == false)
			{
			echo " 0";
			}
			else
			{
			foreach($data['result']->result() as $row)
			{
			$investigation_requested_details = array(
				'investigation_requested' => $row->investigation_requested,
				'investigation_requested_id' => $row->investigation_requested_id,
				'dep_name' => $row->dep_name
				);
			}
			echo json_encode($investigation_requested_details);
			}
	}
	
	public function insert_new_investigation()
	{
		$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['new_message'] = $this->lims_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
			if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'])
		{
			$data['departments'] = $this->lims_department_model->getDepartment();
			$data['positions'] = $this->lims_role_model->show_all_position_role();
			$data['result'] = $this->lims_investigation_model->add_investigation_details();
			$this->load->view('lims_header');
			$this->load->view('lims_navigation',$data);
			$this->load->view('investigation/lims_investigation_registration1',$data);
			$this->load->view('lims_footer');
		}
		else
		{
		redirect(base_url());
		}
	
	}
	public function edit_investigation_requested_by_id_form($investigation_requested_id)
	{	
		$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['new_message'] = $this->lims_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit'])
		{
		$data['departments'] = $this->lims_department_model->getDepartment();
		$data['positions'] = $this->lims_role_model->show_all_position_role();
		$data['investigation_requested'] = $this->lims_investigation_model->get_details_of_investigation_requested_by_id($investigation_requested_id);
		$data['lab_id_in'] = $this->lims_investigation_model->get_lab_investigation_by_id($investigation_requested_id);
		$this->load->view('lims_header');
		$this->load->view('lims_navigation',$data);
		$this->load->view('investigation/lims_edit_investigation_requested_by_id_form',$data);
		}
		else
		{
		redirect(base_url());
		}
	}
	
	public function edit_investigation_requested_by_id($investigation_requested_id)
	{
		$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['new_message'] = $this->lims_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit'])
		{
		$data['departments'] = $this->lims_department_model->getDepartment();
		$data['result'] = $this->lims_investigation_model->edit_investigation_requested_by_id($investigation_requested_id);
		redirect(base_url().'index.php/lims_investigation_controller/show_all_investigation_requested');
		}
		else
		{
		redirect(base_url());
		}

	
	}
	
	public function show_all_investigation_requested($offset = 0)
	{
		$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['new_message'] = $this->lims_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit']|| $role[3]['add']|| $role[3]['delete']) 
		{
		$data['departments'] = $this->lims_department_model->getDepartment();
		$data['positions'] = $this->lims_role_model->show_all_position_role();
		$data['offset'] = $offset;
		$config = array();
		$config["base_url"] = base_url() . "index.php/lims_investigation_controller/show_all_investigation_requested";
		$config["total_rows"] = $this->db->count_all("investigation_requested");
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["results"] = $this->lims_investigation_model->show_all_investigation_requested($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();
		$this->load->view('lims_header');
		$this->load->view('lims_navigation',$data);
		$this->load->view('investigation/lims_show_all_investigation_requested',$data);
		}
		else
		{
		redirect(base_url());
		}
	
	}
	
	public function delete_investigation_requested_by_id($investigation_requested_id)
	{
		$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['delete'])
		{
		$data['result'] = $this->lims_investigation_model->delete_investigation_requested_by_id($investigation_requested_id);
		redirect(base_url().'index.php/lims_investigation_controller/show_all_investigation_requested');
		}
		else
		{
		redirect(base_url());
		}
	}
	public function search_investigation_detail($key)
	{
	$this->lims_investigation_model->search_investigation_detail($key);
	}
	
	public function check_investigation()
	{
	$result = $this->lims_investigation_model->check_investigation();
	if($result)
	echo " 1";
	else
	echo " 0";
	}
	public function get_lab_investigaion()
	{
		$result = $this->lims_investigation_model->get_lab_investigaion();
		$data=array();
		$i=0;
		foreach($result->result() as $row)
		{
		$data[$i]['lab_id'] = $row->lab_id;
		$data[$i]['dep_name'] = $row->dep_name;
		$data[$i]['investigation_id'] = $row->investigation_id;
		$data[$i]['investigation_requested'] = $row->investigation_requested;
		$i++;
		}
		echo json_encode($data);
	}
}