<div class="container">
    <h3> Legal Report</h3><br>
	  <?php
/* 
	if(isset($result))
	{
	if($result==1)
	{
	echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Successfully Registered!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Registration Failed!</b></div>";
	}  */
  ?>

  
      <table class="table" id="table">
        <form method="post" role="form" enctype="multipart/form-data" id="legal_form">      
			
			            <tr>
              <td><label>Collection Date</label></td>
              <td><input type="text" class="form-control" name="collection_date" id="collection_date" value="<?php echo date('m-d-Y'); ?>" ></td>
            </tr>
            <tr>
              <td><label>Received Date</label></td>
              <td><input type="text" class="form-control" name="received_date" id="received_date" value="<?php echo date('m-d-Y'); ?>" ></td>
            </tr>
            <tr>
              <td><label>Extraction Date</label></td>
              <td><input type="text" class="form-control" name="extraction_date" id="extraction_date" value="<?php echo date('m-d-Y'); ?>" ></td>
            </tr>

			<tr>
              <td><label>Sample Subbmitted By</label></td>
              <td><input type="text" class="form-control" name="sample_submitted_by" id="sample_submitted_by" ></td>
            </tr>
			
			<tr>
              <td><label>Flock Size</label></td>
              <td><input type="number" class="form-control" name="flock_size" id="flock_size" ></td>
            </tr>
			
			<tr>
              <td><label>Mortality</label></td>
              <td><input type="number" class="form-control" name="mortality" id="mortality" ></td>
            </tr>


		   <tr>
              <td colspan ='2'><label><b>PCR Result </b> </label></td>
              
            </tr>
          <tr>
              <td><label>M Gene</label></td>
             <td><input type="radio" class="radio-inline" name="pcr_m_gene_result" value="positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_m_gene_result" value="negative" >&nbsp;Negative</td>
            </tr>
            <tr>
              <td><label>H5</label></td>
              <td><input type="radio" class="radio-inline" name="pcr_h5_result" value="positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_h5_result" value="negative" >&nbsp;Negative</td>
            </tr>
			<tr>
              <td><label>N1</label></td>
              <td><input type="radio" class="radio-inline" name="pcr_n1_result" value="positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_n1_result" value="negative" >&nbsp;Negative</td>
            </tr> <tr>
              <td><label>N9</label> </td>
              <td><input type="radio" class="radio-inline" name="pcr_n9_result" value="positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_n9_result" value="negative" >&nbsp;Negative</td>
            </tr>
			<tr>
              <td><label>H9</label></td>
              <td><input type="radio" class="radio-inline" name="pcr_h9_result" value="positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_h9_result" value="negative" >&nbsp;Negative</td>
            </tr>
			<tr>
              <td><label>H7 </label> </td>
              <td><input type="radio" class="radio-inline" name="pcr_h7_result" value="positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_h7_result" value="negative" >&nbsp;Negative</td>
            </tr>			
			
			<tr>
              <td><label>IBD</label></td>
              <td><input type="radio" class="radio-inline" name="pcr_ibd_result" value="positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_ibd_result" value="negative" >&nbsp;Negative</td>
            </tr>
			<tr>
              <td><label>ND</label></td>
              <td><input type="radio" class="radio-inline" name="pcr_nd_result" value="positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_nd_result" value="negative" >&nbsp;Negative</td>
            </tr>
			<tr>
              <td><label>CT Value of Positive</label></td>
              <td><input type="text" class="form-control" name="ct_value" ></td>
            </tr>
			<tr>
              <td><label>Others</label></td>
              <td><input type="text" class="form-control" name="others"></td>
            </tr>
			<tr>
              <td><label>Remarks</label></td>
              <td><textarea type="textarea" class="form-control" name="remarks" ></textarea></td>
            </tr>
			
          
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary" id="legal_submit" >Submit</button></td>
            </tr>
          
        </form>
    </table>
	</div>
</div><!---end of container-->
</body>
</html>