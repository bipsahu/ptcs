<div class="container">
    <h3>Edit Indicator</h3>
  
    <?php
    if (isset($result)) {
        if ($result == 1) {
            echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Department Created!</b></div>";
        } else
            echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Department Creation Failed!</b></div>";
    }
    ?>
     
    <?php
    
    if (isset($project_details)) {
//        echo $error;
        foreach ($project_details as $project_values) {
    // print_r($project_values); die();
          foreach($project_values as $row):
            $project_values['indicator_id'];
            $project_values['indicator_name'];
            $project_values['task_category_id'];
            // $project_values['pareasnt_id'];
        endforeach;
        }
    }
    ?>
    <table class="table" id="table">
       
        <form method="post" role="form" enctype="multipart/form-data"
              action="<?php echo base_url(); ?>index.php/cts_controller/edit_the_indicator/<?php echo $project_values['indicator_id']; ?>">
        
            <tr>
                <td>Indicator Name</td>
                <td><input type="text" class="form-control" name="indicator_name" required value="<?php echo $project_values['indicator_name'] ?>">
            </tr>
            <tr>
                <td>Task Category</td>
                <td>
                  <select name="task_category" class="form-control" required>
                    <option value="">Select</option>
                    <?php foreach ($task_category as $key => $task_categories) {
                      # code...
                    ?>
                    <option value="<?php echo $task_categories['t_category_id']; ?>" <?php if( $task_categories['t_category_id'] == $project_values['task_category_id']){ echo "selected";} ?>><?php echo $task_categories['name'] ?></option>
                    <?php } ?>
                  </select>
                </td>
              </tr>
            <tr>
                <td><input type="submit" name="submit" value="Submit" class="btn btn-primary"></td>
            </tr>
        </form>
    </table>
</div>
</body>
</html>