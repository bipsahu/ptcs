<div class="container">
<center><h1>All Animal</h1></center>
<?php

if(!empty($result_indicator))
{
	if($result_indicator == 1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Deletion Successful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Deletion Failed!</b></div>";
}
?>


<table class="table table-striped" id="table_show_all">
<thead><th>S No.</th><th>Animal Name</th><th>Edit</th><th>Delete</th></thead>
<?php 
$offset = $offset+1;
foreach($results->result() as $row){
?>
<tr>
<td><?php echo $offset;?></td>
<td><?php echo $row->animal_name;?></td>
<td><a href="<?php echo base_url()."index.php/lims_customer_controller/edit_animal_by_id_form/".$row->aid;?>" class='btn btn-primary'><div class='glyphicon glyphicon-edit'></div>Edit Animal</a></td>
<td><a href="<?php echo base_url()."index.php/lims_customer_controller/delete_animal_by_id/".$row->aid;?>" class='btn btn-primary'><div class='glyphicon glyphicon-trash'></div>Delete Animal</a></td>
</tr>
<?php
$offset++;
}
?>
<tr><td colspan="4"><?php echo $links; ?></td></tr>
<tr><td colspan="9"><a class="btn btn-primary" href="<?php echo base_url();?>index.php/lims_customer_controller/add_new_animal1">Add New Animal</a></tr>
</table>
</div>
</body>
</html>