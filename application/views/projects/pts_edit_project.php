<div class="container">
    <h3>Edit Project</h3>
  
    <?php
    if (isset($result)) {
        if ($result == 1) {
            echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Department Created!</b></div>";
        } else
            echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Department Creation Failed!</b></div>";
    }
    ?>
     
    <?php
    
    if (isset($project_details)) 
    {
        // echo "<pre>";
        foreach ($project_details as $project_values) 
        {
        //   foreach($project_values as $row):
        //     $project_values['project_id'];
        //     $project_values['project_name'];
        //     // $project_values['parent_id'];
        // endforeach;
        }
        // print_r($project_values); die();
    }
    ?>
    <form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/cts_controller/edit_the_project/<?php echo $project_values['project_id']; ?>">
        <table class="table" id="table" style="width:50%">
            <tr>
                <td>Project Name</td>
                <td><input type="text" class="form-control" name="project_name" required
                           value="<?php echo $project_values['project_name'] ?>">
            </tr>

            <tr>
                <td><label>Task Category</label></td>
                <td><select type="text" id="multiple-select-taskcategory" name="task[indicator]" required
                              multiple="multiple">
                        <?php 
                        if (isset($task_category)):

                              //convert json encoded data to array and check if key of array and indicator id matches
                              $editIndicators = json_decode($project_values["project_taskcategory"], true);
                                             // <?php print_r($editIndicators);die;
                              // $editachieved = json_decode($task_detail[0]["achieved"], true);


                            foreach ($task_category as $indicator) 
                            {

                                  //assign key-value pair to indicators id and name respectively
                                $keyValueIndicators[$indicator['t_category_id']] = $indicator['name'];

                                  //check for selection for indicators
                                $selected = array_key_exists($indicator['t_category_id'], $editIndicators) ? "selected" : "";

                                echo "<option value='" . $indicator['t_category_id'] . "' " . $selected . " >" . $indicator['name'] ."</option>";
                            }

                        endif; ?>
                    </select>
                </td>
                    <!-- user table added through juery               -->
                <td>
                    <table class="table-bordered alert alert-warning">
                        <tbody id="assigned_indicators" style="margin-top: -40px;">
                        <tr class="indicator_title">
                            <td><strong><em>Task Category Selected</em></strong></td>
                            <td><strong><em>Assign Weightage(%)</em></strong></td>
                        </tr>

                         <!--list of indicators and weightage for edit task only-->
                        <!-- Used achieved array in same loop with indicator key-->
                        <?php if (isset($editIndicators) && $editIndicators != NULL):
//                            print_r($editIndicators);
                            foreach ($editIndicators as $eKey => $eIndicators):
                                ?>

                                <tr id="indicator_<?php echo $eKey; ?>" class="indicator_body">
                                    <td><?php echo $keyValueIndicators[$eKey];?></td>
                                    <td><input type="number" name="task_cat[indicators][<?php echo $eKey; ?>]"
                                               value="<?php echo $eIndicators; ?>">
                                    </td>
                                    <!-- <td style="color:#0F4C8F"><input type="number"
                                                                     name="task[achieved][<?php echo $eKey;?>]"
                                                                     value="<?php echo $editachieved[$eKey]; ?>"></td> -->
                                    <!--                                               value=""></td>-->
                                </tr>
                            <?php endforeach;endif; ?>
                        <!--list of indicators ends here-->
                         
                        <tr class="indicator_total">
                            <td><em><strong>Total: </strong></em></td>
                            <td id="indicator_total_value">
                            </td>
                            <td style="color:#0F4C8F" id="achievement_total_value"></td>
                          <!-- <input type='number' name='task[indicators]["this"]'> -->
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
           
            <tr>
                <td><input type="submit" name="submit" value="Submit" class="btn btn-primary"></td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>
<script>
    $(document).ready(function () {
// alert('hi');

        //flash messages display/hide
     
// console.log(data);
        //show the indicators and member titles for editing task request
        <?php 
        if (isset($editIndicators) && $editIndicators != NULL): ?>
        $("tr.indicator_title").show();
        $("tr.indicator_total").show();

        <?php endif; ?>
      

        //multiple select indicators
        $("#multiple-select-taskcategory").multipleSelect({

            placeholder: "Select Task category",
            width: "100%",
            onCheckAll: function () {
              // alert('all selected');
                  // console.log('checked all');
                $("tr.indicator_title").show();
                $("tr.indicator_total").show();
                //clear first
                $(".indicator_body").each(function () {
                    $(this).remove();
                });
                var checkAllValues = $("#multiple-select-taskcategory").multipleSelect("getSelects");
                console.log(checkAllValues);
                var checkAllUsers = $("#multiple-select-taskcategory").multipleSelect("getSelects", "text");
                console.log(checkAllUsers);
                $.each(checkAllValues, function (index, value) {
                  // console.log(index);
                  // console.log(value);
                    $("#assigned_indicators tr:first-child").after(" <tr class='indicator_body' id='indicator_" + value + "'><td>" + checkAllUsers[index] + "</td><td><input type='number' name='task_cat[indicators][" + value + "]'></td></tr>");
                });
            },
            onUncheckAll: function () {
                  // console.log('unchecked all');
                $("tr.indicator_title").hide();
                $("tr.indicator_total").hide();
                $(".indicator_body").each(function () {
                    $(this).remove();
                });
            },
            onClick: function (view) {
              // console.log('i am here');
                if (view.checked == true) {
                  // console.log('checked');
                  // alert('he');
                    $("tr.indicator_title").show();
                    $("tr.indicator_total").show();
                    // console.log(view.value);
                    $("#assigned_indicators tr:first-child").after("<tr class='indicator_body' id='indicator_" + view.value + "'><td>" + view.label + "</td><td><input type='number' name='task_cat[indicators][" + view.value + "]'></td></tr>");
                }
                else {
                  // console.log('unchecked');
                    var removeCheck = "#assigned_indicators tr#indicator_" + view.value;
                    $(removeCheck).remove();
                }
            },
        });
    });

    //calculate total for indicators weightages

    $("#assigned_indicators ").on("keyup", "input[type='number']", function (event, value) {
        var totalWeightage = {};
        $('#assigned_indicators tr td input[type=number]').each(function () {
          // console.log('data');
            var colIndex = $(this).parent().index();
            if (!(colIndex in totalWeightage)) {
                totalWeightage[colIndex] = 0;
            }
            var input = parseInt($(this).val(), 10);
            if (input) {

                    totalWeightage[colIndex] += input;

            }
        });
        var weight = {
            '1': $("input[name='task[indicators][1]']").val(),
            '2': $("input[name='task[indicators][2]']").val()
        };

        var achieved = {
            '1': $("input[name='task[achieved][1]']").val(),
            '2': $("input[name='task[achieved][2]']").val()
        };

        totalWeightage['2'] = Math.round((achieved['2']/100*weight['2'])+(achieved['1']/100*weight['1']));
        $.each(totalWeightage, function (i, total) {
            if (total > 100) {
                $('.indicator_total td').eq(i).html("<span class='red'>error: percent greater than 100</span>");
            }
            else if(i == 1 && total < 100) {
                $('.indicator_total td').eq(i).html("<span class='red'>error: percent less than 100</span>");
            }
            else {
                $('.indicator_total td').eq(i).text(total + ' %');
            }
        });
    });


// $('#submitform').click(function(e){
// // alert('heha');
//     var postData = $('#formid').serializeArray();
//     $(postData).each(function(i, field){ 
//         alert(field.name+"="+field.value);
//     });
// });


</script>

