<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cts_assignment extends CI_Controller 
{
public function __construct()
	{
	parent::__construct();
	$this->load->model('cts_login');
	$this->load->model('cts_role_model');
	$this->load->model('cts_department_model');
	$this->load->model('cts_assignment_model');
	}
	public function check_assignment()
	{
	$data['total_message_of_user'] = $this->cts_assignment_model->get_total_message();
	//$data['offset'] = $offset;
	$data['result'] = $this->cts_assignment_model->check_assignment();
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
	$data['new_message'] = $this->cts_login->check_new_message();
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('assignment/cts_assignment',$data);
	$this->load->view("cts_footer");
	}
	public function change_status($assignment_id)
	{
	$result = $this->cts_assignment_model->change_status($assignment_id);
	echo $result;
	}

}