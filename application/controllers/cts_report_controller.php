<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * controller that deals with the business logic for report generation
 * */

class cts_report_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('cts_entry_person');
        $this->load->model('cts_role_model');
        $this->load->model('cts_project_model');
        $this->load->model('cts_task_model');
        $this->load->model('cts_login');
        $this->load->model('cts_department_model');
        $this->load->model('cts_report_model');
    }

    /*
     * Index controller for report generation ie project wise report
     * @param : NULL
     * @return : report view
     * */
    public function index($param = false)
    {
        // echo "<pre>";
        if ($param == "search") 
        {
            if (isset($_POST['report-submit'])) {
                $from = $_POST['from_date'];
                $to = $_POST['to_date'];
                $data['result'] = $this->cts_report_model->show_all_peoject_details($from, $to);
                // echo "1";
                // print_r($data);die;
            }
        } 
        else 
        {
            $data['result'] = $this->cts_report_model->show_all_project_details();
            // echo "2";
                // print_r($data);die;
        }
        $data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data1['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data1['positions'] = $this->cts_role_model->show_all_position_role();
        $data1['departments'] = $this->cts_department_model->getDepartment();
        $data1['new_message'] = $this->cts_login->check_new_message();


        $this->load->view('cts_header');
        $this->load->view('cts_navigation', $data1);
        $this->load->view('report/cts_report_generate', $data);
        $this->load->view('cts_footer');
    }

    /*
     * Task Report controller for report generation i.e task wise report
     * @param : NULL
     * @return : report view
     * */
    public function task_report($param = '')
    {
        if ($this->input->is_ajax_request()) {

            $data = $this->cts_report_model->show_all_task_details();
            // print_r($data);
            echo json_encode($data);
        }
        elseif($param != '')
        {
        // echo $param;die;
            // echo $param;die;
            $data['project_id'] = $param;
        }

        $data['projects'] = $this->cts_project_model->show_all_project(10000000, 0);
        $data1['positions'] = $this->cts_role_model->show_all_position_role();
        $data1['departments'] = $this->cts_department_model->getDepartment();
        $data1['new_message'] = $this->cts_login->check_new_message();
        $data['total_task'] = $this->cts_task_model->count_all_task();
        if (!$_POST) {
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data1);
            $this->load->view('report/cts_task_report_generate', $data);
            $this->load->view('cts_footer');
        }
    }

    /*
    * Indocator Report controller for report generation i.e indicator wise report
    * @param : post (project id, task id)
    * @return : report view
    * */
    public function indicator_report()
    {
        if ($this->input->is_ajax_request()) {

            $result = $this->cts_report_model->show_all_indicator_details();
            echo json_encode($result);
        }

        $data['projects'] = $this->cts_project_model->show_all_project(10000000, 0);
        $data1['positions'] = $this->cts_role_model->show_all_position_role();
        $data1['departments'] = $this->cts_department_model->getDepartment();
        $data1['new_message'] = $this->cts_login->check_new_message();

        if (!$_POST) {
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data1);
            $this->load->view('report/cts_indicator_report_generate', $data);
            $this->load->view('cts_footer');
        }
    }

    /*
    * task category Report controller for report generation i.e task category wise report
    * @return : report view
    * */
    public function task_category_report($project_id ='')
    {
        if ($this->input->is_ajax_request()) {
            // unset($data['result']);
            $result = $this->cts_report_model->show_all_task_category_details();
            // print_r($result);die;
            echo json_encode($result);
        }
        elseif($project_id != '')
        {
            $result = $this->cts_report_model->show_all_task_category_details($project_id);
            $data['result'] = $result;    
        }

        // echo "hello";die;
        $data['projects'] = $this->cts_project_model->show_all_project(10000000, 0);
        $data1['positions'] = $this->cts_role_model->show_all_position_role();
        $data1['departments'] = $this->cts_department_model->getDepartment();
        $data1['new_message'] = $this->cts_login->check_new_message();

        if (!$_POST) {
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data1);
            $this->load->view('report/cts_task_category_report_generate', $data);
            $this->load->view('cts_footer');
        }
    }

    public function projectwise_hr_report()
    {
        
        $data['result'] = $this->cts_report_model->show_hr_project();
        $data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data1['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data1['positions'] = $this->cts_role_model->show_all_position_role();
        $data1['departments'] = $this->cts_department_model->getDepartment();
        $data1['new_message'] = $this->cts_login->check_new_message();


        $this->load->view('cts_header');
        $this->load->view('cts_navigation', $data1);
        $this->load->view('report/cts_projectwise_hr_report', $data);
        $this->load->view('cts_footer');
    }


    public function taskwise_hr_report($param = '')
    {
        if ($this->input->is_ajax_request()) 
        {
            // echo "this is json format";
            $data = $this->cts_report_model->show_taskwise_hr_report();
            // print_r($data);
            echo json_encode($data);
        }
        elseif($param != '')
        {
        // echo $param;die;
            // echo $param;die;
            $data['project_id'] = $param;
        }

        $data['projects'] = $this->cts_project_model->show_all_project(10000000, 0);
        
        // $data['result'] = $this->cts_report_model->show_hr_project();
        $data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
        $i = 0;
        foreach ($data1['roles_info']->result_array() as $key => $value) {
            $role[] = $value;
            $module[] = $role[$i]['module_name'];
            $i++;
        }
        $data1['positions'] = $this->cts_role_model->show_all_position_role();
        $data1['departments'] = $this->cts_department_model->getDepartment();
        $data1['new_message'] = $this->cts_login->check_new_message();

        if(!($_POST))
        {
            $this->load->view('cts_header');
            $this->load->view('cts_navigation', $data1);
            $this->load->view('report/cts_taskwise_hr_report', $data);
            $this->load->view('cts_footer');
        }
    }
}