<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>ICN PTS</title>
    <!-- Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url(); ?>resource/bootstrap/css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>resource/css/bootstrap-submenu.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>resource/css/style.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>resource/jquery-ui/jquery-ui.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>resource/css/datatable.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>resource/css/multiple-select.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>resource/js/timepicker/jquery.timepicker.css" type="text/css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">



<!-- <link type="text/css" rel="stylesheet" href="css/popModal.css"> -->
		<script>
		var base_url = '<?php echo base_url();?>'+'index.php/';

		</script>
          <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
  <!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script> -->
  <!-- <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
<script type="text/javascript" src="<?php echo base_url();?>resource/js/canvasjs.min.js"></script>
<?php if($this->uri->segment(1)=="cts_surveyor_deportation_controller"):?>
<link href="<?php echo base_url();?>resource/js/bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?php echo base_url();?>resource/js/bootstrap-editable/js/bootstrap-editable.js"></script>
<?php endif;?>
	</head>
	<body>
    <header>
    <script src="<?php echo base_url(); ?>resource/js/jquery-2.1.1.min.js"></script>
    <script src="<?php echo base_url(); ?>resource/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>resource/js/datatable.js"></script>
    <script src="<?php echo base_url(); ?>resource/jquery-ui/jquery-ui.js"></script>
    <script src="<?php echo base_url(); ?>resource/js/bootstrap-submenu.min.js"></script>
    <script src="<?php echo base_url(); ?>resource/js/sampleregistration.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resource/js/multiple-select.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>resource/js/timepicker/jquery.timepicker.js"></script>
    <!--        <script src="--><?php //echo base_url();?><!--resource/js/tinymce.js"></script>-->
    <!--        <script type="text/javascript">tinymce.init({ mode : "specific_textareas",-->
    <!--        editor_selector : "mceEditor",height: "300"});</script>-->

    <!-- <link type="text/css" rel="stylesheet" href="css/popModal.css"> -->
    <script>
            // alert('this is the page')
          $(function() {
    $( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
    $( "#tabs" ).tabs();
    $('#timepicker').timepicker();
  });
        var base_url = '<?php echo base_url();?>' + 'index.php/';
    </script>

    <script type="text/javascript" src="<?php echo base_url(); ?>resource/js/canvasjs.min.js"></script>
    <?php if ($this->uri->segment(1) == "cts_surveyor_deportation_controller"): ?>
        <link href="<?php echo base_url(); ?>resource/js/bootstrap-editable/css/bootstrap-editable.css"
              rel="stylesheet">
        <script src="<?php echo base_url(); ?>resource/js/bootstrap-editable/js/bootstrap-editable.js"></script>
    <?php endif; ?>
</head>
<body>
    <div class="row-fluid">
        <div class="pull-left col-sm-11">
            <a href="<?php echo base_url(); ?>" id="img_logo_anchor"> <img src="<?php echo base_url()."resource/images/cmdn1.jpg"; ?>" width="70px" height="100px" class="pull-left"/></a>
            <!-- <a href="<?php echo base_url(); ?>" id="img_logo_anchor"> <img src="http://demo.grafioffshorenepal.com/pts_logo.png" width="183px" height="72px" class="pull-left"/></a> -->

            <center>
                <p id="welcome_head"></p>

                <h2 class="main-head" style="color:blue;">
                    CENTER FOR MOLECULAR DYNAMICS - NEPAL(CMDN)
                </h2>
                <h4 style="margin-right: 60px;">Project Tracking And Collaboration System (PTCS)</h4>
                <?php date_default_timezone_set('Asia/Kathmandu'); ?>
                <p class="datetime"><?php echo date('l\, jS \of F Y\, h:i A'); ?></p>
            </center>
        </div>
        <div class="mar20 pull-right " id="head-icons">
            <a href="<?php echo site_url(); ?>"><i class="glyphicon glyphicon-home"></i></a>
            <a href="<?php echo base_url(); ?>index.php/cts_controller/logout"><i
                    class="glyphicon glyphicon-off"></i></a>
        </div>
    </div>
</header>
