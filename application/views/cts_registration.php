<div class="container">
  <h3>Customer Registration Form</h3><br>
   <?php
/* if(isset($result))
{
	if($result==1){
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Successfully Registered!</b></div>";
		}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Registration Failed!</b></div>";
}  
 */
  ?> 
      <table class="table" id="table">

        <form method="post" id="customer_form">
          
          <!---  <tr>
              <td><label>Customer Id.</label></td>
              <td><input type="text" class="form-control" name="cid" required></td>
            </tr>  --->
            <tr>
              <td><label>First Name</label></td>
              <td><input type="text" class="form-control" name="first_name" required></td>
            </tr>
			
			<tr>
              <td><label>Middle Name</label></td>
              <td><input type="text" class="form-control" name="middle_name" ></td>
            </tr>
			
			<tr>
              <td><label>Last Name</label></td>
              <td><input type="text" class="form-control" name="last_name" required></td>
            </tr>
			
			  <tr>
              <td><label>Phone No</label></td>
              <td><input type="text" class="form-control" name="phone"  required></td>
            </tr> 
			
          
            <tr>
              <td colspan="2"><label><b>Address</b></label></td>
               <!--- <td><input type="text" class="form-control" name="address" required></td> --->
            </tr>
          <tr>
              <td><label>Zone</label></td>
              <td><input type="text" class="form-control" name="zone" ></td>
            </tr>
          <tr>
              <td><label>District</label></td>
              <td><input type="text" class="form-control" name="district" required ></td>
            </tr>
          <tr>
              <td><label>VDC/Municipality</label></td>
              <td><input type="text" class="form-control" name="vdc/municipality" ></td>
            </tr>
			
			<tr>
              <td><label>VDC No./Ward No</label></td>
              <td><input type="text" class="form-control" name="vdc/ward_no" ></td>
            </tr>
			
			<tr>
              <td><label>Street</label></td>
              <td><input type="text" class="form-control" name="street" ></td>
            </tr>
			
			<tr>
	          <td><label>Mobile Number </label></td>
              <td><input type="text" class="form-control" name="mobile" ></td>
            </tr>
          
          
			
			<tr>
              <td><label>Email</label></td>
              <td><input type="text" class="form-control" name="email" ></td>
            </tr>
   
            <tr>
        	   <td colspan="2"><button type="button" name="submit" class="btn btn-primary" id="customer_submit">Submit</button></td>
            </tr>
          
        </form>
    </table>
</div><!---end of container-->