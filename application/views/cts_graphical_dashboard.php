<div class="container">
<class="row">
	<div class="col-sm-12">
    	<center><h1  class="dashboard">Dashboard</h1></center>
    </div>
    <div class="dash-filter col-sm-12">
        <div class="col-sm-2 dash-title">
            Statistics
        </div>
        <div class="col-sm-4 col-sm-offset-6 dash-report">
        <form class="report-form pull-right" action="<?php echo site_url('cts_controller/index');?>" method="post">
            <input type="text" class="datepicker" name="date_from" placeholder="from..">
            <input type="text" class="datepicker" name="date_to" placeholder="to..">
            <input class="btn btn-primary" type="submit" name="dash-submit" value="filter">
        </form>
        </div>
    </div>
    <div class="dash-top">
        <div class="col-sm-2">
            <div class="top-number"><span><?php $today = count($today_entries);
			if(isset($total_claim)) echo $total_claim; elseif(isset($today)) echo $today; else echo'0';?></span><p>Total Registered Claim</p></div>
        </div>
        <div class="col-sm-2">
            <div class="top-number"><span><?php if(isset($pending_claim)) echo round($pending_claim); else echo '0';?></span><p>Total Pending Claim</p></div>
        </div>
        <div class="col-sm-2">
            <div class="top-number"><span><?php if(isset($average_claim)) echo round($average_claim); else echo '0';?></span><p>Average Claim Per Day</p></div>
        </div>
        <div class="col-sm-2">
            <div class="top-number"><span><?php if(isset($average_completed)) echo round($average_completed); else echo '0';?></span><p>Avg Completed Claim Per Day</p></div>
        </div>
        <div class="col-sm-2">
            <div class="top-number"><span><?php if(isset($total_paid)) echo $total_paid; else echo '0';?></span><p>Total Paid Claim</p></div>
        </div>
         <div class="col-sm-2">
            <div class="top-number"><span><?php if(isset($paid_amount)) echo $paid_amount; else echo '0';?></span><p>Total Paid Amount</p></div>
        </div>
     </div>
     <div class="col-sm-12 dash-title">
    	Active Document-Stack List
          <hr style="margin-top:5px;">
    </div>
   
    <div id="chartContainer" class="col-sm-6" style="height:400px;width: 50%;"></div>
  	<div id="donchartContainer" style="width: 50%;" class="pull-right"></div>
</div>
</div>
<script type="text/javascript">
var class_count = <?php if(isset($class_count)) 
							echo json_encode($class_count);?>;
 var classes_list = <?php if(isset($classes)) foreach($classes as $key=>$class):
 						$class_names[$key]['label'] =	$class['class_name'];
						$class_names[$key]['y'] = $class_count[$class_names[$key]['label']];
							endforeach;
							echo json_encode($class_names);?>;
	 
      window.onload = function () {
	 
         $(".datepicker").datepicker({ dateFormat: 'yy/mm/dd'});
		  var chart = new CanvasJS.Chart("chartContainer", {
			 zoomEnabled: true,
			
			  panEnabled: true,
			  animationEnabled: true,
			 creditText:"hello",
		  title:{
			text: "No Of Active Claims By Class",
			fontColor: "gray",
			fontSize: "24"            
		  },
		  data: [//array of dataSeries              
			{ //dataSeries object
	
			 /*** Change type "column" to "bar", "area", "line" or "pie"***/
			 type: "column",
			 dataPoints: 
			 <?php if(isset($class_names))echo json_encode($class_names);?>
			
		   }
		   ]
     });

    chart.render();
		    var doughnutchart = new CanvasJS.Chart("donchartContainer",
    		{
			title:{
				text: "Active Files In Department",
				fontColor: "gray",
				fontSize: "24"            
		 	 },
			  data: [
			  {
			   type: "doughnut",
			   dataPoints: [
			   {  y: "<?php if(isset($surveyor_deportation_data)) echo ($surveyor_deportation_data['count']);?>", indexLabel: "Surveyor Deputation" },
			   {  y: "<?php if(isset($claim_note_preparation_data)) echo ($claim_note_preparation_data);?>", indexLabel: "Claim Note Preparation" },
			   {  y: "<?php if(isset($legal_data)) echo ($legal_data);?>", indexLabel: "Legal" },
			   {  y: "<?php if(isset($approve_recommend_data)) echo ($approve_recommend_data);?>", indexLabel: "Approve/Recommend" },
			   {  y:"<?php if(isset($discharge_voucher_generation_data)) echo ($discharge_voucher_generation_data);?>", indexLabel: "DV" },
			    { y: "<?php if(isset($accounts_data)) echo ($accounts_data);?>", indexLabel: "Accounts" }
			   ]
			   }
   		  	]
		  
 			});
			 doughnutchart.render();
          };
</script>