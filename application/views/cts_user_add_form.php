<?php 
$role= array();
$modules= array();
if(isset($roles_info))
{
$i=0;
foreach($roles_info->result_array() as $key=>$value)
{
$role[] = $value;
$module[] = $role[$i]['module_name'];
}
/* print_r($role);
echo "<br>";
print_r($module); */
}
?>

<div class="container">
    <div id="img-icon">
      <img src="<?php echo base_url();?>resource/images/User-icon.jpg" align="left"/>
    </div>
    <div id="breadcumb-text">
      <ol class="breadcrumb">
        <li><strong>User</strong></li>&nbsp;&nbsp;
        <li class="vertical-divider"></li>
        <li class="active">Add User</li>&nbsp;&nbsp;
        <li class="vertical-divider"></li>
		<?php 
		if(strtolower($this->session->userdata('user_position')) == "admin"  || $role[1]['edit'] ==1 || $role[1]['delete'] == 1){
		?>
        <li><a href="<?php echo base_url();?>index.php/cts_controller/show_all_user">List User</a></li>
		<?php } ?>
      </ol>
    </div>
    <hr>
	  <?php

	if(isset($result))
	{
	if($result==1)
	{
	echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Successfully Registered!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Registration Failed!</b></div>";
	} 
  ?>


      <table class="table" id="table">
        <form method="post" role="form" enctype="multipart/form-data" id="user_add_form" action="<?php echo base_url()?>index.php/cts_controller/add_new_user">     
			<tr>
              <td><label>E-mail</label></td>
              <td><input type="email" class="form-control" name="user_email" id="user_email" required></td>
			</tr>
			<tr id="error_msg_tr">
              <td></td><td><div class="error_msg"></div></td>
            </tr>
			<tr>
			<td></td><td>Email will be used as User Name</td>
			</tr>
			
            <tr>
              <td><label>Name</label></td>
              <td><input type="text" class="form-control" name="user_name" required></td>

            </tr>
			
			
			
          <tr>
              <td><label>Password</label></td>
              <td><input type="password" class="form-control" name="user_password" id="user_password" required></td>
            </tr>
			   <tr>
              <td><label>Confirm Password</label></td>
              <td><input type="password" class="form-control" name="confirm_password" id="confirm_password" required/></td>
            </tr>
            <tr>
              <td><label>Address</label></td>
              <td><input type="text" class="form-control" name="user_address" required></td>
            </tr>
            <tr>
              <td><label>Position</label></td>
             <td> <select name="user_position" class="form-control">
						<?php 
						if(isset($position_name))
						foreach($position_name->result() as $row){
							?>
					  <option value="<?php echo $row->position_name ?>"> <?php echo $row->position_name;?> </option>
						  <?php 
							}
							?>
					</select>
			</td>
            </tr>
          
            <tr>
              <td><label>Mobile No.</label></td>
              <td><input type="text" class="form-control" name="user_mobile"></td>
            </tr>

            <tr>
              <td><label>Home Phone No.</label></td>
              <td><input type="text" class="form-control" name="user_phone_home"></td>
            </tr>

            <tr>
              <td><label>Office Phone No.</label></td>
              <td><input type="text" class="form-control" name="user_phone_office"></td>
            </tr>
			<tr>
			<td><label>Select the Department</label></td>
			<td><?php    
			if(isset($department_list))
			foreach( $department_list as $row )
			{
			echo "<input type='checkbox' name='dep_chk_group[]' value='".$row->dep_id."'/> ".$row->dep_name."&nbsp;&nbsp;";
			}
			?></td>
			</tr>
            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary" id="submit" >Submit</button></td>
            </tr>
          
        </form>
    </table>
	</div>
</div><!---end of container-->
</body>
</html>