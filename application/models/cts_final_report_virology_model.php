<?php

class lims_final_report_virology_model extends CI_Model
{

		public function __construct()
	{
		parent::__construct();
		//include('nepali_calendar.php');
	}
	
	public function count_positive($row2)
	{
	$investigation_requested = $this->input->post('investigation_requested');
	if($investigation_requested == 1){
		$cal = new Nepali_Calendar();
		$year = $this->input->post('year');
		$investigation_requested = $this->input->post('investigation_requested');
		$year_arr = explode("/",$year);
		$start_date = $cal->nep_to_eng($year_arr[0],'04','01');
		$start_date = $start_date['year']."-".$start_date['month']."-".$start_date['date'];
		$end_day_of_ashar = $cal->get_third_month_last_day($year_arr[1]);
		$end_year = "20".$year_arr[1];
		$end_date = $cal->nep_to_eng($end_year,'03',$end_day_of_ashar);
		$end_date = $end_date['year']."-".$end_date['month']."-".$end_date['date'];
			$sql= "SELECT COUNT(rapid_anitgen_test) as rat
				FROM  virology
				JOIN sample_registration
				ON sample_registration.sample_id = virology.sample_id
			  Where rapid_anitgen_test = 'positive'
			  AND animal_type='".$row2."'
			  and publish_bit = 1
			  AND sample_registration.date between '".$start_date."' and '".$end_date."'";
			  $result = $this->db->query($sql);

			}
			
			
	elseif ($investigation_requested==2){
	$cal = new Nepali_Calendar();
		$year = $this->input->post('year');
		$investigation_requested = $this->input->post('investigation_requested');
		$year_arr = explode("/",$year);
		$start_date = $cal->nep_to_eng($year_arr[0],'04','01');
		$start_date = $start_date['year']."-".$start_date['month']."-".$start_date['date'];
		$end_day_of_ashar = $cal->get_third_month_last_day($year_arr[1]);
		$end_year = "20".$year_arr[1];
		$end_date = $cal->nep_to_eng($end_year,'03',$end_day_of_ashar);
		$end_date = $end_date['year']."-".$end_date['month']."-".$end_date['date'];
			$sql= "SELECT COUNT(agid) as rat
				FROM  virology
				JOIN sample_registration
				ON sample_registration.sample_id = virology.sample_id
			  Where agid = 'positive'
			  AND animal_type='".$row2."'
			  and publish_bit = 1
			  AND sample_registration.date between '".$start_date."' and '".$end_date."'";
			  $result = $this->db->query($sql);

			  }
			  
	elseif ($investigation_requested==3){
	$cal = new Nepali_Calendar();
		$year = $this->input->post('year');
		$investigation_requested = $this->input->post('investigation_requested');
		$year_arr = explode("/",$year);
		$start_date = $cal->nep_to_eng($year_arr[0],'04','01');
		$start_date = $start_date['year']."-".$start_date['month']."-".$start_date['date'];
		$end_day_of_ashar = $cal->get_third_month_last_day($year_arr[1]);
		$end_year = "20".$year_arr[1];
		$end_date = $cal->nep_to_eng($end_year,'03',$end_day_of_ashar);
		$end_date = $end_date['year']."-".$end_date['month']."-".$end_date['date'];
			$sql= "SELECT COUNT(virology.virology_id) as rat
					FROM  virology
					JOIN sample_registration
					ON sample_registration.sample_id = virology.sample_id
					Where (sample_registration.animal_type = '".$row2."'
					and publish_bit = '1'
					AND sample_registration.date between '".$start_date."' and '".$end_date."')
					and (hi= 'positive'
					or ha = 'positive'
					or rapid_anitgen_test = 'positive')";
					$result = $this->db->query($sql);
				
	}
	
			  return $result;
	
	
	}
	public function count_negative($row2)
	{
	$investigation_requested = $this->input->post('investigation_requested');
	if($investigation_requested==1){
		$cal = new Nepali_Calendar();
		$year = $this->input->post('year');
		$investigation_requested = $this->input->post('investigation_requested');
		$year_arr = explode("/",$year);
		$start_date = $cal->nep_to_eng($year_arr[0],'04','01');
		$start_date = $start_date['year']."-".$start_date['month']."-".$start_date['date'];
		$end_day_of_ashar = $cal->get_third_month_last_day($year_arr[1]);
		$end_year = "20".$year_arr[1];
		$end_date = $cal->nep_to_eng($end_year,'03',$end_day_of_ashar);
		$end_date = $end_date['year']."-".$end_date['month']."-".$end_date['date'];
			$sql= "SELECT COUNT(rapid_anitgen_test) as rat
				FROM  virology
				JOIN sample_registration
				ON sample_registration.sample_id = virology.sample_id
			  Where rapid_anitgen_test = 'negative'
			  AND animal_type='".$row2."'
			  and publish_bit = 1
			  AND sample_registration.date between '".$start_date."' and '".$end_date."'";
			  $result = $this->db->query($sql); 
			  
		}
		
		elseif($investigation_requested == 2)
		{
		$cal = new Nepali_Calendar();
		$year = $this->input->post('year');
		$investigation_requested = $this->input->post('investigation_requested');
		$year_arr = explode("/",$year);
		$start_date = $cal->nep_to_eng($year_arr[0],'04','01');
		$start_date = $start_date['year']."-".$start_date['month']."-".$start_date['date'];
		$end_day_of_ashar = $cal->get_third_month_last_day($year_arr[1]);
		$end_year = "20".$year_arr[1];
		$end_date = $cal->nep_to_eng($end_year,'03',$end_day_of_ashar);
		$end_date = $end_date['year']."-".$end_date['month']."-".$end_date['date'];
			$sql= "SELECT COUNT(agid) as rat
				FROM  virology
				JOIN sample_registration
				ON sample_registration.sample_id = virology.sample_id
			  Where agid = 'negative'
			  AND animal_type='".$row2."'
			  and publish_bit = 1
			  AND sample_registration.date between '".$start_date."' and '".$end_date."'";
			  $result = $this->db->query($sql); 
		}
		
		elseif($investigation_requested == 3)
		{
		$cal = new Nepali_Calendar();
		$year = $this->input->post('year');
		$investigation_requested = $this->input->post('investigation_requested');
		$year_arr = explode("/",$year);
		$start_date = $cal->nep_to_eng($year_arr[0],'04','01');
		$start_date = $start_date['year']."-".$start_date['month']."-".$start_date['date'];
		$end_day_of_ashar = $cal->get_third_month_last_day($year_arr[1]);
		$end_year = "20".$year_arr[1];
		$end_date = $cal->nep_to_eng($end_year,'03',$end_day_of_ashar);
		$end_date = $end_date['year']."-".$end_date['month']."-".$end_date['date'];
			$sql= "SELECT COUNT(virology.virology_id) as rat
					FROM  virology
					JOIN sample_registration
					ON sample_registration.sample_id = virology.sample_id
					Where (sample_registration.animal_type = '".$row2."'
					and publish_bit = '1'
					AND sample_registration.date between '".$start_date."' and '".$end_date."')
					and (hi= 'negative'
					or ha = 'negative'
					or rapid_anitgen_test = 'negative')";
					$result = $this->db->query($sql);
					
				}
			  return $result;
	
	
	}
	
	
		public function final_report_virology_monthly_positive()
	{
		$cal = new Nepali_Calendar();
		$year = $this->input->post('year');
		$year_arr = explode("/",$year);
		$shrawan_end_day = $cal->get_shrawan_last_day($year_arr[1]-1);
		$bhadra_end_day = $cal->get_bhadra_last_day($year_arr[1]-1);
		$ashoj_end_day = $cal->get_ashoj_last_day($year_arr[1]-1);
		$kartik_end_day = $cal->get_kartik_last_day($year_arr[1]-1);
		$magshir_end_day = $cal->get_mangsir_last_day($year_arr[1]-1);
		$poush_end_day = $cal->get_poush_last_day($year_arr[1]-1);
		$magh_end_day = $cal->get_magh_last_day($year_arr[1]-1);
		$falgun_end_day = $cal->get_falgun_last_day($year_arr[1]-1);
		$chaitra_end_day = $cal->get_chaitra_last_day($year_arr[1]-1);
		$baikash_end_day = $cal->get_baisakh_last_day($year_arr[1]);
		$jestha_end_day = $cal->get_jestha_last_day($year_arr[1]);
		$ashar_end_day = $cal->get_third_month_last_day($year_arr[1]);
		
		$dates_arr = array();
		
		$start_date[0] = $cal->nep_to_eng($year_arr[0],'04','01');
		$start_date[1] = $cal->nep_to_eng($year_arr[0],'05','01');
		$start_date[2] = $cal->nep_to_eng($year_arr[0],'06','01');
		$start_date[3] = $cal->nep_to_eng($year_arr[0],'07','01');
		$start_date[4] = $cal->nep_to_eng($year_arr[0],'08','01');
		$start_date[5] = $cal->nep_to_eng($year_arr[0],'09','01');
		$start_date[6] = $cal->nep_to_eng($year_arr[0],'10','01');
		$start_date[7] = $cal->nep_to_eng($year_arr[0],'11','01');
		$start_date[8] = $cal->nep_to_eng($year_arr[0],'12','01');
		$start_date[9] = $cal->nep_to_eng($year_arr[0]+1,'01','01');
		$start_date[10] = $cal->nep_to_eng($year_arr[0]+1,'02','01');
		$start_date[11] = $cal->nep_to_eng($year_arr[0]+1,'03','01');
		
		$end_date[0] = $cal->nep_to_eng($year_arr[0],'04',$shrawan_end_day);
		$end_date[1] = $cal->nep_to_eng($year_arr[0],'05',$bhadra_end_day);
		$end_date[2] = $cal->nep_to_eng($year_arr[0],'06',$ashoj_end_day);
		$end_date[3] = $cal->nep_to_eng($year_arr[0],'07',$kartik_end_day);
		$end_date[4] = $cal->nep_to_eng($year_arr[0],'08',$magshir_end_day);
		$end_date[5] = $cal->nep_to_eng($year_arr[0],'09',$poush_end_day);
		$end_date[6] = $cal->nep_to_eng($year_arr[0],'10',$magh_end_day);
		$end_date[7] = $cal->nep_to_eng($year_arr[0],'11',$falgun_end_day);
		$end_date[8] = $cal->nep_to_eng($year_arr[0],'12',$chaitra_end_day);
		$end_date[9] = $cal->nep_to_eng($year_arr[0]+1,'01',$baikash_end_day);
		$end_date[10] = $cal->nep_to_eng($year_arr[0]+1,'02',$jestha_end_day);
		$end_date[11] = $cal->nep_to_eng($year_arr[0]+1,'03',$ashar_end_day);
		
		$i=0;
		foreach($start_date as $sdate)
		{
		$start_date[$i] = $sdate['year']."-".$sdate['month']."-".$sdate['date'];
		$i++;
		}
		$i = 0;
		foreach($end_date as $edate)
		{
		$end_date[$i] = $edate['year']."-".$edate['month']."-".$edate['date'];
		$i++;
		}
		for($i=0;$i<count($start_date);$i++)
		{
		$investigation_requested = $this->input->post('investigation_requested');
		if ($investigation_requested == 4)
		{
			$sql="SELECT COUNT(virology.virology_id) as virology_positive
					FROM  virology
					JOIN sample_registration
					ON sample_registration.sample_id = virology.sample_id
					Where (publish_bit = '1'
					and sample_registration.date between '".$start_date[$i] ."' and '" . $end_date[$i] ."')
					and (negri_body= 'positive'
					or fat = 'positive'
					or rapid_anitgen_test = 'positive')";
			$result = $this->db->query($sql);
			
		}
		
		elseif ($investigation_requested == 5)
		{
			$sql="SELECT COUNT(virology.virology_id) as virology_positive
					FROM  virology
					JOIN sample_registration
					ON sample_registration.sample_id = virology.sample_id
					Where (publish_bit = '1'
					and sample_registration.date between '".$start_date[$i] ."' and '" . $end_date[$i] ."')
					and (penside_test= 'positive'
					or elisa = 'positive'
					)";
			$result = $this->db->query($sql);
			}
			
			
			foreach($result->result()  as $row)
			{
				$virology_positive[] = $row->virology_positive;
			}
		
		}
		
	$data['positive'] = $virology_positive;
	return $data;
	
}

public function final_report_virology_monthly_negative()
	{
		$cal = new Nepali_Calendar();
		$year = $this->input->post('year');
		$year_arr = explode("/",$year);
		$shrawan_end_day = $cal->get_shrawan_last_day($year_arr[1]-1);
		$bhadra_end_day = $cal->get_bhadra_last_day($year_arr[1]-1);
		$ashoj_end_day = $cal->get_ashoj_last_day($year_arr[1]-1);
		$kartik_end_day = $cal->get_kartik_last_day($year_arr[1]-1);
		$magshir_end_day = $cal->get_mangsir_last_day($year_arr[1]-1);
		$poush_end_day = $cal->get_poush_last_day($year_arr[1]-1);
		$magh_end_day = $cal->get_magh_last_day($year_arr[1]-1);
		$falgun_end_day = $cal->get_falgun_last_day($year_arr[1]-1);
		$chaitra_end_day = $cal->get_chaitra_last_day($year_arr[1]-1);
		$baikash_end_day = $cal->get_baisakh_last_day($year_arr[1]);
		$jestha_end_day = $cal->get_jestha_last_day($year_arr[1]);
		$ashar_end_day = $cal->get_third_month_last_day($year_arr[1]);
		
		$dates_arr = array();
		
		$start_date[0] = $cal->nep_to_eng($year_arr[0],'04','01');
		$start_date[1] = $cal->nep_to_eng($year_arr[0],'05','01');
		$start_date[2] = $cal->nep_to_eng($year_arr[0],'06','01');
		$start_date[3] = $cal->nep_to_eng($year_arr[0],'07','01');
		$start_date[4] = $cal->nep_to_eng($year_arr[0],'08','01');
		$start_date[5] = $cal->nep_to_eng($year_arr[0],'09','01');
		$start_date[6] = $cal->nep_to_eng($year_arr[0],'10','01');
		$start_date[7] = $cal->nep_to_eng($year_arr[0],'11','01');
		$start_date[8] = $cal->nep_to_eng($year_arr[0],'12','01');
		$start_date[9] = $cal->nep_to_eng($year_arr[0]+1,'01','01');
		$start_date[10] = $cal->nep_to_eng($year_arr[0]+1,'02','01');
		$start_date[11] = $cal->nep_to_eng($year_arr[0]+1,'03','01');
		
		$end_date[0] = $cal->nep_to_eng($year_arr[0],'04',$shrawan_end_day);
		$end_date[1] = $cal->nep_to_eng($year_arr[0],'05',$bhadra_end_day);
		$end_date[2] = $cal->nep_to_eng($year_arr[0],'06',$ashoj_end_day);
		$end_date[3] = $cal->nep_to_eng($year_arr[0],'07',$kartik_end_day);
		$end_date[4] = $cal->nep_to_eng($year_arr[0],'08',$magshir_end_day);
		$end_date[5] = $cal->nep_to_eng($year_arr[0],'09',$poush_end_day);
		$end_date[6] = $cal->nep_to_eng($year_arr[0],'10',$magh_end_day);
		$end_date[7] = $cal->nep_to_eng($year_arr[0],'11',$falgun_end_day);
		$end_date[8] = $cal->nep_to_eng($year_arr[0],'12',$chaitra_end_day);
		$end_date[9] = $cal->nep_to_eng($year_arr[0]+1,'01',$baikash_end_day);
		$end_date[10] = $cal->nep_to_eng($year_arr[0]+1,'02',$jestha_end_day);
		$end_date[11] = $cal->nep_to_eng($year_arr[0]+1,'03',$ashar_end_day);
		
		$i=0;
		foreach($start_date as $sdate)
		{
		$start_date[$i] = $sdate['year']."-".$sdate['month']."-".$sdate['date'];
		$i++;
		}
		$i = 0;
		foreach($end_date as $edate)
		{
		$end_date[$i] = $edate['year']."-".$edate['month']."-".$edate['date'];
		$i++;
		}
		for($i=0;$i<count($start_date);$i++)
		{
		$investigation_requested = $this->input->post('investigation_requested');
		if ($investigation_requested == 4)
		{
			$sql="SELECT COUNT(virology.virology_id) as virology_negative
					FROM  virology
					JOIN sample_registration
					ON sample_registration.sample_id = virology.sample_id
					Where (publish_bit = '1'
					and sample_registration.date between '".$start_date[$i] ."' and '" . $end_date[$i] ."')
					and (negri_body= 'negative'
					or fat = 'negative'
					or rapid_anitgen_test = 'negative')";
			$result = $this->db->query($sql);
			
		}
		
		elseif ($investigation_requested == 5)
		{
			$sql="SELECT COUNT(virology.virology_id) as virology_negative
					FROM  virology
					JOIN sample_registration
					ON sample_registration.sample_id = virology.sample_id
					Where (publish_bit = '1'
					and sample_registration.date between '".$start_date[$i] ."' and '" . $end_date[$i] ."')
					and (penside_test= 'negative'
					or elisa = 'negative'
					)";
			$result = $this->db->query($sql);
			}
			
			
			foreach($result->result()  as $row)
			{
				$virology_negative[] = $row->virology_negative;
			}
		
		}
		
	$data['negative'] = $virology_negative;
	return $data;
	
}

public function final_report_whole_virology_positive()
	{
		 $cal = new Nepali_Calendar();
		$year = $this->input->post('year');
		$year_arr = explode("/",$year);
		$shrawan_end_day = $cal->get_shrawan_last_day($year_arr[1]-1);
		$bhadra_end_day = $cal->get_bhadra_last_day($year_arr[1]-1);
		$ashoj_end_day = $cal->get_ashoj_last_day($year_arr[1]-1);
		$kartik_end_day = $cal->get_kartik_last_day($year_arr[1]-1);
		$magshir_end_day = $cal->get_mangsir_last_day($year_arr[1]-1);
		$poush_end_day = $cal->get_poush_last_day($year_arr[1]-1);
		$magh_end_day = $cal->get_magh_last_day($year_arr[1]-1);
		$falgun_end_day = $cal->get_falgun_last_day($year_arr[1]-1);
		$chaitra_end_day = $cal->get_chaitra_last_day($year_arr[1]-1);
		$baikash_end_day = $cal->get_baisakh_last_day($year_arr[1]);
		$jestha_end_day = $cal->get_jestha_last_day($year_arr[1]);
		$ashar_end_day = $cal->get_third_month_last_day($year_arr[1]);
		
		$dates_arr = array();
		
		$start_date[0] = $cal->nep_to_eng($year_arr[0],'04','01');
		$start_date[1] = $cal->nep_to_eng($year_arr[0],'05','01');
		$start_date[2] = $cal->nep_to_eng($year_arr[0],'06','01');
		$start_date[3] = $cal->nep_to_eng($year_arr[0],'07','01');
		$start_date[4] = $cal->nep_to_eng($year_arr[0],'08','01');
		$start_date[5] = $cal->nep_to_eng($year_arr[0],'09','01');
		$start_date[6] = $cal->nep_to_eng($year_arr[0],'10','01');
		$start_date[7] = $cal->nep_to_eng($year_arr[0],'11','01');
		$start_date[8] = $cal->nep_to_eng($year_arr[0],'12','01');
		$start_date[9] = $cal->nep_to_eng($year_arr[0]+1,'01','01');
		$start_date[10] = $cal->nep_to_eng($year_arr[0]+1,'02','01');
		$start_date[11] = $cal->nep_to_eng($year_arr[0]+1,'03','01');
		
		$end_date[0] = $cal->nep_to_eng($year_arr[0],'04',$shrawan_end_day);
		$end_date[1] = $cal->nep_to_eng($year_arr[0],'05',$bhadra_end_day);
		$end_date[2] = $cal->nep_to_eng($year_arr[0],'06',$ashoj_end_day);
		$end_date[3] = $cal->nep_to_eng($year_arr[0],'07',$kartik_end_day);
		$end_date[4] = $cal->nep_to_eng($year_arr[0],'08',$magshir_end_day);
		$end_date[5] = $cal->nep_to_eng($year_arr[0],'09',$poush_end_day);
		$end_date[6] = $cal->nep_to_eng($year_arr[0],'10',$magh_end_day);
		$end_date[7] = $cal->nep_to_eng($year_arr[0],'11',$falgun_end_day);
		$end_date[8] = $cal->nep_to_eng($year_arr[0],'12',$chaitra_end_day);
		$end_date[9] = $cal->nep_to_eng($year_arr[0]+1,'01',$baikash_end_day);
		$end_date[10] = $cal->nep_to_eng($year_arr[0]+1,'02',$jestha_end_day);
		$end_date[11] = $cal->nep_to_eng($year_arr[0]+1,'03',$ashar_end_day);
		
		$i=0;
		foreach($start_date as $sdate)
		{
		$start_date[$i] = $sdate['year']."-".$sdate['month']."-".$sdate['date'];
		$i++;
		}
		$i = 0;
		foreach($end_date as $edate)
		{
		$end_date[$i] = $edate['year']."-".$edate['month']."-".$edate['date'];
		$i++;
		}
		for($i=0;$i<count($start_date);$i++)
		{ 
		
		$ai_sql="SELECT COUNT(rapid_anitgen_test) as all_positive
				FROM  virology
				JOIN sample_registration
				ON sample_registration.sample_id = virology.sample_id
			   Where rapid_anitgen_test = 'positive'
			   and publish_bit = 1
			   AND sample_registration.date between '".$start_date[$i] ."' and '".$end_date[$i]."'";
					
 		$ibd_sql = "SELECT COUNT(agid) as all_positive
				FROM  virology
				JOIN sample_registration
				ON sample_registration.sample_id = virology.sample_id
			    Where agid = 'positive'
			    and publish_bit = 1
			    AND sample_registration.date between '".$start_date[$i]."' and '".$end_date[$i]."'";
				
		$nd_sql = "	SELECT COUNT(virology.virology_id) as all_positive
					FROM  virology
					JOIN sample_registration
					ON sample_registration.sample_id = virology.sample_id
					Where ( publish_bit = '1'
					AND sample_registration.date between '".$start_date[$i]."' and '".$end_date[$i]."')
					and (hi= 'positive'
					or ha = 'positive'
					or rapid_anitgen_test = 'positive')";
		
		$rabies_sql= "SELECT COUNT(virology.virology_id) as all_positive
						FROM  virology
						JOIN sample_registration
						ON sample_registration.sample_id = virology.sample_id
						Where (publish_bit = '1'
						and sample_registration.date between '".$start_date[$i] ."' and '" . $end_date[$i] ."')
						and (negri_body= 'positive'
						or fat = 'positive'
						or rapid_anitgen_test = 'positive')";
						
		$ppr_sql = "SELECT COUNT(virology.virology_id) as all_positive
					FROM  virology
					JOIN sample_registration
					ON sample_registration.sample_id = virology.sample_id
					Where (publish_bit = '1'
					and sample_registration.date between '".$start_date[$i] ."' and '" . $end_date[$i] ."')
					and (penside_test= 'positive'
					or elisa = 'positive'
					)";
					 
		$result = $this->db->query($ai_sql);
		 $result1 = $this->db->query($ibd_sql);
		$result2 = $this->db->query($nd_sql);
		$result3 = $this->db->query($rabies_sql);
		$result4 = $this->db->query($ppr_sql); 
		
			foreach($result->result()  as $row)
			{
			$ai[]  = $row->all_positive;
			}
		 foreach($result1->result()  as $row)
			{
			$ibd[] = $row->all_positive;
			}
		foreach($result2->result()  as $row)
			{
			$nd[] = $row->all_positive;
			}
		foreach($result3->result()  as $row)
			{
			$rabies[] = $row->all_positive;
			}
		foreach($result4->result()  as $row)
			{
			$ppr[] = $row->all_positive;
			} 
		
		
		$data['ai_sql_positive'] = $ai;
		 $data['ibd_sql_positive'] = $ibd;
		$data['nd_sql_positive'] = $nd;
		$data['rabies_sql_positive'] = $rabies;
		$data['ppr_sql_positive'] = $ppr; 
	
		
		}
			return $data;
	}	
public function final_report_whole_virology_negative()
	{
		 $cal = new Nepali_Calendar();
		$year = $this->input->post('year');
		$year_arr = explode("/",$year);
		$shrawan_end_day = $cal->get_shrawan_last_day($year_arr[1]-1);
		$bhadra_end_day = $cal->get_bhadra_last_day($year_arr[1]-1);
		$ashoj_end_day = $cal->get_ashoj_last_day($year_arr[1]-1);
		$kartik_end_day = $cal->get_kartik_last_day($year_arr[1]-1);
		$magshir_end_day = $cal->get_mangsir_last_day($year_arr[1]-1);
		$poush_end_day = $cal->get_poush_last_day($year_arr[1]-1);
		$magh_end_day = $cal->get_magh_last_day($year_arr[1]-1);
		$falgun_end_day = $cal->get_falgun_last_day($year_arr[1]-1);
		$chaitra_end_day = $cal->get_chaitra_last_day($year_arr[1]-1);
		$baikash_end_day = $cal->get_baisakh_last_day($year_arr[1]);
		$jestha_end_day = $cal->get_jestha_last_day($year_arr[1]);
		$ashar_end_day = $cal->get_third_month_last_day($year_arr[1]);
		
		$dates_arr = array();
		
		$start_date[0] = $cal->nep_to_eng($year_arr[0],'04','01');
		$start_date[1] = $cal->nep_to_eng($year_arr[0],'05','01');
		$start_date[2] = $cal->nep_to_eng($year_arr[0],'06','01');
		$start_date[3] = $cal->nep_to_eng($year_arr[0],'07','01');
		$start_date[4] = $cal->nep_to_eng($year_arr[0],'08','01');
		$start_date[5] = $cal->nep_to_eng($year_arr[0],'09','01');
		$start_date[6] = $cal->nep_to_eng($year_arr[0],'10','01');
		$start_date[7] = $cal->nep_to_eng($year_arr[0],'11','01');
		$start_date[8] = $cal->nep_to_eng($year_arr[0],'12','01');
		$start_date[9] = $cal->nep_to_eng($year_arr[0]+1,'01','01');
		$start_date[10] = $cal->nep_to_eng($year_arr[0]+1,'02','01');
		$start_date[11] = $cal->nep_to_eng($year_arr[0]+1,'03','01');
		
		$end_date[0] = $cal->nep_to_eng($year_arr[0],'04',$shrawan_end_day);
		$end_date[1] = $cal->nep_to_eng($year_arr[0],'05',$bhadra_end_day);
		$end_date[2] = $cal->nep_to_eng($year_arr[0],'06',$ashoj_end_day);
		$end_date[3] = $cal->nep_to_eng($year_arr[0],'07',$kartik_end_day);
		$end_date[4] = $cal->nep_to_eng($year_arr[0],'08',$magshir_end_day);
		$end_date[5] = $cal->nep_to_eng($year_arr[0],'09',$poush_end_day);
		$end_date[6] = $cal->nep_to_eng($year_arr[0],'10',$magh_end_day);
		$end_date[7] = $cal->nep_to_eng($year_arr[0],'11',$falgun_end_day);
		$end_date[8] = $cal->nep_to_eng($year_arr[0],'12',$chaitra_end_day);
		$end_date[9] = $cal->nep_to_eng($year_arr[0]+1,'01',$baikash_end_day);
		$end_date[10] = $cal->nep_to_eng($year_arr[0]+1,'02',$jestha_end_day);
		$end_date[11] = $cal->nep_to_eng($year_arr[0]+1,'03',$ashar_end_day);
		
		$i=0;
		foreach($start_date as $sdate)
		{
		$start_date[$i] = $sdate['year']."-".$sdate['month']."-".$sdate['date'];
		$i++;
		}
		$i = 0;
		foreach($end_date as $edate)
		{
		$end_date[$i] = $edate['year']."-".$edate['month']."-".$edate['date'];
		$i++;
		}
		for($i=0;$i<count($start_date);$i++)
		{ 
	
		
		$ai_sql="SELECT COUNT(rapid_anitgen_test) as all_positive
				FROM  virology
				JOIN sample_registration
				ON sample_registration.sample_id = virology.sample_id
			   Where rapid_anitgen_test = 'negative'
			   and publish_bit = 1
			   AND sample_registration.date between '".$start_date[$i] ."' and '".$end_date[$i]."'";
					
 		$ibd_sql = "SELECT COUNT(agid) as all_positive
				FROM  virology
				JOIN sample_registration
				ON sample_registration.sample_id = virology.sample_id
			    Where agid = 'negative'
			    and publish_bit = 1
			    AND sample_registration.date between '".$start_date[$i] ."' and '".$end_date[$i]."'";
				
		$nd_sql = "	SELECT COUNT(virology.virology_id) as all_positive
					FROM  virology
					JOIN sample_registration
					ON sample_registration.sample_id = virology.sample_id
					Where ( publish_bit = '1'
					AND sample_registration.date between '".$start_date[$i] ."' and '".$end_date[$i]."')
					and (hi= 'negative'
					or ha = 'negative'
					or rapid_anitgen_test = 'negative')";
		
		$rabies_sql= "SELECT COUNT(virology.virology_id) as all_positive
						FROM  virology
						JOIN sample_registration
						ON sample_registration.sample_id = virology.sample_id
						Where (publish_bit = '1'
						and sample_registration.date between '".$start_date[$i] ."' and '" . $end_date[$i] ."')
						and (negri_body= 'negative'
						or fat = 'negative'
						or rapid_anitgen_test = 'negative')";
						
		$ppr_sql = "SELECT COUNT(virology.virology_id) as all_positive
					FROM  virology
					JOIN sample_registration
					ON sample_registration.sample_id = virology.sample_id
					Where (publish_bit = '1'
					and sample_registration.date between '".$start_date[$i] ."' and '" . $end_date[$i] ."')
					and (penside_test= 'negative'
					or elisa = 'negative'
					)";
					 
		$result = $this->db->query($ai_sql);
		 $result1 = $this->db->query($ibd_sql);
		$result2 = $this->db->query($nd_sql);
		$result3 = $this->db->query($rabies_sql);
		$result4 = $this->db->query($ppr_sql); 
		
			foreach($result->result()  as $row)
			{
			$ai[]  = $row->all_positive;
			}
		 foreach($result1->result()  as $row)
			{
			$ibd[] = $row->all_positive;
			}
		foreach($result2->result()  as $row)
			{
			$nd[] = $row->all_positive;
			}
		foreach($result3->result()  as $row)
			{
			$rabies[] = $row->all_positive;
			}
		foreach($result4->result()  as $row)
			{
			$ppr[] = $row->all_positive;
			} 
		
		
		$data['ai_sql_negative'] = $ai;
		$data['idb_sql_negative'] = $ibd;
		$data['nd_sql_negative'] = $nd;
		$data['rabies_sql_negative'] = $rabies;
		$data['ppr_sql_negative'] = $ppr; 
		
		
		}
		return $data;
}
}