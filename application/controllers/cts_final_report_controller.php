<?php

class lims_final_report_controller extends CI_controller
{
		public function __construct()
		{
			parent::__construct();
			$this->load->model('lims_final_report_model');
			$this->load->model('lims_investigation_model');
			$this->load->model('lims_department_model');
			$this->load->model('lims_role_model');
			$this->load->model('lims_animal_type_model');
			$this->load->model('lims_login');
		}
		
		public function serology_final_report_execute($lab_id)
		{
			$data['positions'] = $this->lims_role_model->show_all_position_role();
			$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
			$i=0;
			foreach($data['roles_info']->result_array() as $key=>$value)
				{
					$role[] = $value;
					$module[] = $role[$i]['module_name'];
					$i++;
				}
			if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[0]['view'] == 1) 
			{
			$data['departments'] = $this->lims_department_model->getDepartment();
			$data['new_message'] = $this->lims_login->check_new_message();
			$data['year'] = $this->input->get('year');
			$data['test_method'] = $this->input->get('test_method');
			$investigation = $this->input->get('investigation_requested');
			$investigation_result = $this->lims_investigation_model->get_investigation_name($investigation);
			foreach($investigation_result->result() as $row)
			{
			$data['investigation'] = $row->investigation_requested;
			}
			$data['lab_id'] = $lab_id;
			$data['investigation_requested'] = $this->lims_investigation_model->get_lab_investigation($lab_id);
			$data['result_positive'] = $this->lims_final_report_model->final_serology_result_positive();
			$data['result_negative'] = $this->lims_final_report_model->final_serology_result_negative();
			$this->load->view('lims_header');
			$this->load->view('lims_navigation',$data);
			$this->load->view('finalreport/lims_final_report_serology', $data);
			}
			else
			{
			redirect(base_url());
			}
		}
		
		public function serology_final_report($lab_id)
		{
			$data['positions'] = $this->lims_role_model->show_all_position_role();
			$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
			$i=0;
			foreach($data['roles_info']->result_array() as $key=>$value)
				{
					$role[] = $value;
					$module[] = $role[$i]['module_name'];
					$i++;
				}
			if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[0]['view'] == 1) 
			{
			$data['lab_id'] = $lab_id;
			$data['new_message'] = $this->lims_login->check_new_message();
			$data['investigation_requested'] = $this->lims_investigation_model->get_lab_investigation($lab_id);
			$data['departments'] = $this->lims_department_model->getDepartment();
			$this->load->view('lims_header');
			$this->load->view('lims_navigation',$data);
			$this->load->view('finalreport/lims_final_report_serology', $data);
			}
			else
			{
			redirect(base_url());
			}
		}
		
		public function molecular_biology_final_report_execute($lab_id)
		{
			$data['positions'] = $this->lims_role_model->show_all_position_role();
			$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
			$data['test_methods'] = $this->input->get('test_method');
			$i=0;
			foreach($data['roles_info']->result_array() as $key=>$value)
				{
					$role[] = $value;
					$module[] = $role[$i]['module_name'];
					$i++;
				}
			if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[0]['view'] == 1) 
			{
				$data['new_message'] = $this->lims_login->check_new_message();
				$data['departments'] = $this->lims_department_model->getDepartment();
				$data['year'] = $this->input->get('year');
				$investigation = $this->input->get('investigation_requested');
				$investigation_result = $this->lims_investigation_model->get_investigation_name($investigation);
				foreach($investigation_result->result() as $row)
				{
				$data['investigation'] = $row->investigation_requested;
				}
				$data['lab_id'] = $lab_id;
				$data['investigation_requested'] = $this->lims_investigation_model->get_lab_investigation($lab_id);
				$data['result'] = $this->lims_final_report_model->final_molecular_biology_report();
				$this->load->view('lims_header');
				$this->load->view('lims_navigation',$data);
				$this->load->view('finalreport/lims_final_report_molecular_biology',$data);
			}
			else
			{
			redirect(base_url());
			}
		}
		
		public function molecular_biology_final_report($lab_id)
		{
			$data['positions'] = $this->lims_role_model->show_all_position_role();
			$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
			$data['new_message'] = $this->lims_login->check_new_message();
			$i=0;
			foreach($data['roles_info']->result_array() as $key=>$value)
				{
					$role[] = $value;
					$module[] = $role[$i]['module_name'];
					$i++;
				}
			if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[0]['view'] == 1) 
			{
				$data['lab_id'] = $lab_id;
				$data['departments'] = $this->lims_department_model->getDepartment();
				$data['investigation_requested'] = $this->lims_investigation_model->get_lab_investigation($lab_id);
				$this->load->view('lims_header');
				$this->load->view('lims_navigation',$data);
				$this->load->view('finalreport/lims_final_report_molecular_biology',$data);
			}
			else
			{
				redirect(base_url());
			}
		}
		
		public function virology_final_report($lab_id)
		{
			$data['positions'] = $this->lims_role_model->show_all_position_role();
			$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
			$data['new_message'] = $this->lims_login->check_new_message();
			$i=0;
			foreach($data['roles_info']->result_array() as $key=>$value)
				{
					$role[] = $value;
					$module[] = $role[$i]['module_name'];
					$i++;
				}
			if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[0]['view'] == 1) 
			{
			$data['lab_id'] = $lab_id;
			$data['departments'] = $this->lims_department_model->getDepartment();
			$data['investigation_requested'] = $this->lims_investigation_model->get_lab_investigation($lab_id);
			$this->load->view('lims_header');
			$data['departments'] = $this->lims_department_model->getDepartment();
			$this->load->view('lims_navigation',$data);
			$this->load->view('finalreport/lims_final_report_virology',$data); 
		}
		else
			{
				redirect(base_url());
			}
}

public function virology_final_report_display($lab_id)
		{
		$data['positions'] = $this->lims_role_model->show_all_position_role();
			$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
			$data['new_message'] = $this->lims_login->check_new_message();
			$i=0;
			foreach($data['roles_info']->result_array() as $key=>$value)
				{
					$role[] = $value;
					$module[] = $role[$i]['module_name'];
					$i++;
				}
			if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[0]['view'] == 1) 
			{
		$report_type = $this->input->get('investigation_requested');
		
		if ($report_type == "allresult")
		{
			$data['lab_id'] = $lab_id;
			$data['investigation_requested'] = $this->lims_investigation_model->get_lab_investigation($lab_id);
			$data['allresultpositive'] = $this->lims_final_report_model->final_report_whole_virology_positive();
			$data['allresultnegative'] = $this->lims_final_report_model->final_report_whole_virology_negative();
			$this->load->view('lims_header');
			$data['departments'] = $this->lims_department_model->getDepartment();
			$this->load->view('lims_navigation',$data);
			$this->load->view('finalreport/lims_final_report_virology',$data); 
		
		}
		
		else {
		$data['lab_id'] = $lab_id;
		$data['result'] = $this->lims_animal_type_model->show_all_animal_type_for_final_report();
		//$data['result'] = $this->lims_final_report_virology_model->count_positive();
		$data['investigation_requested'] = $this->lims_investigation_model->get_lab_investigation($lab_id);
		$investigation_requested= $this->input->get('investigation_requested');
		
				if($investigation_requested == 1 || $investigation_requested == 2 ||$investigation_requested ==  3){
				$data['lab_id'] = $lab_id;
				$this->load->view('lims_header');
				$data['departments'] = $this->lims_department_model->getDepartment();
				$this->load->view('lims_navigation',$data);
				$this->load->view('finalreport/lims_final_report_virology',$data);
				}
				
				else
				{
					$data['lab_id'] = $lab_id;
					$data['result'] = $this->lims_final_report_model->final_report_virology_monthly_positive();
					$data['results'] = $this->lims_final_report_model->final_report_virology_monthly_negative();
					$data['investigation_requested'] = $this->lims_investigation_model->get_lab_investigation($lab_id);
					$this->load->view('lims_header');
					$data['departments'] = $this->lims_department_model->getDepartment();
					$this->load->view('lims_navigation',$data);
					$this->load->view('finalreport/lims_final_report_virology_monthly', $data);
				
				}
		}
		}
		else
			{
				redirect(base_url());
			}
		}
}


