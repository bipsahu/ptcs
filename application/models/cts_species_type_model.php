<?php

Class lims_species_type_model extends CI_model
{
	public function insert_species_type_details()
	{
		$data = array(
		'species_type' => $this->input->post('species_type')	
		);
		$result = $this->db->get_where('species_type',array('species_type'=>$data['species_type']));
		if($result->num_rows >= 1)
		{
		return false;
		}
		else
		{
		$result = $this->db->insert('species_type', $data);
		$result = $this->db->get_where('species_type',array('species_type_id'=>$this->db->insert_id()));
		return $result;
	}
	}
	
	public function insert_new_species_type()
	{
	$data = array(
	'species_type' => $this->input->post('species_type')	
	);	
		$result = $this->db->get_where('species_type',array('species_type'=>$data['species_type']));
		if($result->num_rows == 1)
		{
		return false;
		}
		else
		{
		$result = $this->db->insert('species_type', $data);
		return $result;
		}
	}
	
	public function edit_species_type_by_id($species_type_id)
		{
		$data = array(
		'species_type' => $this->input->post('species_type')
	
		);
		$result = $this->db->get_where('species_type',array('species_type'=>$data['species_type']));
		if($result->num_rows >= 1)
		{
		return false;
		}
		else
		{
		$this->db->where('species_type_id', $species_type_id);
		$result = $this->db->update('species_type', $data);
		return $result;
	}
}
	public function search_species_type_detail($key)

	{
		$result = $this->db->query("select * from species_type where species_type like '".$key."%'");
		$species_types= array();
		foreach($result->result() as $row)
		{
			$species_type="";
			$species_type.=$row->species_type;		
			$species_types[]=$species_type;
		}
		echo json_encode($species_types);
	}
	public function check_species_type()
	{
		$species_type = $this->input->post('species_type');
		$result = $this->db->get_where('species_type',array('species_type'=>$species_type));
		if($result->num_rows()>=1)
		{
		return true;
		}
		else
		{
		return false;
		}
	}
	public function show_all_species_type($limit, $start)
	{
		$this->db->order_by("species_type_id","asc");
		$this->db->limit($limit, $start);
		$query = $this->db->get('species_type');
		return $query;

	
	}
	
	public function get_details_of_species_type_by_id($species_type_id)
	{
	$result = $this->db->get_where('species_type', array('species_type_id' => $species_type_id));
	return $result->result_array();
	}
	
		public function delete_species_type_by_id($species_type_id)
	{
		$result = $this->db->delete('species_type', array('species_type_id' => $species_type_id)); 
		return $result;
	}

}