<div class="container">
<center><h1>Virology Annual Report</h1></center>
<table class="table" id="table">
<form action="<?php echo base_url();?>index.php/lims_final_report_controller/virology_final_report_display/<?php echo $lab_id;?>" method="get">
<tr><td>
<select name="year" class="form-control">
<option value="2071/72">2071/72</option>
<option value="2072/73">2072/73</option>
<option value="2072/74">2072/74</option>
<option value="2072/75">2072/75</option>
</select></td>

<td>
<select name="investigation_requested" class="form-control">

<option value="allresult"> All Reports</option>
<?php 

foreach( $investigation_requested->result() as $row )
			{ ?>
<option value="<?php echo $row->investigation_requested_id; ?> "><?php echo $row->investigation_requested; ?></option>
<?php } ?>
</select></td>

<td><input type="submit" value="Submit" class="btn btn-primary"></td></tr>
</table>
<?php if(isset($result))

{
?>
</form>
<table class="table table-striped" id="table"><thead>
	<tr><th>Month</th><th>Total Sample Tested</th><th>Positive</th><th>Negative</th></tr></thead>

	
	<tr><td>Shrawan</td><td><?php echo $result['positive'][0]+$results['negative'][0];?></td><td><?php echo $result['positive'][0];?> </td><td><?php echo $results['negative'][0];?></td></tr>

	<tr><td>Bhadra</td><td><?php echo $result['positive'][1]+$results['negative'][1];?></td><td><?php echo $result['positive'][1];?> </td><td><?php echo $results['negative'][1];?></td></tr>

	<tr><td>Ashwin</td><td><?php echo $result['positive'][2]+$results['negative'][2];?></td><td><?php echo $result['positive'][2];?> </td><td><?php echo $results['negative'][2];?></td></tr>

	<tr><td>Kartik</td><td><?php echo $result['positive'][3]+$results['negative'][3];?></td><td><?php echo $result['positive'][3];?> </td><td><?php echo $results['negative'][3];?></td></tr>

	<tr><td>Mangsir</td><td><?php echo $result['positive'][4]+$results['negative'][4];?></td><td><?php echo $result['positive'][4];?> </td><td><?php echo $results['negative'][4];?></td></tr>

	<tr><td>Poush</td><td><?php echo $result['positive'][5]+$results['negative'][5];?></td><td><?php echo $result['positive'][5];?> </td><td><?php echo $results['negative'][5];?></td></tr>

	<tr><td>Magh</td><td><?php echo $result['positive'][6]+$results['negative'][6];?></td><td><?php echo $result['positive'][6];?> </td><td><?php echo $results['negative'][6];?></td></tr>

	<tr><td>Falgun</td><td><?php echo $result['positive'][7]+$results['negative'][7];?></td><td><?php echo $result['positive'][7];?> </td><td><?php echo $results['negative'][7];?></td></tr>

	<tr><td>Chaitra</td><td><?php echo $result['positive'][8]+$results['negative'][8];?></td><td><?php echo $result['positive'][8];?> </td><td><?php echo $results['negative'][8];?></td></tr>

	<tr><td>Baisakh</td><td><?php echo $result['positive'][9]+$results['negative'][9];?></td><td><?php echo $result['positive'][9];?> </td><td><?php echo $results['negative'][9];?></td></tr>

	<tr><td>Jestha</td><td><?php echo $result['positive'][10]+$results['negative'][10];?></td><td><?php echo $result['positive'][10];?> </td><td><?php echo $results['negative'][10];?></td></tr>

	<tr><td>Aashad</td><td><?php echo $result['positive'][11]+$results['negative'][11];?></td><td><?php echo $result['positive'][11];?> </td><td><?php echo $results['negative'][11];?></td></tr>



<?php
}

?>