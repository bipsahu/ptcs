
<div class="container">
    <center><h1>TaskWise HR Report</h1></center>
    <div id="print_btn" class="glyphicon glyphicon-print"></div>
    <div class="col-md-6 col-md-offset-3">
        <form class="form-horizontal">
            <div class="form-group">

                <select id="project" name="project" class="form-control">
                    <option value=" ">Select Project</option>
                    <?php

                    if (isset($projects)):
                        foreach ($projects->result_array() as $project) {
                            // echo '1';die;
                            if(isset($project_id) && $project_id != '' && $project_id == $project['project_id'])
                            {
                                // echo '2';
                                $stat = "selected";
                            }
                            else
                            {
                                // echo '3';
                                // $project_id = '';
                                $stat = '';
                            }
                            echo "<option ".$stat. " value='" . $project['project_id'] . "'>" . $project['project_name'] . "</option>";
                        }
                    endif;
                    ?>
                </select>
            </div>
        </form>
    </div>
    <table class="table table-bordered" id="taskDatatable">
        <caption></caption>
        <!-- <br/><br/>
        <strong id="totalTask" style="clear: both;float:left;font-weight: bold;font-size: 21px"></strong>
        <strong id="totalcomplete" style="clear: both;float:left;font-weight: bold;font-size: 21px"></strong> -->
        <thead>
        <tr id="new_customer_table_row">
            <th>S.N</th>
            <th>Task</th>
            <th>HR Involved</th>
            <th>Total Assigned Hour</th>
            <th>Total Completed Hour</th>
            <th>Total Cost To Company</th>
        </tr>
        </thead>

    </table>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        // $("#taskDatatable").DataTable().destroy();d
            // var project = '<?php echo $project_id;?>';
            // console.log(project);


            // $("#taskDatatable").find("caption").text("Project Name : " + $(this).find("option:selected").text());
            // $("#taskDatatable").DataTable({
            //     "ajax": {
            //         "url": '<?php echo site_url("cts_report_controller/taskwise_hr_report");?>',
            //         "type": "POST",
            //         "data": {"project": project}

            //     },
            //     "columns": [
            //         {"data": "s_no"},
            //         {"data": "task_name"},
            //         {"data": "taskcategoryname"},
            //         {"data": "percent_complete"},
            //         {"data": "status"},
            //         {"data": "cost_allocated"},

            //     ],

            //     "fnInitComplete": function(data) {
            //         // alert(data.json);
            //         // console.log(data.json);
            //         // $("#totalTask").text("Total Task In This Project : " + data.json.data.length);
            //         // $("#totalcomplete").text("Total Percentage Completed : " + data.json.overall_complete);
            //     }

            // });

        $("#project").change(function () {

            //remove existing datatables
            $("#taskDatatable").DataTable().destroy();
            var project = $(this).val();
            $("#taskDatatable").find("caption").text("Project Name : " + $(this).find("option:selected").text());
            $("#taskDatatable").DataTable({
                "ajax": {
                    "url": '<?php echo site_url("cts_report_controller/taskwise_hr_report");?>',
                    "type": "POST",
                    "data": {"project": project}

                },
                "columns": [
                    {"data": "s_no"},
                    {
                        "data": "task_name",
                        // "data": null,
                        // className: "center",
                        // defaultContent: ajax.data.task_name,
                        // defaultContent: '<a href="" class="editor_edit">'+project.task_name+'</a>'
                    },
                    {"data": "no_of_member"},
                    {"data": "total_estimated_hour"},
                    {"data": "total_completed_hour"},
                    {"data": "total_cost_to_company"},

                ],

                "fnInitComplete": function(data) {
                    // console.log(data);
                    // $("#totalTask").text("Total Task In This Project : " + data.json.data.length);
                    // $("#totalcomplete").text("Total Percentage Completed : " + data.json.overall_complete);
                }

            });
            // console.log(project);

        });


        $('#print_btn').click(function () {
            $("#img_logo_anchor").attr('href', '#');
            window.print();
            $('#print_btn').show();
            $("#img_logo_anchor").attr('href', '<?php echo base_url();?>');
            $("#cts_logo_anchor").show();
        });
    });
</script>
<style type="text/css">
    input[type="search"] {
        margin-right: 35px;
    }

    #print_btn {
        position: absolute;
        margin-top: 65px;
        right: 20px;
    }
</style>
<style type="text/css" media="print">
    @page {
        size: auto;
        margin: 0mm;
    }

    body {
        font-weight: bold !important;
        font: Arial !important;
    }

    #cts_logo_anchor {
        display: none;
    }

    .select-report {
        display: none;
    }

    #head-icons {
        display: none;
    }

    #new_customer_tab_length {
        display: none;
    }

    #new_customer_tab_filter {
        display: none;
    }

    #new_customer_tab_info {
        display: none;
    }

    #new_customer_tab_paginate {
        display: none;
    }

    .footer-line {
        display: none;
    }

    #footer {
        display: none;
    }

    #print_btn {
        display: none;
    }
    #project,#task{
        display: none;
    }
    .main-head,.datetime{
        text-align: center;
    }
    table{
        width: 100% !important;
    }
    #taskDatatable_length,#taskDatatable_filter,#taskDatatable_info,#taskDatatable_paginate{
        display: none;
        display: none;
    }
    .main-head {
        font-size: 24px;
        width: 600px;
    }

    #cts_logo_anchor img {
        width: 140px;
    }

    .container h1 {
        font-size: 16px;
        margin-top: 0px;
    }
</style>
</body>
</html>