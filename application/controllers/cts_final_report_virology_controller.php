<?php


class lims_final_report_virology_controller extends CI_controller
{
		public function __construct()
		{
			parent::__construct();
			$this->load->model('lims_final_report_virology_model'); 
			$this->load->model('lims_investigation_model');
			$this->load->model('lims_animal_type_model');
			$this->load->model('lims_sample_model');
		} 
		
		
	public function virology_final_report_display()
		{
			$data['investigation_requested'] = $this->lims_investigation_model->getInvestigationRequested();
			$this->load->view('lims_header');
			$this->load->view('finalreport/lims_final_report_virology',$data); 
		}
		
	
	public function virology_final_report($offset=0)
		{
		$report_type = $this->input->post('investigation_requested');
		
		if ($report_type == "allresult")
		{
			$data['investigation_requested'] = $this->lims_investigation_model->getInvestigationRequested();
			$data['allresultpositive'] = $this->lims_final_report_virology_model->final_report_whole_virology_positive();
			$data['allresultnegative'] = $this->lims_final_report_virology_model->final_report_whole_virology_negative();
			$this->load->view('lims_header');
			$this->load->view('finalreport/lims_final_report_virology',$data); 
		
		}
		
		else {
		$data['offset'] = $offset;
		$data['result'] = $this->lims_animal_type_model->show_all_animal_type_for_final_report();
		//$data['result'] = $this->lims_final_report_virology_model->count_positive();
		$data['investigation_requested'] = $this->lims_investigation_model->getInvestigationRequested();
		$investigation_requested= $this->input->post('investigation_requested');
		
		if($investigation_requested == 1 || $investigation_requested == 2 ||$investigation_requested ==  3){
		$this->load->view('lims_header');
		$this->load->view('finalreport/lims_final_report_virology',$data);
		}
		
		else
		{
			$data['result'] = $this->lims_final_report_virology_model->final_report_virology_monthly_positive();
			$data['results'] = $this->lims_final_report_virology_model->final_report_virology_monthly_negative();
			$this->load->view('lims_header');
			$this->load->view('finalreport/lims_final_report_virology_monthly', $data);
		
		}
		}

}	
}