<div class="container">
    <h3>Edit Department</h3>
    <?php
    if (isset($result)) {
        if ($result == 1) {
            echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Department Created!</b></div>";
        } else
            echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Department Creation Failed!</b></div>";
    }
    ?>
    <?php
    if (isset($department_details)) {
        foreach ($department_details->result() as $row) {
            $dep_details['dep_id'] = $row->dep_id;
            $dep_details['dep_name'] = $row->dep_name;
            $dep_details['parent_id'] = $row->parent_id;
        }
    }
    ?>
    <table class="table" id="table">
        <form method="post" role="form" enctype="multipart/form-data"
              action="<?php echo base_url(); ?>index.php/cts_controller/edit_the_department/<?php echo $dep_details['dep_id']; ?>">
            <tr>
                <td>Department Name</td>
                <td><input type="text" class="form-control" name="dep_name" required
                           value="<?php echo $dep_details['dep_name'] ?>">
            </tr>
           
            <tr>
                <td><input type="submit" name="submit" value="Submit" class="btn btn-primary"></td>
            </tr>
        </form>
    </table>
</div>
</body>
</html>