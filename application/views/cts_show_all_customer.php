<div class="container">
<center><h1>All Customer</h1></center>
<?php
if(!empty($result_indicator))
{
	if($result_indicator == 1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Deletion Successful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Deletion Failed!</b></div>";
}
?>
<table class="table table-striped" id="table_show_all_customer">
<thead>
	<td>S.No.</td>
	<td>
		<ul style="margin-bottom: 0px; padding-left: 0px;">
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">First Name <span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu">
			    <li> <a href="<?php echo base_url()?>index.php/lims_customer_controller/show_all_customer"> Ascending Order </a> </li>
			    <li> <a href="<?php echo base_url()?>index.php/lims_customer_controller/show_all_customer_desc"> Descending Order </a> </li>
			</ul>
		</li>
		</ul>
	</td>
	<td>Middle Name</td>
	<td>
		<ul style="margin-bottom: 0px; padding-left: 0px;">
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">Last Name <span class="caret"></span></a>
		    <ul class="dropdown-menu" role="menu">
		        <li> <a href="<?php echo base_url()?>index.php/lims_customer_controller/show_all_customer_by_last_asc"> Ascending Order </a> </li>
		        <li> <a href="<?php echo base_url()?>index.php/lims_customer_controller/show_all_customer_desc"> Descending Order </a> </li>        
			</ul>
		</li>
		</ul>
	</td>
	<td>Zone</td>
	<td>
		<ul style="margin-bottom: 0px; padding-left: 0px;">
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">District<span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu">
			    <li> <a href="<?php echo base_url()?>index.php/lims_customer_controller/show_all_customer_by_district_asc"> Ascending Order </a> </li>
			    <li> <a href="<?php echo base_url()?>index.php/lims_customer_controller/show_all_customer_by_district_desc"> Descending Order </a> </li>
			</ul>
		</li>
		</ul>
	</td>
	<td>VDC/Municipality</td>
	<td>VDC/Municipality No.</td>
	<td>Street</td>
	<td>Mobile No.</td>
	<td>Phone</td>
	<td>Email</td>
	<td>Edit</td>
	<td>Delete</td>
</thead>

<?php 
$offset = $offset+1;
foreach($results->result() as $row){
?>
<tr>
<td><?php echo $offset;?></td>
<td><?php echo $row->customer_first_name;?></td>
<td><?php echo $row->customer_middle_name;?></td>
<td><?php echo $row->customer_last_name;?></td>
<td><?php echo $row->customer_zone;?></td>
<td><?php echo $row->customer_district;?></td>
<td><?php echo $row->customer_vdc_municipality;?></td>
<td><?php echo $row->customer_vdc_ward_no;?></td>
<td><?php echo $row->customer_street;?></td>
<td><?php echo $row->customer_mobile;?></td>
<td><?php echo $row->customer_phone;?></td>
<td><?php echo $row->customer_email;?></td>
<td><a href="<?php echo base_url()."index.php/lims_customer_controller/edit_customer_by_id_form/".$row->cid;?>"><div class='glyphicon glyphicon-edit'></div></a></td>
<td><a href="<?php echo base_url()."index.php/lims_customer_controller/delete_customer_by_id/".$row->cid;?>"><div class='glyphicon glyphicon-trash'></div></a></td>
</tr>
<?php
$offset++;
}
?>
<tr><td colspan="13"> <?php echo $links; ?></td></tr>
<tr>

<td colspan="9"><a class="btn btn-primary" href="<?php echo base_url();?>index.php/lims_customer_controller/add_new_customer">Add New Customer</a>




</td>
</tr>
</table>
</div>
</body>
</html>