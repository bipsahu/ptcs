<?php
if(isset($result))
{
	if($result==1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Update Succesful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Update Failed!</b></div>";
}
if(isset($firm_details)){
foreach($firm_details as $row)
{
$fid = $row['fid'];
$firm_name = $row['firm_name'];
$zone = $row['firm_zone'];
$district = $row['firm_district'];
$vdc_municipality = $row['firm_vdc_municipality'];
$vdc_ward_no = $row['firm_vdc_ward_no'];
$street = $row['firm_street'];
$mobile = $row['firm_mobile'];
$phone = $row['firm_phone'];
$email = $row['firm_email'];
}
}
?>
  <div class="container">
    <h3>Edit Project</h3><br>
      <table class="table" id="table">

        <form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/lims_customer_controller/edit_firm_by_id/<?php echo $fid;?>">
          
          
            <tr>
              <td><label>Firm Name</label></td>
              <td><input type="text" class="form-control" name="firm_name" value="<?php echo $firm_name;?>" required></td>
            </tr>
	
            <tr>
              <td colspan="2"><label><b>Address</b></label></td>
               <!--- <td><input type="text" class="form-control" name="address" required></td> --->
            </tr>
          <tr>
              <td><label>Zone</label></td>
              <td><input type="text" class="form-control" name="zone" value="<?php echo $zone;?>"></td>
            </tr>
          <tr>
              <td><label>District</label></td>
              <td><input type="text" class="form-control" name="district" value="<?php echo $district;?>" required></td>
            </tr>
          <tr>
              <td><label>VDC/Municipality</label></td>
              <td><input type="text" class="form-control" name="vdc/municipality" value="<?php echo $vdc_municipality;?>"></td>
            </tr>
			
			<tr>
              <td><label>VDC No./Ward No</label></td>
              <td><input type="text" class="form-control" name="vdc/ward_no" value="<?php echo $vdc_ward_no;?>"></td>
            </tr>
			
			<tr>
              <td><label>Street</label></td>
              <td><input type="text" class="form-control" name="street" value="<?php echo $street;?>" ></td>
            </tr>
			
			<tr>
	          <td><label>Mobile Number </label></td>
              <td><input type="text" class="form-control" name="mobile" value="<?php echo $mobile;?>" ></td>
            </tr>
          
            <tr>
              <td><label>Phone No</label></td>
              <td><input type="text" class="form-control" name="phone" value="<?php echo $phone;?>" ></td>
            </tr> 
			
			<tr>
              <td><label>Email</label></td>
              <td><input type="text" class="form-control" name="email" value="<?php echo $email;?>" ></td>
            </tr>
   

            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary">Submit</button></td>
            </tr>
          
        </form>
    </table>
	
</div><!---end of container-->
 </body>
 </html>
 