<?php
Class  Cts_change_password_controller extends CI_Controller
{
		public function __construct()
		{
			parent::__construct();
			$this->load->model('cts_change_password_model');
	
		}
		
		public function index ()
		{
			$this->load->view('login_header');
			$this->load->view('password/cts_change_password');
		
		}
		
		public function generateRandomString($length = 10) {
				$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$randomString = '';
				for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, strlen($characters) - 1)];
			}
				return $randomString;
			}
		
		public function change_password()
		{
			
			$data['result'] = $this->cts_change_password_model->change_email();
		
			if($data['result']->num_rows() == 1)
		{
				
				$this->load->view('login_header');
				/* 
					$ci = get_instance();
					$ci->load->library('email');
					$config['protocol'] = "smtp";
					$config['smtp_host'] = "ssl://smtp.gmail.com";
					$config['smtp_port'] = "465";
					$config['smtp_user'] = "nickesh.maharjan@gmail.com"; 
					$config['smtp_pass'] = "character1";
					$config['charset'] = "utf-8";
					$config['mailtype'] = "html";
					$config['newline'] = "\r\n";

					$ci->email->initialize($config);

					$ci->email->from('nickesh.maharjan@gmail.com', 'Blabla');
					
					$ci->email->to($data['result']->result()[0]->user_email);
					
					$ci->email->subject('change password');
					$ci->email->message('It is working. Great!');
					$ci->email->send(); */
				
				$this->load->library('email');
				
				/* $config['protocol'] = "smtp";
				$config['smtp_host'] = "smtp.subisu.net.np";
				$config['smtp_port'] = "25";
				$config['smtp_user'] = ""; 
				$config['smtp_pass'] = "";
				$config['charset'] = "utf-8";*/
				$config['mailtype'] = "html"; 
				$this->email->initialize($config);
				$result_email = $data['result']->result();
				$this->email->from('info@shikharinsurance.com', 'Shikhar Insurance');
				$this->email->to($result_email[0]->user_email);
				$this->email->subject('Change Password');
				$random_string =  $this->generateRandomString();
				$this->session->set_userdata("random", $random_string);
				$this->email->message("<a href='".base_url()."index.php/cts_change_password_controller/new_password/".$random_string."'>Click here </a>to change your password");

				$this->email->send();
				$this->session->set_userdata('reset_email', $result_email[0]->user_email);
				$data['mail_sent'] = true;
				$this->load->view('password/cts_change_password',$data);
				
		}
			else 
			{
				$data['error'] = "Email address donot Exist.";
				$this->load->view("login_header");
				$this->load->view('password/cts_change_password', $data);
			
			}
		}
		public function new_password($random_string)
		{
			if($random_string == $this->session->userdata('random'))
			{
			$this->load->view('login_header');
			$this->load->view('password/cts_new_password');
			}
		}

		public function update_password()
		{
						
			$update = $this->cts_change_password_model->update_password();
			if($update)
				$data['updated'] = true;
			$this->load->view('login_header',$data);
			$this->load->view('cts_login',$data);
			$this->session->unset_userdata('random_string');
		}

}