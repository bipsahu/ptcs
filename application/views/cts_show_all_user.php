<?php
if(isset($roles_info))
{
$i=0;
$role = array();
foreach($roles_info->result_array() as $key=>$value)
{
$role[] = $value;
$module[] = $role[$i]['module_name'];
$i++;
}
}
?>
<div class="container">
<div id="img-icon">
      <img src="<?php echo base_url();?>resource/images/User-icon.jpg" align="left"/>
    </div>
    <div id="breadcumb-text">
      <ol class="breadcrumb">
        <li><strong>User</strong></li>&nbsp;&nbsp;
        <li class="vertical-divider"></li>
        <li><a href="<?php echo base_url();?>index.php/cts_controller/add_new_user_form">Add User</a></li>&nbsp;&nbsp;
        <li class="vertical-divider"></li>
        <li class="active">List User</li>
      </ol>
    </div>
    <hr>
<?php
if(!empty($result_indicator))
{
	if($result_indicator == 1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Deletion Successful!</b></div>";
	}
	elseif($result_indicator == 0)
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Deletion Failed!</b></div>";
}
?>
<table class="table table-striped" id="table_show_all_user">
<thead>		
				<td>S.No.</td>
				<td>	
					<ul style="margin-bottom: 0px; padding-left: 0px;">
					<li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Name <span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li> <a href="<?php echo base_url()?>index.php/cts_controller/show_all_user"> Ascending Order </a> </li>
			            <li> <a href="<?php echo base_url()?>index.php/cts_controller/show_all_user_desc"> Descending Order </a> </li>
			          </ul>
	       			</li>
	       			</ul>
				</td>
				<td>Address</td>
				<td>Email</td>
				<td>
					<ul style="margin-bottom: 0px; padding-left: 0px;">
					<li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Position <span class="caret"></span></a>
			          <ul class="dropdown-menu" role="menu">
			            <li> <a href="<?php echo base_url()?>index.php/cts_controller/show_all_position_asc"> Ascending Order </a> </li>
			            <li> <a href="<?php echo base_url()?>index.php/cts_controller/show_all_position_desc"> Descending Order </a> </li>  
			          </ul>
	       			</li>
	       			</ul>
	       		</td>
				<td>Mobile No.</td>
				<td>Home Phone No.</td>
				<td>Office Phone No.</td>
				<td>Created Date</td>
				<td>Created By</td>
				<?php 
				if(strtolower($this->session->userdata('user_position'))=="admin")
				{
				echo "<th>Edit</th>";
				}
				elseif($role[1]['edit'])
				{
				echo "<th>Edit</th>";
				}
				?>
				<?php
				if(strtolower($this->session->userdata('user_position')) == "admin")
				{
				echo "<th>Delete</th>";
				}
				elseif($role[1]['delete'])
				{
				echo "<th>Delete</th>";
				}
				?>
</thead>
<?php 
$offset = $offset+1;
foreach($results->result() as $row){
?>
<tbody>
<tr>
<td><?php echo $offset;?></td>
<td><?php echo $row->user_name;?></td>
<td><?php echo $row->user_address;?></td>
<td><?php echo $row->user_email;?></td>
<td><?php echo $row->user_position;?></td>
<td><?php echo $row->user_mobile;?></td>
<td><?php echo $row->user_phone_home;?></td>
<td><?php echo $row->user_phone_office;?></td>
<td><?php echo $row->user_created_date;?></td>
<td><?php echo $row->user_created_by;?></td>
<?php 
if(strtolower($this->session->userdata('user_position'))=="admin")
{
echo "<td><a href='".base_url()."index.php/cts_controller/edit_user_by_id_form/".$row->user_id."'><span class='glyphicon glyphicon-edit'></span></a></td>";
}
elseif($role[1]['edit'])
{
echo "<td><a href='".base_url()."index.php/cts_controller/edit_user_by_id_form/".$row->user_id."'><span class='glyphicon glyphicon-edit'></span></a></td>";
}
if(strtolower($this->session->userdata('user_position'))=="admin")
{
echo "<td><a href='".base_url()."index.php/cts_controller/delete_user_by_id/".$row->user_id."'><span class='glyphicon glyphicon-trash'></span></a></td>";
}
elseif($role[1]['delete'])
{
echo "<td><a href='".base_url()."index.php/cts_controller/delete_user_by_id/".$row->user_id."'><span class='glyphicon glyphicon-trash'></span></a></td>";
}
?>
</tr>
<?php
$offset++;
}

?>

<tr><td colspan="11"> <?php echo $links; ?></td></tr>
<tr>

<td><a class="btn btn-primary" href="<?php echo base_url();?>index.php/cts_controller/add_new_user_form">Add New User</a>



</td>

</tr>
</tbody>
</table>
</div>
</body>
</html>