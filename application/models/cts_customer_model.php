<?php 
class Cts_customer_model extends CI_Model 
{
//This is the main customer model
	public function insert_customer_details()
	{
	$data = array(
	'customer_first_name' => $this->input->post('first_name'),
	'customer_middle_name' => $this->input->post('middle_name'),
	'customer_last_name' => $this->input->post('last_name'),
	'customer_zone' => $this->input->post('zone'),
	'customer_district' => $this->input->post('district'),
	'customer_vdc_municipality' => $this->input->post('vdc/municipality'),
	'customer_vdc_ward_no' => $this->input->post('vdc/ward_no'),
	'customer_street' => $this->input->post('street'),
	'customer_mobile' => $this->input->post('mobile'),
	'customer_phone' => $this->input->post('phone'),
	'customer_email' => $this->input->post('email'),
	);
	$result = $this->db->insert('customer', $data);
		if($this->db->_error_number()==1062)
		{
		echo "Insertion failed";
		}
		else
		{
			$result = $this->db->query("select cid,customer_first_name,customer_last_name,customer_middle_name from customer where cid=".$this->db->insert_id());
			foreach($result->result() as $row)
			{
			$customer_details = array(
			'cid' => $row->cid,
			'customer_first_name' => $row->customer_first_name,
			'customer_last_name' => $row->customer_last_name,
			'customer_middle_name' => $row->customer_middle_name
			);
			}
			echo json_encode($customer_details);
		}
	}
	
	public function show_all_customer($limit, $start)
	{
	$this->db->order_by("customer_first_name","asc");
	$this->db->limit($limit, $start);
	$query = $this->db->get('customer');
	return $query;

	
	}
	
	public function insert_new_customer()
	{
	$data = array(
	'customer_first_name' => $this->input->post('first_name'),
	'customer_middle_name' => $this->input->post('middle_name'),
	'customer_last_name' => $this->input->post('last_name'),
	'customer_zone' => $this->input->post('zone'),
	'customer_district' => $this->input->post('district'),
	'customer_vdc_municipality' => $this->input->post('vdc/municipality'),
	'customer_vdc_ward_no' => $this->input->post('vdc/ward_no'),
	'customer_street' => $this->input->post('street'),
	'customer_mobile' => $this->input->post('mobile'),
	'customer_phone' => $this->input->post('phone'),
	'customer_email' => $this->input->post('email'),
	);
	
	$result = $this->db->insert('customer', $data);
	
	return $result;
	
	}
	
	public function show_all_customer_desc($limit, $start)
	{
	$this->db->order_by("customer_first_name","desc");
	$this->db->limit($limit, $start);
	$query = $this->db->get('customer');
	
       
        return $query;
	
	}
	
	public function show_all_customer_by_last_asc($limit, $start)
	{
	$this->db->order_by("customer_last_name","asc");
	$this->db->limit($limit, $start);
	$query = $this->db->get('customer');
	
        
        return $query;
	
	}
	public function show_all_customer_by_last_desc($limit, $start)
	{
	$this->db->order_by("customer_last_name","desc");
	$this->db->limit($limit, $start);
	$query = $this->db->get('customer');
	
      
        return $query;
	
	}
	public function show_all_customer_by_district_asc($limit, $start)
	{
	$this->db->order_by("customer_district","asc");
	$this->db->limit($limit, $start);
	$query = $this->db->get('customer');
	
        return $query;
	
	}
	public function show_all_customer_by_district_desc($limit, $start)
	{
	$this->db->order_by("customer_district","desc");
	$this->db->limit($limit, $start);
	$query = $this->db->get('customer');
	
      
        return $query;
	
	}

	
	public function get_details_of_customer_by_id($cid)
	{
	$result = $this->db->get_where('customer', array('cid' => $cid));
	return $result->result_array();
	}
	
	public function edit_customer_by_id($cid)
	{
	$data = array(
	
	
	'customer_first_name' => $this->input->post('first_name'),
	'customer_middle_name' => $this->input->post('middle_name'),
	'customer_last_name' => $this->input->post('last_name'),
	'customer_zone' => $this->input->post('zone'),
	'customer_district' => $this->input->post('district'),
	'customer_vdc_municipality' => $this->input->post('vdc/municipality'),
	'customer_vdc_ward_no' => $this->input->post('vdc/ward_no'),
	'customer_street' => $this->input->post('street'),
	'customer_mobile' => $this->input->post('mobile'),
	'customer_phone' => $this->input->post('phone'),
	'customer_email' => $this->input->post('email'),
	);
	$this->db->where('cid', $cid);
	$result = $this->db->update('customer', $data);
	return $result;
	}
	
	public function delete_customer_by_id($cid)
	{
	$result = $this->db->delete('customer', array('cid' => $cid)); 
	return $result;
	}
	public function search_customer($key)
	{
	$result = $this->db->query("select * from customer where customer_first_name like'".$key."%'");
	
	$customers=array();
		foreach($result->result() as $row)
		{
			$customer= "";
			$customer .= $row->cid."/".$row->customer_first_name." ";
			if($row->customer_middle_name != "")
			$customer.=$row->customer_middle_name." ".$row->customer_last_name;
			else
			$customer.=$row->customer_last_name;
			$customer.="/".$row->customer_phone;
			$customers[]= $customer;
		}
	echo json_encode($customers);
	}
	public function check_customer()
	{
	$customer_name = $this->input->post('customer_nam');
	$customer_name_arr = explode(" ",$customer_name);
		if(count($customer_name_arr) == 1)
		return false;
		if(count($customer_name_arr)==2){
		$result = $this->db->get_where('customer',array('customer_first_name'=>$customer_name_arr[0],'customer_last_name'=>$customer_name_arr[1]));
		}
		elseif(count($customer_name_arr)==3){
		$result = $this->db->get_where('customer',array('customer_first_name'=>$customer_name_arr[0],'customer_middle_name'=>$customer_name_arr[1],'customer_last_name'=>$customer_name_arr[2]));
		}
		if($result->num_rows()>=1)
		return true;
		else
		return false;
		
	
	}

}