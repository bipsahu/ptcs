<?php
// echo "<pre>";
// print_r($links);die;
// print_r($results->result_array());die;
if(isset($roles_info))
{
	$i=0;
	$role = array();
	foreach($roles_info->result_array() as $key=>$value)
	{
	$role[] = $value;
	$module[] = $role[$i]['module_name'];
	$i++;
	}
}
?>
<div class="container">
<!-- <div id="img-icon">
      <img class="image-icon" src="<?php echo base_url();?>resource/images/User-icon.jpg" align="left"/>
    </div> -->
    <div id="breadcumb-text">
      <ul class="nav nav-tabs">
        <li class="tab_length"><a href=""><strong>User</strong></a></li>
        <!-- <li class="vertical-divider"></li>&nbsp; -->
		<?php if(strtolower($this->session->userdata('user_position'))=="admin" || $role[1]['add']){?>
        <li class="tab_length"><a href="<?php echo base_url();?>index.php/cts_controller/add_new_user_form">Add User</a></li>&nbsp;
        <!-- <li class="vertical-divider"></li>&nbsp; -->
		<?php } ?>
        <li class="active tab_length"><a href="">List User</a></li>
      </ul>
    </div>
    <hr>
<?php
if(!empty($result_indicator))
{
	if($result_indicator == 1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Deletion Successful!</b></div>";
	}
	elseif($result_indicator == 0)
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Deletion Failed!</b></div>";
}
?>
<table class="table table-striped" id="table_show_all_user">
<thead>		
	<th>S.NO.</th>
	<?php 
	if(strtolower($this->session->userdata('user_position'))=="admin")
	{
		echo "<th>Edit</th>";
	}
	elseif($role[1]['edit'])
	{
		echo "<th>Edit</th>";
	}
	?>
	<th>Name</th>
	<th>Email ( Username )</th>
	<th>position</th>
	<th>Mobile No.</th>
	<th>Home No.</th>
	<th>Office No.</th>
	<th>Created Date</th>
	<th>Created By</th>
	<th>Address</th>
	<?php 
	if(strtolower($this->session->userdata('user_position')) == "admin")
	{
		echo "<th>Delete</th>";
	}
	elseif($role[1]['delete'])
	{
		echo "<th>Delete</th>";
	}
	?>
</thead>
<tbody>
<?php 
$offset = $offset+1;
foreach($results->result() as $row){
?>
<tr>
	<td style="width:10px;"><?php echo $offset;?></td>
	<?php
	if(strtolower($this->session->userdata('user_position'))=="admin")
	{
		echo "<td><a href='".base_url()."index.php/cts_controller/edit_user_by_id_form/".$row->user_id."'><span class='glyphicon glyphicon-edit'></span></a></td>";
	}
	elseif($role[1]['edit'])
	{
		echo "<td><a href='".base_url()."index.php/cts_controller/edit_user_by_id_form/".$row->user_id."'><span class='glyphicon glyphicon-edit'></span></a></td>";
	}?>
	<td><?php echo $row->user_name;?></td>
	<td><?php echo $row->user_email;?></td>
	<td><?php echo $row->user_position;?></td>
	<td><?php echo $row->user_mobile;?></td>
	<td><?php echo $row->user_phone_home;?></td>
	<td><?php echo $row->user_phone_office;?></td>
	<td><?php echo $row->user_created_date;?></td>
	<td><?php echo $row->user_created_by;?></td>
	<td><?php echo $row->user_address;?></td>
	<?php 

	if(strtolower($this->session->userdata('user_position'))=="admin")
	{
		echo "<td><a href='#'><span class='glyphicon glyphicon-trash' alt='".$row->user_id."'></span></a></td>";
	}
	elseif($role[1]['delete'])
	{
		echo "<td><a href='#'><span class='glyphicon glyphicon-trash' alt='".$row->user_id."'></span></a></td>";
	}
	?>
</tr>
<?php
$offset++;
}

?>

<!-- <tr>
	<td colspan="12"> <?php echo $links; ?></td>
</tr> -->
<?php if(strtolower($this->session->userdata('user_position'))=="admin" || $role[1]['add']){?>
<!-- <tr>
<td><a class="btn btn-primary" href="<?php echo base_url();?>index.php/cts_controller/add_new_user_form">Add New User</a></td>
</tr> -->
<?php } ?>
</tbody>
</table>
</div>

<div id="dialog-confirm" title="Delete the User?">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

</body>
<script type="text/javascript">
$(document).ready(function(){
	$('#table_show_all_user').DataTable( {
        "scrollX": true
    } );
	$( "#dialog-confirm" ).hide();

	$('.glyphicon-trash').click(function(){

		var user_id = $(this).attr('alt');
		$( "#dialog-confirm" ).dialog({
			resizable: false,
			height:160,
			modal: true,
			show: {
				effect: "blind",
				duration: 300
			},
			hide: {
				effect: "blind",
				duration: 300
			},
			buttons: {
				"Delete": function() {
					window.location.assign('<?php echo base_url();?>'+'index.php/cts_controller/delete_user_by_id/'+user_id);
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			}
		});
	});
});
</script>
</html>