<?php

/*
 * model class that deals with the database logic for report
 * */

Class cts_report_model extends CI_model
{

    /*
     * project wise report generation
     * @params :  date from, date to
     * @return : boolean
     * */
    public function show_all_project_details($from = NULL, $to = NULL)
    {
        $this->db->select("project.project_id,project.project_name,SUM(cost_allocated) AS cost_allocated,count(task_id) as countTask,GROUP_CONCAT(task_registration.members SEPARATOR ';') AS members ,GROUP_CONCAT(task_registration.indicators SEPARATOR ';') AS indicators,GROUP_CONCAT(task_registration.percent_complete SEPARATOR ';') AS percent_complete,GROUP_CONCAT(task_registration.status SEPARATOR ';') AS status");
        $this->db->from('project');
        $this->db->join('task_registration', 'task_registration.project = project.project_id', 'inner');
        $this->db->group_by('task_registration.project');
        $this->db->order_by("project.project_id", "desc");

        if (isset($from) && $from != NULL && isset($to) && $to != NULL) 
        {
            $from = str_replace('/', '-', $from);
            $to = str_replace('/', '-', $to);
            $this->db->where("task_registration.intimation_date >=", $from);
            $this->db->where("task_registration.intimation_date <=", $to);
        }
        $result = $this->db->get();
        $result = $result->result_array();
// echo "<pre>";
//         print_r($result);
        foreach ($result as $resultKey => $res) 
        {
            $project_id = $res['project_id'];
            // echo $project_id;die;
            $result_task_category = $this->show_all_task_category_details($project_id);
            $all_task_category_weightage = $this->db->query(" SELECT project_taskcategory FROM project where project_id = $project_id")->result_array();

            $all_task_category_weightage = json_decode($all_task_category_weightage[0]['project_taskcategory'],true);
            // echo "<pre>";
            // print_r($all_task_category_weightage[3]);die;
            $sum_task_based_percent_complete = 0;
            foreach($result_task_category['data'] as $result_task_categories)
            {
                $task_category_id = $result_task_categories['task_category_id'];
                $task_category_weightage = $all_task_category_weightage[$task_category_id];
                $task_based_percent_complete = $result_task_categories['cat_com'] * $task_category_weightage/100;
                $sum_task_based_percent_complete = $sum_task_based_percent_complete + $task_based_percent_complete;
            }
            //explode grouped data of user:hour json format into array
        // echo "<pre>";
        // print_r($sum_task_based_percent_complete);die;
        $result[$resultKey]["task_based_percent_complete"] = $sum_task_based_percent_complete;
            $membersResult = explode(";", $res["members"]);

            $result[$resultKey]["total_hours"] = 0;

            //calculate total hours assigned from the json data
            foreach ($membersResult as $member) {
                if (isset($member) && $member != "null")
                    $result[$resultKey]["total_hours"] += array_sum(json_decode($member, true));
            }

            //calculate average of percent complete of all the task assigned to a project
            $result[$resultKey]["percent_complete"] = round(array_sum(explode(";", $res["percent_complete"])) / $res["countTask"]);

            //check for status, flip to check if all values 0,1 or 2
            $statusArray = explode(";", $res["status"]);
            $status = array_flip($statusArray);
            if (count($status) == 1 && array_key_exists(0, $status)) {
                $result[$resultKey]["status"] = "<span class='red'>Inactive</span>";
            } elseif (in_array("1", $statusArray))
                $result[$resultKey]["status"] = "<span class='yellow'>On Going</span>";
            elseif (count($status) == 1 && array_key_exists(2, $status)) {
                $result[$resultKey]["status"] = "<span class='green'>Complete</span>";
            } else {
                $result[$resultKey]["status"] = "Inactive";
            }

        }
        // print_r($result);die;
        return $result;
    }

    /*
     * return json format data to task report which is to be loaded in datatable
     * @params : NULL
     * @return : json
     * */
    public function show_all_task_details($projectID = '')
    {
        if($projectID == '')
        {
            $projectID = $this->input->post('project');
            if(isset($_POST['task_category']))
            {
                $task_category = $_POST['task_category'];
                $this->db->where('task_registration.task_category_id', $task_category);
            // print_r($_POST);die;
            }
        }
        // if(($this->input->post('task_category_id')))
        // {
        //     echo $this->input->post('task_category');die;
        // }   

        $this->db->where('task_registration.project', $projectID);
        $this->db->order_by("task_registration.task_id", "desc");
        $resultArray = $this->db->get("task_registration")->result_array();
        $count = 1;
        $totalsum = 0;
        foreach ($resultArray as $key => $result) {
            $t_id = $result['task_category_id'];
            $taskcategoryname = $this->db->query("SELECT name FROM task_category where t_category_id = $t_id")->result_array();
            // print_r($taskcategoryname);die;
            $resultArray[$key]['taskcategoryname'] = $taskcategoryname[0]['name'];
            $totalsum = $totalsum + $resultArray[$key]["percent_complete"];
            // print_r($resultArray);die;
            $resultArray[$key]["s_no"] = $count;
            $count++;
            if ($result["status"] == "0") {
                $resultArray[$key]["status"] = "<span class='red'>Inactive</span>";
            } elseif ($result["status"] == "1") {
                $resultArray[$key]["status"] = "<span class='yellow'>On Going</span>";
            } elseif ($result["status"] == "2") {
                $resultArray[$key]["status"] = "<span class='green'>Complete</span>";
            } else {

            }
        }
        $result["data"] = $resultArray;
        // print_r($resultArray);die;
        $result["overall_complete"] = number_format($totalsum/($count-1),2);
        // print_r($result);die;

        return $result;
    }


    /*
     * return json format data to indicator report which is to be loaded in datatable
     * @params : NULL
     * @return : json
     * */
    public function show_all_indicator_details()
    {
        $taskId = $this->input->post('task');

        $this->db->where('task_registration.task_id', $taskId);

        $this->db->order_by("task_registration.task_id", "desc");
        $taskResults = $this->db->get("task_registration")->result_array();
        $indicatorResult = array();
        foreach ($taskResults as $taskKey => $taskResult) 
        {
            $count = 1;
            // print_r($taskResult);
            $indicatorAchieved = json_decode($taskResult['achieved'], true);
            foreach (json_decode($taskResult["indicators"], true) as $indicatorKey => $indicator) 
            {
                $this->db->where('indicators.indicator_id', $indicatorKey);
                $indicatorResult[$indicatorKey] = $this->db->get("indicators")->result_array()[0];
                $indicatorResult[$indicatorKey]["weightage"] = $indicator;
                $indicatorResult[$indicatorKey]["achieved"] = $indicatorAchieved[$indicatorKey];
                $indicatorResult[$indicatorKey]["s_no"] = $count;
                $count++;
            }
        }
        // print_r($indicatorResult);die;
        $result["data"] = array_values($indicatorResult);

        return $result;
    }


    public function show_all_task_category_details($project_id = '')
    {
        $pass_data = array();
        if($project_id == '')
        {
            $project_id = $this->input->post('project');
        }
        $tc_num = $this->db->query("SELECT project_taskcategory FROM project WHERE project_id = '$project_id'")->result_array();
        // print_r($tc_num);
        $tc_nums = json_decode($tc_num[0]['project_taskcategory'],true);
        foreach ($tc_nums as $key => $value) {
            $check = $this->db->query("SELECT * FROM task_registration where project = '$project_id' AND  task_category_id = $key")->result_array();
            if(empty($check))
            {
            // print_r($check);
                $taskcategory = $this->db->query("SELECT * FROM task_category where t_category_id = $key")->result_array();
                $get_value['id'] = $key;
                $get_value['weightage'] = $value;
                $get_value['name'] = $taskcategory[0]['name'];
                $pass_data[] = $get_value;
            }
            # code...
        }
       
        if($tc_num)
        {
            $tc_number = count($tc_nums);
        }
        else
        {
            $tc_number = 0;
        }
        // echo $tc_number; die;
        $this->db->join('task_category','task_category.t_category_id = task_registration.task_category_id','left');
        $this->db->where('task_registration.project', $project_id);
        $this->db->group_by('task_registration.task_category_id');
        $this->db->order_by("task_registration.task_category_id", "desc");
        $da = $this->db->get("task_registration")->result_array();

        $this->db->select('project_taskcategory');
        $this->db->where('project.project_id', $project_id);
        $project_info = $this->db->get("project")->result_array();
        $task_category_weight = json_decode($project_info[0]['project_taskcategory'],true);
        $count = 1;
        $totalsum = 0;
        foreach ($da as $key => $dat) 
        {
            $project_id = $dat['project'];
            $task_category_id = $dat['task_category_id'];
            
            $query = $this->db->query("SELECT *,avg(percent_complete) as cat_com ,sum(cost_allocated) as cost_allocated FROM task_registration WHERE project = $project_id AND task_category_id = $task_category_id group by task_category_id")->result_array();
$task_detail = $this->db->query("SELECT task_name, percent_complete FROM task_registration where project = $project_id and task_category_id = $task_category_id")->result_array();
            $show_task_detail[$key] = '';
            foreach ($task_detail as $tkey => $tvalue) 
            {
                # code...$line
                if($show_task_detail[$key] == '')
                {
                    $line_break = '';
                }
                else
                {
                    $line_break = "<br>";
                }
                $show_task_detail[$key] = $show_task_detail[$key].$line_break.$tvalue['task_name'].' <span class="red">( '.$tvalue['percent_complete'].'%'.' ) </span>'; 

            }
        // print_r($query[0]);die;
           $resultArray[$key] = $query[0];
$resultArray[$key]['task_detail']= $show_task_detail[$key];
           $resultArray[$key]['cat_com'] = number_format($resultArray[$key]['cat_com'],1);
           $resultArray[$key]['serial_no'] = $count;
           $resultArray[$key]['tast_cat_name'] = $dat['name'];
           $resultArray[$key]['task_category_weight'] = $task_category_weight[$task_category_id];
           $resultArray[$key]['actual_cat_complete'] = $resultArray[$key]['task_category_weight']*$resultArray[$key]['cat_com']/100;
           $totalsum = $totalsum + $resultArray[$key]['actual_cat_complete'];

            if ($dat["status"] == "0") {
                $resultArray[$key]["status"] = "<span class='red'>Inactive</span>";
            } elseif ($dat["status"] == "1") {
                $resultArray[$key]["status"] = "<span class='yellow'>On Going</span>";
            } elseif ($dat["status"] == "2") {
                $resultArray[$key]["status"] = "<span class='green'>Complete</span>";
            } else {

            }
            $count++;
        }
      // print_r($resultArray);die;
        $result["data"] = $resultArray;
        $result['overall_complete'] = number_format($totalsum,2);
        $result['tc_number'] = $tc_number;
        $result['pass_data'] = $pass_data;
        // print_r($result);die;
        // $result["data"] = array('1' => '2');
        return $result;
    }

    /*
     * Generates total completed projects month wise
     * @return array
     */
    public function project_month_report()
    {
        $sql = "SELECT COUNT( i.project_name ) AS completed_projects,
                i.month,i.STATUS as status, i.year FROM ( SELECT project.project_name ,
                 SUM( cost_allocated ) AS cost_allocated, COUNT( task_id ) AS countTask,
                  YEAR(task_registration.created_at) AS YEAR,
                  GROUP_CONCAT( task_registration.members SEPARATOR ';' ) AS members,
                  GROUP_CONCAT( task_registration.indicators SEPARATOR ';' ) AS indicators,
                  -- GROUP_CONCAT( task_registration.percent_complete SEPARATOR ';' ) AS percent_complete,
                   GROUP_CONCAT( task_registration.status SEPARATOR ';' ) AS STATUS ,
                    DATE_FORMAT( task_registration.created_at, '%M' ) AS MONTH
                    FROM ( project ) INNER JOIN task_registration ON task_registration.project = project.project_id
                    GROUP BY task_registration.project ORDER BY project.project_id DESC ) AS i
                     WHERE i.year = YEAR(CURDATE()) GROUP BY i.month";
        $result = $this->db->query($sql);
        $result = $result->result_array();
        // print_r($result);die;
        return $result;

    }

    public function show_hr_project()
    {
        // echo "<pre>";
        $this->db->select('*');
        $this->db->from('project');
        $this->db->join('task_registration','project.project_id = task_registration.project','inner');
        $this->db->group_by('project.project_id');
        $result = $this->db->get()->result_array();
        foreach ($result as $key => $value) 
        {
            $project_id = $value['project_id'];
            $result_members = [];
            $total_hour_sum = 0;
            $total_completed_hour_sum = 0;
            $total_cost_to_company = 0;
            $project_task = $this->db->query("select members,completed_hour from task_registration where project = '$project_id'")->result_array();
            // print_r($project);die;
            foreach ($project_task as $value1) 
            {
                $members = json_decode($value1['members'],true);
                if($value1['completed_hour'] != '')
                {
                    $comp_hour = json_decode($value1['completed_hour'],true);
                    $sum_completed_hour = array_sum($comp_hour);
                    $total_completed_hour_sum = $total_completed_hour_sum + $sum_completed_hour;
                    foreach ($comp_hour as $ckey => $cvalue) {
                        $array_per_hr_cost = $this->db->query("SELECT hourly_salary FROM user WHERE user_id = $ckey")->result_array();
                        $per_hr_cost = $array_per_hr_cost[0]['hourly_salary']; 
                        // print_r($per_hr_cost);
                        $ctc = $cvalue * $per_hr_cost;
                        $total_cost_to_company = $total_cost_to_company + $ctc;
                    }
                }
                $hour_sum = array_sum($members);
                $total_hour_sum = $total_hour_sum + $hour_sum;
                // $result_members = array_merge($result_members, $members);
                $result_members = $result_members + $members;
                // print_r($members);
            }
            // $count_member_involved = $count($result_members);
            $pass[$key]['project_id'] = $value['project_id'];
            $pass[$key]['project_name'] = $value['project_name'];
            $pass[$key]['no_of_member'] = count($result_members);
            $pass[$key]['total_estimated_hour'] = $total_hour_sum;
            $pass[$key]['total_completed_hour'] = $total_completed_hour_sum;
            $pass[$key]['total_cost_to_company'] = $total_cost_to_company;
            // die;
        }
        // print_r($pass);
        // die;
        return $pass;
    } 

    public function show_taskwise_hr_report()
    {
        // print_r($_POST);
        $project_id = $_POST['project'];
        $count = 1;
        $task_detail = $this->db->query("SELECT * FROM task_registration WHERE project = '$project_id'")->result_array();
        // print_r($task_detail);
        foreach ($task_detail as $key => $value) {
            // $dd = json_decode($value['members']);
            // print_r($dd);
            $members_assignment = json_decode($value['members'],true);
            $total_assigned_hour = array_sum($members_assignment);
            $no_of_member = count($members_assignment);
            $total_cost_to_company = 0;
            if($value['completed_hour']!= '')
            {
            // print_r($value['completed_hour']);
                $completed_assignment = json_decode($value['completed_hour'],true);
                foreach ($completed_assignment as $akey => $avalue) {
                    $salary_hr = $this->db->query("SELECT hourly_salary FROM user WHERE user_id = '$akey'")->result_array();
                    $cost_to_company = $avalue * $salary_hr[0]['hourly_salary'];
                    $total_cost_to_company = $total_cost_to_company + $cost_to_company;
                    # code...
                }
                // print_r($completed_assignment);
                $total_completed_hour = array_sum($completed_assignment);
            }
            else
            $total_completed_hour = 0;

            $resultArray[$key]['s_no'] = $count;
            // $resultArray[$key]['task_id'] = $value['task_id'];
            $resultArray[$key]['task_name'] = $value['task_name'];
            $resultArray[$key]['no_of_member'] = $no_of_member;
            $resultArray[$key]['total_estimated_hour'] = $total_assigned_hour;
            $resultArray[$key]['total_completed_hour'] = $total_completed_hour;
            $resultArray[$key]['total_cost_to_company'] = $total_cost_to_company;
            $count++;
        }
        // echo "<pre>";
        // print_r($resultArray);die;
        // die;
        $result['data'] = $resultArray;
        return $result;
    }

}