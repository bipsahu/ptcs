<style type="text/css">
    .ui-tabs .ui-tabs-nav
{
background: white;
border-color: white;
}

.ui-tabs .ui-tabs-panel /* just in case you want to change the panel */
{
/*background: blue;*/
}
.products-admin {

margin-top: 0px;
/*margin-left: 5px;*/
padding-left: 0px;
}

.table {
    width: 85%;
    margin-top: 30px;
}

.table label{
    font-size: 13px;
}


.form-control {
    /*height:30px;*/
    /*width: 75%;*/
}

.ms-parent{
    /*width: 75% !important;*/
} 

.ui-widget-content
{
    text-align:none;
}
</style>
<div class="container">
    <?php if ($this->session->userdata('user_position') == 'Entry Person') {
        ?>
        <h1 style="float:left;">Today's Entries</h1>
        <a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/cts_claim_controller/show_all_claim"
           style="float:right; margin-top: 10px; margin-right: 10px; ">Show All claim</a>
        <a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/cts_report_controller"
           style="float:right; margin-top: 10px; margin-right: 10px; ">Reports</a>
        <a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/cts_claim_controller"
           style="float:right; margin-top: 10px; margin-right: 10px; ">Register New claim</a>

        <table class="table table-striped datatable" id="today-entry">
            <thead>
            <tr id="new_customer_table_row">
                <th>Regd No.</th>
                <th>DOI</th>
                <th>Claim No.</th>
                <th>Branch</th>
                <th>Class</th>
                <th>Vehicle No.</th>
                <th>Insure</th>
                <th>Policy No.</th>
                <th>Estimated Amount</th>
                <th>Remarks</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = count($today_entries['result']->result());
            foreach ($today_entries['result']->result() as $row) {
                ?>
                <tr>
                    <td><?php echo $row->regd_no; ?></td>
                    <td><?php echo $row->intimation_date; ?></td>
                    <td><?php echo $row->claim_no; ?></td>
                    <td><?php echo $row->claim_branch; ?></td>
                    <td><?php echo $row->class; ?></td>
                    <td><?php echo $row->vehicle_no; ?></td>
                    <td><?php echo $row->insure; ?></td>
                    <td><?php echo $row->policy_no; ?></td>
                    <td><?php echo $row->claim_amount; ?></td>
                    <td><?php echo $row->remarks; ?></td>

                </tr>
                <?php
                $i--;
            }
            ?>
            </tbody>
        </table>
    <?php } elseif ($this->session->userdata('user_position') == 'Admin' || $this->session->userdata('position_id') == '2') {
        ?>
        <!--main dashboard starts here -->
        <div class="row">
            <div class="col-sm-7">

                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">Dashboard</a></li>
                        <?php //if($this->session->userdata('user_position') == 'Admin'){?>

                        <li><a href="#tabs-2">Set Meeting</a></li>
                         <?php //  }  ?>
                        <li><a href="#tabs-3">My Meeting</a></li>
                    </ul>
                    <div id="tabs-1">
                        <?php if($this->session->userdata('user_position') == 'Admin'){?>
                        <ul class="products products-admin">
                        <!-- <h1>Dashboard</h1> -->
                            <li>
                                <a href="<?php echo base_url(); ?>index.php/cts_controller/dashboard_info"
                                   class="dashboard">
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo site_url("cts_task_controller/show_all_task"); ?>" class="add_task">

                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>index.php/cts_task_controller/showTask"
                                   class="add_update">
                                </a>
                            </li>
                            <br>
                           
                            <li>
                                <a href="<?php echo base_url(); ?>index.php/cts_controller/add_new_user_form" class="add_user">
                                </a>
                            </li>
                           
                            <li>
                                <a href="<?php echo base_url(); ?>index.php/cts_controller/show_all_project"
                                   class="add_project">
                                </a>
                            </li>
                               
                            <!-- more list items -->
                                
                            <li>
                                <a href="<?php echo base_url(); ?>index.php/cts_report_controller"
                                   class="add_report">
                                </a>
                            </li>
                            <br>

                           
                            <li>
                                <!-- <a href="<?php echo base_url(); ?>index.php/cts_role_controller/add_new_role_display" -->
                                   <!-- class="add_indicator"> -->
                                <!-- </a> -->
                                <a href="<?php echo base_url(); ?>index.php/cts_controller/add_indicator_form"
                                   class="add_indicator">
                                </a>
                            </li>

                            
                            
                        </ul>
                        <?php  }  
                        else
                        {?>
                        <ul class="products products-admin">
                        <!-- <h1>Dashboard</h1> -->
                            <li>
                                <a href="<?php echo base_url(); ?>index.php/cts_controller/dashboard_info"
                                   class="dashboard">
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>index.php/cts_report_controller"
                                   class="add_report">
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>index.php/cts_task_controller/showTask"
                                   class="add_update">
                                </a>
                            </li>
                            <br/> 
                            <li>
                                <a href="<?php echo base_url(); ?>index.php/cts_controller/add_indicator_form" class="add_indicator">
                                </a>
                            </li>
                        </ul>
                       <?php }
                        ?>
                    </div>
                    <?php //if($this->session->userdata('user_position') == 'Admin'){?>
                    <div id="tabs-2">
                        <form method="post" action="<?php echo base_url() . 'index.php/cts_controller/insert_meeting_detail'; ?>">
                            <table class="table" style="width: 100%">
                                <?php date_default_timezone_set("UTC"); ?>
                                <tr>
                                    <td class=""><label>Meeting Date*</label></td>
                                    <td>
                                        <input type="text" class="form-control" tabindex="0" name="task[meeting_date]" required id="datepicker" value="">
                                    </td>
                                </tr>

                                <tr>
                                    <td class=""><label>Meeting Time*</label></td>
                                    <td><input type="text" class="form-control ui-timepicker-input" tabindex="0" name="task[meeting_time]" required id="timepicker" value="<?php if (isset($edit_id) && $edit_id != NULL) echo $task_detail[0]['meeting_time']; ?>">
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=""><label>Meeting Title.*</label></td>
                                    <td><input type="text" class="form-control" tabindex="1" name="task[meeting_title]" required value="<?php if (isset($edit_id) && $edit_id != NULL) echo $task_detail[0]['meeting_title']; ?>">
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=""><label>Meeting Location.*</label></td>
                                    <td><input type="text" class="form-control" tabindex="1" name="task[meeting_location]" required value="<?php if (isset($edit_id) && $edit_id != NULL) echo $task_detail[0]['meeting_title']; ?>">
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=""><label>Meeting Details.</label></td>
                                    <td><textarea class="form-control" tabindex="1" name="task[meeting_details]" value="<?php if (isset($edit_id) && $edit_id != NULL) echo $task_detail[0]['meeting_title']; ?>"></textarea>
                                    </td>
                                    <td></td>
                                </tr>
                                
                                <tr>
                                    <td><label>Team Members*</label></td>
                                    <td style="text-align: left;"><select type="text" id="multiple-select-team" multiple="multiple" name="task[member][]" required multiple="multiple">

                                            <?php if (isset($users)):
                                                //convert json encoded data to array and check if key of array and user id matches
                                                $editUsers = json_decode($task_detail[0]["members"], true);

                                                foreach ($users->result_array() as $user) {

                                                    //assign key-value pair to users/members id and name respectively
                                                    $keyValueUsers[$user['user_id']] = $user['user_name'];

                                                    //check for selection for users/members
                                                    $selected = array_key_exists($user['user_id'], $editUsers) ? "selected" : "";
                                                    echo "<option value='" . $user['user_id'] . "'" . $selected . ">" . $user['user_name'] . "</option>";
                                                }
                                            endif; ?>
                                        </select>
                                    </td>
                                    <td>
                                    

                                    </td>
                                   
                                    <td style="text-align: right;">
                                        <button type="submit" name="submit" class="btn btn-primary" id="submit">Submit</button>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </form>
                    </div>
                         <?php //  }  ?>
                    <div id="tabs-3">
                        <table class="table table-striped" id="department_table">
                            <thead style="font-size: 13px;">
                                <!-- <th>S NO.</th> -->
                                <th>Date</th>
                                <th>Time</th>
                                <th>Title</th>
                                <th>Participant</th>
                                <th>Status</th>
                                <th>Operation</th>
                            </thead>

                            <tbody style="font-size: 13px;">
                              
                                <?php $count = 1; 
                                if(!empty($meeting_info))
                                { 
                                    foreach ($meeting_info as $key => $value) 
                                    { 
                                        // echo "<pre>";
                                        // print_r($value['assigned_to']);
                                    # code...?>
                                        <tr>
                                            <!-- <td><?php echo $count;$count++  ?></td> -->
                                            <td><?php echo $value['meeting_date'];?></td> 
                                            <td><?php echo $value['meeting_time'];?></td> 
                                            <td><?php echo $value['meeting_title'];?></td> 
                                            <td>
                                                <?php 
                                                foreach($value['assigned_to'] as $key => $value1)
                                                {
                                                    if($value1['status'] == 2)
                                                    {
                                                        $sat = "Not Confirmed";
                                                    }
                                                    else if($value1['status'] == 1)
                                                    {
                                                        $sat = "Accepted";
                                                    }
                                                    else if($value1['status'] == 0)
                                                    {
                                                        $sat = "Rejected";
                                                    }
                                                    echo $value1['user_name'].' ( '.$sat.' )'."<br>";
                                                }?>
                                            </td>
                                            <td>
                                                <?php 
                                                $urmember =0; $authority =0; 
                                                foreach($value['assigned_to'] as $key => $value1)
                                                {
                                                    // echo $value1['member_id'];
                                                    // print_r($value['assigned_to']);die;
                                                    if($this->session->userdata('user_id') == $value1['member_id']) 
                                                    { 
                                                        $urmember = 1;
                                                        $status = $value1['status'];
                                                    } 
                                                    // else $urmember = 0;
                                                }
                                                if($urmember == 1)
                                                {
                                                    if($status == 2)
                                                    {?>
                                                        <!-- <a href="<?php// echo base_url().'cts_controller/change_staus/1' ?>">Accept</a> -->
                                                        <a href="<?php echo base_url().'index.php/cts_controller/change_status/1/'.$value['unique_id'].'/'.$this->session->userdata('user_id'); ?>">Accept</a><br>
                                                        <a href="<?php echo base_url().'index.php/cts_controller/change_status/0/'.$value['unique_id'].'/'.$this->session->userdata('user_id');?>" >Reject</a>
                                                        <!-- <a href="<?php// echo base_url().'cts_controller/change_staus/0' ?>">Reject</a> -->
                                                    <?php 
                                                    }    
                                                    else if($status == 1)
                                                    {
                                                        echo 'Accepted';
                                                    }
                                                    else if($status == 0)
                                                    {
                                                        echo 'Rejected';
                                                    }
                                                }   
                                                    // echo $this->session->userdata('user_id');
                                                ?>
                                                
                                            </td> 
                                            <td>
                                            <?php foreach($value['assigned_to'] as $key => $value1)
                                            {
                                                if($this->session->userdata('user_id') == $value1['created_by']) 
                                                { 
                                                    $authority = 1;
                                                } 
                                                else $authority = 0;
                                            }
                                            if($authority == 1)
                                            {?>
                                                <a href="<?php echo base_url().'index.php/cts_controller/cancel_meeting/'.$value1['unique_id']; ?>">Cancel</a>
                                                 <!-- echo 'Cancel'; -->
                                            <?php }   
                                                
                                            ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } ?>

                            </tbody>

                        </table>
                    </div>
                </div>
                
            </div>
            <div class="col-sm-5">

                <div class="dash-updates">
                    <h1>Updates</h1>
                    <ul class="list-group">
                        <?php 
                        foreach ($updates as $update): 
                            if($this->session->userdata('position_id') == 1)
                            {
                                $url = base_url().'index.php/cts_task_controller/update_task_completion/'.$update['task_id'];
                            }
                            else $url = '#';?>
                            <li class=" list-group-item">
                            <u><a style="color: blue;" href="<?php echo $url;?>"><?php echo $update["task_name"]; ?></a></u>, has been updated
                                by <strong><?php echo $update["updated_by"]; ?></strong>.......<span
                                    class="updates-time "><?php echo date("l jS M H:i", strtotime($update["updated_at"])); ?></span>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <a href="<?php echo site_url("cts_task_controller/show_all_task"); ?>" class="pull-right">view
                        all &raquo; </a>
                </div>
            </div>
        </div>
    <?php } elseif ($this->session->userdata('position_id') == '8') {
        ?>

    <?php } elseif ($this->session->userdata('position_id') == '3' || $this->session->userdata('position_id') == '4' || $this->session->userdata('position_id') == '2') {
        ?>
       
    <?php }
    ?>
</div>
<!-- <div id="dialog-confirm" title="Meeting Confirmation?">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p>
</div> -->
<script>
// window.onload = alert('12');
    $(document).ready(function () {
// alert('12');
    // var id = <?php echo $this->session->userdata('user_id'); ?>;
    // // alert(id);
    // $.ajax({
    //     url:'<?php echo base_url();?>'+'index.php/cts_controller/get_unknown_meeting/',
    //     type:'Post',
    //     dataType:'json',
    //     data: {id:id},
    //     success:function(msg)
    //     {
    //         // console.log(msg);            
    //         if(msg)
    //         {
    //             // alert('new message');
    //             $("#dialog-confirm" ).dialog({
    //               resizable: false,
    //               height:160,
    //               modal: true,
    //                show: {
    //               effect: "blind",
    //               duration: 300
    //               },
    //               hide: {
    //               effect: "blind",
    //               duration: 300
    //               },
    //               buttons: {
    //               "Attend": function() {
    //               window.location.assign('<?php echo base_url();?>'+'index.php/cts_controller/deleteIndicator/'+project_id);
    //               },
    //               Cancel: function() {
    //               $( this ).dialog( "close" );
    //               }
    //               }
    //             });
    //         }
    //     }
    // });

     // $( "#dialog-confirm" ).hide();
    // var project_id = $(this).attr('alt');
    
        // $(".status_accept").click(function(){
        //     // alert('accepted');
        //     var status_id = $(this).attr("data-index");
        //     var member_id = $(this).attr("member_id");
        //     var unique_id = $(this).attr("unique_id");
        //     // alert(status_id);
        //      $.ajax({
        //         url:'<?php echo base_url();?>'+'index.php/cts_controller/change_status/',
        //         type:'Post',
        //         dataType:'json',
        //         data: {id:status_id, member_id:member_id, unique_id:unique_id},
        //         success:function(msg)
        //         {
                  
        //         }
        //     });
        // });



        //multiple select members
        $("#multiple-select-team").multipleSelect({
            placeholder: "Select Members",
            width: "100%",
            // onCheckAll: function () {


            //     $(".user_body").each(function () {
            //         $(this).remove();
            //     });

            //     var checkAllValues = $("#multiple-select-team").multipleSelect("getSelects");
            //     var checkAllUsers = $("#multiple-select-team").multipleSelect("getSelects", "text");
            //     console.log(checkAllUsers);
            //     $.each(checkAllValues, function (index, value) {
            //         $("#assigned_users tr:first-child").after(" <tr class='user_body' id='user_" + value + "'><td>" + checkAllUsers[index] + "</td><td><input type='number' name='task[members][" + value + "]'> </td></tr>");
            //     });
            // },
            // onUncheckAll: function () {
              
            //     $(".user_body").each(function () {
            //         $(this).remove();
            //     });
            // },
            // onClick: function (view) {
            //     if (view.checked == true) {
               
            //         $("#assigned_users tr:first-child").after(" <tr class='user_body' id='user_" + view.value + "'><td>" + view.label + "</td><td><input type='number' name='task[members][" + view.value + "]'> </td></tr>");
            //     }
            //     else {
            //         var removeCheck = "#assigned_users tr#user_" + view.value;
            //         $(removeCheck).remove();
            //     }

            // },
        });



});


</script>