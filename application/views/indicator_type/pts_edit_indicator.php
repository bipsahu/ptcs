<div class="container">
    <h3>Edit Project</h3>
  
    <?php
    if (isset($result)) {
        if ($result == 1) {
            echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Department Created!</b></div>";
        } else
            echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Department Creation Failed!</b></div>";
    }
    ?>
     
    <?php
    
    if (isset($project_details)) {
//        echo $error;
//     print_r($project_details); die();
        foreach ($project_details as $project_values) {
          foreach($project_values as $row):
            $project_values['indicator_id'];
            $project_values['indicator_name'];
            $project_values['parent_id'];
        endforeach;
        }
    }
    ?>
    <table class="table" id="table">
       
        <form method="post" role="form" enctype="multipart/form-data"
              action="<?php echo base_url(); ?>index.php/cts_controller/edit_the_indicatortype/<?php echo $project_values['id']; ?>">
        
            <tr>
                <td>Indicator Type</td>
                <td><input type="text" class="form-control" name="indicator_name" required
                           value="<?php echo $project_values['name'] ?>">
            </tr>
           
            <tr>
                <td><input type="submit" name="submit" value="Submit" class="btn btn-primary"></td>
            </tr>
        </form>
    </table>
</div>
</body>
</html>