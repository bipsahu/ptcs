<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cts_notification extends CI_controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('cts_notification_model');
	}


	// public function updateNotification()
	// {
	// 	// echo "<pre>";
	// 	$result = $this->db->query("SELECT * FROM notification ")->result_array();
	// 	foreach ($result as $key => $results) {
	// 		$deadline_date = $results['deadline_date'];
	// 		$alert_date = $results['alert_date'];
	// 		$current_date = date('Y-m-d');
	// 		echo $deadline_date;echo "/";
	// 		echo $current_date;echo "/";
	// 		echo $alert_date;echo "<br>";
	// 	}
	// 	// echo 'this';die;
	// }

	public function show_notification()
	{

		date_default_timezone_set('Asia/Katmandu');
		$user_id = $this->session->userdata['user_id'];
		if(strtolower($this->session->userdata['user_position']) == "admin")
		{
			$result = $this->db->query("SELECT * FROM notification left join task_registration on notification.task_id = task_registration.task_id group by notification.task_id")->result_array();
		}
		else
		{
			$result = $this->db->query("SELECT * FROM notification left join task_registration on notification.task_id = task_registration.task_id where notification.user_id = $user_id group by notification.task_id")->result_array();
		}
		foreach ($result as $key => $results) 
		{

			$deadline_date = new DateTime($results['deadline_date']);
			$alert_date = new DateTime($results['alert_date']);
			$current_date = date('Y-m-d');
			$current_date = date_create($current_date);
			$interval_alert = $alert_date->diff($current_date);
			$interval_deadline = $current_date->diff($deadline_date);
			$i_alert = $interval_alert->format('%R%a');
			$i_deadline = $interval_deadline->format('%R%a days');

			if($i_alert < 0 || $i_deadline < 0)
			{
				// $data['message'][$key] = '';
				// echo 'negative';
			}
			else 
			{
				$days = $interval_deadline->format('%a');
				$days = $days;

				if($days == 0)
				{
					$data['message'][$key] = "Today is deadline for <u>".$results['task_name']."</u> (".$results['deadline_date']." )";
				}
				else
				{
					$data['message'][$key] = "<u>".$results['task_name']."</u> has ".$days." days to reach its deadline( ".$results['deadline_date']." )";			
				}
			}

		}
		if(!isset($data))
		{
			$data['message'] = '';
		}

		$this->load->view('cts_header');
        $this->load->view('cts_navigation', $data);
        $this->load->view('notification/cts_notification', $data);
        $this->load->view("cts_footer");

	}
}