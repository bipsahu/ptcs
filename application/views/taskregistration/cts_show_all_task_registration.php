<!-- <pre> -->
<?php

// print_r($results->result());die;
if (isset($roles_info)) {
    $i = 0;
    $role = array();
    foreach ($roles_info->result_array() as $key => $value) {
        $role[] = $value;
        $module[] = $role[$i]['module_name'];
        $i++;
    }
}
?>
<div class="container">
    <div id="breadcumb-text">
      <ul class="nav nav-tabs">
        <li><a href=""><strong>Task</strong></a></li>
        <!-- <li class="vertical-divider"></li>&nbsp; -->
        <li><a href="<?php echo base_url();?>index.php/cts_task_controller">Add Task</a></li>
        <li><a href="<?php echo base_url();?>index.php/cts_task_controller/showTask">Update Task</a></li>
    <?php if(strtolower($this->session->userdata('user_position'))=="admin" || $role[2]['add']){?>
        <li class="active"><a href="">List Task</a></li>
        <!-- <li class="vertical-divider"></li>&nbsp; -->
    <?php } ?>

      </ul>
    </div>
    <hr>
    <!-- <center><h1>Task List</h1></center> -->
    <!-- <?php if (strtolower($this->session->userdata('user_position')) == "admin" || isset($role) && $role[4]['add'] == 1) { ?>
        <a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/cts_report_controller"
           style="float:right; margin-top: 10px; margin-right: 10px; margin-bottom: 10px; ">Reports</a>
        <a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/cts_task_controller"
           style="float:right; margin-top: 10px; margin-right: 10px; ">Add New Task</a>
       <?php } ?> -->
    
    <table class="table table-striped" id="tabledata">
        <thead>
            <tr id="new_customer_table_row">


                <th>S NO.</th>
                <th>Task NO</th>
                <?php
                if ($this->session->userdata('position_id') == 1 || $this->session->userdata('position_id') == 2 || $this->session->userdata('position_id') == 3) {
                    echo "<th>Edit</th>";
                } elseif (isset($role) || $role[4]['edit']) {
                    //echo "<th>Edit</th>";
                }
                ?>
                <th>Status</th>
                <th>% Complete</th>
                <th>Task Name</th>
                <th>Assigned Date</th>
                <th>Deadline</th>
                <th>Project</th>
                <th>Task Category</th>
                <th>Department</th>
                <th>Indicators</th>
                <th>Members</th>
                <th>Cost Allocated (Nrs.)</th>
                <th>Remarks</th>
                <?php
                if ($this->session->userdata('position_id') == 1 ) {
                    echo "<th>Delete</th>";
                } elseif (isset($role) || $role[4]['delete']) {
                    //echo "<th>Delete</th>";
                }
                ?>



            </tr>
        </thead>
<tbody>
        <?php
        $count = 1;
        foreach ($results->result() as $rowKey => $row) {
            ?>


                <?php
                $rowMembers = array(); //for reseting the rowmembers
               /* creating single array*/
                foreach ($members[$rowKey] as $member) {

                    $rowMembers[] = $member["member"];
                }
                /*checking if the user login has been assigned to the task or not*/
                if (in_array($this->session->userdata('user_name'), $rowMembers) ||$this->session->userdata('position_id') == 1 ) {
                    ?>

                    <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $row->regd_no; ?></td>
                        <?php
                            $result = $this->cts_task_registration_model->is_editable($row->task_id);
                            if ($result == 0) {
                                if ($this->session->userdata('position_id') == 1 || $this->session->userdata('position_id') == 2 || $this->session->userdata('position_id') == 3) {
                                    echo "<td><a href='" . base_url() . "index.php/cts_task_controller/edit_task_by_id_form/" . $row->task_id . "'><span class='glyphicon glyphicon-edit'></span></a></td>";
                                }
                              
                            ?>
                            <?php
                        } else {
                            if (strtolower($this->session->userdata('position_id')) == "1" || strtolower($this->session->userdata('position_id')) == "2" || strtolower($this->session->userdata('position_id')) == "3") {
                                echo "<td>&nbsp;</td>";
                            } elseif (isset($role) || $role[4]['edit']) {
                                //echo "<td>&nbsp;</td>";
                            }
                            

                            ?>
                        <?php }?>
                        <td><?php
                    if ($row->status == 0)
                        echo '<span class="red">Inactive</span>';
                    elseif ($row->status == 1)
                        echo '<span class="yellow">On Going</span>';
                    elseif ($row->status == 2 || $row->percent_complete == 100)
                        echo '<span class="green">Complete</span>';
                    ?>
                        </td>
                        <td><?php echo $row->percent_complete . " %"; ?></td>
                        <td><?php echo $row->task_name; ?></td>
                        <td><?php echo $row->assign_date; ?></td>
                        <td><?php echo $row->deadline_date; ?></td>
                        <td><?php echo $row->project_name; ?></td>
                        <td><?php echo $row->name; ?></td>
                        <td><?php echo $row->dep_name; ?></td>
                        <td style="text-align: left"><?php
            foreach ($indicators[$rowKey] as $key => $indicatorRow):
                echo "<li>" . $indicatorRow["indicator"] . " <span class='red'>(" . $indicatorRow["weightage"] . "%)</span></li>";
            endforeach;
                    ?></td>
                        <td style="text-align: left"><?php
                    foreach ($members[$rowKey] as $memberRow):
                        echo "<li>" . $memberRow["member"] . " <span class='red'>(" . $memberRow["hour"] . " hrs)</span></li>";
                    endforeach;
                    ?></td>
                        <td><?php echo number_format($row->cost_allocated); ?></td>
                        <td><?php echo $row->remarks; ?></td>

                        <?php
                            $result = $this->cts_task_registration_model->is_editable($row->task_id);
                            if ($result == 0) {
                                
                                if ($this->session->userdata('position_id') == 1) {
                                    echo "<td><a href='#' class='trash' alt='" . $row->task_id . "'><span class='glyphicon glyphicon-trash'></span></a></td>";
                                }
                            ?>
                            <?php
                            } 
                            else 
                            {
                           
                                if (strtolower($this->session->userdata('position_id')) == "1") {
                                    echo "<td>&nbsp;</td>";
                                } elseif (isset($role) || $role[4]['delete']) {
                                    //echo "<td>&nbsp;</td>";
                                }

                            ?>
                        <?php }?>


                            <?php

                        $count++;
                        ?>
                    </tr>
                <?php }
            } ?>
<!-- <div id="dialog-confirm" title="Are you sure to delete this task?">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted. Are you sure?</p>
</div> -->
        </tbody>
    </table>
    <div id="dialog-confirm">
        Are you sure?
    </div>
</div>

</body>

</html>
<script type="text/javascript">
$(document).ready(function(){
    $('#tabledata').DataTable( {
        "scrollX": true
    } );
    $( "#dialog-confirm" ).hide();
    $('.trash').click(function(){
    var task_id = $(this).attr('alt');
    // console.log(task_id);
    // alert(task_id);
    $("#dialog-confirm" ).dialog({
          resizable: false,
          height:160,
          modal: true,
          show: {
          effect: "blind",
          duration: 300
          },
          hide: {
          effect: "blind",
          duration: 300
          },
          buttons: {
            "Delete": function() {
              // alert(task_id);exit();
              window.location.assign('<?php echo base_url();?>'+'index.php/cts_task_controller/delete_the_task/'+task_id);
            },
            Cancel: function() {
              $( this ).dialog( "close" );
            }
          }
    });
  });
});
</script>