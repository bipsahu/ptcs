<?php
if(isset($result))
{
	if($result==1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Update Succesful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Update Failed!</b></div>";
}
if(isset($sample_details)){
foreach($sample_details as $row)
{
$sid = $row['sid'];
$sample_type = $row['sample_type'];
$total_sample = $row['total_sample'];
}
}
?>
  <div class="container">
    <h3>Edit Sample Type Detail Form</h3><br>
      <table class="table" id="table">

        <form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/lims_customer_controller/edit_sample_by_id/<?php echo $sid;?>">
   
            <tr>
              <td><label>Sample Id</label></td>
              <td><input type="text" class="form-control" name="sample_id" value="<?php echo $sample_id;?>" required></td>
            </tr>
			<tr>
              <td><label>Customer Name</label></td>
              <td><input type="text" class="form-control" name="customer_name" value="<?php echo $customer_name;?>" required></td>
            </tr>
			<tr>
              <td><label>Sample Type</label></td>
              <td><input type="text" class="form-control" name="sample_type" value="<?php echo $sample_type;?>" required></td>
            </tr>

          <tr>
              <td><label>Total Sample</label></td>
              <td><input type="text" class="form-control" name="total_sample" value="<?php echo $total_sample;?>"required></td>
            </tr>
			
			<tr>
              <td><label>Sample Date</label></td>
              <td><input type="text" class="form-control" name="sample_date" value="<?php echo $sample_date;?>"required></td>
            </tr>
			
			<tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary">Submit</button></td>
            </tr>
          
        </form>
    </table>
	
</div><!---end of container-->
 </body>
 </html>
 