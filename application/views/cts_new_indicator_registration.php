<?php $task_category = $this->db->query("SELECT * FROM  task_category")->result_array(); ?>
<div class="container">
    <div class='alert alert-danger' role='alert' id="indicator_alert_error">
        <div class='glyphicon glyphicon-remove'></div>
        &nbsp;<b>Indicator Already Exists</b>
    </div>
    <div class='alert alert-success' role='alert' id="indicator_alert_success">
        <div class='glyphicon glyphicon-ok-sign'></div>
        &nbsp;<b>Indicator Added Successfully !!!</b>
    </div>
    <h3>Indicator Registration Form</h3><br>

    <form method="post" role="form" enctype="multipart/form-data" id="indicator_form">
        <table class="table" id="table">
            <tr>
                <td><label>Indicator Name </label></td>
                <td><input type="text" class="form-control" name="indicator_name" required></td>
            </tr>
            <tr>
                <td>Task Category</td>
                <td>
                  <select name="task_category" class="form-control" required>
                    <option value="">Select</option>
                    <?php foreach ($task_category as $key => $task_categories) {
                      # code...
                    ?>
                    <option value="<?php echo $task_categories['t_category_id'] ?>"><?php echo $task_categories['name'] ?></option>
                    <?php } ?>
                  </select>
                </td>
              </tr>
            <tr>
                <td colspan="2">
                    <button type="submit" name="submit" class="btn btn-primary" id="indicator_submit">Save</button>
                </td>
            </tr>
        </table>
    </form>

</div><!---end of container-->