<?php
if(isset($roles_info))
{
$i=0;
$role = array();
foreach($roles_info->result_array() as $key=>$value)
{
$role[] = $value;
$i++;
}
}
?>
<div class="container">
<center><h1>All Investigation Requested Details</h1></center>
<?php
if(!empty($result_indicator))
{
	if($result_indicator == 1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Deletion Successful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Deletion Failed!</b></div>";
}
?>


<table class="table table-striped" id="table_show_all_investigation">
<thead><th>S No.</th> <th> Investigation Requested </th> 
<?php 
		if(strtolower($this->session->userdata('user_position'))=="admin")
				{
				echo "<th>Edit</th>";
				}
				elseif(isset($role) || $role[3]['edit'])
				{
				echo "<th>Edit</th>";
				}
				?>
				<?php
				if(strtolower($this->session->userdata('user_position')) == "admin")
				{
				echo "<th>Delete</th>";
				}
				elseif(isset($role) || $role[3]['delete'])
				{
				echo "<th>Delete</th>";
				}


?></thead>

<?php 
$offset = $offset+1;
foreach($results->result() as $row){
?>
<tr>
<td><?php echo $offset;?></td>
<td><?php echo $row->investigation_requested;?></td>
<?php 
	if(strtolower($this->session->userdata('user_position'))=="admin")
	{
	echo "<td><a href='".base_url()."index.php/lims_investigation_controller/edit_investigation_requested_by_id_form/".$row->investigation_requested_id."' class='btn btn-primary'><span class='glyphicon glyphicon-edit'></span>Edit Investigation Requested</a></td>";
	}
	elseif(isset($role) || $role[3]['edit'])
	{
	echo "<td><a href='".base_url()."index.php/lims_investigation_controller/edit_investigation_requested_by_id_form/".$row->investigation_requested_id."' class='btn btn-primary'><span class='glyphicon glyphicon-edit'></span>Edit Investigation Requested</a></td>";
	}
	if(strtolower($this->session->userdata('user_position'))=="admin")
	{
	echo "<td><a href='#' class='btn btn-primary trash' alt='".$row->investigation_requested_id."'><span class='glyphicon glyphicon-trash'></span>Delete Investigation Requested</a></td>";
	}
	elseif(isset($role) || $role[3]['delete'])
	{
	echo "<td><a href='#' class='btn btn-primary trash' alt='".$row->investigation_requested_id."'><span class='glyphicon glyphicon-trash'></span>Delete Investigation Requested</a></td>";
	}
?>
</tr>
<?php
$offset++;
}
?>
<tr><td colspan="4"> <?php echo $links; ?></td></tr>
<?php if(strtolower($this->session->userdata('user_position'))=="admin" || isset($role) || $role[3]['add']){?>
<tr>
<td colspan="9"><a class="btn btn-primary" href="<?php echo base_url();?>index.php/lims_investigation_controller/add_new_investigation_requested">Add New Investigation Requested</a>
</tr>
<?php } ?>
</table>
</div>
<div id="dialog-confirm" title="Delete the Investigation Requested?">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>
</body>
<script type="text/javascript">
$(document).ready(function(){
 $( "#dialog-confirm" ).hide();
$('.trash').click(function(){
var investigation_id = $(this).attr('alt');
 $( "#dialog-confirm" ).dialog({
resizable: false,
height:160,
modal: true,
 show: {
effect: "blind",
duration: 300
},
hide: {
effect: "blind",
duration: 300
},
buttons: {
"Delete": function() {
window.location.assign('<?php echo base_url();?>'+'index.php/lims_investigation_controller/delete_investigation_requested_by_id/'+investigation_id);
},
Cancel: function() {
$( this ).dialog( "close" );
}
}
});
});
});
</script>
</html>