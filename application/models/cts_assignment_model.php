<?php
class cts_assignment_model extends CI_Model 
{
	public function check_assignment()
		{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('assignment_table','user.user_id = assignment_table.send_from');
		$this->db->where('send_to',$this->session->userdata('user_id'));
		$this->db->order_by("assignment_id", "desc");
		//$this->db->limit(10, $offset*10);
		$result = $this->db->get();
		$this->db->where('send_to',$this->session->userdata('user_id'));
		$this->db->update('assignment_table',array('status' => 1));
		return $result;
		}
		public function change_status($assignment_id)
		{
		$this->db->where('assignment_id',$assignment_id);
		$result = $this->db->update('assignment_table',array('check_status'=>1));
		return $result;
		}
		public function get_total_message()
		{
		$result = $this->db->get_where('assignment_table',array('send_to'=>$this->session->userdata('user_id')));
		return $result->num_rows();
		}

}