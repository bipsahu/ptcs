<?php 

Class cts_search_controller extends CI_controller
{
	
	public function __construct()
	{
	parent::__construct();
	$this->load->model('cts_search_model');
	$this->load->model('cts_role_model');
	$this->load->model('cts_department_model');
	$this->load->library('pagination');
	$this->load->model('cts_login');
	}
	
	public function index()
	{
		$data1['positions'] = $this->cts_role_model->show_all_position_role();
		$data1['new_message'] = $this->cts_login->check_new_message();
		$data1['departments'] = $this->cts_department_model->getDepartment();
		$data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data1);
		$this->load->view('search/cts_search');
	
	}

	public function search($offset=0)
	{	$data = array(
		'insure' => $this->input->post('insure'),
		'class' => $this->input->post('class'),
		'claim_no' => $this->input->post('claim_no'),
		'department' => $this->input->post('department'),
		'regd_no' => $this->input->post('regd_no'),
		'surveyor' => $this->input->post('surveyor')
		);
		$data['offset'] = $offset;
	    $data["result"] = $this->cts_search_model->search($offset, $data);
		$count = count($data['result']);
		$config['base_url'] = base_url().'/index.php/cts_search_controller/search';
		$config['total_rows'] = $count;
		$config['per_page'] = 20;
		/* $config['num_rows'] = 10; */
		$this->pagination->initialize($config);
		
		$data["results"] = array_slice( $data["result"],$offset,$config['per_page']);
		/* $data['results'] = $this->cts_search_model->pagination($page); */
		$data["links"] = $this->pagination->create_links();
		$data1['positions'] = $this->cts_role_model->show_all_position_role();
		$data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data1['departments'] = $this->cts_department_model->getDepartment();
		$data1['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data1);
		$this->load->view('search/cts_search',$data);
	
	}
	
	public function clear_session()
	{
		$this->session->unset_userdata('data');
		$this->index();
	}
}