<div class="container table-small">
<h1>Claim Branches</h1>
<?php if($this->session->userdata('position_id')=="1" || $this->session->userdata('position_id')=='4' ||$this->session->userdata('position_id')==5) { ?>
<a class="btn btn-primary add_new_button" href="<?php echo base_url();?>index.php/cts_branch_controller/add_new_branch" >Add New Branch</a>
<?php } ?>

<table class="table table-striped datatable">
<thead>
<tr id="new_customer_table_row"><th>S.N.</th><th>Branch</th><th>Address</th>
<?php 
				if($this->session->userdata('position_id')==1||$this->session->userdata('position_id')==4||$this->session->userdata('position_id')==5)
				{
				echo "<th>Edit</th>";
				}
				elseif(isset($role) || $role[4]['edit'])
				{
				//echo "<th>Edit</th>";
				}
				?>
				<?php
				if($this->session->userdata('position_id')==1||strtolower($this->session->userdata('position_id'))==4||$this->session->userdata('position_id')==5)
				{
				echo "<th>Delete</th>";
				}
				elseif(isset($role) || $role[4]['delete'])
				{
				//echo "<th>Delete</th>";
				}
				?>
                </tr>
</thead>
<tbody>
<?php 
$offset = 1;
//print_r($results->result());
foreach($result as $row){
?>

<tr>
	<td><?php echo $offset;?></td>
	<td><?php echo $row['branch_name'];?></td>
	<td><?php echo $row['address'];?></td>
   
	

<?php	//$result = $this->cts_claim_branch_model->is_editable($row->claim_id);
//if ($result == 0) { 
	if($this->session->userdata('position_id')==1||$this->session->userdata('position_id')==4||$this->session->userdata('position_id')==5)
	{
	echo "<td><a href='".base_url()."index.php/cts_branch_controller/get_branch_by_id/".$row['branch_id']."'><span class='glyphicon glyphicon-edit'></span></a></td>";
	
	echo "<td><a href='#' class='trash' alt='".$row['branch_id']."'><span class='glyphicon glyphicon-trash'></span></a></td>";
	
?>
<?php
}
else {
if($this->session->userdata('position_id')=="1"||$this->session->userdata('position_id')=="4"||$this->session->userdata('position_id')=="5")
	{
	echo "<td>&nbsp;</td>";
	}
	elseif(isset($role) || $role[4]['edit'])
	{
	echo "<td>&nbsp;</td>";
	}
	if(strtolower($this->session->userdata('position_id'))=="1"||strtolower($this->session->userdata('position_id'))=="4"||strtolower($this->session->userdata('position_id'))=="5")
	{
	echo "<td>&nbsp;</td>";
	}
	elseif(isset($role) || $role[4]['delete'])
	{
	echo "<td>&nbsp;</td>";
	}

}
$offset++;
}
?>
</tr>
</tbody>
</table>
</div>
<div id="dialog-confirm" title="Delete the Registerd Sample?">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>
</body>
<script type="text/javascript">
$(document).ready(function(){
$(".insured-alert").hide();
 $( "#dialog-confirm" ).hide();
$('.trash').click(function(){
var branch_id = $(this).attr('alt');
 $( "#dialog-confirm" ).dialog({
resizable: false,
height:160,
 show: {
effect: "blind",
duration: 300
},
hide: {
effect: "blind",
duration: 300
},
modal: true,
buttons: {
"Delete": function() {
window.location.assign('<?php echo base_url();?>'+'index.php/cts_branch_controller/delete_branch_by_id/'+branch_id);
},
Cancel: function() {
$( this ).dialog( "close" );
}
}
});
});
});
</script>
</html>