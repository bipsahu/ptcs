<?php
echo "hello";die;
if(isset($result))
{
	if($result==1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Update Succesful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Update Failed!</b></div>";
}

if(isset($task_details)){
//print_r($task_details);
foreach($task_details as $row)
{
	$task_id = isset($row['task_id'])?$row['task_id']:NULL;
	$task_no = $row['task_no'];
	$date = $row['intimation_date'];
	$regd_no = $row['regd_no'];
	$applicant_name = $row['applicant_name'];
	$applicant_contact = $row['applicant_contact'];
	$marketting_code = $row['marketting_code'];
	$policy_no = $row['policy_no'];
	$sum_insured = $row['sum_insured'];
	$policy_from = $row['policy_from'];
	$policy_to = $row['policy_to'];
	$task_amount = $row['task_amount'];
	$receipt_no = $row['receipt_no'];
	$premium_paid_date = $row['premium_paid_date'];
	$insure_name = $row['insure'];
	$projectname = $row['class'];
	$task_branch = $row['task_branch'];
	$vehicle_no = $row['vehicle_no'];
	$remarks = $row['remarks'];
}

}
?>
  <div class="container">
    <h3>Edit Task</h3><br>

        <form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/cts_task_controller/edit_task_by_id/<?php echo $task_id;?>">

			 <table class="table" id="table">
<?php date_default_timezone_set("UTC"); ?>
			<tr>

              <td><input type="hidden" class="form-control" tabindex="0" name="task_id" required id="task_id" value="<?php echo $task_id;?>"></td>
			  <td></td>
            </tr>
			<tr>
              <td><label>Task Create Date</label></td>
              <td><input type="text" class="form-control" tabindex="0" name="date" required id="date" value="<?php echo $date;?>"></td>
			  <td></td>
            </tr>

			<tr>
              <td><label>Task No.</label></td>
              <td><input type="text" class="form-control" tabindex="1" name="regd_no" required value="<?php echo $regd_no;?>"> </td>
			  <td></td>
            </tr>
				 <tr>
					 <td><label>Branch</label></td>
					 <td><select type="text" class="form-control" tabindex="4" name="task_branch" id="task_branch" required>
							 <option value="">Select Branch</option>
							 <?php if(isset($branches)):
								 foreach($branches as $branch){
									 if($task_branch == $branch['branch_name']) $selected = "selected"; else $selected= NULL;
									 echo "<option value=".$branch['branch_name']." ".$selected.">".$branch['branch_name']."</option>";
								 }
							 endif;?>
						 </select></td>
					 <td>
						 <button type="button" name="button" class="btn btn-primary" data-toggle="modal" data-target="#task_branch_model" id="new_branch">
							 <span class="glyphicon glyphicon-plus"></span>
						 </button>

					 </td>
				 </tr><tr>
					 <td><label>Project</label></td>
					 <td><select type="text" class="form-control" tabindex="4" name="class" id="class" required>
							 <option value="">Select Class</option>
							 <?php

							 if(isset($projects)):

								 foreach($projects as $project){
									 $selected = ($project['class_name']==$projectname)?"selected":" ";
									 echo "<option ".$selected." name=".$project['class_name'].">".$project['class_name']."</option>";
								 }
							 endif;?>
						 </select></td>
					 <td>
						 <button type="button" name="button" class="btn btn-primary" data-toggle="modal" data-target="#class_model" id="new_class">
							 <span class="glyphicon glyphicon-plus"></span>
						 </button>
						 <?php /*?><div class="alert alert-info" id="class_alert">Add New Class</div><?php */?>
					 </td>
				 </tr>
			<tr>
              <td><label>Task Detail</label></td>
              <td><textarea type="text" class="form-control" tabindex="6" name="remarks" id="remarks"><?php echo $remarks;?></textarea></td>

			<input type="hidden" name="task_customer_id" id="task_customer_id" value=""/>
			<input type="hidden" name="task_insure_id" id="task_insure_id" value=""/>
            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary" id="submit">Submit</button></td>
			   <td></td>
            </tr>
    </table>

        </form>

</div><!---end of container-->
 </body>
 <script type="text/javascript">
$(document).ready(function(){

$("#class").change(function(){
if($(this).val()=="CV" || $(this).val()=="PV"){
	$("#vehicle_no").show();
}
});
$(".labs").each(function(){
	if($(this).is(':checked'))
	var dep = $(this).attr("alt");
	$("."+dep).show();
		});
		$(".labs").change(function()
		{
		var uncheck_dep = $(this).attr("alt");
			if($(this).is(':checked'))
			{
			$("."+uncheck_dep).show();
			}
			else
			{
			$("."+uncheck_dep).hide();
			$("."+uncheck_dep).children().removeAttr('checked');
			}
		});

});
 </script>
 </html>
