<br><center><h2>All Published Reports</h2></center>
<br>

<div id="tabs">
<ul>
	<li><a href="#tabs-1" id="head_tab1">Virology</a></li>
	<li><a href="#tabs-2" id="head_tab2">Serology</a></li>
    <li><a href="#tabs-3" id="head_tab3">Molecular Biology</a>
	</li>
	
	
<a class="btn btn-primary" href="<?php echo base_url();?>index.php/lims_sample_controller/show_all_sample" style="float:right; margin-right: 10px; ">Show All Sample</a>
<a class="btn btn-primary" href="<?php echo base_url();?>index.php/lims_sample_controller" style="float:right; margin-right: 10px; ">Add New Sample</a>

</ul>
	<div id="tabs-1">
	<table class="table table-striped" id="published_virology_details">
	<thead>
	<th>Virology. No.</th><th>Regd. No.</th><th>Customer Name</th><th>Firm Name</th><th>Zone</th><th>District</th><th>VDC/Municipality</th><th>VDC/Ward No</th><th>Street</th><th>Phone No.</th><th>Mobile No:</th><th>Email</th><th>Generate Report</th>
	</thead>
	<?php
	foreach($virology_reports['result']->result() as $row)
	{
	echo "<tr><td>".$row->virology_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->customer_zone."</td><td>".$row->customer_district."</td><td>".$row->customer_vdc_municipality."</td><td>".$row->customer_vdc_ward_no."</td><td>".$row->customer_street."</td><td>".$row->customer_phone."</td><td>".$row->customer_mobile."</td><td>".$row->customer_email."</td><td><a class='btn btn-primary' href='".base_url()."index.php/lims_virology_controller/generate_report/".$row->virology_id."' target='_blank' >Generate Report</a></td></tr>";
	}
	?>
	<tr>
	<td colspan="13">
	<?php 
	echo "<center><span class='glyphicon glyphicon-backward'></span>&nbsp;";
	for($i=1;$i<=ceil($virology_reports['count']/5);$i++)
	{
	
		if($i == 1)
		{
		echo "<b>".$i."</b>";
		}
		else
		{
		echo "<a href='javascript:virology_pagination(".($i-1).")'> ".$i." </a>";
		}
	
	}
	if($virology_reports['count'] <= 5)
	{
	echo "&nbsp;<span class='glyphicon glyphicon-forward'></span>";
	}
	else
	echo "<a href='javascript:virology_pagination(1)'>&nbsp;<span class='glyphicon glyphicon-forward'></span></a><center>";
	?>
	</td>
	</tr>
	</table>
	</div>
	
	<div id="tabs-2">
	<table class="table table-striped" id="published_serology_details">
	<thead>
	<th>Serology. No.</th><th>Regd. No.</th><th>Customer Name</th><th>Firm Name</th><th>Zone</th><th>District</th><th>VDC/Municipality</th><th>VDC/Ward No</th><th>Street</th><th>Phone No.</th><th>Mobile No:</th><th>Email</th><th>Generate Report</th>
	</thead>
	<?php 
	foreach($serology_reports['result']->result() as $row)
	{
	echo "<tr><td>".$row->serology_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->customer_zone."</td><td>".$row->customer_district."</td><td>".$row->customer_vdc_municipality."</td><td>".$row->customer_vdc_ward_no."</td><td>".$row->customer_street."</td><td>".$row->customer_phone."</td><td>".$row->customer_mobile."</td><td>".$row->customer_email."</td><td><a class='btn btn-primary' href='".base_url()."index.php/lims_serology_controller/generate_report/".$row->sample_id." ' target='_blank'>Generate Report</a></td></tr>";
	}
	?>
	<tr>
	<td colspan="13"><?php 
	echo "<center><span class='glyphicon glyphicon-backward'></span>&nbsp;";
	for($i=1;$i<=ceil($serology_reports['count']/5);$i++)
	{
	$j= $i-1;
	if($i==1)
	echo "<b>".$i."</b>";
	else
	echo "<a href='javascript:serology_pagination(".$j.")'> ".$i." </a>";
	}
	if(ceil($serology_reports['count']/5) != 1)
	echo "<a href='javascript:serology_pagination(1)'>&nbsp;<span class='glyphicon glyphicon-forward'></span></a></center>";
	else
	echo "&nbsp;<span class='glyphicon glyphicon-forward'></span></center>";
	?>
	</tr>
	</table>
	</div>
	
	<div id="tabs-3">
	<table class="table table-striped" id="published_biology_details">
	<thead>
	<th>Molecular Biology. No.</th><th>Regd. No.</th><th>Customer Name</th><th>Firm Name</th><th>Zone</th><th>District</th><th>VDC/Municipality</th><th>VDC/Ward No</th><th>Street</th><th>Phone No.</th><th>Mobile No:</th><th>Email</th><th>Generate Report</th>
	</thead>
	<?php 
	foreach($biology_reports['result']->result() as $row)
	{
	echo "<tr><td>".$row->biology_id."</td><td>".$row->labno."</td><td>".$row->customer_name."</td><td>".$row->firm_name."</td><td>".$row->customer_zone."</td><td>".$row->customer_district."</td><td>".$row->customer_vdc_municipality."</td><td>".$row->customer_vdc_ward_no."</td><td>".$row->customer_street."</td><td>".$row->customer_phone."</td><td>".$row->customer_mobile."</td><td>".$row->customer_email."</td><td><a class='btn btn-primary' href='".base_url()."index.php/lims_biology_controller/generate_report/".$row->sample_id."' target='_blank'>Generate Report</a></td></tr>";
	}
	?>
	<tr>
	<td colspan="13">
	<?php 
	echo "<center><span class='glyphicon glyphicon-backward'></span>&nbsp;";
	for($i=1;$i<=ceil($biology_reports['count']/5);$i++)
	{
	$j= $i-1;
	if($i==1)
	echo "<b>".$i."</b>";
	else
	echo "<a href='javascript:biology_pagination(".$j.")'> ".$i." </a>";
	}
	if(ceil($biology_reports['count']/5) != 1)
	echo "<a href='javascript:biology_pagination(1)'>&nbsp;<span class='glyphicon glyphicon-forward'></span></a></center>";
	else
	echo "&nbsp;<span class='glyphicon glyphicon-forward'></span></center>";
	?>
	</tr>
	</table>
	</div>
</div>
<script>
$(document).ready(function(){
$("#tabs").tabs();
});
function biology_pagination(offset)
{
	$.ajax({
	url:'<?php echo base_url();?>/'+'index.php/lims_report_generate/biology_pagination/'+offset,
	type:'POST',
	success:function(msg)
	{
	$("#published_biology_details").html(msg);
	}
	});
}
function serology_pagination(offset)
{
	$.ajax({
	url:'<?php echo base_url();?>'+'index.php/lims_report_generate/serology_pagination/'+offset,
	type:'POST',
	success:function(msg)
	{
	$("#published_serology_details").html(msg);
	}
	});
}
function virology_pagination(offset)
{
	$.ajax({
	url:'<?php echo base_url();?>'+'index.php/lims_report_generate/virology_pagination/'+offset,
	type:'POST',
	success:function(msg)
	{
	$("#published_virology_details").html(msg);
	}
	});
}
</script>
</body>
</html>