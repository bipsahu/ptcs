<?php
/* if(isset($result))
{
	if($result==1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Update Succesful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Update Failed!</b></div>";
}
if(isset($serology_details)){
foreach($serology_details as $row)
{
$serology_id = $row['serology_id'];
$test_method = $row['test_method'];
$result = $row['result'];
$remark = $row['remark'];
$approved_remark = $row['approved_remark'];
$tested_date = $row['tested_date'];
$approved_date = $row['approved_date'];
}
} */
?>

  <div class="container">
    <h3>Edit Serology Result Form</h3><br>
          <table class="table" id="table">
        <form method="post" role="form" enctype="multipart/form-data" id="edit_serology_form">      
            <tr>
              <td><label>Test Method</label></td>
              <td colspan="2"><select name="test_method" class="form-control" >
				<option value="ELISA"> ELISA </option>
				<option value="PAT">PAT </option>
				<option value="OTHERS">OTHERS </option>
				
			  </select>
			  </td>
            </tr>
          <tr>
              <td><label> Result </label></td>
              <td><input type="radio" class="radio-inline" name="result" value="positive" id="positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="result" value="negative" id="negative" >&nbsp;Negative</td>
            </tr>
            <tr>
              <td><label>Remarks </label> </td>
              <td colspan="2"><textarea type="textarea" class="form-control" name="remark" id="remark" rows="3" value="<?php //echo $remark;?>" ></textarea></td>
            </tr>
			<tr>
              <td><label>Sample Collection Date</label> </td>
              <td colspan="2"><input type="text" class="form-control" name="sample_collection_date" id="sample_collection_dat" value="" ></td>
            </tr>
		
            <tr>
        	   <td colspan="3"><button type="submit" name="submit" class="btn btn-primary" id="submit" >Submit</button></td>
            </tr>
          
        </form>
    </table>
	
</div><!---end of container-->
 </body>
 </html>
 