<?php 
class cts_surveyor_controller extends CI_Controller 
{
	public function __construct()
	{
	parent::__construct();
		$this->load->model('cts_surveyor_model');
		$this->load->model('cts_role_model');
		$this->load->model('cts_department_model');
		$this->load->model('cts_login');
	}
	public function index()
	{
		$data['positions'] = $this->cts_role_model->show_all_position_role();
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data);
		$this->load->view('animaltype/cts_surveyor_registration');
		$this->load->view('cts_footer');
	}
	
	public function add_new_surveyor()
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'])
		{
			$data['positions'] = $this->cts_role_model->show_all_position_role();
			$data['departments'] = $this->cts_department_model->getDepartment();
			$data['new_message'] = $this->cts_login->check_new_message();
			$this->load->view('cts_header');
			$this->load->view('cts_navigation',$data);
			$this->load->view('animaltype/cts_surveyor_registration1');
			$this->load->view('cts_footer');
		}
		else
		{
		redirect(base_url());
		}
	}
	
	public function insert_surveyor_details ()
	{
		$data['result'] = $this->cts_surveyor_model->insert_surveyor();
		if($data['result'] == false)
			{
			echo " 0";
			}
			else
			{
		foreach($data['result']->result() as $row)
		{
		$surveyor_details = array(
			'surveyor' => $row->surveyor
			);
		}
		echo json_encode($surveyor_details);
	}
	}
	
	public function insert_new_surveyor()
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'])
		{
		$data['positions'] = $this->cts_role_model->show_all_position_role();
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['result'] = $this->cts_surveyor_model->insert_new_surveyor();
		$data['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data);
		$this->load->view('animaltype/cts_surveyor_registration1', $data);
		$this->load->view('cts_footer');
		}
		else
		{
		redirect(base_url());
		}
	
	}
	public function edit_surveyor_by_id_form($surveyor_id)
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit'])
		{
		$data['positions'] = $this->cts_role_model->show_all_position_role();
		$data['surveyor'] = $this->cts_surveyor_model->get_details_of_surveyor_by_id($surveyor_id);
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data);
		$this->load->view('animaltype/cts_edit_surveyor_by_id_form',$data);
		}
		else
		{
		redirect(base_url());
		}
	}
	
	public function edit_surveyor_by_id($surveyor_id)
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit'])
		{
		$data['result'] = $this->cts_surveyor_model->edit_surveyor_by_id($surveyor_id);
		redirect(base_url().'index.php/cts_surveyor_controller/show_all_surveyor');
		}
		else
		{
		redirect(base_url());
		}
	
	}
	
	
	public function show_all_surveyor($offset = 0)
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit']|| $role[3]['add']|| $role[3]['delete']) 
		{
		$data['positions'] = $this->cts_role_model->show_all_position_role();
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$data['offset'] = $offset;
		$config = array();
		$config["base_url"] = base_url() . "index.php/cts_surveyor_controller/show_all_surveyor";
		$config["total_rows"] = $this->db->count_all("surveyor");
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["results"] = $this->cts_surveyor_model->show_all_surveyor($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data);
		$this->load->view('animaltype/cts_show_all_surveyor',$data);
		}
		else
		{
		redirect(base_url());
		}
	}
	
	public function delete_surveyor_by_id($surveyor_id)
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['delete'])
		{
		$data['result'] = $this->cts_surveyor_model->delete_surveyor_by_id($surveyor_id);
		redirect(base_url().'index.php/cts_surveyor_controller/show_all_surveyor');
		}
		else
		{
		redirect(base_url());
		}
	}
	public function search_surveyor_detail($key)
	{
	$this->cts_surveyor_model->search_surveyor_detail($key);
	}
	public function check_surveyor()
	{
	$result = $this->cts_surveyor_model->check_surveyor();
		if($result)
		echo " 1";
		else
		echo " 0";
	}
}