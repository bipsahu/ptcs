<html>
  <head>
    <title>Vetenary System</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="css/style.css" type="text/css"/>

    <!-- Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="resource/bootstrap/css/bootstrap.min.css" type="text/css"/>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  </head>
  
  <body>
    <div class="container">
    <h1>Vetenary System</h1>
    <div id="form">
      <form method="post" role="form" enctype="multipart/form-data">
        <div class="form-group">
          <label>Investigation Requested For</label>
          <input type="text" class="form-control" name="investigation">
        </div>
        <div class="form-group">
          <label>Egg Inoculation</label>
          <input type="text" class="form-control" name="egg">
        </div>
        <div class="form-group">
          <label>Plate Agglutination</label>
          <input type="text" class="form-control" name="plate">
        </div>
        <div class="form-group">
          <label>HA</label>
          <input type="text" class="form-control" name="ha">
        </div>
        <div class="form-group">
      	<label>HI</label>
      	<input type="text" class="form-control" name="hi">
        </div>
        <div class="form-group">
      	<label>CFT</label>
      	<input type="text" class="form-control" name="cft">
        </div>
        <div class="form-group">
        <label>AGID</label>
        <input type="text" class="form-control" name="agid">
        </div>
        <div class="form-group">
        <label>ELISA</label>
        <input type="text" class="form-control" name="elisa">
        </div>
        <div class="form-group">
        <label>Conformative Diagnosis</label>
        <input type="text" class="form-control" name="diagnosis">
        </div>
        <div class="btn-group">
      	<button type="submit" name="submit" class="btn btn-default">Submit</button>
        </div>
      </form>
    </div><!--end of form-->
    </div><!---end of container-->