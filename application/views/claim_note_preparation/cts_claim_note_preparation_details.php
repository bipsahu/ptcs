<div class="container insure-detail">
	<div class="row">
		<div class="col-sm-4"><h5>Intimation Date:</h5></div>
    	<div class="col-sm-8"><?php if(isset($row->intimation_date)) echo $row->intimation_date;?></div>
    </div>
    	<div class="row">

   		 <div class="col-sm-4"><h5>Accident Date</h5></div>
   		 <div class="col-sm-8"><?php if(isset($row->accident_date)) echo $row->accident_date;?></div>
    </div>
    <div class="row">
		<div class="col-sm-4"><h5>Claim Number:</h5></div>
   		<div class="col-sm-8"><?php if(isset($row->claim_no)) echo $row->claim_no;?></div>
    </div>
    
    <div class="row">
   		<div class="col-sm-4"><h5>Class:</h5></div>
    	<div class="col-sm-8"><?php if(isset($row->class)) echo $row->class;?></div>
    </div>
    <div class="row">
   		<div class="col-sm-4"><h5>Insured:</h5></div>
    	<div class="col-sm-8"><?php if(isset($row->insure)) echo $row->insure;?></div>
    </div>
    <?php if($row->vehicle_no):?>
    <div class="row">
    <div class="col-sm-4"><h5>Vehicle Number:</h5></div>
    <div class="col-sm-8"><?php if(isset($row->vehicle_no)) echo $row->vehicle_no;?></div>
    </div>
    <?php endif;?>
    <div class="row">
   		<div class="col-sm-4"><h5>Claim Amount:</h5></div>
    	<div class="col-sm-8"><?php if(isset($row->claim_amount)) echo $row->claim_amount;?></div>
     </div>
    <div class="row">
    	<div class="col-sm-4"><h5>Policy Number:</h5></div>
    	<div class="col-sm-8"><?php if(isset($row->policy_no)) echo $row->policy_no;?></div>
    </div>
    <div class="row">
    	<div class="col-sm-4"><h5>Policy Validity:</h5></div>
   		 <div class="col-sm-8"><?php if(isset($row->policy_from)&&isset($row->policy_to)) echo $row->policy_from.' to '.$row->policy_to;?></div>
     </div>
     <div class="row">
    	<div class="col-sm-4"><h5>Claim Remarks:</h5></div>
    	<div class="col-sm-8"><?php if(isset($row->remarks)) echo $row->remarks;?></div>
    </div>
    <div class="row">
   		<div class="col-sm-4"><h5>Surveyor:</h5></div>
    	<div class="col-sm-8"><?php if(isset($row->surveyor)) echo $row->surveyor;?></div>
    </div>
    <div class="row">
   		<div class="col-sm-4"><h5>Surveyor Appointed Date:</h5></div>
    	<div class="col-sm-8"><?php if(isset($row->appointment_date)) echo $row->appointment_date;?></div>
    </div>
    <div class="row">
   		<div class="col-sm-4"><h5>Surveyor Result Received Date:</h5></div>
    	<div class="col-sm-8"><?php if(isset($row->result_received_date)) echo $row->result_received_date;?></div>
    </div>
    <div class="row">
   		<div class="col-sm-4"><h5>Access Amount</h5></div>
    	<div class="col-sm-8"><?php if(isset($row->surveyor_estimated_loss)) echo $row->surveyor_estimated_loss;?></div>
    </div>
    <div class="row">
   		<div class="col-sm-4"><h5>Surveyor Remarks:</h5></div>
    	<div class="col-sm-8"><?php if(isset($row->surveyor_remarks)) echo $row->surveyor_remarks;?></div>
    </div>
</div>