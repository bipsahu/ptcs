<div class="container">
<center><h1>Virology Annual Report</h1></center>
<table class="table" id="table">
<form action="<?php echo base_url();?>index.php/lims_final_report_controller/virology_final_report_display/<?php echo $lab_id;?>" method="GET">
<tr><td>
<select name="year" class="form-control">
<option value="2071/72">2071/72</option>
<option value="2072/73">2072/73</option>
<option value="2072/74">2072/74</option>
<option value="2072/75">2072/75</option>
</select></td>

<td>
<select name="investigation_requested" class="form-control">
<option value="allresult"> All Reports</option>
<?php 

foreach( $investigation_requested->result() as $row )
			{ ?>

<option value="<?php echo $row->investigation_requested_id; ?> "><?php echo $row->investigation_requested; ?></option>
<?php } ?>
</select></td>

<td><input type="submit" value="Submit" class="btn btn-primary"></td></tr>
</table>
</form>
<?php if(isset($allresultnegative))
{ ?>
<table class="table table-striped" id="table"><thead>
	<tr><th rowspan='2'>Months </th><th colspan='3'><center>AI</center></th><th colspan='3'><center>ND</center></th><th colspan='3'><center>IBD</center></th><th colspan='3'><center>POX</center></th><th colspan='3'><center>Rabies</center></th><th colspan='3'><center>PPR</center></th></tr>
	<tr><th>Positive</th><th>Negative</th><th>Total</th><th>Positive</th><th>Negative</th><th>Total</th><th>Positive</th><th>Negative</th><th>Total</th><th>Positive</th><th>Negative</th><th>Total</th><th>Positive</th><th>Negative</th><th>Total</th><th>Positive</th><th>Negative</th><th>Total</th></tr></thead>


	<tr><td>Shrawan</td><td><?php echo $allresultpositive['ai_sql_positive'][0]; ?></td><td><?php echo $allresultnegative['ai_sql_negative'][0]; ;?> </td><td><?php echo $allresultpositive['ai_sql_positive'][0]+$allresultnegative['ai_sql_negative'][0];?></td><td><?php echo $allresultpositive['ibd_sql_positive'][0]; ?></td><td><?php echo $allresultnegative['idb_sql_negative'][0];?> </td><td><?php echo $allresultpositive['ibd_sql_positive'][0]+$allresultnegative['idb_sql_negative'][0];?></td><td><?php echo $allresultpositive['nd_sql_positive'][0]; ?></td><td><?php echo $allresultnegative['nd_sql_negative'][0];?> </td><td><?php echo $allresultpositive['nd_sql_positive'][0]+ $allresultnegative['nd_sql_negative'][0];?></td><td><?php echo "pox positive here"; ?></td><td><?php echo"pox negative here";?> </td><td><?php echo "pox total goes here";?></td><td><?php echo $allresultpositive['rabies_sql_positive'][0]; ?></td><td><?php echo $allresultnegative['rabies_sql_negative'][0]; ?> </td><td><?php echo $allresultpositive['rabies_sql_positive'][0]+$allresultnegative['rabies_sql_negative'][0];?></td><td><?php echo $allresultpositive['ppr_sql_positive'][0]; ?></td><td><?php echo $allresultnegative['ppr_sql_negative'][0] ;?> </td><td><?php echo $allresultpositive['ppr_sql_positive'][0]+$allresultnegative['ppr_sql_negative'][0];?></td></tr>

	<tr><td>Bhadra</td><td><?php echo $allresultpositive['ai_sql_positive'][1]; ?></td><td><?php echo $allresultnegative['ai_sql_negative'][1]; ;?> </td><td><?php echo $allresultpositive['ai_sql_positive'][1]+$allresultnegative['ai_sql_negative'][1];?></td><td><?php echo $allresultpositive['ibd_sql_positive'][1]; ?></td><td><?php echo $allresultnegative['idb_sql_negative'][1];?> </td><td><?php echo $allresultpositive['ibd_sql_positive'][1]+$allresultnegative['idb_sql_negative'][1];?></td><td><?php echo $allresultpositive['nd_sql_positive'][1]; ?></td><td><?php echo $allresultnegative['nd_sql_negative'][1];?> </td><td><?php echo $allresultpositive['nd_sql_positive'][1]+ $allresultnegative['nd_sql_negative'][1];?></td><td><?php echo "pox positive here"; ?></td><td><?php echo"pox negative here";?> </td><td><?php echo "hello";?></td><td><?php echo $allresultpositive['rabies_sql_positive'][1]; ?></td><td><?php echo $allresultnegative['rabies_sql_negative'][1]; ;?> </td><td><?php echo $allresultpositive['rabies_sql_positive'][1]+$allresultnegative['rabies_sql_negative'][1];?></td><td><?php echo $allresultpositive['ppr_sql_positive'][1]; ?></td><td><?php echo $allresultnegative['ppr_sql_negative'][1] ;?> </td><td><?php echo $allresultpositive['ppr_sql_positive'][1]+$allresultnegative['ppr_sql_negative'][1];?></td></tr>
	
	<tr><td>Ashwin</td><td><?php echo $allresultpositive['ai_sql_positive'][2]; ?></td><td><?php echo $allresultnegative['ai_sql_negative'][2]; ;?> </td><td><?php echo$allresultpositive['ai_sql_positive'][2]+$allresultnegative['ai_sql_negative'][2];?></td><td><?php echo $allresultpositive['ibd_sql_positive'][2]; ?></td><td><?php echo $allresultnegative['idb_sql_negative'][2];?> </td><td><?php echo $allresultpositive['ibd_sql_positive'][2]+$allresultnegative['idb_sql_negative'][2];?></td><td><?php echo $allresultpositive['nd_sql_positive'][2]; ?></td><td><?php echo $allresultnegative['nd_sql_negative'][2];?> </td><td><?php echo $allresultpositive['nd_sql_positive'][2]+ $allresultnegative['nd_sql_negative'][2];?></td><td><?php echo "pox positive here"; ?></td><td><?php echo"pox negative here";?> </td><td><?php echo "hello";?></td><td><?php echo $allresultpositive['rabies_sql_positive'][2]; ?></td><td><?php echo $allresultnegative['rabies_sql_negative'][2]; ;?> </td><td><?php echo $allresultpositive['rabies_sql_positive'][2]+$allresultnegative['rabies_sql_negative'][2];?></td><td><?php echo $allresultpositive['ppr_sql_positive'][2]; ?></td><td><?php echo $allresultnegative['ppr_sql_negative'][2] ;?> </td><td><?php echo $allresultpositive['ppr_sql_positive'][2]+$allresultnegative['ppr_sql_negative'][2];?></td></tr>
	
	<tr><td>Kartik</td><td><?php echo $allresultpositive['ai_sql_positive'][3]; ?></td><td><?php echo $allresultnegative['ai_sql_negative'][3]; ;?> </td><td><?php echo$allresultpositive['ai_sql_positive'][3]+$allresultnegative['ai_sql_negative'][3];?></td><td><?php echo $allresultpositive['ibd_sql_positive'][3]; ?></td><td><?php echo $allresultnegative['idb_sql_negative'][3];?> </td><td><?php echo $allresultpositive['ibd_sql_positive'][3]+$allresultnegative['idb_sql_negative'][3];?></td><td><?php echo $allresultpositive['nd_sql_positive'][3]; ?></td><td><?php echo $allresultnegative['nd_sql_negative'][3];?> </td><td><?php echo $allresultpositive['nd_sql_positive'][3]+ $allresultnegative['nd_sql_negative'][3];?></td><td><?php echo "pox positive here"; ?></td><td><?php echo"pox negative here";?> </td><td><?php echo "hello";?></td><td><?php echo $allresultpositive['rabies_sql_positive'][3]; ?></td><td><?php echo $allresultnegative['rabies_sql_negative'][3]; ;?> </td><td><?php echo $allresultpositive['rabies_sql_positive'][3]+$allresultnegative['rabies_sql_negative'][3];?></td><td><?php echo $allresultpositive['ppr_sql_positive'][3]; ?></td><td><?php echo $allresultnegative['ppr_sql_negative'][3] ;?> </td><td><?php echo $allresultpositive['ppr_sql_positive'][3]+$allresultnegative['ppr_sql_negative'][3];?></td></tr>
	
	<tr><td>Mangsir</td><td><?php echo $allresultpositive['ai_sql_positive'][4]; ?></td><td><?php echo $allresultnegative['ai_sql_negative'][4]; ;?> </td><td><?php echo$allresultpositive['ai_sql_positive'][4]+$allresultnegative['ai_sql_negative'][4];?></td><td><?php echo $allresultpositive['ibd_sql_positive'][4]; ?></td><td><?php echo $allresultnegative['idb_sql_negative'][4];?> </td><td><?php echo $allresultpositive['ibd_sql_positive'][4]+$allresultnegative['idb_sql_negative'][4];?></td><td><?php echo $allresultpositive['nd_sql_positive'][4]; ?></td><td><?php echo $allresultnegative['nd_sql_negative'][4];?> </td><td><?php echo $allresultpositive['nd_sql_positive'][4]+ $allresultnegative['nd_sql_negative'][4];?></td><td><?php echo "pox positive here"; ?></td><td><?php echo"pox negative here";?> </td><td><?php echo "hello";?></td><td><?php echo $allresultpositive['rabies_sql_positive'][4]; ?></td><td><?php echo $allresultnegative['rabies_sql_negative'][4]; ;?> </td><td><?php echo $allresultpositive['rabies_sql_positive'][4]+$allresultnegative['rabies_sql_negative'][4];?></td><td><?php echo $allresultpositive['ppr_sql_positive'][4]; ?></td><td><?php echo $allresultnegative['ppr_sql_negative'][4] ;?> </td><td><?php echo $allresultpositive['ppr_sql_positive'][4]+$allresultnegative['ppr_sql_negative'][4];?></td></tr>
	
	<tr><td>Poush</td><td><?php echo $allresultpositive['ai_sql_positive'][5]; ?></td><td><?php echo $allresultnegative['ai_sql_negative'][5]; ;?> </td><td><?php echo$allresultpositive['ai_sql_positive'][5]+$allresultnegative['ai_sql_negative'][5];?></td><td><?php echo $allresultpositive['ibd_sql_positive'][5]; ?></td><td><?php echo $allresultnegative['idb_sql_negative'][5];?> </td><td><?php echo $allresultpositive['ibd_sql_positive'][5]+$allresultnegative['idb_sql_negative'][5];?></td><td><?php echo $allresultpositive['nd_sql_positive'][5]; ?></td><td><?php echo $allresultnegative['nd_sql_negative'][5];?> </td><td><?php echo $allresultpositive['nd_sql_positive'][5]+ $allresultnegative['nd_sql_negative'][5];?></td><td><?php echo "pox positive here"; ?></td><td><?php echo"pox negative here";?> </td><td><?php echo "hello";?></td><td><?php echo $allresultpositive['rabies_sql_positive'][5]; ?></td><td><?php echo $allresultnegative['rabies_sql_negative'][5]; ;?> </td><td><?php echo $allresultpositive['rabies_sql_positive'][5]+$allresultnegative['rabies_sql_negative'][5];?></td><td><?php echo $allresultpositive['ppr_sql_positive'][5]; ?></td><td><?php echo $allresultnegative['ppr_sql_negative'][5] ;?> </td><td><?php echo $allresultpositive['ppr_sql_positive'][5]+$allresultnegative['ppr_sql_negative'][5];?></td></tr>
	
	<tr><td>Magh</td><td><?php echo $allresultpositive['ai_sql_positive'][6]; ?></td><td><?php echo $allresultnegative['ai_sql_negative'][6]; ;?> </td><td><?php echo$allresultpositive['ai_sql_positive'][6]+$allresultnegative['ai_sql_negative'][6];?></td><td><?php echo $allresultpositive['ibd_sql_positive'][6]; ?></td><td><?php echo $allresultnegative['idb_sql_negative'][6];?> </td><td><?php echo $allresultpositive['ibd_sql_positive'][6]+$allresultnegative['idb_sql_negative'][6];?></td><td><?php echo $allresultpositive['nd_sql_positive'][6]; ?></td><td><?php echo $allresultnegative['nd_sql_negative'][6];?> </td><td><?php echo $allresultpositive['nd_sql_positive'][6]+ $allresultnegative['nd_sql_negative'][6];?></td><td><?php echo "pox positive here"; ?></td><td><?php echo"pox negative here";?> </td><td><?php echo "hello";?></td><td><?php echo $allresultpositive['rabies_sql_positive'][6]; ?></td><td><?php echo $allresultnegative['rabies_sql_negative'][6]; ;?> </td><td><?php echo $allresultpositive['rabies_sql_positive'][6]+$allresultnegative['rabies_sql_negative'][6];?></td><td><?php echo $allresultpositive['ppr_sql_positive'][6]; ?></td><td><?php echo $allresultnegative['ppr_sql_negative'][6] ;?> </td><td><?php echo $allresultpositive['ppr_sql_positive'][6]+$allresultnegative['ppr_sql_negative'][6];?></td></tr>
	
	<tr><td>Falgun</td><td><?php echo $allresultpositive['ai_sql_positive'][7]; ?></td><td><?php echo $allresultnegative['ai_sql_negative'][7]; ;?> </td><td><?php echo$allresultpositive['ai_sql_positive'][7]+$allresultnegative['ai_sql_negative'][7];?></td><td><?php echo $allresultpositive['ibd_sql_positive'][7]; ?></td><td><?php echo $allresultnegative['idb_sql_negative'][7];?> </td><td><?php echo $allresultpositive['ibd_sql_positive'][7]+$allresultnegative['idb_sql_negative'][7];?></td><td><?php echo $allresultpositive['nd_sql_positive'][7]; ?></td><td><?php echo $allresultnegative['nd_sql_negative'][7];?> </td><td><?php echo $allresultpositive['nd_sql_positive'][7]+ $allresultnegative['nd_sql_negative'][7];?></td><td><?php echo "pox positive here"; ?></td><td><?php echo"pox negative here";?> </td><td><?php echo "hello";?></td><td><?php echo $allresultpositive['rabies_sql_positive'][7]; ?></td><td><?php echo $allresultnegative['rabies_sql_negative'][7]; ;?> </td><td><?php echo $allresultpositive['rabies_sql_positive'][7]+$allresultnegative['rabies_sql_negative'][7];?></td><td><?php echo $allresultpositive['ppr_sql_positive'][7]; ?></td><td><?php echo $allresultnegative['ppr_sql_negative'][7] ;?> </td><td><?php echo $allresultpositive['ppr_sql_positive'][7]+$allresultnegative['ppr_sql_negative'][7];?></td></tr>
	
	<tr><td>Chaitra</td><td><?php echo $allresultpositive['ai_sql_positive'][8]; ?></td><td><?php echo $allresultnegative['ai_sql_negative'][8]; ;?> </td><td><?php echo$allresultpositive['ai_sql_positive'][8]+$allresultnegative['ai_sql_negative'][8];?></td><td><?php echo $allresultpositive['ibd_sql_positive'][8]; ?></td><td><?php echo $allresultnegative['idb_sql_negative'][8];?> </td><td><?php echo $allresultpositive['ibd_sql_positive'][8]+$allresultnegative['idb_sql_negative'][8];?></td><td><?php echo $allresultpositive['nd_sql_positive'][8]; ?></td><td><?php echo $allresultnegative['nd_sql_negative'][8];?> </td><td><?php echo $allresultpositive['nd_sql_positive'][8]+ $allresultnegative['nd_sql_negative'][8];?></td><td><?php echo "pox positive here"; ?></td><td><?php echo"pox negative here";?> </td><td><?php echo "hello";?></td><td><?php echo $allresultpositive['rabies_sql_positive'][8]; ?></td><td><?php echo $allresultnegative['rabies_sql_negative'][8]; ;?> </td><td><?php echo $allresultpositive['rabies_sql_positive'][8]+$allresultnegative['rabies_sql_negative'][8];?></td><td><?php echo $allresultpositive['ppr_sql_positive'][8]; ?></td><td><?php echo $allresultnegative['ppr_sql_negative'][8] ;?> </td><td><?php echo $allresultpositive['ppr_sql_positive'][8]+$allresultnegative['ppr_sql_negative'][8];?></td></tr>
	
	<tr><td>Baishak</td><td><?php echo $allresultpositive['ai_sql_positive'][9]; ?></td><td><?php echo $allresultnegative['ai_sql_negative'][9]; ;?> </td><td><?php echo$allresultpositive['ai_sql_positive'][9]+$allresultnegative['ai_sql_negative'][9];?></td><td><?php echo $allresultpositive['ibd_sql_positive'][9]; ?></td><td><?php echo $allresultnegative['idb_sql_negative'][9];?> </td><td><?php echo $allresultpositive['ibd_sql_positive'][9]+$allresultnegative['idb_sql_negative'][9];?></td><td><?php echo $allresultpositive['nd_sql_positive'][9]; ?></td><td><?php echo $allresultnegative['nd_sql_negative'][9];?> </td><td><?php echo $allresultpositive['nd_sql_positive'][9]+ $allresultnegative['nd_sql_negative'][9];?></td><td><?php echo "pox positive here"; ?></td><td><?php echo"pox negative here";?> </td><td><?php echo "hello";?></td><td><?php echo $allresultpositive['rabies_sql_positive'][9]; ?></td><td><?php echo $allresultnegative['rabies_sql_negative'][9]; ;?> </td><td><?php echo $allresultpositive['rabies_sql_positive'][9]+$allresultnegative['rabies_sql_negative'][9];?></td><td><?php echo $allresultpositive['ppr_sql_positive'][9]; ?></td><td><?php echo $allresultnegative['ppr_sql_negative'][9] ;?> </td><td><?php echo $allresultpositive['ppr_sql_positive'][9]+$allresultnegative['ppr_sql_negative'][9];?></td></tr>
	
	<tr><td>Jestha</td><td><?php echo $allresultpositive['ai_sql_positive'][10]; ?></td><td><?php echo $allresultnegative['ai_sql_negative'][10]; ;?> </td><td><?php echo$allresultpositive['ai_sql_positive'][10]+$allresultnegative['ai_sql_negative'][10];?></td><td><?php echo $allresultpositive['ibd_sql_positive'][10]; ?></td><td><?php echo $allresultnegative['idb_sql_negative'][10];?> </td><td><?php echo $allresultpositive['ibd_sql_positive'][10]+$allresultnegative['idb_sql_negative'][10];?></td><td><?php echo $allresultpositive['nd_sql_positive'][10]; ?></td><td><?php echo $allresultnegative['nd_sql_negative'][10];?> </td><td><?php echo $allresultpositive['nd_sql_positive'][10]+ $allresultnegative['nd_sql_negative'][10];?></td><td><?php echo "pox positive here"; ?></td><td><?php echo"pox negative here";?> </td><td><?php echo "hello";?></td><td><?php echo $allresultpositive['rabies_sql_positive'][10]; ?></td><td><?php echo $allresultnegative['rabies_sql_negative'][10]; ;?> </td><td><?php echo $allresultpositive['rabies_sql_positive'][10]+$allresultnegative['rabies_sql_negative'][10];?></td><td><?php echo $allresultpositive['ppr_sql_positive'][10]; ?></td><td><?php echo $allresultnegative['ppr_sql_negative'][10] ;?> </td><td><?php echo $allresultpositive['ppr_sql_positive'][10]+$allresultnegative['ppr_sql_negative'][10];?></td></tr>
	
	<tr><td>Ashad</td><td><?php echo $allresultpositive['ai_sql_positive'][11]; ?></td><td><?php echo $allresultnegative['ai_sql_negative'][11]; ;?> </td><td><?php echo$allresultpositive['ai_sql_positive'][11]+$allresultnegative['ai_sql_negative'][11];?></td><td><?php echo $allresultpositive['ibd_sql_positive'][11]; ?></td><td><?php echo $allresultnegative['idb_sql_negative'][11];?> </td><td><?php echo $allresultpositive['ibd_sql_positive'][11]+$allresultnegative['idb_sql_negative'][11];?></td><td><?php echo $allresultpositive['nd_sql_positive'][11]; ?></td><td><?php echo $allresultnegative['nd_sql_negative'][11];?> </td><td><?php echo $allresultpositive['nd_sql_positive'][11]+ $allresultnegative['nd_sql_negative'][11];?></td><td><?php echo "pox positive here"; ?></td><td><?php echo"pox negative here";?> </td><td><?php echo "hello";?></td><td><?php echo $allresultpositive['rabies_sql_positive'][11]; ?></td><td><?php echo $allresultnegative['rabies_sql_negative'][11]; ;?> </td><td><?php echo $allresultpositive['rabies_sql_positive'][11]+$allresultnegative['rabies_sql_negative'][11];?></td><td><?php echo $allresultpositive['ppr_sql_positive'][11]; ?></td><td><?php echo $allresultnegative['ppr_sql_negative'][11] ;?> </td><td><?php echo $allresultpositive['ppr_sql_positive'][11]+$allresultnegative['ppr_sql_negative'][11];?></td></tr>
	
<?php } ?>	
	
<?php if(isset($result))
{

?>

	<table class="table table-striped" id="table"><thead>
	<tr><th rowspan='2'>S No. </th><th rowspan='2'>Species</th><th colspan='3'><center>Flu A</center></th></tr>
	<tr><th>Total Sample Tested</th><th>Total Negative</th><th>Total Positive</th></tr></thead>
	
<tbody>

<?php $result1 = $this->lims_animal_type_model->show_all_animal_type_for_final_report();
foreach ($result1->result() as $row2)
{ ?>
<tr> <td>1</td><td>	 <?php $animal_type= $row2->animal_type; echo $animal_type; ?> </td>

<?php $result1 = $this->lims_final_report_model->count_negative($row2->animal_type);  
foreach($result1->result() as $row3)
{
$positive = $row3->rat; 
}

$result2 = $this->lims_final_report_model->count_positive($row2->animal_type);  foreach($result2->result() as $row4)
{
$negative = $row4->rat; 
}

$sum= $positive+ $negative;
 ?>

<td> <?php echo $sum;?> </td><td> <?php echo $positive; ?> </td><td> <?php echo $negative ?> </td><?php

}

 ?> </td>
<?php } ?>
</tbody?
</table>
