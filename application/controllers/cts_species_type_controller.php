<?php 
class Lims_species_type_controller extends CI_controller
{
	public function __construct()
	{
	parent::__construct();
		$this->load->model('lims_species_type_model');
		$this->load->model('lims_role_model');
		$this->load->model('lims_department_model');
		$this->load->model('lims_login');
	}
	public function index()
	{
		$data1['positions'] = $this->lims_role_model->show_all_position_role();
		$data1['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data1['departments'] = $this->lims_department_model->getDepartment();
		$data1['new_message'] = $this->lims_login->check_new_message();
		$this->load->view('lims_header');
		$this->load->view('lims_navigation',$data1);
		$this->load->view('species/lims_new_species_type_registration');
		$this->load->view('lims_footer');
	}
	public function add_new_species_type()
	{
		$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->lims_department_model->getDepartment();
		$data['new_message'] = $this->lims_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'])
	{
	$data['positions'] = $this->lims_role_model->show_all_position_role();
		$this->load->view('lims_header');
		$this->load->view('lims_navigation',$data);
		$this->load->view('species/lims_new_species_type_registration1');
		$this->load->view('lims_footer');
	}
	else
	{
	redirect(base_url());
	}
	}
	public function insert_species_type_details()
	{
		if($this->input->is_ajax_request())
		{
		$data['result'] = $this->lims_species_type_model->insert_species_type_details();
		if($data['result'] == false)
			{
			echo " 0";
			}
			else
			{	
				foreach($data['result']->result() as $row)
				{
				$species_type_details = array(
					'species_type' => $row->species_type
					);
				}
				echo json_encode($species_type_details);
			}
		}
		else
		{
		redirect(404);
		}
	
	}
	
	public function insert_new_species_type()
	{
		$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));	
		$data['departments'] = $this->lims_department_model->getDepartment();
		$data['new_message'] = $this->lims_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
			if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'])
			{
				$data['positions'] = $this->lims_role_model->show_all_position_role();
				$data['result'] = $this->lims_species_type_model->insert_new_species_type();
				$this->load->view('lims_header');
				$this->load->view('lims_navigation',$data);
				$this->load->view('species/lims_new_species_type_registration1', $data);
				$this->load->view('lims_footer');
		}
			else
		{
			redirect(base_url());
		}
	}
	
		public function edit_species_type_by_id_form($species_type_id)
	{	
		$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['new_message'] = $this->lims_login->check_new_message();
		$data['departments'] = $this->lims_department_model->getDepartment();

		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit'])
			{
		$data['positions'] = $this->lims_role_model->show_all_position_role();
		$data['species_type'] = $this->lims_species_type_model->get_details_of_species_type_by_id($species_type_id);
		$this->load->view('lims_header');
		$this->load->view('lims_navigation',$data);
		$this->load->view('species/lims_edit_species_type_by_id_form',$data);
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function edit_species_type_by_id($species_type_id)
	{
		$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->lims_department_model->getDepartment();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit'])
		{
		$data['result'] = $this->lims_species_type_model->edit_species_type_by_id($species_type_id);
		redirect(base_url().'index.php/lims_species_type_controller/show_all_species_type');
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function show_all_species_type($offset=0)
	{
		$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['new_message'] = $this->lims_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit']|| $role[3]['add']|| $role[3]['delete']) 
		{
		$data['positions'] = $this->lims_role_model->show_all_position_role();
		$data['departments'] = $this->lims_department_model->getDepartment();
		$data['offset'] = $offset;
		$config = array();
		$config["base_url"] = base_url() . "index.php/lims_species_type_controller/show_all_species_type";
		$config["total_rows"] = $this->db->count_all("species_type");
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["results"] = $this->lims_species_type_model->show_all_species_type($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();
		$this->load->view('lims_header');
		$this->load->view('lims_navigation',$data);
		$this->load->view('species/lims_show_all_species_type',$data);
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function delete_species_type_by_id($species_type_id)
	{
	$data['roles_info'] = $this->lims_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['delete'])
		{
		$data['result'] = $this->lims_species_type_model->delete_species_type_by_id($species_type_id);
		redirect(base_url().'index.php/lims_species_type_controller/show_all_species_type');
		}
		else
		{
			redirect(base_url());
		}
	}
	public function search_species_type_detail($key)
	{
	$this->lims_species_type_model->search_species_type_detail($key);
	}
	public function check_species_type()
	{
	$result = $this->lims_species_type_model->check_species_type();
		if($result)
		echo " 1";
		else
		echo " 0";
	}

	
	
	}
