<div class="container">
	<br>
	<center><div id="login">
		<h3>Project Tracking System </h3>
        <?php if(isset($updated)&&$updated==true):?>
       
		<div class="alert alert-success">Password Changed Successfully! Please login below.</div>
         <?php endif;?>
	        <form method="post" class="form-signin" role="form" enctype="multipart/form-data" action="<?php echo base_url()?>index.php/cts_controller/validate_credentials ">
	        <?php
			if(isset($error)) 
			{
			echo "<div class='alert alert-danger' role='alert' id='login-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Log In  Failed! Invalid Username/Password</b></div>";
			}
			?>
	            <div>
	              <input type="text" placeholder="Email..." class="form-control" name="user_email" required></td>
	            </div>
	          
	            <div>
	          
	              <input type="password" placeholder="Password..." class="form-control" name="user_password" required>
	            </div>

	            <div class="pull-right">
        	   		<button type="submit" name="submit" class="btn btn-default submit">Sign In</button>
				 <a href= "<?php echo base_url()?>index.php/cts_change_password_controller"> Forget Password?</a>
           		</div
				
	        ></form>
		
	</div><!--end of login-->
    </center>
</div><!--end of container-->
</body>
</html>