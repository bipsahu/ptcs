<?php

class cts_notification_model extends CI_model
{

	public function insertNotification($task_id)
	{
		// $data = array(
		// 'email' => $this->input->post('email')
		// );
		
		// $result = $this->db->get_where('user', array('user_email' => $data['email']));
		// return $result;
	}

	
	public function insert_comment_task($task_id = '')
	{
		// echo "<pre>";
		// print_r($_POST);die;
		foreach($_POST['textarea'] as $key =>$value)
		{
			// echo $value;
			$data['description'] = $value;
			$data['date'] = date('Y-m-d');
			$data['comment_by'] = $this->session->userdata['email'];
			$data['task_id'] = $task_id;
			// print_r($data);die;
			$check = $this->db->insert('comment', $data);
		}
		return $check;
		// echo $task_id;
		// print_r($data);die;
		// print_r($this->session->userdata['email']);die;
	}

	public function insert_meeting_detail()
	{
		// echo "<pre>"; print_r($_POST);die;
		// echo $this->session->userdata('position_id');die;
		$data1 = $this->db->query("SELECT * FROM meeting group by unique_id order by unique_id desc ")->result();
		// print_r($data1);die;
		if(empty($data1))
		{
			$data['unique_id'] = 1;
		}
		else
		{
			$data['unique_id'] = $data1[0]->unique_id +1 ;
			// echo "j";die;
		}
		// print_r($data);die;
		// $data[0]->unique_id
		foreach($_POST['task']['member'] as $key => $value)
		{
			$data['meeting_date'] = $_POST['task']['meeting_date'];
			$data['meeting_time'] = $_POST['task']['meeting_time'];
			$data['meeting_title'] = $_POST['task']['meeting_title'];
			$data['meeting_location'] = $_POST['task']['meeting_location'];
			$data['meeting_details'] = $_POST['task']['meeting_details'];
			$data['created_by'] = $this->session->userdata('user_id');
			$data['member_id'] = $value;
			if($value == $this->session->userdata('user_id'))
			{
				$data['status'] = 1;
			}
			else
			{
				$data['status'] = 2;
			}
			$result = $this->db->insert('meeting',$data);
			// print_r($this->db->last_query());
		}
		// die;
		return $result;
	}

	public function get_meeting_info()
	{
		$date = date('Y-m-d');
		// echo "<pre>";
		$member_id = $this->session->userdata('user_id');
		// print_r($member_id);die;
		// echo $date;
		if ($this->session->userdata('user_position') == "Admin") 
		{
			$this->db->group_by('unique_id');
			$result = $this->db->get_where('meeting')->result_array();
		}
		else
		{
			$this->db->FROM('meeting');
			$this->db->group_by('unique_id');
			$this->db->where(array( 'member_id' => $member_id));
			$this->db->or_where(array('created_by' => $member_id));
			$result = $this->db->get()->result_array(); 
		}

		// print_r($result);die;
		foreach ($result as $key => $value) {
			# code...
			if($value['meeting_date'] >= $date)
			{
				$meeting_date = $value['meeting_date'];
				$meeting_title = $value['meeting_title'];
				$meeting_time = $value['meeting_time'];
				$data[$key] = $value;
				$data[$key]['assigned_to'] = $this->db->query("SELECT * FROM meeting left join user on meeting.member_id = user.user_id where meeting.meeting_date = '$meeting_date' AND meeting.meeting_title = '$meeting_title' and meeting.meeting_time = '$meeting_time'")->result_array();
				// print_r($this->db->last_query());die;
			}
			// else echo "nothing to display";
		}
			// print_r($data);die;
		return $data;
	}

	public function delete_by_user_id($user_id)
	{
		$this->db->delete('meeting', array('member_id' => $user_id));
		$this->db->delete('notification', array('user_id' => $user_id));


		$list_task = $this->db->query("SELECT * FROM task_registration")->result_array();
		foreach($list_task as $task)
		{
			$members = json_decode($task['members'], true);
			$members_completed = json_decode($task['completed_hour'], true);
			if(isset($members[$user_id]) || isset($members_completed[$user_id]))
			{
				if(isset($members[$user_id]))
				{
					unset($members[$user_id]);
					$member['members'] = json_encode($members);

				}

				if(isset($members_completed[$user_id]))
				{
					unset($members_completed[$user_id]);
					$member['completed_hour'] = json_encode($members_completed);

				}
					$this->db->where('task_id', $task['task_id']);
					$this->db->update('task_registration', $member);
			}
			// print_r($members);
			// die;
		}
	}
}
?>
