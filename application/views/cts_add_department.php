<?php
function getChildOptions($departments){
$outputs="";
	foreach ($departments as $department){
		
		if(count($department['sub_departments']) >0){
			$outputs.= '<optgroup label="'.$department['departments']['dep_name'].'">';
			$outputs.= getChildOptions($department['sub_departments'], $outputs);
			$outputs.="</optgroup>";
		}else{
		$outputs.= '<option value="'.$department['departments']['dep_id'].'">'.$department['departments']['dep_name'].'</option>';
		}
	}
	return $outputs;
}

?>

<div class="container">
	<div id="img-icon">
      <img src="<?php echo base_url();?>resource/images/department-icon.jpg" align="left"/>
    </div>
    <div id="breadcumb-text">
      <ol class="breadcrumb">
        <li><strong>Department</strong></li>&nbsp;&nbsp;
        <li class="vertical-divider"></li>
        <li class="active">Add New Department</li>&nbsp;&nbsp;
        <li class="vertical-divider"></li>
        <li><a href="<?php echo base_url();?>index.php/lims_controller/show_all_department">List Department</a></li>
      </ol>
    </div>
    <hr>
<?php
if(isset($result))
{
	if($result == 1)
	{
	echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Department Created!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Department Creation Failed!</b></div>";
}
?>
<table class="table" id="table">
<form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/lims_controller/insert_department_details">
<tr><td>Enter Department Name</td><td><input type="text" class="form-control" name="dep_name" required></tr>
<?php //print_r($departments);

$options="<select name='parent_department' class='form-control'><option value=''>--Top Level--</option>";

foreach ($departments as $department){
		if(count($department['sub_departments']) >0){
		
			$options.= '<optgroup label="'.$department['department']['dep_name'].'">';
			$options.=getChildOptions($department['sub_departments']);
			$options.="</optgroup>";
		}else{
		$options.= '<option value="'.$department['department']['dep_id'].'">'.$department['department']['dep_name'].'</option>';
		}
}
$options.="</select>";
?>
<tr><td colspan="2"><?php echo $options;?><?php  /* foreach($all_departments as $row) */
//{
?>
<option value="<?php //echo $row->dep_id;?>"><?php //echo $row->dep_name;?></option>
<?php

?></td></tr>
<tr><td colspan="2"><input type="submit" name="submit" value="Submit" class="btn btn-primary"></td></tr>
</form>
</table>
</div>
</body>
</html>