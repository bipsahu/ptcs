<div class="container table-small">
<h3>Add New Project</h3><br>
  <div class="alert alert-success insured_alert">Successfully Registered!</div>

    <table class="table">

        <form method="post" id="insure_form">
            <tr>
                <td><label>Project Name <span class="red">*</span></label></td>
                <td><input type="text" class="form-control" name="insure_name" required></td>
            </tr>
            <tr>
                <td><label>Description <span class="red">*</span></label></td>
                <td><textarea class="form-control" name="address" required></textarea></td>
            </tr>


            <tr>
                <td colspan="2"><button type="button" id="insure_submit" name="submit" class="btn btn-primary">Submit</button></td>
            </tr>
            <input type="hidden" name="customer_id" id="customer_id" value=""/>
        </form>
    </table>
</div><!---end of container-->