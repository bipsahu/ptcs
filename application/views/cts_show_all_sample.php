<div class="container">
<h1>All Samples</h1>
<?php
if(!empty($result_indicator))
{
	if($result_indicator == 1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Deletion Successful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Deletion Failed!</b></div>";
}
?>
<table class="table table-striped">
<tr><th>Sample Type</th><th>Total Sample</th><th>Edit</th><th>Delete</th></tr>
<?php
foreach($result->result() as $row){
?>
<tr>
<td><?php echo $row->sample_type;?></td>
<td><?php echo $row->total_sample;?></td>
<td><a href="<?php echo base_url()."index.php/lims_customer_controller/edit_sample_by_id_form/".$row->sid;?>"><div class='glyphicon glyphicon-edit'></div></a></td> 
<td><a href="<?php echo base_url()."index.php/lims_customer_controller/delete_sample_by_id/".$row->sid;?>"><div class='glyphicon glyphicon-trash'></div></a></td>
</tr>
<?php
}
?>

<tr><td colspan="4"> <?php echo $links; ?></td></tr>
<tr>
<td><a class="btn btn-primary" href="<?php echo base_url();?>/index.php/lims_customer_controller/add_new_sample">Add New sample</a>
</td>
</tr>
</table>
</div>
</body>
</html>