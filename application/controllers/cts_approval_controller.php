<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class lims_approval_controller extends CI_controller
{
	public function __construct()
	{
	parent::__construct();
	$this->load->model('lims_approval_model');
	}
	public function approve_sample()
	{
	$this->lims_approval_model->approve_sample();
	}
}