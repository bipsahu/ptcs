<div class="container">
  <h3>Edit Branch</h3><br>

        <form method="post" role="form" enctype="multipart/form-data" action="<?php echo site_url('cts_branch_controller/edit_branch_by_id');?>" id="edit_branch_form">
        <input type="hidden" name="branch_id" value="<?php echo $result[0]['branch_id'];?>">
              <table class="table" id="table">

			<tr>
              <td><label>Branch Name </label></td>
              <td><input type="text" class="form-control" name="branch_name" value="<?php if($result[0]['branch_name']) echo $result[0]['branch_name'];?>" required> </td>
            </tr>
<tr>
              <td><label>Address </label></td>
              <td><textarea class="form-control" name="address" required  row="5"><?php if($result[0]['address']) echo $result[0]['address'];?> </textarea></td>
            </tr>
            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary" id="branch_submit">Save</button></td>
            </tr>
          </table>
        </form>
    
</div><!---end of container-->