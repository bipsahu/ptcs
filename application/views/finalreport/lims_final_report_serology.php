<div class="container">
<center><h1>Serology Annual Report</h1></center>
<table class="table table-striped" id="table">
<form action="<?php echo base_url();?>index.php/lims_final_report_controller/serology_final_report_execute/<?php echo $lab_id;?>" method="get">
<tr><td>
<select name="year" class="form-control">
<option value="2071/72">2071/72</option>
<option value="2072/73">2072/73</option>
<option value="2073/74">2073/74</option>
<option value="2074/75">2074/75</option>
</select></td>
<td>
<select name="investigation_requested" class="form-control">
<?php 

foreach( $investigation_requested->result() as $row )
			{ ?>
<option value="<?php echo $row->investigation_requested_id; ?> "><?php echo $row->investigation_requested; ?></option>
<?php } ?>
</select></td>
<td><select name="test_method" class="form-control">
<option value="ELISA">ELISA</option>
<option value="PAT">PAT</option>
<option value="Others">Others</option>
</select></td>
<td><input type="submit" class="btn btn-primary" value="Submit"></td>
</tr>
</table>
<?php 
if(isset($investigation)){
echo "<h3>The Analysis Report for : ";
echo $investigation." ";
}
if(isset($year))
echo "for the year of ".$year;
if(isset($test_method))
{
echo " using Test Method:".$test_method."</h3>";
}
?>
<table class="table table-striped">
<thead><tr><th rowspan= '2'>S No.</th><th rowspan='2'>District</th><th rowspan='2'>No. of Sample Tested </th><th colspan='2'>Test Result </th><th rowspan='2'>Percentage Positive (%)</th></tr>
<tr><th>Positive</th> <th>Negative </th> </tr> </thead>
<?php

if(isset($result_positive)){

foreach($result_positive->result() as $row)
{
$district_positive_arr[] = $row->customer_district;
$positive_arr[] = $row->pcount;
}
$i=0;

foreach($result_negative->result() as $row)
{
$negative_mul[$row->customer_district][] = $row->ncount;
$i++; 
}
$i = 0;
foreach($result_positive->result() as $row)
{
if(isset($negative_mul[$row->customer_district]))
{
$negative = $negative_mul[$row->customer_district][0];
}
else
$negative = 0;
$total = $row->pcount+$negative;
$positive_percent = number_format(($row->pcount/$total)*100,2,'.','');
echo "<tr><td>".($i+1)."</td><td>".$row->customer_district."</td><td>".$total."</td><td>".$row->pcount."</td><td>".$negative."</td><td>".$positive_percent."</td></tr>";
$i++;
}
	if(isset($negative_mul))
	{
	foreach($negative_mul as $key=>$val)
	{
	if(!in_array($key,$district_positive_arr))
	echo "<tr><td>".($i+1)."</td><td>".$key."</td><td>".($val[0])."</td><td>0</td><td>".($val[0])."</td><td>0</td></tr>";
	}
	}


}