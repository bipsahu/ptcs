<?php
class cts_entry_person extends CI_Model 
{
	public function get_all_today_entries($offset)
	{
	date_default_timezone_set("UTC");
	$date = date("Y-m-d");
	$this->db->select('*');
	$this->db->from('task_registration');
	//$this->db->join('customer','task_registration.customer_id = customer.cid');
	$this->db->order_by("task_id", "desc");
	$this->db->where('assign_date',$date);
	$data['count'] = $this->db->get()->num_rows();
	$query = $this->db->last_query();
	$data['result'] = $this->db->query($query.' LIMIT '.($offset*10).',10');
	return $data;
	}
	public function published_virology_reports($offset = 0)
	{
	$this->db->select('*');
	$this->db->from('task_registration');
	$this->db->join('virology', 'task_registration.task_id = virology.task_id');
	$this->db->join('customer','task_registration.customer_id = customer.cid');
	$this->db->order_by("virology_id", "desc");
	$this->db->where('publish_bit',1);
	$data['count'] = $this->db->get()->num_rows();
	$query = $this->db->last_query();
	$offset = $offset*5;
	$data['result'] = $this->db->query($query.' LIMIT '.$offset.',5');
	return $data;
	}
	public function published_serology_reports($offset = 0)
	{
	$this->db->select('*');
	$this->db->from('task_registration');
	$this->db->join('serology', 'task_registration.task_id = serology.task_id');
	$this->db->join('customer','task_registration.customer_id = customer.cid');
	$this->db->order_by("serology_id", "desc");
	$this->db->where('publish_bit',1);
	$data['count'] = $this->db->get()->num_rows();
	$query = $this->db->last_query();
	$offset = $offset*5;
	$data['result'] = $this->db->query($query.' LIMIT '.$offset.',5');
	return $data;
	}
	public function published_biology_reports($offset = 0)
	{
	$this->db->select('*');
	$this->db->from('task_registration');
	$this->db->join('biology', 'task_registration.task_id = biology.task_id');
	$this->db->join('customer','task_registration.customer_id = customer.cid');
	$this->db->order_by("biology_id", "desc");
	$this->db->where('publish_bit',1);
	$data['count'] = $this->db->get()->num_rows();
	$query = $this->db->last_query();
	$offset = $offset*5;
	$data['result'] = $this->db->query($query.' LIMIT '.$offset.',5');
	return $data;
	}
}
