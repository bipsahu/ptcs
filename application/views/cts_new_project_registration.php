<div class="container">
    <div class='alert alert-danger' role='alert' id="project_alert_error">
        <div class='glyphicon glyphicon-remove'></div>
        &nbsp;<b>Project Already Exists</b>
    </div>
    <div class='alert alert-success' role='alert' id="project_alert_success">
        <div class='glyphicon glyphicon-remove'></div>
        &nbsp;<b>Project Added Successfully !!!</b>
    </div>
    <h3>Project Registration Form</h3><br>

    <form method="post" role="form" enctype="multipart/form-data" id="project_form">
        <table class="table" id="table">
            <tr>
                <td><label>Project Name </label></td>
                <td><input type="text" class="form-control" name="project_name" required></td>
            </tr>

            <tr>
                <td colspan="2">
                    <button type="submit" name="submit" class="btn btn-primary" id="project_submit">Save</button>
                </td>
            </tr>
        </table>
    </form>

</div><!---end of container-->