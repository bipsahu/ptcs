<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cts_customer_controller extends CI_controller {
	public function __construct()
	{
	parent::__construct();
	$this->load->model('cts_customer_model');
	$this->load->model('cts_insure_model');
	$this->load->model('cts_claim_model');
	$this->load->model('cts_department_model');
	$this->load->model('cts_class_model');
	$this->load->model('cts_role_model');
	$this->load->model('cts_login');
	
	}
	public function index() {
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		$data1['positions'] = $this->cts_role_model->show_all_position_role();
		$data1['departments'] = $this->cts_department_model->getDepartment();
		$data1['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data1);
		$this->load->view('customer/cts_registration');
	}
	
	public function add_new_customer()
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'])
		{
			$data['positions'] = $this->cts_role_model->show_all_position_role();
			$data['new_message'] = $this->cts_login->check_new_message();
			$this->load->view('cts_header');
			$this->load->view('cts_navigation',$data);
			$this->load->view('customer/cts_new_customer');
		}
		else
		{
		redirect(base_url());
		}
	}
	
	public function insert_customer()
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'])
		{
			$data['positions'] = $this->cts_role_model->show_all_position_role();
			$data['new_message'] = $this->cts_login->check_new_message();
			$data['result'] = $this->cts_customer_model->insert_new_customer();
			$this->load->view('cts_header');
			$this->load->view('cts_navigation',$data);
			$this->load->view('customer/cts_new_customer',$data);
		}
		else
		{
		redirect(base_url());
		}
	}
	
	public function insert_customer_details()
	{
		if(!$this->input->is_ajax_request())
		{
		redirect(404);
		}
		else
		{
				$data['result'] = $this->cts_customer_model->insert_customer_details();
					/* if ($data['result'])
					{
						redirect(base_url().'index.php/cts_claim_controller',refresh);
					}
					else
					{
						$this->load->view('cts_registration');
					} */
		}
	}
	public function show_all_customer($offset = 0)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
	$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
	$data['positions'] = $this->cts_role_model->show_all_position_role();
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'] || $role[3]['edit'] || $role[3]['delete']) {
	$data['offset'] = $offset;
	$config = array();
	$config["base_url"] = base_url() . "index.php/cts_customer_controller/show_all_customer";
	$config["total_rows"] = $this->db->count_all("customer");
	$config["per_page"] = 20;
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config);
    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $data["results"] = $this->cts_customer_model->show_all_customer($config["per_page"], $page);
    $data["links"] = $this->pagination->create_links();
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('customer/cts_show_all_customer',$data);
	}

	else 
	{
	redirect(base_url());
		/* $this->load->view('cts_header');
		$this->load->view('cts_login'); */
	}
	}
	
	public function show_all_customer_desc($offset=0)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
	$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'] || $role[3]['edit'] || $role[3]['delete']) {
	$data1['positions'] = $this->cts_role_model->show_all_position_role();
	$data['offset'] = $offset;
	$config = array();
	$config["base_url"] = base_url() . "index.php/cts_customer_controller/show_all_customer_desc";
	$config["total_rows"] = $this->db->count_all("customer");
	$config["per_page"] = 20;
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config);
    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $data["results"] = $this->cts_customer_model->show_all_customer_desc($config["per_page"], $page);
    $data["links"] = $this->pagination->create_links();
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('customer/cts_show_all_customer',$data);
	}

	else 
	{
	redirect(base_url());
		/* $this->load->view('cts_header');
		$this->load->view('cts_login'); */
	}
	}
	
	public function show_all_customer_by_last_asc($offset=0)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
	$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'] || $role[3]['edit'] || $role[3]['delete'])
	{
	$data['positions'] = $this->cts_role_model->show_all_position_role();
	$data['offset'] = $offset;
	$config = array();
	$config["base_url"] = base_url() . "index.php/cts_customer_controller/show_all_customer_by_last_asc";
	$config["total_rows"] = $this->db->count_all("customer");
	$config["per_page"] = 20;
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config);
    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $data["results"] = $this->cts_customer_model->show_all_customer_by_last_asc($config["per_page"], $page);
    $data["links"] = $this->pagination->create_links();
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('customer/cts_show_all_customer',$data);
	}
	else 
	{
	redirect(base_url());
	/* 	$this->load->view('cts_header');
		$this->load->view('cts_login'); */
	}
	}
	public function show_all_customer_by_last_desc($offset=0)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
	$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'] || $role[3]['edit'] || $role[3]['delete']) {
	$data['positions'] = $this->cts_role_model->show_all_position_role();
	$data['offset'] = $offset;
	$config = array();
	$config["base_url"] = base_url() . "index.php/cts_customer_controller/show_all_customer_by_last_desc";
	$config["total_rows"] = $this->db->count_all("customer");
	$config["per_page"] = 20;
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config);
    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $data["results"] = $this->cts_customer_model->show_all_customer_by_last_desc($config["per_page"], $page);
    $data["links"] = $this->pagination->create_links();
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('customer/cts_show_all_customer',$data);
	}

	else 
	{
	redirect(base_url());
		/* $this->load->view('cts_header');
		$this->load->view('cts_login'); */
	}
	}
	
	public function show_all_customer_by_district_asc($offset=0)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
	$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'] || $role[3]['edit'] || $role[3]['delete']) 
	{
	$data['positions'] = $this->cts_role_model->show_all_position_role();
	$data['offset'] = $offset;
	$config = array();
	$config["base_url"] = base_url() . "index.php/cts_customer_controller/show_all_customer_by_district_asc";
	$config["total_rows"] = $this->db->count_all("customer");
	$config["per_page"] = 20;
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config);
    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $data["results"] = $this->cts_customer_model->show_all_customer_by_district_asc($config["per_page"], $page);
    $data["links"] = $this->pagination->create_links();
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('customer/cts_show_all_customer',$data);
	}

	else 
	{
	redirect(base_url());
		/* $this->load->view('cts_header');
		$this->load->view('cts_login'); */
	}
	}
	
	public function show_all_customer_by_district_desc($offset=0)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
	$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'] || $role[3]['edit'] || $role[3]['delete']) {
	$data['positions'] = $this->cts_role_model->show_all_position_role();
	$data['offset'] = $offset;
	$config = array();
	$config["base_url"] = base_url() . "index.php/cts_customer_controller/show_all_customer_by_district_desc";
	$config["total_rows"] = $this->db->count_all("customer");
	$config["per_page"] = 20;
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config);
    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $data["results"] = $this->cts_customer_model->show_all_customer_by_district_desc($config["per_page"], $page);
    $data["links"] = $this->pagination->create_links();
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('customer/cts_show_all_customer',$data);
	}

	else 
	{
	redirect(base_url());
	/* 	$this->load->view('cts_header');
		$this->load->view('cts_login'); */
	}
	}
	
	public function edit_customer_by_id_form($cid)
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
		}
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit'])
	{
	$data['positions'] = $this->cts_role_model->show_all_position_role();
	$data['customer_details'] = $this->cts_customer_model->get_details_of_customer_by_id($cid);
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('customer/cts_registration_edit_form',$data);
	}
	else
	{
	redirect(base_url());
		/* $this->load->view("cts_header");
		$this->load->view('cts_login'); */
	}
	
	}
	
	public function edit_customer_by_id($cid)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$i++;
		}
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit'])
	{
	$data['result'] = $this->cts_customer_model->edit_customer_by_id($cid);
	redirect(base_url().'index.php/cts_customer_controller/show_all_customer');
	}
	else
	{
	redirect(base_url());
		/* $this->load->view("cts_header");
		$this->load->view('cts_login'); */
	}
	}
	
	public function delete_customer_by_id($cid)
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$i++;
		}
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['delete'])
	{
	$data['result'] = $this->cts_customer_model->delete_customer_by_id($cid);
	redirect(base_url().'index.php/cts_customer_controller/show_all_customer');
	}

	else
	{
		redirect(base_url());
		/* $this->load->view("cts_header");
		$this->load->view('cts_login'); */
	}
	}
	public function search_customer($key)
	{
	$this->cts_customer_model->search_customer($key);
	}
	public function check_customer()
	{
	$result = $this->cts_customer_model->check_customer();
	if($result)
	echo " 1";
	else
	echo " 0";
	}
/* 	public function add_new_customer() 
	{
		$this->load->view('cts_header');
		$this->load->view('cts_navigation');
		$this->load->view('claimregistration/cts_claim_registration');
	} */
	
//insure Registration	

	public function add_new_insure() 
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data);
		$this->load->view('insure/cts_new_insure_registration');
		$this->load->view('cts_footer');
	}

	public function add_new_insure1() 
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[3]['add']) 
		{
		$data['positions'] = $this->cts_role_model->show_all_position_role();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data);
		$this->load->view('insure/cts_new_insure_registration1');
		$this->load->view('cts_footer');
		}
		else
		{
			redirect(base_url());
		}
		
	
	}

	public function insert_new_insure1()
	{		
	
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin" || $role[3]['add']) 
		{
			$data['positions'] = $this->cts_role_model->show_all_position_role();
			$data['result'] = $this->cts_insure_model->insert_insure_details1();
			$this->load->view('cts_header');
			$this->load->view('cts_navigation',$data);
			$this->load->view('insure/cts_new_insure_registration1',$data);
			$this->load->view('cts_footer');
		}
		else
		{
			redirect(base_url());
		}
	}


	public function insert_new_insure()
	{
		if(!$this->input->is_ajax_request())
		{
			
			redirect(404);
		}
		else
		{
					$data['result'] = $this->cts_insure_model->insert_insure_details();
		}
	}
	public function search_insure($key)
	{
	$this->cts_insure_model->search_insure($key);
	}
	
	public function show_all_insure($offset = 0)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
	$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
	$data['positions'] = $this->cts_role_model->show_all_position_role();
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit']|| $role[3]['add']|| $role[3]['delete']) {
	$data['offset'] = $offset;
	$config = array();
	$config["base_url"] = base_url() . "index.php/cts_customer_controller/show_all_insure";
	$config["total_rows"] = $this->db->count_all("insure");
	$config["per_page"] = 20;
	$config["uri_segment"] = 3;
	//$this->pagination->initialize($config);
    //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $data["results"] = $this->cts_insure_model->show_all_insure();
    $data["links"] = $this->pagination->create_links();
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('insure/cts_show_all_insure',$data);
	}

	else 
	{
	redirect(base_url());
		/* $this->load->view('cts_header');
		$this->load->view('cts_login'); */
	}
	}
	
	public function edit_insure_form($iid)
	{
	
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
	$data['new_message'] = $this->cts_login->check_new_message();
	$data['insure_details'] = $this->cts_insure_model->get_insure_by_id($iid);
		
	$this->load->view("cts_header");
	$this->load->view("cts_navigation",$data);
	$this->load->view("insure/cts_insure_edit_form",$data);
	$this->load->view('cts_footer');
	
	}
	
	public function edit_insure_by_id()
	{
	$iid =  $this->input->post('iid');
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
	if($this->session->userdata('is_logged_in') || $this->session->userdata('position_id') == "5" ||  $this->session->userdata('positino_id') == "5" ||$this->session->userdata('positino_id') == "4" || $role[3]['edit'] == 1)
	{
	$data['result'] = $this->cts_insure_model->edit_insure_by_id($iid);
	//$this->load->view('cts_header');
	redirect(base_url().'index.php/cts_customer_controller/show_all_insure');
	}
	else
	{
	redirect(base_url());
		/* $this->load->view("cts_header");
		$this->load->view('cts_login'); */
	}
	}
	
	
	public function edit_insure_by_id_form($iid)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
	if($this->session->userdata('is_logged_in') || $this->session->userdata('position_id') == "5" ||  $this->session->userdata('positino_id') == "5" ||$this->session->userdata('positino_id') == "4" || $role[3]['edit'] == 1)
	{
	$data['new_message'] = $this->cts_login->check_new_message();
	$data['positions'] = $this->cts_role_model->show_all_position_role();
	$data['insure_details'] = $this->cts_insure_model->get_details_of_insure_by_id($iid);
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('insure/cts_insure_edit_form',$data);
	}

	else
	{
	redirect(base_url());
		/* $this->load->view("cts_header");
		$this->load->view('cts_login'); */
	}
	
	}

	
	public function delete_insured_by_id($iid)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		
	if($this->session->userdata('is_logged_in') || $this->session->userdata('position_id') == "5" ||  $this->session->userdata('positino_id') == "5" ||$this->session->userdata('positino_id') == "4" || $role[3]['edit'] == 1)	{
	$data['result'] = $this->cts_insure_model->delete_insure_by_id($iid);
	redirect(base_url().'index.php/cts_customer_controller/show_all_insure');

	}

	else
	{
	redirect(base_url());
		/* $this->load->view("cts_header");
		$this->load->view('cts_login'); */
	}
	}
	
	public function check_insure()
	{
	$result = $this->cts_insure_model->check_insure();
	if($result)
	echo " 1";
	else
	echo " 0";
	}
//claim Registration

/* 	public function add_new_claim() 
	{
		$data['department_list'] = $this->cts_department_model->getDepartment();
		$this->load->view('cts_header');
		$this->load->view('cts_new_claim_registration', $data);
	} */
	
	public function insert_new_claim()
	{
		
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin")
		{
		
			if(isset($_POST['submit']))
			{
				if($this->input->post('hidden') != false)
				{
				echo "helo";
				}
				else
				{
					$data['result'] = $this->cts_claim_model->insert_claim_details();
					if ($data['result'])
					{
						$data['result'] = $this->cts_department_model->getDepartment();
						
					{
					
						redirect(base_url().'index.php/cts_claim_controller',refresh);
					}
					}
					else
					{
						$this->load->view('cts_new_claim_registration');
					}
				}
			}
			else
			{
			redirect(404);
			}
			
		}
		
		else 
		{
		redirect(base_url());
			/* $this->load->view("cts_header");
			$this->load->view('cts_login'); */
		}
		
	}
	
	public function show_all_claim($offset = 1)
	{
	$data['positions'] = $this->cts_role_model->show_all_position_role();
	$data['departments'] = $this->cts_department_model->getDepartment();
	$data['new_message'] = $this->cts_login->check_new_message();
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin") {
	$data['offset'] = $offset;
	$config = array();
	$config["base_url"] = base_url() . "index.php/cts_customer_controller/show_all_claim";
	$config["total_rows"] = $this->db->count_all("claim");
	$config["per_page"] = 20;
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config);
    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $data["results"] = $this->cts_claim_model->show_all_claim($config["per_page"], $page);
    $data["links"] = $this->pagination->create_links();
	$data1['positions'] = $this->cts_role_model->show_all_position_role();
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('cts_show_all_claim',$data);
	}

	else 
	{
	redirect(base_url());
		/* $this->load->view('cts_header');
		$this->load->view('cts_login'); */
	}
	}
	
	public function edit_claim_by_id_form($sid)
	{
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin")
	{
	$data['positions'] = $this->cts_role_model->show_all_position_role();
	$data['new_message'] = $this->cts_login->check_new_message();
	$data['departments'] = $this->cts_department_model->getDepartment();
	$data['claim_details'] = $this->cts_claim_model->get_details_of_claim_by_id($sid);
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('cts_edit_claim_by_id_form',$data);
	}

	else
	{
	redirect(base_url());
		/* $this->load->view("cts_header");
		$this->load->view('cts_login'); */
	}
	
	}
	
	public function edit_claim_by_id($sid)
	{
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin")
	{
	$data['result'] = $this->cts_claim_model->edit_claim_by_id($sid);
	//$this->load->view('cts_header');
	$this->show_all_claim();
	}
	else
	{
	redirect(base_url());
		/* $this->load->view("cts_header");
		$this->load->view('cts_login'); */
	}
	}
	
	public function delete_claim_by_id($sid)
	{
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin")
	{
	$data['result'] = $this->cts_customer_model->delete_claim_by_id($sid);
	$this->show_all_claim($data['result']);
	}

	else
	{
	redirect(base_url());
		/* $this->load->view("cts_header");
		$this->load->view('cts_login'); */
	}
	}
	
	
//add new class

	public function add_new_class() 
	{
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin")
		{
		$this->load->view('cts_header');
		$this->load->view('class/cts_new_class_registration', $data);
		}
		else
		{
		redirect(base_url());
		}
	}
	public function add_new_class1() 
	{
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'])
		{
		$data['positions'] = $this->cts_role_model->show_all_position_role();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data);
		$this->load->view('class/cts_new_class_registration1');
		$this->load->view('cts_footer');
		}
		else
		{
		redirect(base_url());
		}
	}
	
	public function insert_new_class()
	{
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin")
		{
		
			if(isset($_POST['submit']))
			{
				if($this->input->post('hidden') != false)
				{
				echo "helo";
				}
				else
				{
					$data['result'] = $this->cts_class_model->insert_class_details();
					if ($data['result'])
					{
						redirect(base_url().'index.php/cts_claim_controller',refresh);
					}
					else
					{
						$this->load->view('cts_new_insure_registration');
					}
				}
			}
			else
			{
			redirect(404);
			}
			
		}
		else 
		{
		redirect(base_url());
			/* $this->load->view("cts_header");
			$this->load->view('cts_login'); */
		}
		
	}
	
	public function insert_new_class1()
	{		
		$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$data['departments'] = $this->cts_department_model->getDepartment();
		$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['add'])
		{
			$data['positions'] = $this->cts_role_model->show_all_position_role();
			$data['result'] = $this->cts_class_model->insert_new_class();
			$this->load->view('cts_header');
			$this->load->view('cts_navigation',$data);
			$this->load->view('class/cts_new_class_registration1', $data);
			$this->load->view('cts_footer');
		}
		else
		{
		redirect(base_url());
		}
			
	}
	
	public function show_all_class($offset = 0)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
	$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
	$data['offset'] = $offset;
	$data['positions'] = $this->cts_role_model->show_all_position_role();
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit']|| $role[3]['add']|| $role[3]['delete']) {
	$config = array();
	$config["base_url"] = base_url() . "index.php/cts_customer_controller/show_all_class";
	$config["total_rows"] = $this->db->count_all("class");
	$config["per_page"] = 20;
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config);
    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $data["results"] = $this->cts_class_model->show_all_class($config["per_page"], $page);
    $data["links"] = $this->pagination->create_links();
	
	$data['positions'] = $this->cts_role_model->show_all_position_role();
	
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('class/cts_show_all_class',$data);
	}

	else 
	{
		redirect(base_url());
		/* $this->load->view('cts_header');
		$this->load->view('cts_login'); */
	}
	}
	
	public function edit_class_by_id_form($aid)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
	$data['new_message'] = $this->cts_login->check_new_message();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit'])
	{
	$data['positions'] = $this->cts_role_model->show_all_position_role();
	$data['class_details'] = $this->cts_class_model->get_details_of_class_by_id($aid);
	$this->load->view('cts_header');
	$this->load->view('cts_navigation',$data);
	$this->load->view('class/cts_edit_class_by_id_form',$data);
	}

	else
	{
		redirect(base_url());
		/* $this->load->view("cts_header");
		$this->load->view('cts_login'); */
	}
	
	}
	
	public function edit_class_by_id($aid)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['edit'])
	{
	$data['result'] = $this->cts_class_model->edit_class_by_id($aid);
	redirect (base_url()."index.php/cts_customer_controller/show_all_class");
	}
	else
	{
	redirect(base_url());
		/* $this->load->view("cts_header");
		$this->load->view('cts_login'); */
	}
	}
	
	public function delete_class_by_id($aid)
	{
	$data['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
	$data['departments'] = $this->cts_department_model->getDepartment();
		$i=0;
		foreach($data['roles_info']->result_array() as $key=>$value)
		{
			$role[] = $value;
			$module[] = $role[$i]['module_name'];
			$i++;
		}
	if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Entry Person" ||  $this->session->userdata('user_position') == "Admin" || $role[3]['delete']==1)
	{
	$data['result'] = $this->cts_class_model->delete_class_by_id($aid);
	redirect(base_url().'index.php/cts_customer_controller/show_all_class/'.$offset);;
	}
	else
	{
	redirect(base_url());
	}
}	
	

	
	
}