<?php
/* print_r($result->result()); */
?>
<center><h1>Messages</h1></center>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
         <h4 class="modal-title">Messages</h4>
      </div>
      <div class="modal-body">
        <table class="table table-stripped">
		<tr><td id='message' style="text-weight:bold;"></td><tr>
		</table>
      </div>
    </div>
  </div>
</div>
<table class="table datatable table-stacked">
<thead>
<tr><th>S.N.</th><th>Sent Date</th><th>Sent From</th><th>Designation</th><th>Message</th><th></th></tr>
</thead>
<tbody>
<?php
$sn_count = 1;
foreach($result->result() as $row)
{
if($row->check_status){
echo "<tr><td>".$sn_count."</td><td>".$row->assigned_date."</td><td>".$row->user_name."</td><td>".$row->user_position."</td><td><button class='view_message btn btn-primary' alt='".$row->assignment_id."'>View Message</button></td><td><input type='hidden' value='".$row->message."' id='message_".$row->assignment_id."'/></td></tr>";
}
else
{
echo "<tr><td>".$sn_count."</td><td>".$row->assigned_date."</td><td>".$row->user_name."</td><td>".$row->user_position."</td><td><button class='view_message btn btn-primary' alt='".$row->assignment_id."'>View Message</button></td><td><input type='hidden' value='".$row->message."' id='message_".$row->assignment_id."'/></td></tr>";

}
?>

<?php 
$sn_count++;
}
 ?>
 </tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){
	$(".view_message").bind('click',function(){
	view_message($(this));
	});
});
function view_message(obj)
{
	var id = obj.attr("alt");
		$.ajax
	({
	url : base_url + 'cts_assignment/change_status/'+id,
	type : 'POST',
		success:function(msg)
		{
		if(parseInt(msg) == 1)
		{
			var parent = obj.parent().parent();
			parent.css("background-color","#fff");
			$('#message').html($('#message_'+id).val());
			$("#myModal").modal('show');
		}
		else
		{
		$("#myModal").modal('hide');
		}
		}
	})
}
/* $("#view_message").each(click(function(){
	selected_id = $(this).attr("alt");
	alert(selected_id);
	/* $.ajax
	({
	'url' : base_url + '',
	data: {id:selected_id},
	type:'POST',
	success:function()
		{
		alert(msg);
		}
	}); 
});
) */
</script>
</body>
</html>