<?php

/*
 * model class to manipulate user database
 * */

class Cts_user_model extends CI_Model
{
    /* Show all users from database
    * @params :
    * @return : object of resource
    * */
    public function show_all_user($limit, $start)
    {
        $this->db->order_by("user_name", "asc");
        $this->db->limit($limit, $start);
        $query = $this->db->get('user');

        return $query;

    }

    /* Get all users from database
     * @params :
     * @return : object of resource
     * */
    public function get_all_user()
    {
        $this->db->order_by("user_name", "asc");
        $result = $this->db->get('user');
        return $result;
    }

    public function get_user_details($user_id)
    {
        $this->db->where('user_id', $user_id);
        $result = $this->db->get('user')->result_array();
        return $result[0]["user_name"];
    }

    public function show_all_user_desc($limit, $start)
    {
        $this->db->order_by("user_name", "desc");
        $this->db->limit($limit, $start);
        $query = $this->db->get('user');

        return $query;


    }

    public function show_all_position_asc($limit, $start)
    {
        $this->db->order_by("user_position", "asc");
        $this->db->limit($limit, $start);
        $query = $this->db->get('user');

        return $query;


    }

    public function show_all_position_desc($limit, $start)
    {
        $this->db->order_by("user_position", "desc");
        $this->db->limit($limit, $start);
        $query = $this->db->get('user');

        return $query;


    }


    public function add_new_user()
    {

        $user_level = $this->input->post('approve_user_level');
        if (isset($user_level) && $user_level != NULL) {
            foreach ($user_level as $key => $userlev) {
                $user_level_array[$key] = $userlev;
            }
            $user_level = json_encode((object)$user_level_array);
        } else
            $user_level = NULL;

        $data = array(
            'user_name' => $this->input->post('user_name'),
            'user_password' => md5($this->input->post('user_password')),
            'user_position' => $this->input->post('user_position'),
            'user_address' => $this->input->post('user_address'),
            'hourly_salary' => number_format($this->input->post('hourly_salary'),2),
            'user_level' => $user_level,
            'user_mobile' => $this->input->post('user_mobile'),
            'user_email' => $this->input->post('user_email'),
            'user_phone_home' => $this->input->post('user_phone_home'),
            'user_phone_office' => $this->input->post('user_phone_office'),
            'user_created_date' => date('m-d-Y'),
            'user_created_by' => $this->session->userdata('email')
        );
        // print_r($data);die;

        $result = $this->db->insert('user', $data);
        echo $this->db->_error_message();
        return $result;
    }

    public function delete_user_by_id($user_id)
    {
        $result = $this->db->delete('user', array('user_id' => $user_id));
        return $result;
    }

    public function get_details_of_user_by_id($user_id)
    {
        $result = $this->db->get_where('user', array('user_id' => $user_id));
        return $result->result_array();
    }

    public function edit_user_by_id($user_id)
    {
        $data = array(
            'user_name' => $this->input->post('user_name'),
            'user_position' => $this->input->post('user_position'),
            'user_address' => $this->input->post('user_address'),
            'user_mobile' => $this->input->post('user_mobile'),
            'user_email' => $this->input->post('user_email'),
            'user_phone_home' => $this->input->post('user_phone_home'),
            'user_phone_office' => $this->input->post('user_phone_office'),
            'hourly_salary' => number_format($this->input->post('hourly_salary'),2),
        );
        if ($this->input->post('user_password') != null)
            $data['user_password'] = md5($this->input->post('user_password'));

        $this->db->where('user_id', $user_id);
        $result = $this->db->update('user', $data);
        return $result;
    }

    public function check_email($email)
    {
        $result = $this->db->get_where('user', array('user_email' => $email));
        if ($result->num_rows() == 1)
            echo "1";
    }

    public function get_last_recorded_user()
    {
        return $this->db->insert_id();
    }

}