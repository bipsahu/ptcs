<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cts_role_controller extends CI_Controller 
{
	public function __construct()
	{
	parent::__construct();
	$this->load->model('cts_role_model');
	$this->load->model('cts_department_model');
	$this->load->model('cts_login');
	
	}
	public function add_new_role_display($result_indicator = "")
	{
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin")
		{
		$data1['positions'] = $this->cts_role_model->show_all_position_role();
		$data1['departments'] = $this->cts_department_model->getDepartment();
		$data1['new_message'] = $this->cts_login->check_new_message();
		$data['result_indicator'] = $result_indicator;
		$data1['positions'] = $this->cts_role_model->show_all_position_role();
		$data['roles_info'] = $this->cts_role_model->get_roles_data();
		$data1['roles_info'] = $this->cts_role_model->show_module_role_by_position($this->session->userdata('position_id'));
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data1);
		$this->load->view('cts_role', $data);
		$this->load->view("cts_footer");
		}
		else
		{
		redirect(base_url());
		}
	}
	public function add_role()
	{
		if($this->session->userdata('is_logged_in') && $this->session->userdata('user_position') == "Admin")
	{
			$data['result_indicator'] = $this->cts_role_model->insert_role();
			redirect(base_url()."index.php/cts_role_controller/add_new_role_display/".$data['result_indicator']);
	}
	else
		{
		redirect(base_url());
		}
	}
	public function add_module_role_display($position_id = 0)
	{
			//this function is used to show all the module roles like master data ,report user, lab, sample registration
		if($this->session->userdata('is_logged_in'))
		{	
			$data1['positions'] = $this->cts_role_model->show_all_position_role();
			$data1['new_message'] = $this->cts_login->check_new_message();
			$data1['departments'] = $this->cts_department_model->getDepartment();
			$data1['modules']  = $this->cts_role_model->show_all_modules();
			$data['roles_info']  = $this->cts_role_model->show_module_role_by_position($position_id);
			$data['position_id'] = $position_id;
			$this->load->view('cts_header');
			$this->load->view('cts_navigation',$data1);
			$this->load->view('role/cts_module_role',$data);
			$this->load->view('cts_footer');
		}
	}
	public function add_module_role($position_id)
	{
		if($this->input->post('submit')!=null){
			$data['result'] = $this->cts_role_model->add_module_role($position_id);
			$data1['departments'] = $this->cts_department_model->getDepartment();
			$data1['positions'] = $this->cts_role_model->show_all_position_role();
			$data1['modules']  = $this->cts_role_model->show_all_modules();
			$data['roles_info']  = $this->cts_role_model->show_module_role_by_position($position_id);
			$data1['new_message'] = $this->cts_login->check_new_message();
			$data['position_id'] = $position_id;
			$this->load->view('cts_header');
			$this->load->view('cts_navigation',$data1);
			$this->load->view('role/cts_module_role',$data);
			$this->load->view('cts_footer');
			}
			else
			{
			redirect(base_url().'index.php/cts_role_controller/add_module_role_display/'.$position_id);
			}
	}
}