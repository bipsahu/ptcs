<?php

/*
 * model class that manipulates indicator table
 * */

class cts_indicator_model extends CI_Model
{
    /*
     * Insert Indicator to indicator tables
     * @params : post data
     * @return : boolean
     * */
   
    public function insert_indicator_details()
    {
        if ($this->input->is_ajax_request()) 
        {
            $formData = $this->input->post("indicator");
            $data['indicator_name'] = $formData[0]["value"];
            $data['task_category_id'] = $formData[1]["value"];
            // print_r($_POST);die;
        } 
        else 
        {
            $data['indicator_name'] = $this->input->post("indicator_name");
            $data['task_category_id'] = $this->input->post("task_category");
        }

        $query_result = $this->db->get_where('indicators', array(
                                                                'indicator_name' => $data['indicator_name'],
                                                                'task_category_id' => $data['task_category_id']
                                                                ));
        if ($query_result->num_rows() == 1)
        {
            if ($this->input->is_ajax_request())
            {
                echo json_encode(false);
            }
            else
            {
                return false;
            }
        }
        else 
        {
            $result = $this->db->insert('indicators', $data);
            if ($this->input->is_ajax_request())
            {
                echo json_encode($this->db->insert_id());
            }
            else
            {
                return $result;
            }
        }
    }
    
    
    public function insert_indicator()
    {

        $data = array(
            'indicator_name' => $this->input->post('indicator_name')
        );
        $result = $this->db->get_where('indicators', array('indicator_name' => $data['indicator_name']));
        if ($result->num_rows >= 1) {
            return false;
        } else {
            $result = $this->db->insert('indicators', $data);
            $result = $this->db->get_where('indicators', array('indicator_id' => $this->db->insert_id()));
            return $result;
        }
    }

    public function insert_new_indicator()
    {
        $data = array(
            'indicator_name' => $this->input->post('indicator_name')
        );
        $result = $this->db->get_where('indicators', array('indicator_name' => $data['indicator_name']));
        if ($result->num_rows == 1) {
            return false;
        } else {
            $result = $this->db->insert('indicators', $data);
            return $result;
        }
    }

    public function edit_indicator_by_id($indicator_id)
    {
        $data = array(
            'indicator' => $this->input->post('indicator')

        );
        $result = $this->db->get_where('indicator', array('indicator' => $data['indicator']));
        if ($result->num_rows >= 1) {
            return false;
        } else {
            $this->db->where('indicator_id', $indicator_id);
            $result = $this->db->update('indicator', $data);
            return $result;
        }
    }

    /*
     * get indicator_name from indicators table by the Indicator id
     *
     * @params : indicator_id
     * @return : array
     * */
    public function get_details_of_indicator_by_id($indicator_id)
    {
        $result = $this->db->get_where('indicators', array('indicator_id' => $indicator_id))->result_array();
        return $result[0]["indicator_name"];
    }


    public function search_indicator_detail($key)
    {

        $result = $this->db->query("select * from indicator where indicator like '%" . $key . "%'");

        $indicators = array();
        foreach ($result->result() as $row) {
            $indicator = "";
            $indicator .= $row->indicator;
            $indicators[] = $indicator;
        }
        echo json_encode($indicators);
    }

    /*
     * Get all indicators from indicators table
     * @params : data limit, resource start value (gatting chunks of datas)
     * @return : object of db resource $query
     * */
    public function getIndicators($limit = NULL, $start = NULL)
    {
        $this->db->order_by("indicator_id", "asc");
        if ($start != NULL && $limit != NULL)
            $this->db->limit($limit, $start);
        $query = $this->db->get('indicators')->result_array();
        return $query;
    }
    public function getIndicator()
    {
        $result = $this->db->get('indicators');
        return $result->result();
    }

    public function get_indicator_with_type()
    {
        $this->db->select('*');
        $this->db->from('indicators');
        $this->db->order_by("indicator_name", "asc");
        // $this->db->join('indicators_type','indicators.indicator_type = indicators_type.id ');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function show_all_indicator_for_final_report()
    {


        $result1 = $this->db->get('indicator');
        return $result1;

    }

    public function delete_indicator_by_id($indicator_id)
    {
        $result = $this->db->delete('indicators', array('indicator_id' => $indicator_id));
        return $result;
    }

    public function check_indicator()
    {
        $indicator = $this->input->post('indicator');
        $result = $this->db->get_where('indicator', array('indicator' => $indicator));
        if ($result->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }
    public function get_all_indicator()
    {
        $result = $this->db->query('SELECT i.indicator_id AS child_project_id,i.indicator_name AS child_name,c.`indicator_id`,c.`indicator_name` AS parent_name FROM indicators AS i LEFT JOIN indicators AS c ON c.indicator_id = i.indicator_id');
        return $result->result_array();
    }
    
    public function get_the_indicator_by_id($project_id)
    {
        $result = $this->db->get_where('indicators', array('indicator_id' => $project_id));
        return $result->result_array();
    }
    
    public function editIndicator($project_id)
    {
        
            $data = array
            (
                'indicator_name' => $this->input->post('indicator_name'),
                // 'parent_id' => $this->input->post('parent_project')
            );
        
        $this->db->where('indicator_id', $project_id);
        $result = $this->db->update('indicators', $data);
        return $result;

    }

    // public function insert_task_category()
    // {
    //     $data = array(
    //         'name' => $this->input->post('task_category')
    //     );
    //     // print_r($data);die;
    //     $result = $this->db->get_where('task_category', array('name' => $data['name']));
    //     if ($result->num_rows == 1) {
    //         return false;
    //     } else {
    //         $result = $this->db->insert('task_category', $data);
    //         return $result;
    //     }
    // }

    // public function getTaskCategory()
    // {
    //     $result = $this->db->get('task_category');
    //     return $result->result_array();
    // }

    // public function get_task_category_by_id($taskid)
    // {
    //     $result = $this->db->get_where('task_category', array('t_category_id' => $taskid));
    //     return $result->result_array();
    // }

    // public function editTaskCategory($taskId)
    // {
    //         $data = array
    //         (
    //             'name' => $this->input->post('task_category'),
    //         );
    //         // echo $taskId;
    //     // print_r($data);die;


    //     $this->db->where('t_category_id', $taskId);
    //     $result = $this->db->update('task_category', $data);
    //     // $this->db->last_query();die;
    //     return $result;

    // }

    // public function delete_task_category_by_id($taskCategoryId)
    // {
    //     // echo $taskCategoryId;die;
    //     $result = $this->db->delete('task_category', array('t_category_id' => $taskCategoryId));
    //     return $result;
    // }

}