<div class="container">
<center><h1>All Projects</h1></center>
<?php
if(!empty($result_indicator))
{
	if($result_indicator == 1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Deletion Successful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Deletion Failed!</b></div>";
}
?>
<table class="table table-striped">
<thead><th>S No.</th><th>Project</th><th>Description</th><th>Edit</th><th>Delete</th></thead>
<?php 
$offset= $offset+1;
foreach($results->result() as $row){
$firmid = $row->fid;
?>
<tr>
<td><?php echo $offset;?></td>
<td><?php echo $row->firm_name;?></td>
<td><?php echo $row->firm_zone;?></td>

<td><a href="<?php echo base_url()."index.php/lims_customer_controller/edit_firm_by_id_form/".$firmid;?>"><div class='glyphicon glyphicon-edit'></div></a></td>
<td><a href="<?php echo base_url()."index.php/lims_customer_controller/delete_firm_by_id/".$firmid;?>"><div class='glyphicon glyphicon-trash'></div></a></td>
</tr>
<?php
$offset++;
}
?>
<tr><td colspan="11"><?php echo $links; ?></td></tr>
<tr>
<td><a class="btn btn-primary" href="<?php echo base_url();?>index.php/lims_customer_controller/add_new_firm1">Add New Firm</a>

</td>
</tr>
</table>
</div>
</body>
</html>