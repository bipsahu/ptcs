<div class="container">
  <div id="img-icon">
    <img src="<?php echo base_url();?>resource/images/add-role.jpg" align="left"/>
  </div>
  <div id="breadcumb-text">
    <ol class="breadcrumb">
      <li><strong>Role</strong></li>&nbsp;&nbsp;
      <li class="vertical-divider"></li>
      <li><a href="#">Add Lab Role</a></li>
      <li class="vertical-divider"></li>
      <li class="active">Add Module Role</li>
    </ol>
  </div>
  <hr>

  <table class="table table-striped" id="table_module_role">
    <form action="" method="post">
      <thead>
        <th>Module</th>
        <th>Add</th>
        <th>Edit</th>
        <th>Delete</th>
        <th>View</th>
      </thead>
      <tbody>
        <tr>
          <td>Report</td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
        </tr>
        <tr>
          <td>User</td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
        </tr>
        <tr>
          <td>Lab</td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
        </tr>
        <tr>
          <td>Master Data</td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
        </tr>
        <tr>
          <td>Sample</td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
          <td><input type="checkbox" class="checkbox-inline" id="" value=""></td>
        </tr>
      </body>
    </form>
  </table>

</div><!--end of container-->