<?php include 'header.php'; ?>
<div class="container">
	<br>
	<div class="glyphicon glyphicon-trash" id="notifyModal_ex1"></div>
	<div id="content2" style="display:none">
		<div id="delete-text"><b>Are you sure you want to delete?</b></div>
		<div id="yes_no"><button type="button" class="btn btn-success">Yes</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-danger">No</button></div>
	</div>
</div><!--end of container-->

	<script>
		$(function(){
		$('#notifyModal_ex1').click(function(){
			$('#content2').notifyModal({
				duration : 25000000,
				placement : 'center',
				overlay : true
				});
			});
		});
	</script>