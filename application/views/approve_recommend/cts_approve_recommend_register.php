<div class="container">
        <form method="post" role="form" class="form-horizontal" enctype="multipart/form-data" id="approve_recommend_form" name="approve_recommend_form" alt="<?php if(isset($claim_id)) echo $claim_id;?>">
          <div class="table" id="approve_table">
          <?php 
		  //check if user has access level for approval and the defined amount of approval
		 	foreach($approve_amount as $approval)
			{
				$approval_value[$approval['arul_id']] = $approval['approve_upto'];
			}
			
		if(isset($user_level_access)&&isset($access_amount)):
		 	 if((array_key_exists('1',$user_level_access)&&$access_amount<=$approval_value[2])||(array_key_exists('2',$user_level_access)&&$access_amount>$approval_value[2]&&$access_amount<=$approval_value[3])||(array_key_exists('3',$user_level_access)&&$access_amount>$approval_value[3]&&$access_amount<=$approval_value[4])):?>
			<div class="form-group approv">
            <div class="col-sm-3">
              <label class="control-label">Approve</label>
            </div>
            <div class="col-sm-9">
              <input type="radio" name="approve_recommend" id="approve" value="0"/>
            	</div>
           
            </div>
              <div class="form-group approved_amount">
                  <div class="col-sm-3">
                      <label class="control-label">Approved Amount</label>
                  </div>																									
                    <div class="col-sm-9">
                     <input type="text" name="approved_amount" id="approved_amount" />
                   </div>					
              </div>
        
           <?php endif;endif;?>
            <div class="form-group">
             <div class="col-sm-3">
              <label class="control-label">Recommend</label>
             </div>
              <div class="col-sm-9">
              	<input type="radio" name="approve_recommend" id="recommend-to" class="recommend_checkbox" value="1"/>
              </div>
          </div>
        
        <div class="form-group recommend-to">
           <div class="col-sm-3">
			<label control-label">Recommend To</label>
            </div>
             <div class="col-sm-9">
             <select name="dv_recommend" id="dv_recommend">
             	<option value="">Select User</option>
             	<?php foreach($access_users->result_array() as $acc_user):
						 if(isset($acc_user['user_level'])&&$acc_user['user_level']!=NULL){
						 	if($this->session->userdata('user_id')!=$acc_user['user_id'])
						 	echo "<option value=".$acc_user['user_id'].">".$acc_user['user_name']."</option>";
						 }
					endforeach;?>
             </select>
           </div>
           </div>
			<div class="form-group recommend-to">
             <div class="col-sm-3">
              <label class="col-sm-3 control-label">Message</label>
              </div>
               <div class="col-sm-9">
              <textarea name="approve_message" rows="5"></textarea>
              </div>
          </div>
          <div class="form-group ">
             <div class="col-sm-3 mar-20">
              <label class="col-sm-3 control-label">Remarks</label>
              </div>
               <div class="col-sm-9 mar-20">
              <textarea name="approve_remarks" rows="5"></textarea>
              </div>
          </div>
           <div class="form-group pull-left">
            <button type="submit" name="submit" class="btn btn-primary" id="discharge_voucher_approve_recommend_submit">Save</button>
           </div>
        </form>
   
	</div>
</div><!---end of container-->
</body>
<script type="text/javascript">
$(document).ready(function(){
	$(".recommend-to").hide();
	$(".approved_amount").hide();
	$(".recommend_checkbox").click(function(){
	var approved = $(this).parents(".form-group").next(".form-group").find("#approved_amount").hide();
			$(approved).val(" ");
		if($(this).is(":checked")){
			$(this).parents(".form-group").next("").show();
            $(this).parents(".form-group").next("").next("").show();
			
		}
		else{
			
			$(this).parents(".form-group").next("").next("").hide();
		}
	});
	
		$("#approve").click(function(){
			$(".recommend-to").hide();
			$("#dv_recommend").val(" ");
		if($(this).is(":checked")){
			$(".approved_amount").show();
			
		}
		else{
			
			$(".approved_amount").hide();
		}
	});
	
	
});
</script>
</html>