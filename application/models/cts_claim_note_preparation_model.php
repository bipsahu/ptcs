<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class cts_claim_note_preparation_model extends CI_model
{	
		public function __construct(){
		parent::__construct();
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|pdf|pptx|docx|doc|ppt';
		$config['max_size']	= '10000';
		$config['max_width']  = '10000';
		$config['max_height']  = '10000';
		$config['mailtype'] = 'html';
		$config['newline']   = "\r\n";
		
		$this->load->library('upload');
		$this->upload->initialize($config);
	}
	public function insert_claim_note_preparation_details($claim_id,$cn_status)
	{
		if($this->input->post('cn_prepared')!= null)
		{
		$data['cn_prepared'] = $this->input->post('cn_prepared');
		}
		$data['cn_prepared'] = $cn_status;
		$data['claim_id'] = $claim_id;
		 $this->db->select('*');
		 $this->db->where('claim_id',$claim_id);
		 $cn_count = $this->db->get('claim_note_preparation')->num_rows();
		 if($cn_count>=1){
		 	 $this->db->where('claim_id',$claim_id);
		 	$result = $this->db->update('claim_note_preparation', $data);
		}
		 else
		$result = $this->db->insert('claim_note_preparation', $data);
			if($this->db->_error_number()==1062)
			{
			return 0;
			}
			else
			{
			return $result;
			}
	}
	
	public function claim_note_checklist($claim_id)
	{
	
		if($this->input->post('claim_intimation_letter')=="on")
		{
			$data['claim_intimation_letter'] = '1';
		}
		else{
			$data['claim_intimation_letter'] = '0';
		}
		if($this->input->post('claim_form')=="on")
		{
			$data['claim_form'] = '1';
		}
		else{
			$data['claim_form'] = '0';
		}
		if($this->input->post('police_report')=="on")
		{
			$data['police_report'] = '1';
		}
		else{
			$data['police_report'] = '0';
		}
		if($this->input->post('license')=="on")
		{
			$data['license'] = '1';
		}
		else{
			$data['license'] = '0';
		}
		if($this->input->post('blue_book')=="on")
		{
			$data['blue_book'] = '1';
		}
		else{
			$data['blue_book'] = '0';
		}
		
		if($this->input->post('photographs')=="on")
		{
			$data['photographs'] = '1';
		}
		else{
			$data['photographs'] = '0';
		}
		
		if($this->input->post('invoice')=="on")
		{
			$data['invoice'] = '1';
		}
		else{
			$data['invoice'] = '0';
		}
		if($this->input->post('quotation')=="on")
		{
			$data['quotation'] = '1';
		}
		else{
			$data['quotation'] = '0';
		}
		if($this->input->post('vdc_reports')=="on")
		{
			$data['vdc_reports'] = '1';
		}
		else{
			$data['vdc_reports'] = '0';
		}
		
		if($this->input->post('survey_reports')=="on")
		{
			$data['survey_reports'] = '1';
		}
		else{
			$data['survey_reports'] = '0';
		}
		if($this->input->post('claim_access_amount'))
		{
			$data['claim_access_amount'] = $this->input->post('claim_access_amount');
		}
		if($this->input->post('surveyor_fee'))
		{
			$data['surveyor_fee'] = $this->input->post('surveyor_fee');
		}
		if($this->input->post('total_amount'))
		{
			$data['total_amount'] = $this->input->post('total_amount');
		}
		if($this->input->post('cnp_remarks'))
		{
			$data['cnp_remarks'] = $this->input->post('cnp_remarks');
		}
		$data['review_by'] = $this->session->userdata('user_id');
		//$data['claim_id'] = $claim_id;
		 $this->db->select('*');
		 $this->db->where('claim_id',$claim_id);
		 $cn_count = $this->db->get('claim_note_preparation')->num_rows();
		 if($cn_count>=1){
		 	 $this->db->where('claim_id',$claim_id);
		 	$result = $this->db->update('claim_note_preparation', $data);
		}
		 else
		$result = $this->db->insert('claim_note_preparation', $data);
			if($this->db->_error_number()==1062)
			{
			return 0;
			}
			else
			{
			return $result;
			}
	}
	
	public function insure_send_email(){
		
		
			$email_to = trim($this->input->post('email_address'));
			$email_cc = trim($this->input->post('email_cc'));
			$subject = $this->input->post('email_subject');
			$body = $this->input->post('email_body');
				 //if($this->upload->do_multi_upload("email_attach")){
				 
       				//foreach($this->upload->get_multi_upload_data() as $multi_image){
						//$attachments[] = $multi_image['full_path'];
						
					//}
				//}
			//foreach($_FILES as $key=>$email_attach){
				//if($this->upload->do_upload('email_attach')){	
					
				//	$attachment[] = $upload_data['full_path'];
					//$this->email->attach($attachment[]);
				//}
			//}
			
			
			//if(isset($attachments)){
				//foreach($attachments as $attach){
					//$this->email->attach($attach);
				//}
			//}		
			$count = count($_FILES['email_attach']['size']);
			foreach($_FILES as $key=>$value)
				for($s=0; $s<=$count-1; $s++) {
				$_FILES['userfile']['name']=$value['name'][$s];
				$_FILES['userfile']['type']    = $value['type'][$s];
				$_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
				$_FILES['userfile']['error']       = $value['error'][$s];
				$_FILES['userfile']['size']    = $value['size'][$s];  
					$config['upload_path'] = './uploads/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '100';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
				$this->load->library('upload', $config);
				$this->upload->do_upload();
				$data = $this->upload->data();
				$name_array[] = $data['file_name'];
				$file_path = FCPATH.'uploads/'.$data['file_name'];
				$this->email->attach($file_path);
			}
			
			//$this->email->bcc('them@their-example.com');
			if(isset($email_cc)) 
			$this->email->cc($email_cc);
			$this->email->from('info@shikharinsurance.com', 'Shikhar Insurance');
			$this->email->to($email_to); 
			$this->email->subject($subject);
			$this->email->message($body);	
			//print_r($this->upload->display_errors());
			$this->email->set_mailtype("html");
			$this->email->send();
			$this->email->clear(TRUE);
			//echo $this->email->print_debugger();
			
			return true;
	}
	
	public function show_all_claim_note_preparation_details()
	{
		$this->db->group_by('claim_registration.claim_id');
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('surveyor_deportation', 'claim_registration.claim_id = surveyor_deportation.claim_id','left');
		$this->db->join('claim_note_preparation', 'claim_registration.claim_id = claim_note_preparation.claim_id','left');
		$this->db->join('insure', 'claim_registration.insure = insure.insure_name','left');
		$this->db->order_by("claim_registration.claim_id", "desc"); 
		$this->db->where('status','1');
		//$this->db->or_where('status',1);
		$result = $this->db->get();
		//$this->db->where('status', 1);
		//$this->db->update('claim_registration',  array('status' => 2)); 
		return $result;
	}
	
	public function count_claim_note_preparation_details()
	{
		$this->db->group_by('claim_registration.claim_id');
		$this->db->select('*');
		$this->db->from('claim_registration');
		//$this->db->join('claim_note_preparation', 'claim_registration.claim_id = claim_note_preparation.claim_id');
		//$this->db->order_by("claim_note_preparation_id", "desc"); 
		$this->db->where('status','1');
		//$this->db->or_where('status',1);
		$result = $this->db->get()->num_rows();
		//$this->db->where('status', 1);
		//$this->db->update('claim_registration',  array('status' => 2)); 
		return $result;
	}
	
	public function show_all_claim_note_preparation_details_for_claim()
	{
		$result = $this->db->get('claim_note_preparation');
		return $result->result_array();
	
	}
	
	public function get_details_of_claim_note_preparation_by_id ($claim_note_preparation_id)
	{
	$result = $this->db->get_where('claim_note_preparation', array('claim_note_preparation_id' => $claim_note_preparation_id));
	return $result->result_array();
	}
	
	public function delete_claim_note_preparation_by_id($claim_note_preparation_id)
	{
	$result = $this->db->delete('claim_note_preparation', array('claim_note_preparation_id' => $claim_note_preparation_id)); 
	return $result;
	}
	
	public function edit_claim_note_preparation_by_id($claim_note_preparation_id)
	{
	
	$data = array(
	'egg_inoculation' => $this->input->post('egg_inoculation'),
	'plate_aggluination' => $this->input->post('pa_result'),
	'rapid_anitgen_test' => $this->input->post('rapid_anitgen_test'),
	'negri_body' =>$this->input->post('negri_body'),
	'penside_test' =>$this->input->post('penside_test'),
	'ha' => $this->input->post('ha'),
	'hi' => $this->input->post('hi'),
	'cft' => $this->input->post('cft_result'),
	'agid' => $this->input->post('agid_result'),
	'elisa' => $this->input->post('elisa_result'),
	'confirmative_diagnosis' => $this->input->post('cd_result'),
	'fat' => $this->input->post('fat')
	);
	$this->db->where('claim_note_preparation_id',$claim_note_preparation_id);
	$result = $this->db->update('claim_note_preparation', $data);
	return $result;
	}

	public function check_new_customer()
	{
		$this->db->group_by('claim_registration.claim_id');
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('claim_note_preparation', 'claim_registration.claim_id = claim_note_preparation.claim_id');
		$this->db->order_by("claim_note_preparation_id", "desc"); 
		$this->db->where('status',0);
		$result = $this->db->get();
		$i = 0;
		$data_arr = array();
		foreach($result->result() as $row)
		{
			$data_arr[$i]["claim_note_preparation_id"] = $row->claim_note_preparation_id;
			$data_arr[$i]["labno"] = $row->labno;
			$data_arr[$i]["claim_id"] = $row->claim_id;
			$data_arr[$i]['customer_name'] = $row->customer_name;
			$data_arr[$i]['firm_name'] = $row->firm_name;
			$data_arr[$i]['animal'] = $row->animal;
			$data_arr[$i]['claim_type'] = $row->claim_type;
			$data_arr[$i]['no_of_claim'] = $row->no_of_claim;
			$data_arr[$i]['age'] = $row->age;
			$data_arr[$i]['sex'] = $row->sex;
			$data_arr[$i]['animal_type'] = $row->animal_type;
			$data_arr[$i]['species_type'] = $row->species_type;
			$data_arr[$i]['investigation_requested'] = $row->investigation_requested;
			$data_arr[$i]['egg_inoculation'] = $row->egg_inoculation;
			$data_arr[$i]['pa_result'] = $row->plate_aggluination;
			$data_arr[$i]['ha'] = $row->ha;
			$data_arr[$i]['hi'] = $row->hi;
			$data_arr[$i]['cft_result'] = $row->cft;
			$data_arr[$i]['agid_result'] = $row->agid;
			$data_arr[$i]['elisa_result'] = $row->elisa;
			$data_arr[$i]['cd_result'] = $row->confirmative_diagnosis;
			$this->db->where('claim_id',$row->claim_id);
			$this->db->update('claim_note_preparation',array('status'=>1));
			$i++;
		}
		echo json_encode($data_arr);
	}

	public function set_accept_on($claim_id)
	{
	$this->db->where('claim_id',$claim_id);
	$result = $this->db->update('claim_note_preparation',array('status' => 3));
	return $result;
	}	
	public function search_pending_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('claim_note_preparation', 'claim_registration.claim_id = claim_note_preparation.claim_id');
		$this->db->order_by("claim_note_preparation_id", "desc"); 
		$this->db->where('status',5);
		$this->db->where('publish_bit',0);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}
	public function set_publish_on($claim_note_preparation_id)
	{
	$this->db->where('claim_note_preparation_id',$claim_note_preparation_id);
	$result = $this->db->update('claim_note_preparation',array('publish_bit' => 1));
	return $result;
	}
	public function search_on_progress_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('claim_note_preparation', 'claim_registration.claim_id = claim_note_preparation.claim_id');
		$this->db->order_by("claim_note_preparation_id", "desc"); 
		$this->db->where('status',3);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}
	public function set_completed_on($claim_note_preparation_id)
	{
	$this->db->where('claim_note_preparation_id',$claim_note_preparation_id);
	$result = $this->db->update('claim_note_preparation',array('status' => 4));
	return $result;
	}
	public function search_completed_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('claim_note_preparation', 'claim_registration.claim_id = claim_note_preparation.claim_id');
		$this->db->order_by("claim_note_preparation_id", "desc"); 
		$this->db->where('status',4);
		$this->db->or_where('status',5);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}

	public function generate_report($claim_note_preparation_id)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('claim_note_preparation', 'claim_registration.claim_id = claim_note_preparation.claim_id');
		$this->db->join('customer','claim_registration.customer_id = customer.cid');
		$this->db->where('claim_note_preparation_id',$claim_note_preparation_id);
		$result = $this->db->get();
		return $result;
	}



}