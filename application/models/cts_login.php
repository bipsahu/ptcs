<?php

class cts_login extends CI_Model
{

    public function can_log_in()
    {

        $this->db->where('user_email', $this->input->post('user_email'));
        $this->db->where('user_password', md5($this->input->post('user_password')));
        //echo $this->input->post('user_password');
        //echo $this->input->post('user_email');
        $query = $this->db->get('user');
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function get_user_id($email)
    {
        $this->db->select('user_id');
        $user_id = $this->db->get_where('user', array('user_email' => $email));
        foreach ($user_id->result() as $row) {
            $user_id = $row->user_id;
        }
        return $user_id;
    }

    public function get_position_id($email)
    {
        /* $this->db->select('user_position');
        $user_position = $this->db->get_where('user',array('user_email'=>$email)); */
        /* foreach($user_position->result() as $row)
        {
        $user_position = $row->user_position;
        } */
        $user_position = $this->get_position($email);
        $this->db->select('position_id');
        $position_id = $this->db->get_where('position', array('position_name' => $user_position));
        foreach ($position_id->result() as $row) {
            $position_id = $row->position_id;
        }
        return $position_id;
    }

    public function get_user_name($email)
    {
        $this->db->select('user_name');
        $user_name = $this->db->get_where('user', array('user_email' => $email));
        foreach ($user_name->result() as $row) {
            $user_name = $row->user_name;
        }
        return $user_name;
    }

    public function get_position($email)
    {
        $this->db->select('user_position');
        $user_position = $this->db->get_where('user', array('user_email' => $email));
        foreach ($user_position->result() as $row) {
            $user_position = $row->user_position;
        }
        return $user_position;
    }

    public function get_row_by_position_id($position_id)
    {
        $result = $this->db->get_where('role', array('position_id' => $position_id));
        return $result;
    }

    public function check_new_message()
    {
        $result = $this->db->get_where('assignment_table', array('send_to' => $this->session->userdata('user_id'), 'check_status' => 0));
        $number_of_unread_message = $result->num_rows();
        return $number_of_unread_message;
    }

    public function check_new_message1()
    {
        //$result = $this->db->get_where('assignment_table',array('status'=>0,'send_to'=>$this->session->userdata('user_id')));
        $this->db->select('*');
        $this->db->from('user');
        $this->db->join('assignment_table', 'user.user_id = assignment_table.send_from');
        $this->db->where('send_to', $this->session->userdata('user_id'));
        $this->db->where('status', 0);
        $this->db->order_by("assignment_id", "desc");
        $result = $this->db->get();
        $i = 0;
        $data = array();
        foreach ($result->result() as $row) {
            $data[$i]['assignment_id'] = $row->assignment_id;
            $data[$i]['assigned_date'] = $row->assigned_date;
            $data[$i]['user_name'] = $row->user_name;
            $data[$i]['user_address'] = $row->user_address;
            $data[$i]['user_position'] = $row->user_position;
            $data[$i]['user_email'] = $row->user_email;
            $data[$i]['user_phone_home'] = $row->user_phone_home;
            $data[$i]['user_mobile'] = $row->user_mobile;
            $data[$i]['message'] = $row->message;
            $data[$i]['lab_no'] = $row->lab_no;
            $data[$i]['dep_name'] = $row->dep_name;
            $i++;
            $this->db->where('assignment_id', $row->assignment_id);
            $this->db->update('assignment_table', array('status' => 1));
        }
        return $data;
    }
}