<?php 
$role= array();
$modules= array();
if(isset($roles_info))
{
$i=0;
foreach($roles_info->result_array() as $key=>$value)
{
$role[] = $value;
$module[] = $role[$i]['module_name'];
}
}
?>



<div class="container">
	<!-- <div id="img-icon">
      <img class="image-icon" src="<?php echo base_url();?>resource/images/department-icon.jpg" align="left"/>
    </div> -->
    <div id="breadcumb-text">
      <ul class="nav nav-tabs">
        <li class="tab_length"><a href="#"><strong>Departments</strong></a></li>
        <!-- <li class="vertical-divider"></li>&nbsp; -->
        <li class="active tab_length"><a>Add New Department</a></li>
        <!-- <li class="vertical-divider"></li>&nbsp; -->
		<?php 
		if(strtolower($this->session->userdata('user_position')) == "admin"  || $role[2]['edit'] ==1 || $role[2]['delete'] == 1){
		?>
        <li class="tab_length"><a href="<?php echo base_url();?>index.php/cts_controller/show_all_department">List Departments</a></li>
		<?php } ?>
      </ul>
    </div>
    <hr>
<?php
if(isset($result))
{
	if($result == 1)
	{
	echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Department Created!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Department Creation Failed!</b></div>";
}
?>
<table class="table" id="table" >
  <form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/cts_controller/insert_department_details">
    <tr>
      <td class="first_td">Department Name</td>
      <td><input type="text" class="form-control" name="dep_name" required></td>
    </tr>
    <tr>
      <td colspan="2"><input type="submit" name="submit" value="Submit" class="btn btn-primary"></td>
    </tr>
  </form>
</table>
</div>
</body>
</html>