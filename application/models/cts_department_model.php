<?php

/*
 * model class that deals with database logic for department table
 * */

class cts_department_model extends CI_Model
{
    /*
     * insert data to department table either through ajax post or normal post
     * @params : none
     * @return : json or respective view
     * */
    public function insert_department_details()
    {
        if ($this->input->is_ajax_request()) {
            $formData = $this->input->post("department");
            $data['dep_name'] = $formData[0]["value"];

        } else {
            $data['dep_name'] = $this->input->post("dep_name");
        }
        $query_result = $this->db->get_where('department', array('dep_name' => $data['dep_name']));
        if ($query_result->num_rows() == 1) {
            if ($this->input->is_ajax_request())
                echo json_encode(false);
            else
                return false;
        } else {
            $result = $this->db->insert('department', $data);
            if ($this->input->is_ajax_request())
                echo json_encode($this->db->insert_id());
            else
                return $result;

        }
    }

    public function getDepartment()
    {
        $result = $this->db->get('department');
        return $result->result();
    }

    public function insert_the_department_of_user($user_id)
    {
        $dep_chk_boxes = $this->input->post('dep_chk_group');
        if (!empty($dep_chk_boxes)) {
            for ($i = 0; $i < count($dep_chk_boxes); $i++) {
                $data = array
                (
                    'user_id' => $user_id,
                    'dep_id' => $dep_chk_boxes[$i]
                );
                $this->db->insert('user_department', $data);
            }
        }
    }

    public function get_department_of_user($user_id)
    {
        $this->db->select('dep_id');
        $result = $this->db->get_where('user_department', array('user_id' => $user_id));
        return $result->result_array();
    }

    public function edit_the_department_of_user($user_id)
    {
        $this->db->delete('user_department', array('user_id' => $user_id));
        $this->insert_the_department_of_user($user_id);
    }

    public function get_all_department_with_parent()
    {
        $result = $this->db->query('SELECT p.dep_id AS child_dep_id,p.dep_name AS child_name,c.`dep_id`,c.`dep_name` AS parent_name FROM department AS p LEFT JOIN department AS c ON c.dep_id = p.parent_id');
        return $result->result_array();
    }

    public function delete_the_department($dep_id)
    {
        $result = $this->db->delete('department', array('dep_id' => $dep_id));
        return $result;
    }

    public function get_the_department_by_id($dep_id)
    {
        $result = $this->db->get_where('department', array('dep_id' => $dep_id));
        return $result;
    }

    public function edit_the_department($dep_id)
    {
        if ($this->input->post('parent_department') == "") {
            $data = array
            (
                'dep_name' => $this->input->post('dep_name'),
                'parent_id' => Null
            );
        } else {
            $data = array
            (
                'dep_name' => $this->input->post('dep_name'),
                'parent_id' => $this->input->post('parent_department')
            );
        }
        $this->db->where('dep_id', $dep_id);
        $result = $this->db->update('department', $data);
        return $result;

    }


    public function get_sub_departments($dep_id)
    {
        $result = $this->db->get_where('department', array('parent_id' => $dep_id));
        return $result->result_array();
    }

    public function get_null_parent_department()
    {
        $result = $this->db->get_where('department', array('parent_id' => Null));
        return $result->result_array();
    }

}