<div class="container">
<div class='alert alert-success' role='alert' id="claim_branch_success">Successfully Registered!</div>
<div class='alert alert-danger' role='alert' id="branch_indicator_alert"'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>branch Already Exists</b></div>
  <h3>Add New Branch</h3><br>

        <form method="post" role="form" width="200px" nctype="multipart/form-data" id="claim_branch_form">
              <table class="table" id="table">

			<tr>
              <td><label>Branch Name </label></td>
              <td><input type="text" class="form-control" name="branch_name"  required> </td>
            </tr>
<tr>
              <td><label>Address </label></td>
              <td><textarea class="form-control" name="address" required rows="5" > </textarea></td>
            </tr>
            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary" id="branch_submit">Save</button></td>
            </tr>
          </table>
        </form>
    
</div>