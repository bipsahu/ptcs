-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 24, 2016 at 08:16 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pts`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignment_table`
--

CREATE TABLE IF NOT EXISTS `assignment_table` (
`assignment_id` int(11) NOT NULL,
  `assigned_date` varchar(255) NOT NULL,
  `send_from` int(11) NOT NULL,
  `send_to` int(11) NOT NULL,
  `message` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `check_status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `assignment_table`
--

INSERT INTO `assignment_table` (`assignment_id`, `assigned_date`, `send_from`, `send_to`, `message`, `status`, `check_status`) VALUES
(1, '10/17/2014', 4, 12, 'please check the report', 1, 1),
(2, '12/29/2014', 2, 12, 'The claim no 2 has been sent to you. please check it.', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE IF NOT EXISTS `branches` (
`branch_id` int(11) NOT NULL,
  `branch_name` varchar(200) NOT NULL,
  `address` varchar(250) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`branch_id`, `branch_name`, `address`) VALUES
(1, 'Head Office', 'Kathmandu,Nepal'),
(2, 'Biratnagar', 'Biratnagar'),
(3, 'Pokhara', ' Pokhara, Nepal');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
`dep_id` int(11) NOT NULL,
  `dep_name` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dep_id`, `dep_name`, `parent_id`) VALUES
(1, 'R&D', NULL),
(2, 'Admin', NULL),
(3, 'Human Resource', NULL),
(4, 'Accounts', NULL),
(5, 'it', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `indicators`
--

CREATE TABLE IF NOT EXISTS `indicators` (
`indicator_id` int(11) NOT NULL,
  `indicator_name` varchar(255) DEFAULT NULL,
  `indicator_type` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `indicators`
--

INSERT INTO `indicators` (`indicator_id`, `indicator_name`, `indicator_type`) VALUES
(2, 'Indicator 2', 6),
(3, 'Indicator 3', 8),
(4, 'Indicator 4', 5),
(5, 'Indicator 5', 6),
(6, 'asdasd', 8),
(7, 'indicator 1', 5);

-- --------------------------------------------------------

--
-- Table structure for table `indicators_type`
--

CREATE TABLE IF NOT EXISTS `indicators_type` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `indicators_type`
--

INSERT INTO `indicators_type` (`id`, `name`) VALUES
(5, 'FIeldwork'),
(6, 'Labwork'),
(8, 'Deskwork');

-- --------------------------------------------------------

--
-- Table structure for table `master_role`
--

CREATE TABLE IF NOT EXISTS `master_role` (
`master_role_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `module_name` varchar(255) NOT NULL,
  `add` tinyint(1) NOT NULL DEFAULT '0',
  `edit` tinyint(1) NOT NULL DEFAULT '0',
  `delete` tinyint(1) NOT NULL DEFAULT '0',
  `view` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=571 ;

--
-- Dumping data for table `master_role`
--

INSERT INTO `master_role` (`master_role_id`, `position_id`, `module_id`, `module_name`, `add`, `edit`, `delete`, `view`) VALUES
(106, 4, 1, 'Final report', 1, 0, 0, 0),
(107, 4, 2, 'User', 1, 0, 0, 0),
(108, 4, 3, 'Lab', 0, 1, 0, 0),
(109, 4, 4, 'Master data', 0, 1, 0, 0),
(110, 4, 5, 'Sample', 0, 0, 1, 1),
(136, 5, 1, 'Final report', 0, 0, 0, 0),
(137, 5, 2, 'User', 1, 0, 0, 0),
(138, 5, 3, 'Lab', 1, 1, 0, 0),
(139, 5, 4, 'Master data', 0, 0, 0, 0),
(140, 5, 5, 'Sample', 1, 0, 0, 0),
(431, 3, 1, 'Final report', 1, 0, 0, 0),
(432, 3, 2, 'User', 0, 0, 0, 0),
(433, 3, 3, 'Lab', 0, 0, 0, 0),
(434, 3, 4, 'Master data', 0, 0, 0, 0),
(435, 3, 5, 'Sample', 0, 0, 0, 0),
(526, 6, 1, 'Final report', 0, 0, 0, 0),
(527, 6, 2, 'User', 0, 0, 0, 0),
(528, 6, 3, 'Lab', 0, 0, 0, 0),
(529, 6, 4, 'Master data', 1, 1, 1, 1),
(530, 6, 5, 'Sample', 1, 1, 1, 1),
(531, 7, 1, 'Final report', 0, 0, 0, 0),
(532, 7, 2, 'User', 0, 0, 0, 0),
(533, 7, 3, 'Lab', 0, 0, 0, 0),
(534, 7, 4, 'Master data', 0, 0, 0, 0),
(535, 7, 5, 'Sample', 0, 0, 0, 0),
(561, 2, 1, 'Final report', 1, 1, 1, 1),
(562, 2, 2, 'User', 0, 0, 0, 1),
(563, 2, 3, 'Lab', 1, 1, 1, 1),
(564, 2, 4, 'Master data', 0, 0, 0, 0),
(565, 2, 5, 'Sample', 1, 0, 0, 0),
(566, 1, 1, 'Final report', 1, 1, 0, 1),
(567, 1, 2, 'User', 1, 1, 1, 1),
(568, 1, 3, 'Lab', 1, 1, 1, 0),
(569, 1, 4, 'Master data', 1, 1, 1, 0),
(570, 1, 5, 'Sample', 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
`module_id` int(11) NOT NULL,
  `modules` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `modules`) VALUES
(1, 'Final report'),
(2, 'User'),
(3, 'Lab'),
(4, 'Master data'),
(5, 'Sample');

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE IF NOT EXISTS `position` (
`position_id` int(11) NOT NULL,
  `position_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`position_id`, `position_name`) VALUES
(1, 'Admin'),
(2, 'Manager');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
`project_id` int(11) NOT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `project_taskcategory` longtext NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`project_id`, `project_name`, `project_taskcategory`) VALUES
(2, 'Project 2', ''),
(3, 'Project 3', ''),
(4, 'Project 4', ''),
(5, 'Project 5', ''),
(6, 'Project 6', ''),
(9, 'Genetical analysis for white Tiger', ''),
(20, 'myfirstproject', '{"6":"10","4":"30","3":"60"}'),
(24, 'improvement company', '{"8":"30","7":"30","9":"40"}');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
`role_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `add_role` tinyint(1) DEFAULT NULL,
  `edit_role` tinyint(1) DEFAULT NULL,
  `approve_role` tinyint(1) DEFAULT NULL,
  `report_generate_role` tinyint(1) DEFAULT NULL,
  `delete_role` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=457 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `position_id`, `add_role`, `edit_role`, `approve_role`, `report_generate_role`, `delete_role`) VALUES
(455, 1, 0, 1, 0, 1, 0),
(456, 2, 1, 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `task_category`
--

CREATE TABLE IF NOT EXISTS `task_category` (
`t_category_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `task_category`
--

INSERT INTO `task_category` (`t_category_id`, `name`) VALUES
(3, 'try'),
(4, 'try3'),
(6, 'try 4'),
(7, 'fieldwork'),
(8, 'laborwork'),
(9, 'report');

-- --------------------------------------------------------

--
-- Table structure for table `task_registration`
--

CREATE TABLE IF NOT EXISTS `task_registration` (
`task_id` int(11) NOT NULL,
  `assign_date` date DEFAULT NULL,
  `regd_no` int(255) DEFAULT NULL,
  `task_name` varchar(500) NOT NULL,
  `indicators` longtext NOT NULL,
  `members` longtext NOT NULL,
  `project` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  `cost_allocated` int(20) NOT NULL,
  `percent_complete` int(11) NOT NULL,
  `achieved` longtext NOT NULL,
  `remarks` text,
  `status` enum('0','1','2') NOT NULL COMMENT '"0" = "inactive","1" = "active","2" = "complete"',
  `assigned_by` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `task_category_id` int(11) NOT NULL,
  `task_category_completed` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `task_registration`
--

INSERT INTO `task_registration` (`task_id`, `assign_date`, `regd_no`, `task_name`, `indicators`, `members`, `project`, `department`, `cost_allocated`, `percent_complete`, `achieved`, `remarks`, `status`, `assigned_by`, `created_at`, `updated_at`, `task_category_id`, `task_category_completed`) VALUES
(1, '2016-01-20', 1, 'Sample', '{"2":"50","1":"50"}', '{"2":"5","1":"4"}', 1, 1, 20000, 100, '', 'remarks', '1', 'Admin (admin@admin.com)', '2016-02-11 11:25:19', '2016-01-26 10:21:23', 0, 0),
(9, '2016-01-20', 2, 'Test Task', '{"2":"40","1":"60"}', '{"2":"5","1":"4"}', 1, 1, 200000, 50, '', 'remarks', '1', 'test', '2016-02-11 11:25:16', '2016-01-27 10:22:54', 0, 0),
(10, '2016-01-20', 1, 'New task', '{"2":"50","1":"50"}', '{"2":"5","1":"4"}', 1, 1, 20000, 100, '', 'remarks', '2', 'Admin (admin@admin.com)', '2016-01-29 11:49:14', '2016-01-28 10:21:23', 0, 0),
(11, '2016-01-20', 1, 'Sample', '{"2":"50","1":"50"}', '{"2":"5","1":"4"}', 1, 1, 20000, 100, '', 'remarks', '1', 'Admin (admin@admin.com)', '2016-02-11 11:25:21', '2016-01-26 10:21:23', 0, 0),
(12, '2016-01-20', 1, 'Sample', '{"2":"50","1":"50"}', '{"2":"5","1":"4"}', 1, 1, 20000, 100, '', 'remarks', '1', 'Admin (admin@admin.com)', '2016-02-11 11:25:57', '2016-01-26 10:21:23', 0, 0),
(13, '2016-01-20', 1, 'Sample', '{"2":"50","1":"50"}', '{"2":"5","1":"4"}', 1, 1, 20000, 100, '', 'remarks', '1', 'Admin (admin@admin.com)', '2016-02-11 11:26:00', '2016-01-26 10:21:23', 0, 0),
(14, '2016-01-20', 1, 'Sample', '{"2":"50","1":"50"}', '{"2":"5","1":"4"}', 1, 1, 20000, 100, '', 'remarks', '1', 'Admin (admin@admin.com)', '2016-02-11 11:26:05', '2016-01-26 10:21:23', 0, 0),
(15, '2016-01-20', 1, 'Sample', '{"2":"50","1":"50"}', '{"2":"5","1":"4"}', 1, 1, 20000, 100, '', 'remarks', '1', 'Admin (admin@admin.com)', '2016-02-11 11:26:08', '2016-01-26 10:21:23', 0, 0),
(16, '2016-01-20', 1, 'Sample', '{"2":"50","1":"50"}', '{"2":"5","1":"4"}', 1, 1, 20000, 100, '', 'remarks', '1', 'Admin (admin@admin.com)', '2016-02-11 11:26:10', '2016-01-26 10:21:23', 0, 0),
(17, '2016-01-20', 1, 'Sample', '{"2":"50","1":"50"}', '{"2":"5","1":"4"}', 1, 1, 20000, 100, '', 'remarks', '1', 'Admin (admin@admin.com)', '2016-02-11 11:26:12', '2016-01-26 10:21:23', 0, 0),
(18, '2016-01-20', 1, 'Sample', '{"2":"50","1":"50"}', '{"2":"5","1":"4"}', 1, 1, 20000, 100, '', 'remarks', '1', 'Admin (admin@admin.com)', '2016-02-11 11:26:15', '2016-01-26 10:21:23', 0, 0),
(19, '2016-01-29', 3, 'Sample 1 analysis.', '{"2":"50","1":"50"}', '{"4":"6"}', 9, 1, 20000, 45, '{"2":"78","1":"12"}', 'test', '1', 'Admin (admin@admin.com)', '2016-02-05 09:07:15', '2016-02-05 09:07:15', 0, 0),
(20, '2016-02-08', 4, '121', 'null', 'null', 10, 2, 120012, 0, 'null', '1231', '1', '', '2016-02-08 11:32:17', '2016-02-08 11:32:17', 0, 0),
(21, '2016-02-08', 4, '121', 'null', 'null', 10, 2, 120012, 0, 'null', '1231', '1', '', '2016-01-08 11:32:30', '2016-02-08 11:32:30', 0, 0),
(22, '2016-02-09', 5, '123124', '{"3":"","2":"15","1":"12"}', '{"5":"","4":"","2":"","1":""}', 2, 2, 1231231, 0, '{"2":"","1":""}', '123123', '1', '', '2016-02-15 16:23:45', '2016-02-15 11:38:45', 0, 0),
(23, '2016-02-11', 6, '12223', '{"2":"100"}', '{"2":"","1":""}', 1, 2, 123, 30, '{"2":"30"}', '12', '1', '', '2016-02-12 08:21:45', '2016-02-12 08:21:45', 0, 0),
(24, '2016-02-18', 7, 'hg', '{"2":"100"}', '{"4":"12","2":"23","1":"12"}', 3, 1, 19, 80, '{"2":"80"}', 'gh', '1', '', '2016-02-18 10:19:50', '2016-02-18 05:34:50', 0, 0),
(25, '2016-02-18', 8, 'demo', '{"2":"100"}', '{"6":"23"}', 3, 1, 12, 20, '{"2":"20"}', 'sdfasdf', '1', '', '2016-02-18 09:49:01', '2016-02-18 05:04:01', 0, 0),
(26, '2016-02-18', 9, 'demo2', '{"3":"100"}', '{"6":"12"}', 3, 1, 56, 20, '{"3":"20"}', 'jhgjk', '1', '', '2016-02-18 10:09:05', '2016-02-18 05:24:05', 0, 0),
(27, '2016-02-18', 10, 'fawef', '{"3":"","2":"","4":""}', '{"6":"12"}', 23, 1, 12, 0, 'null', 'asdasd', '1', '', '2016-02-19 05:13:00', '2016-02-19 00:28:00', 4, 0),
(28, '2016-02-19', 11, 'record1', '{"3":"50","2":"50"}', '{"1":"12","6":"12"}', 23, 1, 123123, 0, 'null', 'asfsadfsdf', '1', '', '2016-02-19 00:53:31', '2016-02-19 00:53:31', 4, 50),
(29, '2016-02-19', 12, 'task force', '{"3":"78"}', '{"1":"6"}', 23, 2, 567, 0, 'null', 'rdthdhg', '1', '', '2016-02-19 04:01:47', '2016-02-19 04:01:47', 3, 45),
(30, '2016-02-19', 13, 'hehehe', '{"2":"100"}', '{"6":"1"}', 23, 3, 100, 0, 'null', 'sdfsdf', '1', '', '2016-02-19 04:51:13', '2016-02-19 04:51:13', 3, 100),
(31, '2016-02-22', 14, 't1', '{"3":"50","2":"50"}', '{"6":"12"}', 24, 1, 123, 25, '{"3":"20","2":"30"}', 'vfvf', '1', '', '2016-02-22 06:28:14', '2016-02-22 01:43:14', 7, 40),
(32, '2016-02-22', 15, 't2', '{"4":"100"}', '{"1":"12"}', 24, 1, 123, 40, '{"4":"40"}', 'sdafasdf', '1', '', '2016-02-22 06:20:46', '2016-02-22 01:35:46', 8, 20),
(33, '2016-02-22', 16, 't3', '{"5":"100"}', '{"2":"12"}', 24, 2, 13, 60, '{"5":"60"}', 'sdafasdf', '1', '', '2016-02-22 06:21:08', '2016-02-22 01:36:08', 8, 60),
(34, '2016-02-22', 17, 't4', '{"6":"100"}', '{"6":"12"}', 24, 2, 123, 80, '{"6":"80"}', 'asdfsdf', '1', '', '2016-02-22 06:21:21', '2016-02-22 01:36:21', 9, 10);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_position` varchar(255) NOT NULL,
  `user_level` varchar(250) DEFAULT NULL,
  `user_address` varchar(255) NOT NULL,
  `user_mobile` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone_home` varchar(255) NOT NULL,
  `user_phone_office` varchar(255) NOT NULL,
  `user_created_date` varchar(255) DEFAULT NULL,
  `user_last_login_date` varchar(255) DEFAULT NULL,
  `user_created_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_password`, `user_position`, `user_level`, `user_address`, `user_mobile`, `user_email`, `user_phone_home`, `user_phone_office`, `user_created_date`, `user_last_login_date`, `user_created_by`) VALUES
(1, 'Admin', '202cb962ac59075b964b07152d234b70', 'Admin', NULL, 'Bafal', '12345', 'admin@admin.com', '1234', '1234', '10-18-2014', '02-24-16', 'admin@admin.com'),
(2, 'Asmin', '96e89a298e0a9f469b9ae458d6afae9f', 'Department Head', NULL, 'kathmandu', '', 'head@icn.com.np', '', '', '10-28-2014', '03-17-15', 'admin@admin.com'),
(4, 'Bijay Raut', '202cb962ac59075b964b07152d234b70', 'Manager', NULL, 'Kathmandu', '', 'manager@icn.com.np', '', '', '11-06-2014', '02-02-16', 'admin@admin.com'),
(5, 'Saurav Pokharel', '81dc9bdb52d04dc20036dbd8313ed055', 'Manager', NULL, 'Kathmandu', '', 'saurav.pokharel@icn.com.np', '', '', '01-29-2016', '01-29-16', 'admin@admin.com'),
(6, 'bipin', 'a83383d30b2da2f863cb12739254bb02', 'Manager', NULL, 'bkt', '98767876787', 'bipin@bipin.com', '', '', '02-16-2016', '02-22-16', 'admin@admin.com');

-- --------------------------------------------------------

--
-- Table structure for table `user_department`
--

CREATE TABLE IF NOT EXISTS `user_department` (
`user_department_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dep_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

--
-- Dumping data for table `user_department`
--

INSERT INTO `user_department` (`user_department_id`, `user_id`, `dep_id`) VALUES
(8, 1, 1),
(9, 1, 2),
(10, 1, 3),
(47, 2, 1),
(48, 2, 2),
(49, 2, 3),
(50, 2, 4),
(67, 4, 1),
(68, 4, 2),
(69, 4, 3),
(70, 4, 4),
(75, 5, 1),
(76, 5, 2),
(77, 5, 3),
(78, 5, 4),
(79, 6, 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assignment_table`
--
ALTER TABLE `assignment_table`
 ADD PRIMARY KEY (`assignment_id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
 ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
 ADD PRIMARY KEY (`dep_id`);

--
-- Indexes for table `indicators`
--
ALTER TABLE `indicators`
 ADD PRIMARY KEY (`indicator_id`);

--
-- Indexes for table `indicators_type`
--
ALTER TABLE `indicators_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_role`
--
ALTER TABLE `master_role`
 ADD PRIMARY KEY (`master_role_id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
 ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
 ADD PRIMARY KEY (`position_id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
 ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
 ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `task_category`
--
ALTER TABLE `task_category`
 ADD PRIMARY KEY (`t_category_id`);

--
-- Indexes for table `task_registration`
--
ALTER TABLE `task_registration`
 ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_department`
--
ALTER TABLE `user_department`
 ADD PRIMARY KEY (`user_department_id`), ADD KEY `user_id` (`user_id`), ADD KEY `dep_id` (`dep_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assignment_table`
--
ALTER TABLE `assignment_table`
MODIFY `assignment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
MODIFY `branch_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
MODIFY `dep_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `indicators`
--
ALTER TABLE `indicators`
MODIFY `indicator_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `indicators_type`
--
ALTER TABLE `indicators_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `master_role`
--
ALTER TABLE `master_role`
MODIFY `master_role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=571;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
MODIFY `position_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=457;
--
-- AUTO_INCREMENT for table `task_category`
--
ALTER TABLE `task_category`
MODIFY `t_category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `task_registration`
--
ALTER TABLE `task_registration`
MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_department`
--
ALTER TABLE `user_department`
MODIFY `user_department_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=80;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_department`
--
ALTER TABLE `user_department`
ADD CONSTRAINT `user_department_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `user_department_ibfk_2` FOREIGN KEY (`dep_id`) REFERENCES `department` (`dep_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
