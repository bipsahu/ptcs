<?php 
class cts_branch_controller extends CI_Controller 
{
	public function __construct()
	{
	parent::__construct();
		$this->load->model('cts_claim_branch_model');
		$this->load->model('cts_login');
		$this->load->model('cts_role_model');
		$this->load->model('cts_department_model');
		
	}
	public function index()
	{
		$data1['positions'] = $this->cts_role_model->show_all_position_role();
		$data1['departments'] = $this->cts_department_model->getDepartment();
		$data1['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data1);
		$this->load->view('branch/cts_new_branch_registration');
	}
	
	public function insert_branch_details()
	{
		$data['result'] = $this->cts_claim_branch_model->insert_branch_details();
		if($data['result'] == false)
		{
		echo " 0";
		}
		else
		{
			foreach($data['result']->result() as $row)
			{
			$branch_details = array(
				'branch_name' => $row->branch_name,
				'branch_id' => $row->branch_id
				);
			}
			echo json_encode($branch_details);
		}
	
	}
	
	public function add_new_branch()
	{
		$data1['positions'] = $this->cts_role_model->show_all_position_role();
		$data1['departments'] = $this->cts_department_model->getDepartment();
		$data1['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data1);
		$this->load->view('branch/cts_register_new_branch');
		$this->load->view('cts_footer');
	}
	
	
	public function show_all_branches()
	{
		
		$result = $this->cts_claim_branch_model->show_all_branches();
		$data['result'] = $result;
		$data1['positions'] = $this->cts_role_model->show_all_position_role();
		$data1['departments'] = $this->cts_department_model->getDepartment();
		$data1['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header',$data);
		$this->load->view('cts_navigation',$data1);
		$this->load->view('branch/cts_show_all_branches');
		$this->load->view('cts_footer');
	}
	
	public function search_branch_detail($key)
	{
	$this->cts_claim_branch_model->search_branch_detail($key);
	}
	public function check_branch_name()
	{
	$result = $this->cts_claim_branch_model->check_branch_name();
	if($result)
	echo " 1";
	else
	echo " 0";
	}
	
	public function delete_branch_by_id($id)
	{
		$this->cts_claim_branch_model->delete_branch_by_id($id);
		$result = $this->cts_claim_branch_model->show_all_branches();
		$data['result'] = $result;
		$data1['positions'] = $this->cts_role_model->show_all_position_role($id);
		$data1['departments'] = $this->cts_department_model->getDepartment();
		$data1['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data1);
		$this->load->view('branch/cts_show_all_branches',$data);
		$this->load->view('cts_footer');
	}
	public function edit_branch_by_id()
	{
		$id=$this->input->post('branch_id');
		$this->cts_claim_branch_model->edit_branch_by_id($id);
		$result = $this->cts_claim_branch_model->show_all_branches();
		$data['result'] = $result;
		$data1['positions'] = $this->cts_role_model->show_all_position_role($id);
		$data1['departments'] = $this->cts_department_model->getDepartment();
		$data1['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data1);
		$this->load->view('branch/cts_show_all_branches',$data);
		$this->load->view('cts_footer');
	}
	public function get_branch_by_id($id)
	{
		
		$result = $this->cts_claim_branch_model->get_branch_by_id($id);
		$data['result'] = $result;
		$data1['positions'] = $this->cts_role_model->show_all_position_role($id);
		$data1['departments'] = $this->cts_department_model->getDepartment();
		$data1['new_message'] = $this->cts_login->check_new_message();
		$this->load->view('cts_header');
		$this->load->view('cts_navigation',$data1);
		$this->load->view('branch/cts_edit_branch_by_id',$data);
		$this->load->view('cts_footer');
	}
}