<?php
if(isset($result))
{
	if($result==1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Update Succesful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Update Failed!</b></div>";
}
if(isset($sample_details)){
foreach($sample_details as $row)
{
$sample_id = $row['sample_id'];
$date = $row['date'];
$labno = $row['labno'];
$customer_name = $row['customer_name'];
$firm_name = $row['firm_name'];
$animal = $row['animal'];
$sample_type = $row['sample_type'];
$no_of_sample = $row['no_of_sample'];
$age = $row['age'];
$sex = $row['sex'];
$species_type = $row['species_type'];
$refrence_no = $row['refrence_no'];
$paid_amount = $row['paid_amount'];

$remarks = $row['remarks'];
}
}
?>
  <div class="container">
    <h3>Edit Sample Detail Form</h3><br>
           <table class="table" id="table">

        <form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/lims_sample_controller/edit_sample_by_id/<?php echo $sample_id;?>">

			
			<tr>
              <td><label>Date</label></td>
              <td><input type="text" class="form-control" name="date" value="<?php echo $date;?>" required></td>
			  <td></td>
            </tr> 
            <tr>
              <td><label>Customer Name</label></td>
              <td><input type="text" class="form-control" name="customer_name" id="customer_name" value="<?php echo $customer_name;?>" required></td>
            </tr>
			
			<tr>
              <td><label>Firm Name</label></td>
              <td><input type="text" class="form-control" name="firm_name" value="<?php echo $firm_name;?>" ></td>
            </tr>
			
			<tr>
              <td><label>Animal</label></td>
              <td><input type="text" class="form-control" name="animal" value="<?php echo $animal;?>" required></td>
			  
            </tr>
			<tr>
              <td><label>Sample Type</label></td>
              <td><input type="text" class="form-control" name="sample_type" value="<?php echo $sample_type;?>" required></td>
			  
            </tr>
			
			<tr>
              <td><label>Age</label></td>
              <td><input type="text" class="form-control" name="age" value="<?php echo $age;?>" ></td>
			  <td></td>
            </tr>
			
			<tr>
              <td><label>Sex </label></td>
			<?php if ($sex == "male"){ ?>
              <td><input type="radio" class="form-control" name="sex" value="male" id="male" checked >Male</td>
              <td><input type="radio" class="form-control" name="sex" value="male" id="male"  >Female</td>
			  <?php } else{?>
			  <td><input type="radio" class="form-control" name="sex" value="female" id="female" >Male</td>
              <td><input type="radio" class="form-control" name="sex" value="female" id="female"  checked >Female</td>
			  <?php } ?>
            </tr>
			
			
			<tr>
              <td><label>Species Type </label></td>
              <td><input type="text" class="form-control" name="species_type" value="<?php echo $species_type;?>" ></td>
			  <td></td>
            </tr>
			
			
			<tr>
              <td><label>Refrence No.</label></td>
              <td><input type="text" class="form-control" name="refrence_no" value="<?php echo $refrence_no;?>" ></td>
			  <td></td>
            </tr>
			
			<tr>
              <td><label>Paid Amount</label></td>
              <td><input type="text" class="form-control" name="paid_amount" value="<?php echo $paid_amount;?>" ></td>
			  <td></td>
            </tr>
			
			
		
			
			<tr>
              <td><label> Remarks </label></td>
              <td><input type="textarea" class="form-control" name="remarks" value="<?php echo $remarks;?>" ></td>
			  <td></td>
            </tr>

   
            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary">Submit</button></td>
			   <td></td>
            </tr>
          
        </form>
    </table>
	
</div><!---end of container-->
 </body>
 </html>
 