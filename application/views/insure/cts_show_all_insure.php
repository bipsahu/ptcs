<div class="container">
    <h1>All Projects</h1>
    <?php
    if (!empty($result_indicator)) {
        if ($result_indicator == 1) {
            echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Deletion Successful!</b></div>";
        } else
            echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Deletion Failed!</b></div>";
    }
    ?>
    <?php if ($this->session->userdata('position_id') == "1" || $this->session->userdata('position_id') == '4' || $this->session->userdata('position_id') == 5) { ?>
        <a class="btn btn-primary add_new_button"
           href="<?php echo base_url(); ?>index.php/cts_customer_controller/add_new_insure">Add New Project</a>
    <?php } ?>
    <table class="table table-striped datatable">
        <thead>
        <th>S No.</th>
        <th>Project</th>
        <th>Branch</th>
        <th>Description</th>
        <th>% completion</th>
        <?php
        if ($this->session->userdata('position_id') == "1" || $this->session->userdata('position_id') == "4" || $this->session->userdata('position_id') == "5") {
            echo "<th>Edit</th>";
        }

        ?>
        <?php
        if ($this->session->userdata('position_id') == "1" || $this->session->userdata('position_id') == "4" || $this->session->userdata('position_id') == "5") {
            echo "<th>Delete</th>";
        }

        ?>
        </thead>
        <tbody>
        <?php
        $offset = $offset + 1;
        $commplete = array("40","60","80","90","100","50","60","80","90","100","50","90","100","50","90","100","50","10");
        foreach ($results->result() as $row) {
            $insuredid = $row->iid;
            ?>
            <tr>
                <td><?php echo $offset; ?></td>
                <td><?php echo $row->insure_name; ?></td>
                <td><?php echo $row->insure_address; ?></td>
                <td><?php ?></td>
                <td><?php echo $commplete[$offset]."%"?></td>

                <?php
                if ($this->session->userdata('position_id') == "1" || $this->session->userdata('position_id') == "4" || $this->session->userdata('position_id') == "5") {
                    echo "<td><a href='" . base_url() . "index.php/cts_customer_controller/edit_insure_form/" . $insuredid . "'><span class='glyphicon glyphicon-edit'></span></a></td>";
                }
                /*elseif( isset($role) ||  $role[3]['edit'])
                {
                echo "<td><a href='".base_url()."index.php/lims_customer_controller/edit_insured_by_id_form/".$insuredid."'><span class='glyphicon glyphicon-edit'></span></a></td>";
                }*/
                if ($this->session->userdata('position_id') == "1" || $this->session->userdata('position_id') == "4" || $this->session->userdata('position_id') == "5") {
                    echo "<td><a href='#'><span class='glyphicon glyphicon-trash' alt='" . $insuredid . "'></span></a></td>";
                }
                /*//elseif( isset($role) ||  $role[3]['delete'])
                //{
                echo "<td><a href='#'><span class='glyphicon glyphicon-trash'  alt='".$insuredid."'></span></a></td>";
                }*/
                ?>
            </tr>
            <?php
            $offset++;
        }
        ?>

        <?php /*?>//if(strtolower($this->session->userdata('user_position'))=="admin" || isset($role) ||  $role[3]['add']){?>
<tr>
<td colspan="12"><a class="btn btn-primary" href="<?php echo base_url();?>index.php/lims_customer_controller/add_new_insured1">Add New Firm</a>
</td>
</tr>
<?php }<?php */ ?>
        </tbody>
    </table>
</div>

<div id="dialog-confirm" title="Delete the Firm?">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently
        deleted and cannot be recovered. Are you sure?</p>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#dialog-confirm").hide();
        $('.glyphicon-trash').click(function () {
            var insured_id = $(this).attr('alt');
            $("#dialog-confirm").dialog({
                resizable: false,
                height: 160,
                modal: true,
                show: {
                    effect: "blind",
                    duration: 300
                },
                hide: {
                    effect: "blind",
                    duration: 300
                },
                buttons: {
                    "Delete": function () {
                        window.location.assign('<?php echo base_url();?>' + 'index.php/cts_customer_controller/delete_insured_by_id/' + insured_id);
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
        });
    });
</script>
</body>
</html>