<?php

class cts_project_model extends CI_Model
{
    /*
     * insert to project table through post data
     * @params : NULL
     * @return :json data
     * */
    public function insert_project_details()
    {
        // echo "1";
        if ($this->input->is_ajax_request()) {
            $formData = $this->input->post("project");
            $data['project_name'] = $formData[0]["value"];
            // echo "2";

        } 
        else 
        {
            // echo "<pre>";
            // echo "3";
            $data['project_taskcategory'] = json_encode($_POST['task_cat']['indicators']);
            $data['project_name'] = $this->input->post("project_name");
            // print_r($data);
        }
        // die;
        $query_result = $this->db->get_where('project', array('project_name' => $data['project_name']));
        if ($query_result->num_rows() == 1) 
        {
            if ($this->input->is_ajax_request())
                echo json_encode(false);
            else
                return false;
        } 
        else 
        {
            $result = $this->db->insert('project', $data);
            if ($this->input->is_ajax_request())
                echo json_encode($this->db->insert_id());
            else
                return $result;

        }
    }

    public function insert_new_project()
    {
        $data = array(
            'project_name' => $this->input->post('project_name')
        );
        $result = $this->db->get_where('project', array('project_name' => $data['project_name']));
        if ($result->num_rows == 1) {
            return false;
        } else {
            $result = $this->db->insert('project', $data);
            return $result;
        }
    }
//get list of project
    public function getProject()
    {
        $result = $this->db->get('project');
        return $result->result();
    }

    public function show_all_project($limit, $start)
    {
        $this->db->order_by("project_id", "asc");
        $this->db->limit($limit, $start);
        $query = $this->db->get('project');
        return $query;

    }

    public function get_details_of_project_by_id($project_id)
    {
        $result = $this->db->get_where('project', array('project_id' => $project_id));
        return $result->result_array();
    }

    public function edit_project_by_id($project_id)
    {
        $data = array(
            'project_name' => $this->input->post('project_name')

        );

        $result = $this->db->get_where('project', array('project_name' => $data['project_name']));
        if ($result->num_rows == 1) {

            return false;
        } else {
            $this->db->where('project_id', $project_id);
            $result = $this->db->update('project', $data);
            return $result;
        }
    }

    public function delete_project_by_id($project_id)
    {
        $result = $this->db->delete('project', array('project_id' => $project_id));
        $taskresult = $this->db->query("select * from task_registration where project = $project_id")->result_array();
        if($taskresult != '')
        {
            $this->db->delete('task_registration', array('project' => $project_id));
        }
        return $result;
    }

    public function search_project_detail($key)
    {
        $result = $this->db->query("select * from project where project_name like '" . $key . "%'");
        $projects = array();
        foreach ($result->result() as $row) {
            $project = "";
            $project .= $row->project_name;

            $projects[] = $project;
        }
        echo json_encode($projects);
    }

    public function check_project_name()
    {
        $project_name = $this->input->post('project_name');
        $result = $this->db->get_where('project', array('project_name' => $project_name));
        if ($result->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }
    public function get_all_project()
    {
        $result = $this->db->query('SELECT p.project_id AS child_project_id,p.project_name AS child_name,c.`project_id`,c.`project_name` AS parent_name FROM project AS p LEFT JOIN project AS c ON c.project_id = p.project_id');
        return $result->result_array();
    }
    
      public function get_the_project_by_id($project_id)
    {
        $result = $this->db->get_where('project', array('project_id' => $project_id));
        return $result->result_array();
    }
    
    public function edit_the_project($project_id)
    {
        // echo "<pre>";
        // print_r($_POST);
        
        $data['project_name'] = $this->input->post('project_name');
        $data['project_taskcategory'] = json_encode($_POST['task_cat']['indicators']);
           // print_r($data);die;
       
        $this->db->where('project_id', $project_id);
        $result = $this->db->update('project', $data);
        return $result;

    }

}
