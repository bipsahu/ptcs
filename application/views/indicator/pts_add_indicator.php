<?php 
$role= array();
$modules= array();
if(isset($roles_info))
{
$i=0;
foreach($roles_info->result_array() as $key=>$value)
{
$role[] = $value;
$module[] = $role[$i]['module_name'];
}
}
?>



<div class="container">
<!--	<div id="img-icon">
      <img src="<?php echo base_url();?>" align="left"/>
    </div>-->
    <div id="breadcumb-text">
      <ul class="nav nav-tabs">
        <li><a href=""><strong>Indicator</strong></a></li>
        <!-- <li class="vertical-divider"></li>&nbsp; -->
    <?php if(strtolower($this->session->userdata('user_position'))=="admin" || $role[2]['add']){?>
        <li class="active"><a href="">Add New Indicator</a></li>
        <!-- <li class="vertical-divider"></li>&nbsp; -->
    <?php } ?>
        <li><a href="<?php echo base_url();?>index.php/cts_controller/show_all_indicator">List Indicators</a></li>
      </ul>
    </div>
    <hr>
<?php
if(isset($result))
{
	if($result == 1)
	{
	echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Indicator Added!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Indicator Addition Failed!</b></div>";
}
?>
<table class="table" id="table">
<form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/cts_controller/insert_indicator_details">
  <tr>
    <td class="first_td">Indicator Name</td>
    <td><input type="text" class="form-control" name="indicator_name" required></td>
  </tr>
  <tr>
    <td class="first_td">Task Category</td>
    <td>
      <select name="task_category" class="form-control" required>
        <option value="">Select</option>
        <?php foreach ($task_category as $key => $task_categories) {
          # code...
        ?>
        <option value="<?php echo $task_categories['t_category_id'] ?>"><?php echo $task_categories['name'] ?></option>
        <?php } ?>
      </select>
    </td>
  </tr>

  <tr>
    <td colspan="2"><input type="submit" name="submit" value="Submit" class="btn btn-primary"></td>
  </tr>
</form>
</table>
</div>
</body>
</html>