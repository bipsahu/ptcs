var flag = 1;//green submit enable
$(document).ready(function()
{
		 $(".datatable").dataTable();
			function split(val) {
                return val.split(/,\s*/);
            }
            function extractLast(term) {
                return split(term).pop();
            }
			$('#date').datepicker();
			$('#selection_date').datepicker();
			$("#error_msg_tr").hide();
			$("#class_msg").hide();
			$("#claim_msg").hide();
			$(".class_msg").hide();
			$(".species_msg").hide();
			
			$("#investigation_msg").hide();
			$("#insure_msg").hide();
			$("#customer_msg").hide();
			$( "#dialog-confirm" ).hide();
			$(".insured_alert").hide();
			$("#class_indicator_alert").hide();
			$('#claim_branch_success').hide();
			$("#branch_indicator_alert").hide();
			$("#investigation_indicator_alert").hide();
			$("#surveyor_registration_alert").hide();
			$("#species_type_registration_alert").hide();
			$("#claim_registration_alert").hide();
			
			//$("#customer_name").autocomplete({minLength: 0, autoFocus: true,source:base_url+'cts_customer_controller/check/1'}); 
			
			
			
		/*	$("#class_name").autocomplete({minLength: 1, autoFocus: true,source:""});
			$("#surveyor").autocomplete({minLength: 1, autoFocus: true,source:""});
			$("#claim_type").autocomplete({minLength: 1, autoFocus: true,source:""});
			 
			$("#insure_name").autocomplete({minLength: 1, autoFocus: true,source:""});
			$("#species_typ").autocomplete({minLength: 1, autoFocus: true,source:""});
			$("#investigation_requested").autocomplete({minLength: 1, autoFocus: true,source:""});*/
			
			$("#user_email").change(function(){
				$.ajax({
					url: base_url+'cts_controller/check_email',
					type: 'POST',
					data: {email:$("#user_email").val()},
					success:function(msg)
					{
						if(msg == "1")
						{
							$("#error_msg_tr").show();
							$('.error_msg').html("<p>Email already exist");
							$('#submit').attr('disabled','disabled');
						}
						else
						{
							$("#error_msg_tr").hide();
							$('#submit').removeAttr('disabled');
						}
					}
				});
			});
			$('#new_class').click(function()
			{
			$("#class_indicator_alert").hide();
			$("#class_form")[0].reset();
			});
			$('#new_branch').click(function()
			{
			$("#branch_indicator_alert").hide();
			$("#claim_branch_form")[0].reset();
			});
			$('#new_investigation_requested').click(function()
			{
			$("#investigation_indicator_alert").hide();
			$("#investigation_requested_form")[0].reset();
			});
			$('#new_surveyor').click(function()
			{
			$("#surveyor_registration_alert").hide();
			$("#surveyor_form")[0].reset();
			});
			$('#new_species_type').click(function()
			{
			$("#species_type_registration_alert").hide();
			$("#species_type_form")[0].reset();
			});
			$('#new_claim').click(function()
			{
			$("#claim_registration_alert").hide();
			$("#species_type_form")[0].reset();
			});
			
			$("#customer_submit").click(function(event)
			{
			if(!$("#customer_form")[0].checkValidity())
			{
			alert("* field is required");	
			}
			else
			{
				$.ajax({
				url:base_url+'cts_customer_controller/insert_customer_details',
				type:'POST',
				data:$('#customer_form').serialize(),
				dataType:'json',
				success:function(msg)
					{
						var cid = msg.cid;
						var customer_name = "";
						if(msg.customer_middle_name == "")
						{
						customer_name = msg.customer_first_name +" "+ msg.customer_last_name;
						}
						else
						{
						customer_name = msg.customer_first_name +" "+msg.customer_middle_name+" "+msg.customer_last_name;
						}
						$('#customer_id').val(cid);
						$('#claim_customer_id').val(cid);
						$('#customer_name').val(customer_name);
						$('#customer_model').modal('hide');
						$("#customer_form")[0].reset();
					}
				});
			}
			});
			
			
			$("#insure_submit").click(function()
			{
				if(!$("#insure_form")[0].checkValidity())
				{
				alert("* field is required");
				}
				else
				{
					$.ajax({
						url:base_url+'cts_customer_controller/insert_new_insure',
						type:'POST',
						data:$('#insure_form').serialize(),
						dataType:'json',
						success:function(msg)
							{
							
							var insure_name = msg.insure_name;
							var insure_id = msg.insure_id;
							$('#claim_insure_id').val(insure_id);
							$("#insure_name").val(insure_name);
							$('#insure_model').modal('hide');
							$("#insure_form")[0].reset();
							$(".insured_alert").show();
							}
				
					});
				}
			});
			//investigation_requested_submit
			$("#investigation_requested_form").submit(function()
			
			{
				/* if(!$("#investigation_requested_form")[0].checkValidity())
				{
				alert("* field is required");
				}
				else
				{ */
					$.ajax({
						url:base_url+'cts_investigation_controller/insert_investigation_details',
						type:'POST',
						data:$('#investigation_requested_form').serialize(),
						dataType:'json',
						success:function(msg)
							{
							if(parseInt(msg) == 0)
								{
								$("#investigation_indicator_alert").show();
								}
								else{
								var investion_requested = msg.investigation_requested;
								if($("."+msg.dep_name.replace(" ","_")).is(":visible"))
								{
								$("#investigation_requested_td").append("<ul><div class='"+msg.dep_name.replace(" ","_")+"'><input type='checkbox' name='investigation["+msg.investigation_requested+"]' value='"+msg.investigation_requested_id+"' />"+investion_requested+"</div></ul>");
								}
								$('#investigation_modal').modal('hide');
								$("#investigation_requested_form")[0].reset();
							}
							}
				
					});
					return false;
				//}
			});
			
			$("form#claim_branch_form").submit(function()
			{
				 if(!$("#claim_branch_form")[0].checkValidity())
				{
				alert("* field is required");
				}
				else
				{ 
					var data= $(this).serialize();
					$.ajax({
						url:base_url+'cts_branch_controller/insert_branch_details',
						type:'POST',
						data:data,
						dataType:'json',
						error:function(error_msg,params,err){
						alert('error');
							},
						success:function(msg)
							{
								
								
								if(parseInt(msg) == 0)
								{
								$("#branch_indicator_alert").show();
								}
								else
								{
								var branch_name = msg.branch_name;
								$("#claim_branch").append($("<option value='"+branch_name+"' selected ></option>").text(branch_name));								
								$('#claim_branch_success').show();
								$('#claim_branch_model').modal('hide');
								$("#claim_branch_form")[0].reset();
								}
							}
				
					});
					return false;
				}
			});
			
			$("form#class_form").submit(function()
			{
				/* if(!$("#class_form")[0].checkValidity())
				{
				alert("* field is required");
				}
				else
				{ */
					$.ajax({
						url:base_url+'cts_class_controller/insert_class_details',
						type:'POST',
						data:$('#class_form').serialize(),
						dataType:'json',
						success:function(msg)
							{
								//alert(msg.class_name);
								
								if(parseInt(msg) == 0)
								{
								$("#class_indicator_alert").show();
								}
								else
								{
								var class_name = msg.class_name;
								$("#class").append($("<option value='"+class_name+"' selected ></option>").text(class_name));
								$('#class_model').modal('hide');
								$("#class_form")[0].reset();
								}
							}
				
					});
					return false;
				//}
			});
			
			
			$("#class_name")
            // don't navigate away from the field on tab when selecting an item
            .keydown(function (event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).data("autocomplete").menu.active) {
                    event.preventDefault();
                }
            })
        .autocomplete({


            minLength: 1,
            source: function (request, response) {
                $.getJSON(base_url+'cts_class_controller/search_class_detail/'+$("#class_name").val(), {
                    term: extractLast(request.term)
                }, response);

            },
            search: function () {
                // custom minLength
                var term = extractLast(this.value);
                if (term.length < 1) {
                    return false;
                }
            },
            focus: function () {
                // prevent value inserted on focus
                return true;
            },
            select: function (event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
				this.value = terms.join("");
                //this.value = terms.join(", ");
                return false;
            }

        });	
			/*$("#class_name").keyup(function()
			{
				var auto_complete_array = new Array();
				if($.trim($("#class_name").val())!="")
				{
					$.ajax({
					type:'post',
					url:base_url+'cts_class_controller/search_class_detail',
					data:{class_name:$("#class_name").val()},
					success:function(msg)
					{
					if(msg!="")
					msg+=",";
					console.log(msg);
					auto_complete_array = msg.split(",");
					console.log(auto_complete_array);
					$("#class_name").autocomplete({
					minLength: 0,
					source:auto_complete_array
					});
					}
					});
				}
				else
				{
				$("#class_name").autocomplete({
					minLength: 0,
					source:""
					});
				}
			});*/
			
			
			$("#class_name").focusout(function()
			{
			if($.trim($(this).val())!="")
			{
				$.ajax({
				url:base_url+'cts_class_controller/check_class_name',
				type:'POST',
				data:{class_name:$(this).val()},
					success:function(msg)
					{
						if(parseInt(msg) == 0)
						{
						flag = 0;
						$("#class_msg").show();
						$('#submit').attr('disabled','disabled');
						}
						else
						{
						$("#class_msg").hide();
							if($(".species_msg").is(":visible") || $("#investigation_msg").is(":visible") || $("#claim_msg").is(":visible") || $("#species_type_msg").is(":visible") || $("#insure_msg").is(":visible")|| $("#customer_msg").is(":visible"))
							{
							$('#submit').attr('disabled','disabled');
							}
							else
							{
							$('#submit').removeAttr('disabled');
							}
						}
					}
				});
				}
					else
				{
				$("#class_msg").hide();
				$('#submit').removeAttr('disabled');
				}
			});
			
			
			
			$("#surveyor_form").submit(function()
			{
				/* if(!$("#surveyor_form")[0].checkValidity())
				{
				alert("* field is required");
				}
				else
				{ */
					$.ajax({
						url:base_url+'cts_surveyor_controller/insert_surveyor_details',
						type:'POST',
						data:$('#surveyor_form').serialize(),
						dataType:'json',
						success:function(msg)
							{
								if(parseInt(msg) == 0)
								{
								$("#surveyor_registration_alert").show();
								}
								else
								{
								var surveyor = msg.surveyor;
								$(".surveyor").val(surveyor);
								$('#surveyor_modal').modal('hide');
								$("#surveyor_form")[0].reset();
								}
							}
				
					});
				//}
				return false;
			});
			 // don't navigate away from the field on tab when selecting an item
            //.keydown(function (event) {
               // if (event.keyCode === $.ui.keyCode.TAB &&
                     //   $(this).data("autocomplete").menu.active) {
                    //event.preventDefault();
               // }
            //})
			
			/*$("#surveyor").keyup(function()
			{
				var auto_complete_array = new Array();
				if($.trim($("#surveyor").val())!="")
				{
					$.ajax({
					type:'post',
					url:base_url+'cts_surveyor_controller/search_surveyor_detail',
					data:{surveyor:$("#surveyor").val()},
					success:function(msg)
					{
					auto_complete_array = msg.split(",");
					$("#surveyor").autocomplete({minLength: 1,source:auto_complete_array});
					}
					});
				}
				else
				{
				$("#surveyor").autocomplete({
					minLength: 0,
					source:""
					});
				}
			});	
			*/
			$("#species_typ")
            // don't navigate away from the field on tab when selecting an item
            .keydown(function (event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).data("autocomplete").menu.active) {
                    event.preventDefault();
                }
            })
        .autocomplete({


            minLength: 1,
            source: function (request, response) {
                $.getJSON(base_url+'cts_species_type_controller/search_species_type_detail/'+$("#species_typ").val(), {
                    term: extractLast(request.term)
                }, response);

            


            },
            search: function () {
                // custom minLength
                var term = extractLast(this.value);
                if (term.length < 1) {
                    return false;
                }
            },
            focus: function () {
                // prevent value inserted on focus
                return true;
            },
            select: function (event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join("");
				//this.value = terms.join(", ");
                return false;
            }

        });	
		
			/*$("#species_typ").keyup(function(){
			var auto_complete_array = new Array();
				if($.trim($("#species_typ").val())!="")
				{
					$.ajax({
					type:'post',
					url:base_url+'cts_species_type_controller/search_species_type_detail',
					data:{species_type:$("#species_typ").val()},
					success:function(msg)
					{
					auto_complete_array = msg.split(",");
					$("#species_typ").autocomplete({minLength: 0,source:auto_complete_array});
					}
					});
				}
				else
				{
				$("#species_typ").autocomplete({
					minLength: 0,
					source:""
					});
				}
			});
			*/
			
			$(".surveyor").focusout(function()
			{
			var survey_mes = $(this).parents(".form-group").find('.species_msg');
			
			if($.trim($(this).val())!=""){
				$.ajax({
				url:base_url+'cts_surveyor_controller/check_surveyor',
				type:'POST',
				data:{surveyor:$(this).val()},
					success:function(msg)
					{
						if(parseInt(msg) == 0)
						{
						flag = 0;
						survey_mes.show();
						$('#submit').attr('disabled','disabled');
						}
						else
						{
						survey_mes.hide();
						if($("#class_msg").is(":visible") || $("#investigation_msg").is(":visible") || $("#claim_msg").is(":visible") || $("#species_type_msg").is(":visible") || $("#insure_msg").is(":visible")|| $("#customer_msg").is(":visible"))
							{
							$('#submit').attr('disabled','disabled');
							}
							else
							{
							$('#submit').removeAttr('disabled');
							}
						}
					}
				});
				}
				else
				{
				$(".species_msg").hide();
				$('#submit').removeAttr('disabled');
				}
			});
			
			$("#species_typ").focusout(function(){
			if($.trim($(this).val())!=""){
				$.ajax({
				url:base_url+'cts_species_type_controller/check_species_type',
				type:'POST',
				data:{species_type:$(this).val()},
					success:function(msg)
					{
						if(parseInt(msg) == 0)
						{
						flag = 0;
						$("#species_type_msg").show();
						$('#submit').attr('disabled','disabled');
						}
						else
						{
						$("#species_type_msg").hide();
							if($("#class_msg").is(":visible") || $("#investigation_msg").is(":visible") || $("#claim_msg").is(":visible") || $(".species_msg").is(":visible") || $("#insure_msg").is(":visible")|| $("#customer_msg").is(":visible"))
							{
							$('#submit').attr('disabled','disabled');
							}
							else
							{
							$('#submit').removeAttr('disabled');
							}
						}
					}
					
				});
				}
				else
				{
				$("#species_type_msg").hide();
				$('#submit').removeAttr('disabled');
				}
			});
			
			
			$("#species_type_form").submit(function(){
				/* if(!$("#species_type_form")[0].checkValidity())
				{
				alert("You left some field");
				}
				else
				{ */
				$.ajax({
						url:base_url+'cts_species_type_controller/insert_species_type_details',
						type:'POST',
						data:$('#species_type_form').serialize(),
						dataType:'json',
						success:function(msg)
							{
							if(parseInt(msg) == 0)
								{
								$("#species_type_registration_alert").show();
								}
								else
								{
							var species_type = msg.species_type;
							$("#species_typ").val(species_type);
							$('#species_type_modal').modal('hide');
							$("#species_type_form")[0].reset();
							}
						}
					});
				//}
				return false;
			});
			
			$("#claim_form").submit(function()
			{/* 
				if(!$("#claim_form")[0].checkValidity())
				{
				alert("* field is required");
				}
				else
				{ */
					$.ajax({
						url:base_url+'cts_sampl_controller/insert_sampl_details',
						type:'POST',
						data:$('#claim_form').serialize(),
						dataType:'json',
						success:function(msg)
							{
							if(parseInt(msg) == 0)
								{
								$("#claim_registration_alert").show();
								}
								else
								{
							var claim_type = msg.claim_type;
							$("#claim_type").val(claim_type);
							$('#claim_model').modal('hide');
							$("#claim_form")[0].reset();
							}
						}
					});
				//}
				return false;
			});
			
			
			$("#claim_type")
            // don't navigate away from the field on tab when selecting an item
            .keydown(function (event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).data("autocomplete").menu.active) {
                    event.preventDefault();
                }
            })
        .autocomplete({


            minLength: 1,
            source: function (request, response) {
                $.getJSON(base_url+'cts_sampl_controller/search_sampl_detail/'+$("#claim_type").val(), {
                    term: extractLast(request.term)
                }, response);

            


            },
            search: function () {
                // custom minLength
                var term = extractLast(this.value);
                if (term.length < 1) {
                    return false;
                }
            },
            focus: function () {
                // prevent value inserted on focus
                return true;
            },
            select: function (event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
				this.value = terms.join("");
                //this.value = terms.join(", ");
                return false;
            }

        });	
		
			/*
			$("#claim_type").keyup(function()
			{
				var auto_complete_array = new Array();
				if($.trim($("#claim_type").val())!="")
				{
					$.ajax({
					type:'post',
					url:base_url+'cts_sampl_controller/search_sampl_detail',
					data:{claim_type:$("#claim_type").val()},
					success:function(msg)
					{
					auto_complete_array = msg.split(",");
					$("#claim_type").autocomplete({minLength: 0,source:auto_complete_array});
					}
					});
				}
				else
				{
				$("#claim_type").autocomplete({
					minLength: 0,
					source:""
					});
				}
			});	*/
			
			$("#claim_type").focusout(function()
			{
			if($.trim($(this).val())!="")
			{
				$.ajax({
				url:base_url+'cts_sampl_controller/check_claim_type',
				type:'POST',
				data:{claim_type:$(this).val()},
					success:function(msg)
					{
						if(parseInt(msg) == 0)
						{
						flag = 0;
						$("#claim_msg").show();
						$('#submit').attr('disabled','disabled');
						}
						else
						{
						$("#claim_msg").hide();
							if($("#class_msg").is(":visible") || $("#investigation_msg").is(":visible") || $("#species_type_msg").is(":visible") || $(".species_msg").is(":visible") || $("#insure_msg").is(":visible")|| $("#customer_msg").is(":visible"))
							{
							$('#submit').attr('disabled','disabled');
							}
							else
							{
							$('#submit').removeAttr('disabled');
							}
						}
					}
				});
				}
				else
				{
				$("#claim_msg").hide();
				$('#submit').removeAttr('disabled');
				}
			});
			/*$("#investigation_requested").keyup(function()
			{
				var auto_complete_array = new Array();
				if($.trim($("#investigation_requested").val())!="")
				{
					$.ajax({
					type:'post',
					url:base_url+'cts_investigation_controller/search_investigation_detail',
					data:{investigation_requested:$("#investigation_requested").val()},
					success:function(msg)
					{
					auto_complete_array = msg.split(",");
					$("#investigation_requested").autocomplete({minLength: 0,source:auto_complete_array});
					}
					});
				}
				else
				{
				$("#investigation_requested").autocomplete({
					minLength: 0,
					source:""
					});
				}
			});	*/
			
			$("#investigation_requested")
            // don't navigate away from the field on tab when selecting an item
            .keydown(function (event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).data("autocomplete").menu.active) {
                    event.preventDefault();
                }
            })
        .autocomplete({


            minLength: 1,
            source: function (request, response) {
                $.getJSON(base_url+'cts_investigation_controller/search_investigation_detail/'+$("#investigation_requested").val(), {
                    term: extractLast(request.term)
                }, response);

            


            },
            search: function () {
                // custom minLength
                var term = extractLast(this.value);
                if (term.length < 1) {
                    return false;
                }
            },
            focus: function () {
                // prevent value inserted on focus
                return true;
            },
            select: function (event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
				this.value = terms.join("");
                ////this.value = terms.join(", ");
				//alert(this.value);
                return false;
            }

        });	
		
			$("#investigation_requested").focusout(function()
			{
				if($.trim($(this).val())!="")
				{
				$.ajax({
				url:base_url+'cts_investigation_controller/check_investigation',
				type:'POST',
				data:{investigation_requested:$(this).val()},
					success:function(msg)
					{
						if(parseInt(msg) == 0)
						{
						flag = 0;
						$("#investigation_msg").show();
						$('#submit').attr('disabled','disabled');
						}
						else
						{
						$("#investigation_msg").hide();
							if($("#class_msg").is(":visible") || $("#claim_msg").is(":visible") || $("#species_type_msg").is(":visible") || $(".species_msg").is(":visible") || $("#insure_msg").is(":visible")|| $("#customer_msg").is(":visible"))
							{
							$('#submit').attr('disabled','disabled');
							}
							else
							{
							$('#submit').removeAttr('disabled');
							}
						}
					}
				});
				}
				else
				{
				$("#investigation_msg").hide();
				$('#submit').removeAttr('disabled');
				}
			});	
			 // don't navigate away from the field on tab when selecting an item
            //.keydown(function (event) {
               // if (event.keyCode === $.ui.keyCode.TAB &&
                     //   $(this).data("autocomplete").menu.active) {
                    //event.preventDefault();
               // }
            //})
		
	
		$(".surveyor").autocomplete({
            minLength: 1,
            source: function (request, response) {
                $.getJSON(base_url+'cts_surveyor_controller/search_surveyor_detail/'+$(".surveyor").val(), {
                    term: extractLast(request.term)
                }, response);
            },
           search: function () {
                // custom minLength
                var term = extractLast(this.value);
                if (term.length < 1) {
                    return false;
                }
            },
            focus: function () {
                // prevent value inserted on focus
                return true;
            },
            select: function (event, ui) {
				
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
               this.value = terms.join("");
			    //this.value = terms.join(", ");
                return false;
            }
		});
			$("#customer_name")
            // don't navigate away from the field on tab when selecting an item
            .keydown(function (event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).data("autocomplete").menu.active) {
                    event.preventDefault();
                }
            })
        .autocomplete({


            minLength: 1,
            source: function (request, response) {
                $.getJSON(base_url+'cts_customer_controller/search_customer/'+$("#customer_name").val(), {
                    term: extractLast(request.term)
                }, response);

            


            },
            search: function () {
                // custom minLength
                var term = extractLast(this.value);
                if (term.length < 1) {
                    return false;
                }
            },
            focus: function () {
                // prevent value inserted on focus
                return true;
            },
            select: function (event, ui) {
				
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
               this.value = terms.join("");
			    //this.value = terms.join(", ");
                return false;
            }

        });
			/*$("#customer_name").keyup(function()
			{
			
			if($.trim($("#customer_name").val())!=""){
			$("#customer_name").keyup(function()
			{
				//console.log(base_url+'cts_customer_controller/search_customer/'+$("#customer_name").val());
				$("#customer_name").autocomplete({
source: base_url+'cts_customer_controller/search_customer/'+$("#customer_name").val(),
minLength: 0,//search after two characters
select: function(event,ui){
    console.log(event);
    }
});
			})
			}
			else
				{
				$("#customer_name").autocomplete({
					minLength: 0,
					source:""
					});
				}
			});*/
			
			$("#customer_name").focusout(function()
			{
			var customer_name_arr = $("#customer_name").val().split("/");
			
			if(customer_name_arr.length !=1)
			{
			var customer_id = customer_name_arr[0];
			var customer_name = customer_name_arr[1];
			}
			else
			{
			customer_id = "";
			var customer_name = customer_name_arr[0];
			}
			if($.trim($(this).val())!="")
			{
			$.ajax({
			url:base_url+'cts_customer_controller/check_customer',
			type:'POST',
			data:{customer_nam:customer_name},
				success:function(msg)
				{
					if(parseInt(msg) == 0)
					{
					$("#customer_msg").show();
					$('#submit').attr('disabled','disabled');
					}
					else
					{
						$("#customer_msg").hide();
						if($("#class_msg").is(":visible") || $("#claim_msg").is(":visible") || $("#species_type_msg").is(":visible") || $(".species_msg").is(":visible") || $("#investigation_msg").is(":visible")|| $("#insure_msg").is(":visible"))
							{
							$('#submit').attr('disabled','disabled');
							}
							else
							{
							$('#submit').removeAttr('disabled');
							}
					}
				}
			});
			}
			else
			{
				$("#customer_msg").hide();
				$('#submit').removeAttr('disabled');
			}	
			$("#claim_customer_id").val(customer_id);
			$("#customer_id").val(customer_id);
			
			});
			
			
			
			$(".ui-menu-item").click(function()
			{
			alert("hello");
			});
			
			
			
			/*
			$("#insure_name").keyup(function()
			{
			var auto_complete_array = new Array();
			if($.trim($("#insure_name").val())!="")
			{
			$.ajax({
			type:'post',
			url:base_url+'cts_customer_controller/search_insure',
			data:{insure_name:$("#insure_name").val()},
				success:function(msg)
				{
				auto_complete_array = msg.split(",");
				$("#insure_name").autocomplete({minLength: 0,source:auto_complete_array});
				}
			});
			}
			else
				{
				$("#insure_name").autocomplete({
					minLength: 0,
					source:""
					});
				}
			});*/
			
			$("#insure_name")
            // don't navigate away from the field on tab when selecting an item
            .keydown(function (event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).data("autocomplete").menu.active) {
                    event.preventDefault();
                }
				
            })
        .autocomplete({


            minLength: 1,
            source: function (request, response) {
                $.getJSON(base_url+'cts_customer_controller/search_insure/'+$("#insure_name").val(), {
                    term: extractLast(request.term)
                }, response);

            


            },
            search: function () {
                // custom minLength
                var term = extractLast(this.value);
                if (term.length < 1) {
                    return false;
                }
            },
            focus: function () {
                // prevent value inserted on focus
                return true;
            },
            select: function (event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
				this.value = terms.join("");
                //this.value = terms.join(", ");
                return false;
            }

        });			
			$("#insure_name").focusout(function()
			{
				var insure_id_arr = $(this).val().split("-");
				$('#claim_insure_id').val(insure_id_arr[0]);
				if($.trim($(this).val())!="")
				{
				$.ajax({
				url:base_url+'cts_customer_controller/check_insure',
				type:'POST',
				data:{insure_name:$(this).val()},
					success:function(msg)
					{
						if(parseInt(msg) == 0)
						{
						flag = 0;
						$("#insure_msg").show();
						$('#submit').attr('disabled','disabled');
						}
						else
						{
						$("#insure_msg").hide();
							if($("#class_msg").is(":visible") || $("#claim_msg").is(":visible") || $("#species_type_msg").is(":visible") || $(".species_msg").is(":visible") || $("#investigation_msg").is(":visible")|| $("#customer_msg").is(":visible"))
							{
							$('#submit').attr('disabled','disabled');
							}
							else
							{
							$('#submit').removeAttr('disabled');
							}
						}
					}
				});
				}
				else
				{
				$("#insure_msg").hide();
				$('#submit').removeAttr('disabled');
				}
				
			});	
			$(".lab").change(function(){
			$(".select_lab").hide();
			var n = $( ".lab:checked" ).length;
			if(n==0)
			$(".select_lab").show();
			var lab_id = $(this).val();
			if($(this).is(':checked') == false)
			{
			$("."+$(this).attr('alt')).remove();
			}
			else{
				$.ajax({
					url:base_url+'cts_investigation_controller/get_lab_investigaion',
					type:'POST',
					dataType:'json',
					data:{labid:lab_id},
					success:function(msg)
					{
						for(i=0;i<msg.length;i++)
						{
						var investigation = replaceAll(" ","_",msg[i].investigation_requested);
						$('#investigation_requested_td').append("<div class='"+msg[i].dep_name.replace(' ','_')+"'><ul><input type='checkbox' name='investigation["+investigation+"]' value='"+msg[i].investigation_id+"' class='"+msg[i].dep_name+"'/>"+msg[i].investigation_requested+"</ul></div>")
						}
					}
				});
				}
			});
			
			$("#user_add_form").submit(function()
			{
			var pass = $("#user_password").val();
			var cpass = $("#confirm_password").val();
			if(pass != cpass)
			{
					 $( "#dialog-confirm" ).dialog({
						resizable: false,
						height:160,
						modal: true,
						 show: {
						effect: "blind",
						duration: 300
						},
						hide: {
						effect: "blind",
						duration: 300
						},
						buttons: {
						"OK": function() {
						$( this ).dialog( "close" );
						$("#confirm_password").focus();
						//window.location.assign('<?php echo base_url();?>'+'index.php/cts_surveyor_controller/delete_surveyor_by_id/'+species_id);
						}
						}
						});
			}
			else
			{
			$("#user_add_form").submit();
			}
			return false;
			});
			
			
			/* $("#confirm_password").focusout(function(){
			var pass = $("#user_password").val();
			var cpass = $("#confirm_password").val();
			
			}); */
			});
			
			setInterval(check_message,20000);
			
			function replaceAll(find, replace, str) {
				return str.replace(new RegExp(find, 'g'), replace);
			}
			function check_message()
			{
			$.ajax({
				url: base_url+'cts_controller/check_new_messages',
				type:'POST',
				dataType:'json',
				data:{abc:$.now()},
				error :function(xhr, ajaxOptions, thrownError)
							{
							console.log(xhr);
							console.log(ajaxOptions);
							console.log(thrownError);
							},
				success:function(msg)
				{
				var inbox_cnt = msg.length;
				var inbox_msg = parseInt($("#new_message").text())+msg.length;
				$("#new_message").html(inbox_msg);
					for(var i=0; i < inbox_cnt; i++)
					{
					$("#assignent_tr").after("<tr class='message_tr'><td>"+msg[i].lab_no+"</td><td>"+msg[i].assigned_date+"</td><td>"+msg[i].dep_name+"</td><td>"+msg[i].user_name+"</td><td>"+msg[i].user_position+"</td><td>"+msg[i].user_address+"</td><td>"+msg[i].user_email+"</td><td>"+msg[i].user_phone_home+"</td><td>"+msg[i].user_mobile+"</td><td><button class='view_message btn btn-primary' alt='"+msg[i].assignment_id+"'>View Message</button></td></tr><input type='hidden' value='"+msg[i].message+"' id='message_"+msg[i].assignment_id+"'/>");
					
					$(".view_message").bind('click',function(){
					view_message($(this));
					check_message();
						});
					
					}
				}
				});
			}
			
			




