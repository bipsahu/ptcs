<?php
if(isset($roles_info))
{
$i=0;
$role = array();
foreach($roles_info->result_array() as $key=>$value)
{
$role[] = $value;
$i++;
}
}
?>
<div class="container">
<center><h1>All Species Details</h1></center>
<?php
if(!empty($result_indicator))
{
	if($result_indicator == 1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Deletion Successful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Deletion Failed!</b></div>";
}
?>


<table class="table table-striped" id="table_show_all">
<thead><th>S No.</th> <th> Species </th> 
<?php 
		if(strtolower($this->session->userdata('user_position'))=="admin")
				{
				echo "<th>Edit</th>";
				}
				elseif(isset($role) || $role[3]['edit'])
				{
				echo "<th>Edit</th>";
				}
				?>
				<?php
				if(strtolower($this->session->userdata('user_position')) == "admin")
				{
				echo "<th>Delete</th>";
				}
				elseif(isset($role) || $role[3]['delete'])
				{
				echo "<th>Delete</th>";
				}


?></thead>

<?php 
$offset = $offset+1;
foreach($results->result() as $row){
?>
<tr>
<td><?php echo $offset;?></td>
<td><?php echo $row->animal_type;?></td>
<?php 
	if(strtolower($this->session->userdata('user_position'))=="admin")
	{
	echo "<td><a href='".base_url()."index.php/lims_animal_type_controller/edit_animal_type_by_id_form/".$row->animal_type_id."' class='btn btn-primary'><span class='glyphicon glyphicon-edit'></span>Edit Species</a></td>";
	}
	elseif(isset($role) || $role[3]['edit'])
	{
	echo "<td><a href='".base_url()."index.php/lims_animal_type_controller/edit_animal_type_by_id_form/".$row->animal_type_id."' class='btn btn-primary'><span class='glyphicon glyphicon-edit'></span>Edit Species</a></td>";
	}
	if(strtolower($this->session->userdata('user_position'))=="admin")
	{
	echo "<td><a href='#' class='btn btn-primary trash' alt='".$row->animal_type_id."'><span class='glyphicon glyphicon-trash'></span>Delete Species</a></td>";
	}
	elseif(isset($role) || $role[3]['delete'])
	{
	echo "<td><a href='#' class='btn btn-primary trash'  alt='".$row->animal_type_id."'><span class='glyphicon glyphicon-trash'></span>Delete Species</a></td>";
	}
?>
</tr>
<?php
$offset++;
}
?>
<tr style="margin-left:50%;"><td colspan="11"> <?php echo $links; ?></td></tr>
<?php if(strtolower($this->session->userdata('user_position'))=="admin" || isset($role) || $role[3]['add']){?>
<tr>
<td colspan="9"><a class="btn btn-primary" href="<?php echo base_url();?>index.php/lims_animal_type_controller/add_new_animal_type">Add New Species</a>
</tr>
<?php } ?>
</table>
</div>

<div id="dialog-confirm" title="Delete the Species?">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

</body>
<script type="text/javascript">
$(document).ready(function(){
 $( "#dialog-confirm" ).hide();
$('.trash').click(function(){
var species_id = $(this).attr('alt');
 $( "#dialog-confirm" ).dialog({
resizable: false,
height:160,
modal: true,
 show: {
effect: "blind",
duration: 300
},
hide: {
effect: "blind",
duration: 300
},
buttons: {
"Delete": function() {
window.location.assign('<?php echo base_url();?>'+'index.php/lims_animal_type_controller/delete_animal_type_by_id/'+species_id);
},
Cancel: function() {
$( this ).dialog( "close" );
}
}
});
});
});
</script>
</html>