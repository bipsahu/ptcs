<?php
if(isset($result))
{
	if($result==1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Update Succesful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Update Failed!</b></div>";
}
if(isset($animal_details)){
foreach($animal_details as $row)
{
$aid = $row['aid'];
$animal_name = $row['animal_name'];
}
}
?>

  
  

  <div class="container">
    <h3>Edit Animal Detail Form</h3><br>
      <table class="table" id="table">

        <form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/lims_customer_controller/edit_animal_by_id/<?php echo $aid;?>">
   
            
		
			<tr>
              <td><label>Animal Name</label></td>
              <td><input type="text" class="form-control" name="animal_name" value="<?php echo $animal_name;?>" required></td>
            </tr>
			
			<tr>
        	   <td colspan="2"><button  type="submit" name="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Submit</button></td>
            </tr>
          
        </form>
    </table>
	
</div><!---end of container-->
 </body>
 </html>
 