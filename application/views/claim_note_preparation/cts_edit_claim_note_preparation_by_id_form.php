<?php
/* if(isset($result))
{
	if($result==1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Update Succesful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Update Failed!</b></div>";
}
if(isset($virology_details)){
foreach($virology_details as $row)
{
$virology_id = $row['virology_id'];
$investigation_requested = $row['investigation_requested'];
$egg_inoculation = $row['egg_inoculation'];
$plate_aggluination = $row['plate_aggluination'];
$ha = $row['ha'];
$hi = $row['hi'];
$cft = $row['cft'];
$agid = $row['agid'];
$confirmative_diagnosis = $row['confirmative_diagnosis'];
}
} */
?>

<div class="container">
    <h3>Edit Virology Detail Form</h3><br>
	<table class="table" id="table">
        <form method="post" role="form" enctype="multipart/form-data" id="edit_virology_form"> 
          <tr>
              <td><label>Egg Inoculation</label></td>
              <td><input type="checkbox" name="egg_inoculation"  id="egg_inoculatio" value="<?php //echo $egg_inoculation;?>"></td>
            </tr>
            <tr>
              <td><label>Plate Aggluination</label></td>
              <td><input type="radio" class="radio-inline" name="pa_result" value="positive" id="pa_positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pa_result" value="negative" id="pa_negative" >&nbsp;Negative</td>
             
            </tr>
			
			 <tr>
              <td><label>Rapid Anitigen Test</label></td>
               <td><input type="radio" class="radio-inline" name="rapid_anitgen_test" value="positive" id="rapid_anitgen_test_positive" >&nbsp;Positive</td>
               <td><input type="radio" class="radio-inline" name="rapid_anitgen_test" value="negative" id="rapid_anitgen_test_negative" >&nbsp;Negative</td>
            </tr>
			
            <tr>
              <td><label>HA</label></td>
              <td><input type="radio" class="radio-inline" name="ha" value="positive" id="ha_positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="ha" value="negative" id="ha_negative" >&nbsp;Negative</td>
            </tr>
			
          
            <tr>
              <td><label>HI</label></td>
              <td><input type="radio" class="radio-inline" name="hi" value="positive" id="hi_positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="hi" value="negative" id="hi_negative" >&nbsp;Negative</td>
            </tr>
			
			<tr>
              <td><label>Penside Test for PPR</label></td>
              <td><input type="radio" class="radio-inline" name="penside_test" value="positive" id="penside_test_positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="penside_test" value="negative" id="penside_test_negative" >&nbsp;Negative</td>
            </tr>
			
			<tr>
              <td><label>Negri Body Test</label></td>
              <td><input type="radio" class="radio-inline" name="negri_body" value="positive" id="negri_body_positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="negri_body" value="negative" id="negri_body_negative" >&nbsp;Negative</td>
            </tr>
			
			

            <tr>
              <td><label>CFT</label></td>
              <td><input type='radio' class="radio-inline" name="cft_result" value="positive" id="cft_positive" >&nbsp;Positive</td>
              <td><input type='radio' class="radio-inline" name="cft_result" value="negative" id="cft_negative" >&nbsp;Negative</td>
            </tr>

            <tr>
               <td><label>AGID</label></td>
               <td><input type="radio" class="radio-inline" name="agid_result" value="positive" id="agid_positive" >&nbsp;Positive</td>
               <td><input type="radio" class="radio-inline" name="agid_result" value="negative" id="agid_negative" >&nbsp;Negative</td>
            </tr>
		
			
			<tr>
              <td><label>ELISA</label></td>
              <td><input type="radio" class="radio-inline" name="elisa_result" value="positive" id="elisa_positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="elisa_result" value="negative" id="elisa_negative" >&nbsp;Negative</td>
            </tr>
			
			<tr>
              <td><label>Confirmative Diagnosis</label></td>
               <td><input type="radio" class="radio-inline" name="cd_result" value="positive" id="cd_positive" >&nbsp;Positive</td>
               <td><input type="radio" class="radio-inline" name="cd_result" value="negative" id="cd_negative" >&nbsp;Negative</td>
            </tr>
			
			 <tr>
              <td><label>FAT</label></td>
              <td><input type="text" class="form-control" name="fat" id="fat" value="<?php //echo $ha;?>" ></td>
            </tr>
			
            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary" id="submit" >Submit</button></td>
            </tr>
          
        </form>
    </table>
	</div>
</div><!---end of container-->
</body>
</html>