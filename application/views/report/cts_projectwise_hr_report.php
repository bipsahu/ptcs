<div class="container">
    <center><h1>Projectwise HR Report</h1></center>
    <div id="print_btn" class="glyphicon glyphicon-print"></div>
    <table class="table table-bordered datatable" id="project">

        <?php //echo site_url('cts_report_controller/index/search'); ?><!--"-->

        <thead>
        <tr id="projectle_row">
            <th>S.N</th>
            <th>Project</th>
            <!-- <th>Indicator Based % Complete</th> --> 
            <th>Total No of HR Involved</th>
            <th>Total Estimated Hours</th>
            <th>Total Completed Hours</th>
            <th>Total Cost To Company</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $count = 1;

        foreach ($result as $row):

// print_r($row);die;
        ?>
        <tr>
            <td><?php echo $count; ?></td>
            <!-- <td><a href="<?php// echo base_url();?>index.php/cts_report_controller/task_report/<?php //echo $row['project_id']; ?>"><?php// echo $row["project_name"]; ?></a></td> -->
            <td><?php echo $row["project_name"]; ?></td>
            <td><?php echo $row["no_of_member"]; ?></td>
            <td><?php echo $row["total_estimated_hour"]; ?></td>
            <td><?php echo $row["total_completed_hour"]; ?></td>
            <td><?php echo $row["total_cost_to_company"]; ?></td>

            <?php
            $count++;
            endforeach; ?></tr>

        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".datepicker").datepicker({dateFormat: 'yy/mm/dd'});
        $('#print_btn').click(function () {
            $("#img_logo_anchor").attr('href', '#');
            window.print();
            $('#print_btn').show();
            $("#img_logo_anchor").attr('href', '<?php echo base_url();?>');
            $("#cts_logo_anchor").show();
        });
    });
</script>
<style type="text/css">
    input[type="search"] {
        margin-right: 35px;
    }

    #print_btn {
        position: absolute;
        margin-top: 65px;
        right: 20px;
    }
</style>
<style type="text/css" media="print">
    @page {
        size: auto;
        margin: 0mm;
    }

    body {
        font-weight: bold !important;
        font: Arial !important;
    }

    #cts_logo_anchor {
        display: none;
    }

    .select-report {
        display: none;
    }

    #head-icons {
        display: none;
    }

    .main-head,.datetime{
        text-align: center;
    }
    table{
        width: 100% !important;
    }
    #project_length,#project_filter,#project_info,#project_paginate{
        display: none;
        display: none;
    }
    th{
        font-weight: bold;
    }
    .footer-line {
        display: none;
    }

    #footer {
        display: none;
    }

    #print_btn {
        display: none;
    }

    .main-head {
        font-size: 24px;
        width: 600px;
    }

    #cts_logo_anchor img {
        width: 140px;
    }

    .container h1 {
        font-size: 16px;
        margin-top: 0px;
    }
</style>
</body>
</html>