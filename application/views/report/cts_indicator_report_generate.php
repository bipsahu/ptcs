<div class="container">
    <center><h1>Indicator Report</h1></center>
    <div id="print_btn" class="glyphicon glyphicon-print"></div>
    <div class="col-md-8 col-md-offset-3">
        <form class="form-horizontal" action="<?php echo site_url('cts_report_controller/task_report'); ?>"
              method="post">
            <div class="form-group col-sm-5">

                <select name="project" id="project" class="form-control">
                    <option value=" ">Select Project</option>
                    <?php

                    if (isset($projects)):
                        foreach ($projects->result_array() as $project) {
                            echo "<option value='" . $project['project_id'] . "'>" . $project['project_name'] . "</option>";
                        }
                    endif;
                    ?>
                </select>
            </div>
            <div class="form-group col-sm-5" style="margin-left: 50px;">
                <select name="task" id="task" class="form-control">
                    <option value=" ">Select Task</option>
                    <?php

                    if (isset($tasks)):
                        foreach ($tasks->result_array() as $task) {
                            echo "<option value='" . $task['task_id'] . "'>" . $task['task_name'] . "</option>";
                        }
                    endif;
                    ?>
                </select>
            </div>
        </form>
    </div>
    <table class="table table-bordered" id="indicatorDatatable">
<caption></caption>        
<thead>
        <tr id="new_customer_table_row">
            <th>S.N</th>
            <th>Indicators</th>
            <th>Weightage (%)</th>
            <th>% Complete</th>
        </tr>
        </thead>

    </table>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var project;
        //ajax get task list from the project id
        $("#project").change(function () {
            $("#task").html("<option value=' '>Select Task</option>");
            project = $(this).val();
            $.ajax({
                url: '<?php echo site_url("cts_task_controller/get_all_task_by_project_id");?>',
                type: "POST",
                data: {"project": project},
                dataType: "json",
                success: function (msg) {
                    $(msg).each(function (index, value) {
                        $("#task").append("<option value=" + value.task_id + ">" + value.task_name + "</option>");
                    });
                }
            });

        });

        //add the data to datatables dynamically using ajax
        $("#task").change(function () {

            //remove existing datatables
            $("#indicatorDatatable").DataTable().destroy();
            var task = $(this).val();
            $("#indicatorDatatable").find("caption").text("Project Name: " +$("#project").find('option:selected').text()+", Task Name : " + $(this).find('option:selected').text());
            $("#indicatorDatatable").DataTable({
                "ajax": {
                    "url": '<?php echo site_url("cts_report_controller/indicator_report");?>',
                    "type": "POST",
                    "data": {"task": task}
                },
                "columns": [
                    {"data": "s_no"},
                    {"data": "indicator_name"},
                    {"data": "weightage"},
                    {"data": "achieved"},
                ]

            });
        });
        $('#print_btn').click(function () {
            $("#img_logo_anchor").attr('href', '#');
            window.print();
            $('#print_btn').show();
            $("#img_logo_anchor").attr('href', '<?php echo base_url();?>');
            $("#cts_logo_anchor").show();
        });
    });
</script>
<style type="text/css">
    input[type="search"] {
        margin-right: 35px;
    }

    #print_btn {
        position: absolute;
        margin-top: 65px;
        right: 20px;
    }
</style>
<style type="text/css" media="print">
    @page {
        size: auto;
        margin: 0mm;
    }

    body {
        font-weight: bold !important;
        font: Arial !important;
    }

    #cts_logo_anchor {
        display: none;
    }

    .select-report {
        display: none;
    }

    #head-icons {
        display: none;
    }

    #new_customer_tab_length {
        display: none;
    }

    #new_customer_tab_filter {
        display: none;
    }

    #new_customer_tab_info {
        display: none;
    }

    #new_customer_tab_paginate {
        display: none;
    }

    .footer-line {
        display: none;
    }

    #footer {
        display: none;
    }

    #print_btn {
        display: none;
    }

    .main-head {
        font-size: 24px;
        width: 600px;
    }
    #project,#task{
        display: none;
    }
    .main-head,.datetime{
        text-align: center;
    }
    table{
        width: 100% !important;
    }
    #indicatorDatatable_length,#indicatorDatatable_filter,#indicatorDatatable_info,#indicatorDatatable_paginate{
        display: none;
        display: none;
    }
    #cts_logo_anchor img {
        width: 140px;
    }

    .container h1 {
        font-size: 16px;
        margin-top: 0px;
    }
</style>
</body>
</html>