<div class="container">
<div class="delete_msg alert alert-success">Delete Successful!</div>
<div class="table edit-surveyor table-striped" style="display:table">
<?php $count=1;
if(isset($assigned_surveyors) && $assigned_surveyors!=NULL)
foreach($assigned_surveyors->result_array() as $assigned):
	if($assigned['survey_type']=="0")
	$survey_type = "spot";
	if($assigned['survey_type']=="1")
	$survey_type = "Final";
	if($assigned['survey_type']=="2")
	$survey_type = "Legal Investigation";
	if($assigned['survey_type']=="3")
	$survey_type = "N/S";
?>
<div class="row"  style="border:2px solid;">
<div class="col-sm-6 <?php if($count==3) echo "clear";?>">
    <div class="surveyor-row-head"><span class="num">S.N <?php echo $count;?></span></div><div class="surveyor-row-head">Deputed Date:</div><div class="surveyor-row-head">Survey Type:</div><div class="surveyor-row-head">Surveyor:</div><div class="surveyor-row-head">Report Received Date:</div><div class="surveyor-row-head">Assess Amount:</div><div class="surveyor-row-head">Remarks:</div>
    
</div>

<div class="col-sm-6">

	<div class="surveyor-row"><a href="#" onclick="javascript:confirm('Are you Sure');" class="glyphicon glyphicon-trash" alt="<?php echo $assigned['as_id'];?>" style="color:#fff;"></a></div>
	
    <div class="surveyor-row"><a  href="#" class="appointment_date" id="appointment_date" data-type="date" data-placement="right" data-pk="<?php echo $assigned['sd_id'];?>" data-url="<?php echo site_url('cts_surveyor_deportation_controller/edit_surveyor_deportation_by_id');?>" data-title="Enter Deputed Date"><?php if($assigned['appointment_date']!=NULL) echo $assigned['appointment_date']; ?></a></div>
    
    <div class="surveyor-row"><a  href="#" class="survey_type" id="survey_type" data-type="select" data-placement="right" data-pk="<?php echo $assigned['sd_id'];?>" data-url="<?php echo site_url('cts_surveyor_deportation_controller/edit_surveyor_deportation_by_id');?>" data-title="Enter Survey Type"><?php if($assigned['survey_type']!=NULL) echo $survey_type?></a></div>
    
    <div class="surveyor-row"><a  href="#" class="surveyor_name" id="surveyor" data-type="text" data-placement="right" data-pk="<?php echo $assigned['sd_id'];?>" data-url="<?php echo site_url('cts_surveyor_deportation_controller/edit_surveyor_deportation_by_id');?>" data-title="Enter Surveyor"><?php if($assigned['surveyor']!=NULL) echo $assigned['surveyor']?></a></a></div>
    
    <div class="surveyor-row"><a  href="#" class="result_received_date" id="result_received_date" data-type="date" data-placement="right" data-pk="<?php echo $assigned['sd_id'];?>" data-url="<?php echo site_url('cts_surveyor_deportation_controller/edit_surveyor_deportation_by_id');?>" data-title="Enter Report Received Date"><?php if($assigned['result_received_date']!=NULL) echo $assigned['result_received_date']?></a></div>
    
    <div class="surveyor-row"><a  href="#" class="surveyor_estimated_loss" id="surveyor_estimated_loss" data-type="text" data-placement="right" data-pk="<?php echo $assigned['sd_id'];?>"data-url="<?php echo site_url('cts_surveyor_deportation_controller/edit_surveyor_deportation_by_id');?>" data-title="Enter Assess Amount"><?php if($assigned['surveyor_estimated_loss']!=NULL) echo $assigned['surveyor_estimated_loss']?></a></a></div>
    
    <div class="surveyor-row"><a  href="#" class="surveyor_remarks" id="surveyor_remarks" data-type="textarea" data-placement="right" data-pk="<?php echo $assigned['sd_id'];?>" data-url="<?php echo site_url('cts_surveyor_deportation_controller/edit_surveyor_deportation_by_id');?>" data-title="Enter Surveyor Remarks"><?php if($assigned['surveyor_remarks']!=NULL) echo $assigned['surveyor_remarks']?></a></div>
</div>
</div>
<?php $count++; endforeach;?>
<div id="confirm-dialog"></div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    //toggle `popup` / `inline` mode
	$(".delete_msg").hide();
	$(".glyphicon-trash").click(function(){
		
		var sd_id = $(this).attr("alt");
		
		$.ajax({
		url:"<?php echo base_url('');?>index.php/cts_surveyor_deportation_controller/delete_surveyor_deportation_by_id/"+sd_id,
		type:"POST",
		dataType:"JSON",
		success:function(msg){
	
			if(msg==1){
				$(".delete_msg").show();
			}
		}
		});
	});
	$(".glyphicon-trash").click(function(){
		
	});
    $.fn.editable.defaults.mode = 'popup';  
	$.fn.editable.defaults.ajaxOptions = {type: "POST"};   
	$.fn.editable.defaults.send = "always";
    
	$('.appointment_date').editable({
		 format: 'yyyy-mm-dd',    
        viewformat: 'dd/mm/yyyy',    
        datepicker: {
                weekStart: 1
           }
  	});
     $('.survey_type').editable({ 
        source: [
              {value: "", text: 'Select'},
              {value: 0, text: 'Spot'},
              {value: 1, text: 'Final'},
			  {value: 2, text: 'Legal Investigation'},
			  {value: 3, text: 'N/S'}
           ]
	 });
	 
	  $('.surveyor_name').editable();
	   $('.result_received_date').editable();
	    $('.surveyor_estimated_loss').editable();
		 $('.surveyor_remarks').editable();
  
});
</script>