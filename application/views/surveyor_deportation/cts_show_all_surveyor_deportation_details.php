<div class="container">
<center><h1>Surveyor Deputation </h1></center>
<?php
foreach($departments as $department){
		$listDepartments[$department->dep_id] = $department->dep_name;
	}
	?>
<div id="tabs-1">
<div class="dataTables_select">
	<select id="filter_send_to_dept" name="send_to_dept">
			<option value=""> Transfer Files To</option>
            <?php foreach($listDepartments as $key=>$dep):
			?>
            <option value="<?php echo $key;?>"><?php echo $dep;?></option>
            <?php endforeach;?>
    	</select>
<button type="button" id="apply_filter_button">Ok</button>
</div>
    <table class="table table-bordered datatable" id="new_customer_tab">
    <thead>
	<tr id="new_customer_table_row">
    <th><input type="checkbox" id="checkall" data-type="check"/></th><th>Regd No.</th><th>Claim No.</th><th>Intimation Date</th><th>Branch</th><th>Class</th><th>Insured</th><th>Surveyor</th><th>Claim Amount</th><th>Assess Amount</th><th>Surveyor Remarks</th><th>Add Surveyor Detail</th><th>Edit Detail</th><th>Claim Detail</th><th>Send To</th><th></th>
    </tr>
	</thead>
    <tbody>
	<?php 
	//print_r($result['result']->result());
	foreach($result['result']->result() as $row){
	?>
	<tr>
	<td><input type="checkbox" id="<?php echo $row->claim_id;?>" class="input-checkbox"/></td>
	<td><?php echo $row->regd_no;?></td>
	<td><?php echo $row->claim_no;?></td>
    <td><?php echo $row->intimation_date;?></td>
    <td><?php echo $row->claim_branch;?></td>
   	<td><?php echo $row->class;?></td>
	<td><?php echo $row->insure;?></td>
    
	<td><?php if(isset($row->surveyor)&&($row->surveyor)!= '0') echo $row->surveyor;?></td>
    
	<td><?php if(($row->claim_amount)!= '0') echo $row->claim_amount;?></td>
  <td><?php if(isset($row->surveyor_estimated_loss)&&($row->surveyor_estimated_loss)!= '0') echo $row->surveyor_estimated_loss;?></td>
  <td><?php if(isset($row->surveyor_remarks))echo $row->surveyor_remarks;?></td>
      <td><a class="btn btn-primary glyphicon-plus glyphicon"  alt="<?php echo $row->claim_id;?>" data-toggle='modal' data-target='#register_surveyor_deportation_<?php echo $row->claim_id;?>'></a></td>
      
 <div class="modal fade" id="register_surveyor_deportation_<?php echo $row->claim_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Surveyor Details</h4>
      </div>
      <div class="modal-body">
        <?php $details['result']= $row; $this->load->view('surveyor_deportation/cts_surveyor_deportation_register',$details);?>
      </div>
    </div>
  </div>
</div>
      
      <td><a class="btn btn-primary glyphicon-edit glyphicon"  alt="<?php echo $row->claim_id;?>" data-toggle='modal' data-target='#edit_surveyor_deportation-<?php echo $row->claim_id;?>'></a></td>
      
<div class="modal fade" id="edit_surveyor_deportation-<?php echo $row->claim_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Assigned Surveyors</h4>
      </div>
      <div class="modal-body">
        <?php $this->load->model('cts_surveyor_deportation_model'); $details['assigned_surveyors']= $this->cts_surveyor_deportation_model->get_assigned_surveyors($row->claim_id); 
		$this->load->view('surveyor_deportation/cts_edit_surveyor_deportation',$details);?>
      </div>
   		 </div>
      </div>
	</div>
       <td><a class="btn btn-primary glyphicon-list glyphicon"  alt="<?php echo $row->claim_id;?>" data-toggle='modal' data-target='#sd_show_detail-<?php echo $row->claim_id;?>'></a></td>
       <div class="modal fade" id="sd_show_detail-<?php echo $row->claim_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Claim Details</h4>
      </div>
      <div class="modal-body">
        <?php $details['row']=$row;$this->load->view('surveyor_deportation/cts_sd_show_all_claim_details',$details);?>
      </div>
    </div>
  </div>
</div>

   <?php /*?> <td><button class="btn btn-primary delete_surveyor" id="" alt="<?php echo $row->claim_id;?>"><span class="glyphicon glyphicon-trash"></span></button></td><?php */?>
	<td class="sumoSelect"><select id="send_to_dept_<?php echo $row->claim_id;?>" name="send_to_dept" alt="<?php echo $row->claim_id;?>">
			<option value=""> Select Department</option>
            <?php foreach($listDepartments as $key=>$dep):
				
				if($key!=0):?>
            <option value="<?php echo $key;?>"><?php echo $dep;?></option>
            <?php endif;endforeach;?>
    	</select>
      
    </td>
     <td> <a class="btn btn-primary glyphicon glyphicon-forward" alt="<?php echo $row->claim_id;?>">Send</a></td>
       <div id="confirm-dialog"></div>
	</tr>
	<?php
	}
	?>
    </tbody>
	</table>
  </div>
  <?php /*?><div id="tabs-2">
   <table class="table table-bordered" id="accepted_surveyor_deportation_table"></table>
  </div>
    <div id="tabs-3">
   <table class="table table-bordered" id="on_progess_surveyor_deportation_table"></table>
  </div>
   <div id="tabs-4">
   <table class="table table-bordered" id="completed_surveyor_deportation_table"></table>
  </div><?php */?>
</div>


<div class="modal fade" id="edit_surveyor_deportation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit surveyor_deportation Result</h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('surveyor_deportation/cts_edit_surveyor_deportation_by_id_form');?>
      </div>
    </div>
  </div>
</div>

</div>
<script type="text/javascript">

var selected_id=0;
var selected_edit_id = 0;
var surveyor_deportation_pending_offset;
var surveyor_deportation_completed_offset;
var surveyor_deportation_offset;
$(document).ready(function(){

$(".sd_success").hide();
$("#positive_negative_alert_edit").hide();
$( "#total_notification" ).hide();
$("no_of_sample_alert_edit").hide();
$("no_of_sample_alert_add").hide();
$( "#no_of_sample_alert" ).hide();
$("#positive_negative_alert").hide();
$( "#tabs" ).tabs();

//apply filter to transfer multiple files using multiple checkboxes
$("#apply_filter_button").click(function(){
	$(".input-checkbox").each(function(){
		
		if(($(this).is(":checked"))==true){
			var claim_id = $(this).attr('id');
			filter_send_to_dept(claim_id);
		}
	});
});

//send to departments
var send_forward = $(".glyphicon-forward");
$(send_forward).click(function(){
send_forward = $(this);
$("#confirm-dialog").html("Are You Sure?");
$("#confirm-dialog").dialog({
						resizable: false,
						height:140,
						modal: true,
						 show: {
						effect: "blind",
						duration: 300
						},
						hide: {
						effect: "blind",
						duration: 300
						},
						buttons: {
        "Yes": function() {
			
          glyphicon_forward_click(send_forward);
		 
        },
        Cancel: function() {
          $( this ).dialog( "close" );
		 
        }
      }
});
});



//setInterval(check_status,20000);

$(".glyphicon-plus").bind('click',function(){glyphicon_plus_click($(this))});
$(".glyphicon-edit").bind('click',function(){glyphicon_edit_click($(this))});
$(".set_accept_btn").bind('click',function(){set_accept_function($(this))});
$("#head_tab2").bind('click',function(){head_tab2_click_function(0);});
$("#head_tab3").bind('click',function(){head_tab3_click_function(0);});
$("#head_tab4").bind('click',function(){head_tab4_click_function(0);});


$("#checkall").click(function(){
	$(".input-checkbox").prop("checked", $("#checkall").prop("checked"))
});

$("#approve_form").submit(function(event)
{
if(!$("#approve_form")[0].checkValidity())
			{
			event.preventDefault();
			}
			else
			{
				$.ajax({
				url:'<?php  echo base_url();?>'+'index.php/cts_approval_controller/approve_sample/',
				type:'POST',
				data:$('#approve_form').serialize(),
				error :function(xhr, ajaxOptions, thrownError)
				{
					console.log(xhr);
					console.log(ajaxOptions);
					console.log(thrownError);
					head_tab3_click_function();
				},
				success:function(msg)
				{
					if(msg == 1)
					$( "#no_of_sample_alert" ).show();
					else
					{
					head_tab4_click_function(surveyor_deportation_completed_offset);
					$("#approve_surveyor_deportation").modal("hide");
					$("#approve_form")[0].reset();
					}
					//}
				} 
			});
			}
			return false;
});

function check_status()
{
$.ajax({
	url:'<?php  echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/check_new_customer',
	dataType:'json',
	error :function(xhr, ajaxOptions, thrownError)
				{
				console.log(xhr);
				console.log(ajaxOptions);
				console.log(thrownError);
				},
	success:function(msg)
	{
		console.log(msg);
		for(i=0; i<msg.length;i++)
		{
			var index = sample_ids.indexOf(parseInt(msg[i].sample_id));
			if(index == -1)
			{
			$("#new_customer_table_row").after("<tr><td>"+msg[i].labno+"</td><td>"+msg[i].customer_name+"</td><td>"+msg[i].firm_name+"</td><td>"+msg[i].animal+"</td><td>"+msg[i].age+"</td><td>"+msg[i].sex+"</td><td>"+msg[i].sample_type+"</td><td>"+msg[i].no_of_sample+"</td><td>"+msg[i].investigation_requested+"</td><td><button class='btn btn-primary set_accept_btn' id='' alt='"+msg[i].sample_id+"'><span class='glyphicon glyphicon-saved set_accept_btn'></span>&nbsp;Accept</button></td></tr>");
			$(".set_accept_btn").bind('click',function(){set_accept_function($(this))});
			sample_ids.push(parseInt(msg[i].sample_id));
	/* 		$(".glyphicon-plus").bind('click',function(){glyphicon_plus_click($(this))});
			$(".glyphicon-edit").bind('click',function(){glyphicon_edit_click($(this))}); */
			}
		}
	}
})
}
function glyphicon_plus_click(obj)
{
$("#no_of_sample_alert_add").hide();
selected_id =obj.attr("alt");
}

$(".surveyor_deportation_form").submit(function(event){
var ser = $(this).serialize();

				$.ajax({
				url:'<?php  echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/insert_surveyor_deportation_details/'+selected_id,
				type:'POST',
				data:ser,
				error :function(xhr, ajaxOptions, thrownError)
				{
				console.log(xhr);
				console.log(ajaxOptions);
				console.log(thrownError);
				//head_tab3_click_function(surveyor_deportation_offset);
				},
				
				success:function(msg)
				{
				
					if(parseInt(msg)==1){
						//$("#register_surveyor_deportation").modal("hide");
						$(".surveyor_deportation_form")[0].reset();
						$(".sd_success").show();
						
						//window.location.assign("<?php echo base_url()?>"+"index.php/cts_surveyor_deportation_controller");
					}
				}
			});
			
		return false;
	});
	
$("#edit_surveyor_deportation_form").submit(function(event){
		if(!$("#edit_surveyor_deportation_form")[0].checkValidity())
		{
			event.preventDefault();
		}
		else
		{
			$.ajax
			({
				url : '<?php echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/edit_surveyor_deportation_by_id/'+selected_edit_id,
				type: 'POST',
				data: $("#edit_surveyor_deportation_form").serialize(),
				success:function(msg)
				{
					if(parseInt(msg)==1)
					{
					head_tab3_click_function(surveyor_deportation_offset);
						$("#edit_surveyor_deportation").modal("hide");
					}
					else if(parseInt(msg) == 0)
					{
					alert("Edit failed");
						$("#register_surveyor_deportation").modal("hide");
						$("#surveyor_deportation_form")[0].reset();
					}
					else if(parseInt(msg) == 2)
					{
						$("#positive_negative_alert_edit").hide();
						$("#no_of_sample_alert_edit").show();
						$("#total_sample_tested").focus();
					}
					else if(parseInt(msg) == 3)
					{
						$("#no_of_sample_alert_edit").hide();
						$("#positive_negative_alert_edit").show();
						$("#total_sample_tested").focus();
					}
					
				}
			});
		}
		return false;
	});	
});
function head_tab2_click_function(offset)
{
	$.ajax
	({
	url:'<?php echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/search_pending_sample/'+offset,
	type: 'POST',
		success:function(msg)
		{
		$('#accepted_surveyor_deportation_table').html(msg);
		$(".set_publish_btn").bind('click',function(){set_publish_function($(this))});
		}
	});
}
function head_tab3_click_function(offset)
{
$.ajax
	({
	url:'<?php echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/search_all_claim/'+offset,
	type: 'POST',
		success:function(msg)
		{
		$('#on_progess_surveyor_deportation_table').html(msg);
		$(".glyphicon-plus").bind('click',function(){glyphicon_plus_click($(this));});
		$(".glyphicon-edit").bind('click',function(){glyphicon_edit_click($(this));});
		$(".glyphicon-trash").bind('click',function(){glyphicon_trash_click($(this));});
		$(".set_completed_button").bind('click',function(){set_completed_function($(this));});
		
		}
	});
}
function head_tab4_click_function(offset)
{
$.ajax
	({
	url:'<?php echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/search_completed_sample/'+offset,
	type: 'POST',
		success:function(msg)
		{
		$('#completed_surveyor_deportation_table').html(msg);
		$('.set_approved_btn').bind('click',function(){set_approved_btn($(this));});
		}
	});
}
function set_approved_btn(obj)
{
$("#no_of_sample_alert").hide();
var id = obj.attr("alt");
$("#investigation_requested_td").empty();
$("#surveyor_deportation_id").val(id);
$("#approve_form")[0].reset();
}
function set_completed_function(obj)
{
var id = obj.attr("alt");
$.ajax({
		url:'<?php echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/set_completed_on/'+id,
		type: 'POST',
			success:function(msg)
			{
				if(parseInt(msg) == 1)
				{
				head_tab3_click_function(surveyor_deportation_offset);
				}
			}
		});
}

function set_accept_function(obj)
{
	var id =obj.attr("alt");
		$.ajax({
		url:'<?php echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/set_accept_on/'+id,
		type: 'POST',
			success:function(msg)
			{
				if(parseInt(msg) == 1)
				{
				window.location.assign("<?php echo base_url()?>"+"index.php/cts_surveyor_deportation_controller");
				}
			}
		});
}
function set_publish_function(obj)
{
var id= obj.attr("alt");
	$.ajax({
	url:'<?php echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/set_publish_on/'+id,
	type:'Post',
		success:function(msg)
		{
			if(parseInt(msg) == 1)
			{
			head_tab2_click_function(surveyor_deportation_pending_offset);
			}
		}
	});
}
function glyphicon_trash_click(obj)
{
	selected_delete_id = obj.attr("alt");
	 $( "#dialog-confirm" ).dialog({
						resizable: false,
						height:160,
						modal: true,
						 show: {
						effect: "blind",
						duration: 300
						},
						hide: {
						effect: "blind",
						duration: 300
						},
						buttons: {
						"OK": function() 
						{
							$.ajax
									({
										url : '<?php echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/delete_surveyor_deportation_by_id/'+selected_delete_id,
										type: 'POST',
										error:function(xhr, ajaxOptions, thrownError)
										{
										console.log(xhr);
											console.log(ajaxOptions);
											console.log(thrownError);
										},
											success:function(msg)
											{
												if(parseInt(msg)==1)
												{
												head_tab3_click_function(surveyor_deportation_offset);
												$("#dialog-confirm").dialog( "close");
												}
											}
								});
						},
						Cancel: function() {
						$( this ).dialog( "close" );
						}
						}
						});

}
function glyphicon_forward_click(obj)
{

claim_id =obj.attr("alt");
send_to_dept(claim_id);
}



function send_to_dept(obj)
{
	var id =obj;
	var send_to_select = $("#send_to_dept_"+id).val();
		$.ajax({
		type: 'POST',
		url:'<?php echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/send_to_dept/'+id,
		data:{'send_values':send_to_select,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
		dataType:'json',
		error:function(xhr, ajaxOptions, thrownError)
										{
										console.log(xhr);
											console.log(ajaxOptions);
											//console.log(thrownError);
										},
		success:function()
			{
			
				 window.location.href = ("<?php echo base_url()?>"+"index.php/cts_surveyor_deportation_controller");
			}
		});
}


function filter_send_to_dept(claim_id,status)
{
	var id =claim_id;
	var send_to_select = $("#filter_send_to_dept").val();
		$.ajax({
		type: 'POST',
		url:'<?php echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/send_to_dept/'+id,
		data:{'send_values':send_to_select,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
		dataType:'json',
		error:function(xhr, ajaxOptions, thrownError)
										{
										console.log(xhr);
											console.log(ajaxOptions);
											//console.log(thrownError);
										},
		success:function()
			{
			
				 window.location.href = ("<?php echo base_url()?>"+"index.php/cts_surveyor_deportation_controller");
			}
		});
}

 function glyphicon_edit_click(obj)
{
	$("#no_of_sample_alert_edit").hide();
	selected_edit_id = obj.attr("alt");
	$.ajax
	({
			url : '<?php echo base_url();?>'+'index.php/cts_surveyor_deportation_controller/edit_surveyor_deportation_by_id_form/'+selected_edit_id,
			type: 'POST',
			dataType:'json',
			success:function(msg)
			{
			console.log(msg);
			if(msg.test_method == "ELISA")
			{
			$("#elisa").attr("selected",true)
			}
			if(msg.test_method == "PAT")
			{
			$("#pat").attr("selected",true)
			}
			if(msg.test_method == "Others")
			{
			$("#others").attr("selected",true)
			}
			$("#remark").val(msg.remark);
			$("#approved_remark").val(msg.approved_remark);
			$("#total_sample_tested").val(msg.total_sample_tested);
			$("#positive_result_edit").val(msg.positive_result);
			$("#negative_result_edit").val(msg.negative_result);
			$("#sample_collection_dat").val(msg.sample_collection_date);
			$("#tested_date").val(msg.tested_date);
			$("#approved_date").val(msg.approved_date);
			if(msg.result=='positive')
			$("#positive").attr("checked",true);
			if(msg.result=='negative')
			$("#negative").attr("checked",true);
			}
		});
}
function surveyor_deportation_pagination(offset)
{
surveyor_deportation_offset = offset;
head_tab3_click_function(surveyor_deportation_offset);
}
function surveyor_deportation_completed_pagination(offset)
{
surveyor_deportation_completed_offset = offset;
head_tab4_click_function(surveyor_deportation_completed_offset);
}
function surveyor_deportation_pending_pagination(offset)
{
surveyor_deportation_pending_offset = offset;
head_tab2_click_function(surveyor_deportation_pending_offset);
}
</script>
<?php /*?><script src="<?php echo base_url('');?>/resource/js/jquery.sumoselect.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url('');?>/resource/css/sumoselect.css"><?php */?>
</body>
</html>