<div class="container">
<center><h1>Molecular Biology Annual Report</h1></center>
<table class="table" id="table">
<form action="<?php echo base_url();?>index.php/lims_final_report_controller/molecular_biology_final_report_execute/<?php echo $lab_id;?>" method="GET">
<tr><td>
<select name="year" class="form-control">

<option value="2071/72">2071/72</option>
<option value="2072/73">2072/73</option>
<option value="2073/74">2073/74</option>
<option value="2074/75">2074/75</option>
</select></td>

<td>
<select name="investigation_requested" class="form-control">
<?php 

foreach( $investigation_requested->result() as $row )
			{ ?>
<option value="<?php echo $row->investigation_requested_id; ?> "><?php echo $row->investigation_requested; ?></option>
<?php } ?>
</select></td>
<td><input type="checkbox" name="test_method[]" value="m_gene"/>&nbsp;M Gene&nbsp;<input type="checkbox" name="test_method[]" value="h5"/>&nbsp;H5&nbsp;<input type="checkbox" name="test_method[]" value="n1"/>&nbsp;N1&nbsp;<input type="checkbox" name="test_method[]" value="n9"/>&nbsp;N9&nbsp;<input type="checkbox" name="test_method[]" value="h9"/>&nbsp;H9&nbsp;<input type="checkbox" name="test_method[]" value="h7"/>&nbsp;H7&nbsp;<input type="checkbox" name="test_method[]" value="ibd"/>&nbsp;IBD&nbsp;<input type="checkbox" name="test_method[]" value="nd"/>&nbsp;ND&nbsp;</td>
<td><input type="submit" value="Submit" class="btn btn-primary"></td></tr>
</table>
</form>
<?php 
if(isset($investigation))
{
echo "<h3>The Analysis Report for : ";
echo $investigation." ";
}
if(isset($year))
echo "for the year of ".$year;
if(isset($test_methods))
{	
	$number_of_test_methods = count($test_methods);
	$i=1;
	echo " for ";
	foreach($test_methods as $value)
	{	
		if($i == $number_of_test_methods)
		echo strtoupper(str_replace("_"," ",$value));
		else
		echo strtoupper(str_replace("_"," ",$value)).",";
		$i++;
	}
	echo "</h3>";
}
?>
<?php if(isset($result)){ ?>
	<table class="table table-striped"><thead>
	<tr><th rowspan='2'>Month</th><th rowspan='2'>Total Sample Tested</th><th colspan='<?php echo count($result)+1;?>'><center>Result</center></th></tr>
	<tr>
	<?php 
	foreach($test_methods as $value)
	{
	$test_method[] = $value;
	echo "<th>".strtoupper(str_replace("_"," ",$value))."</th>";
	}
?>
	<th>Negative</th></tr></thead>
	<?php foreach($result as $row)
			{
				echo "<tr><td>Shrawan</td><td>".$result['total_sample'][0]."</td>";
				for($i = 0;$i<count($result)-2;$i++)
				{
				echo "<td>".$result[$test_method[$i]][0]."</td>";
				}
				echo "<td>".$result['negative_sample'][0]."</td></tr>";
				
				echo "<tr><td>Bhadra</td><td>".$result['total_sample'][1]."</td>";
				for($i = 0;$i<count($result)-2;$i++)
				echo "<td>".$result[$test_method[$i]][1]."</td>";
				echo "<td>".$result['negative_sample'][1]."</td></tr>";
				
				echo "<tr><td>Ashwin</td><td>".$result['total_sample'][2]."</td>";
				for($i = 0;$i<count($result)-2;$i++)
				echo "<td>".$result[$test_method[$i]][2]."</td>";
				echo "<td>".$result['negative_sample'][2]."</td></tr>";
				
				echo "<tr><td>Kartik</td><td>".$result['total_sample'][3]."</td>";
				for($i = 0;$i<count($result)-2;$i++)
				echo "<td>".$result[$test_method[$i]][3]."</td>";
				echo "<td>".$result['negative_sample'][3]."</td></tr>";
				
				echo "<tr><td>Mangshir</td><td>".$result['total_sample'][4]."</td>";
				for($i = 0;$i<count($result)-2;$i++)
				echo "<td>".$result[$test_method[$i]][4]."</td>";
				echo "<td>".$result['negative_sample'][4]."</td></tr>";
				
				echo "<tr><td>Poush</td><td>".$result['total_sample'][5]."</td>";
				for($i = 0;$i<count($result)-2;$i++)
				echo "<td>".$result[$test_method[$i]][5]."</td>";
				echo "<td>".$result['negative_sample'][5]."</td></tr>";
				
				echo "<tr><td>Magh</td><td>".$result['total_sample'][6]."</td>";
				for($i = 0;$i<count($result)-2;$i++)
				echo "<td>".$result[$test_method[$i]][6]."</td>";
				echo "<td>".$result['negative_sample'][6]."</td></tr>";
				
				echo "<tr><td>Falgun</td><td>".$result['total_sample'][7]."</td>";
				for($i = 0;$i<count($result)-2;$i++) 
				echo "<td>".$result[$test_method[$i]][7]."</td>";
				echo "<td>".$result['negative_sample'][7]."</td></tr>";
				
				echo "<tr><td>Chaitra</td><td>".$result['total_sample'][8]."</td>";
				for($i = 0;$i<count($result)-2;$i++)
				echo "<td>".$result[$test_method[$i]][8]."</td>";
				echo "<td>".$result['negative_sample'][8]."</td></tr>";
				
				echo "<tr><td>Baisakha</td><td>".$result['total_sample'][9]."</td>";
				for($i = 0;$i<count($result)-2;$i++)
				echo "<td>".$result[$test_method[$i]][9]."</td>";
				echo "<td>".$result['negative_sample'][9]."</td></tr>";
				
				echo "<tr><td>Jestha</td><td>".$result['total_sample'][10]."</td>";
				for($i = 0;$i<count($result)-2;$i++)
				echo "<td>".$result[$test_method[$i]][10]."</td>";
				echo "<td>".$result['negative_sample'][10]."</td></tr>";
				
				echo "<tr><td>Ashad</td><td>".$result['total_sample'][11]."</td>";
				for($i = 0;$i<count($result)-2;$i++)
				echo "<td>".$result[$test_method[$i]][11]."</td>";
				echo "<td>".$result['negative_sample'][11]."</td></tr>";
				
				echo "<tr><td><b>Total</b></td><td><b>".(array_sum($result['total_sample'])/2)."</b></td>";
				for($i = 0; $i<count($result)-2; $i++)
				{
				echo "<td><b>".array_sum($result[$test_method[$i]])."</b></td>";
				}
				echo "<td><b>".(array_sum($result['negative_sample'])/2)."</b></td></tr>";
				
				break;
				
			}
	?>
	</table>
	<?php } ?>
</div>

