<?php 

Class cts_surveyor_deportation_model extends CI_model 
{
	public function show_all_surveyor_deportation_details($offset = 0)
	{	
	
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('surveyor_deportation', 'claim_registration.claim_id = surveyor_deportation.claim_id','left');
		//$this->db->join('assigned_surveyors', 'surveyor_deportation.sd_id = assigned_surveyors.sd_id','left');
		$this->db->order_by("claim_registration.claim_id", "desc"); 
		$this->db->where('claim_registration.status','0');
		$this->db->group_by('claim_registration.claim_id');
		//$this->db->or_where('status',1);
		$result['result'] = $this->db->get();
		$result['count'] = $result['result']->num_rows();
		//$this->db->where('status', 1);
		//$this->db->update('claim_registration',  array('status' => 2)); 
		//print_r($result['result']->result_array());
		return $result;
	}
	
	public function show_all_surveyor_deportation_details_for_claim()
	{
		$result = $this->db->get('surveyor_deportation');
		//$this->db->join('assigned_surveyors', 'surveyor_deportation.sd_id = assigned_surveyors.sd_id','left');
		return $result->result_array();
	
	}
	
	 
	public function insert_surveyor_deportation_details($claim_id)
	{
		if($this->input->post('sd_id') != null)
			$data['sd_id'] = strval($this->input->post('sd_id'));
		if($this->input->post('survey_type') != null)
			$data['survey_type'] = strval($this->input->post('survey_type'));
		
		if($this->input->post('surveyor') != null)
			$data['surveyor'] = $this->input->post('surveyor');
		if($this->input->post('surveyor_estimated_loss') != null)
			$data['surveyor_estimated_loss'] = $this->input->post('surveyor_estimated_loss');
		if($this->input->post('appointment_date') != null)
			$data['appointment_date'] = $this->input->post('appointment_date');
		if($this->input->post('result_received_date') != null)
			$data['result_received_date'] = $this->input->post('result_received_date');
		if($this->input->post('surveyor_remarks') != null)
			$data['surveyor_remarks'] = $this->input->post('surveyor_remarks');
		//if($this->input->post('remarks') != null)
		//$data['remark'] = $this->input->post('remarks');
		
		//if($this->input->post('claim_collection_date') != null)
		//$data['claim_collection_date'] = $this->input->post('claim_collection_date');
		//$data['tested_date'] =  date('m-d-Y');
	/* $data = array(
	'test_method' => $this->input->post('test_method'),
	'result' => $this->input->post('result'),
	'remark' => $this->input->post('remarks'),
	'total_claim_tested' => $this->input->post('total_claim_tested'),
	'tested_date' => date('m-d-Y'),
	'claim_collection_date' => $this->input->post('claim_collection_date'),
	'approved_date' => date('m-d-Y')
	
	); */
	$data['claim_id'] = $claim_id;
	//// $this->db->select('*'); 
	 //$this->db->where('claim_id',$claim_id);
	 // $this->db->where('surveyor',trim($data['surveyor']));
	 //$deportation = $this->db->get('surveyor_deportation')->num_rows();
	//if($deportation>=1){
	//	$this->db->where('claim_id',$claim_id);
		//$this->db->where('surveyor',trim($data['surveyor']));
		//$result = $this->db->update('surveyor_deportation', $data);
	//}
	//else
	$result = $this->db->insert('assigned_surveyors', $data);
		
		for($count=1;$count<=10;$count++){
		//injsert if surveyor is not empty
			if($this->input->post('sd_id') != null)
			$data1['sd_id'] = strval($this->input->post('sd_id'));
			if($this->input->post('surveyor_'.$count) != null){
				if($this->input->post('survey_type_'.$count) != null)
					$data1['survey_type'] = strval($this->input->post('survey_type_'.$count));
				
					$data1['surveyor'] = $this->input->post('surveyor_'.$count);
				if($this->input->post('surveyor_estimated_loss_'.$count) != null)
					$data1['surveyor_estimated_loss'] = $this->input->post('surveyor_estimated_loss_'.$count);
				if($this->input->post('appointment_date_'.$count) != null)
					$data1['appointment_date'] = $this->input->post('appointment_date_'.$count);
				if($this->input->post('result_received_date_'.$count) != null)
					$data1['result_received_date'] = $this->input->post('result_received_date_'.$count);
				if($this->input->post('surveyor_remarks_'.$count) != null)
					$data1['surveyor_remarks'] = $this->input->post('surveyor_remarks_'.$count);
				//if($this->input->post('remarks') != null)
				//$data['remark'] = $this->input->post('remarks');
				
				//if($this->input->post('claim_collection_date') != null)
				//$data['claim_collection_date'] = $this->input->post('claim_collection_date');
				//$data['tested_date'] =  date('m-d-Y');
			/* $data = array(
			'test_method' => $this->input->post('test_method'),
			'result' => $this->input->post('result'),
			'remark' => $this->input->post('remarks'),
			'total_claim_tested' => $this->input->post('total_claim_tested'),
			'tested_date' => date('m-d-Y'),
			'claim_collection_date' => $this->input->post('claim_collection_date'),
			'approved_date' => date('m-d-Y')
			
			); */
			$data1['claim_id'] = $claim_id;
		$result = $this->db->insert('assigned_surveyors', $data1);
		}
		
		}
		
			if($this->db->_error_number()==1062)
			{
				return 0;
			}
			else
			{
				return 1;
			}
	
	}
	public function get_assigned_surveyors($claim_id){
		
		$this->db->select("*");
		$this->db->from('assigned_surveyors');
		//$this->db->join('surveyor_deportation', 'surveyor_deportation.sd_id = assigned_surveyors.sd_id','left');
		$this->db->where("assigned_surveyors.claim_id",$claim_id);
		$result = $this->db->get();
		return $result;
	}
	public function get_details_of_surveyor_deportation_by_id ($surveyor_deportation_id)
	{
		$this->db->select("*");
		$this->db->from('assigned_surveyors');
		$this->db->join('surveyor_deportation', 'surveyor_deportation.sd_id = assigned_surveyors.sd_id','left');
		$this->db->where('surveyor_deportation_id',$surveyor_deportation_id);
		$result = $this->db->get();
		return $result->result_array();
	}
	
	public function delete_surveyor_deportation_by_id($assigned_id)
	{
	$result = $this->db->delete('assigned_surveyors', array('as_id' => $assigned_id)); 
	if($result)
	echo "1";
	}
	
	public function edit_surveyor_deportation_by_id($surveyor_deportation_id)
	{
		$param = $this->input->post("name");
		$value = $this->input->post("value");
		
		$this->db->where("sd_id",$surveyor_deportation_id);
		$result = $this->db->update('assigned_surveyors',array($param=>$value));
		return $result;
		
	}
	
	
	
	public function check_new_customer()
	{	
		$this->db->group_by('claim_registration.claim_id');
		$this->db->select('*');
		$this->db->from('claim_registration');
		//$this->db->join('surveyor_deportation', 'claim_registration.claim_id = surveyor_deportation.claim_id');
		$this->db->order_by("claim_id", "desc"); 
		$this->db->where('status',0);
		$result = $this->db->get();
		$i = 0;
		$data_arr = array();
		foreach($result->result() as $row)
		{
			$data_arr[$i]["surveyor_deportation_id"] = $row->surveyor_deportation_id;
			$data_arr[$i]["regd-no"] = $row->regd_no;
			$data_arr[$i]["claim_id"] = $row->claim_id;
			$data_arr[$i]['customer_name'] = $row->customer_name;
			$data_arr[$i]['firm_name'] = $row->firm_name;
			$data_arr[$i]['animal'] = $row->animal;
			$data_arr[$i]['investigation_requested'] = $row->investigation_requested;
			$data_arr[$i]['claim_type'] = $row->claim_type;
			$data_arr[$i]['no_of_claim'] = $row->no_of_claim;
			$data_arr[$i]['age'] = $row->age;
			$data_arr[$i]['sex'] = $row->sex;
			$data_arr[$i]['animal_type'] = $row->animal_type;
			$data_arr[$i]['test_method'] = $row->test_method;
			$data_arr[$i]['result'] = $row->result;
			$data_arr[$i]['remark'] = $row->remark;
			$data_arr[$i]['approved_remarks'] = $row->approved_remarks;
			$data_arr[$i]['tested_date'] = $row->tested_date;
			$data_arr[$i]['approved_date'] = $row->approved_date;
			$this->db->where('claim_id',$row->claim_id);
			$this->db->update('surveyor_deportation',array('registered'=>1));
			$i++;
		}

		echo json_encode($data_arr);
	}
	
	public function set_accept_on($claim_id)
	{
	$this->db->where('claim_id',$claim_id);
	$result = $this->db->update('claim_registration',array('status' => 1));
	return $result;
	}	
	public function search_pending_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('surveyor_deportation', 'claim_registration.claim_id = surveyor_deportation.claim_id');
		$this->db->order_by("surveyor_deportation_id", "desc"); 
		$this->db->where('registered',5);
		$this->db->where('publish_bit',0);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}
	public function set_publish_on($surveyor_deportation_id)
	{
	$this->db->where('surveyor_deportation_id',$surveyor_deportation_id);
	$result = $this->db->update('surveyor_deportation',array('publish_bit' => 1));
	return $result;
	}
	public function search_on_progress_claim($offset = 0)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('surveyor_deportation', 'claim_registration.claim_id = surveyor_deportation.claim_id');
		$this->db->order_by("surveyor_deportation_id", "desc"); 
		$this->db->where('registered',3);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
		
	}
	public function set_completed_on($surveyor_deportation_id)
	{
	$this->db->where('surveyor_deportation_id',$surveyor_deportation_id);
	$result = $this->db->update('surveyor_deportation',array('registered' => 4));
	return $result;
	}
	public function search_completed_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('surveyor_deportation', 'claim_registration.claim_id = surveyor_deportation.claim_id');
		$this->db->order_by("surveyor_deportation_id", "desc"); 
		$this->db->where('registered',4);
		$this->db->or_where('registered',5);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}
	
	public function send_to_dept($claim_id,$status)
	{
		$this->db->where('claim_id',$claim_id);
		$result = $this->db->update('claim_registration',array('status' => $status));
		if($status == 0){
			$this->db->where('claim_id',$claim_id);
			$sd_check = $this->db->get('surveyor_deportation')->num_rows();
			if($sd_check<1||$sd_check==NULL)
				$this->db->insert('surveyor_deportation',array('claim_id' => $claim_id));
				
		}
		if($status == 1){
			$this->db->where('claim_id',$claim_id);
			$sd_check = $this->db->get('claim_note_preparation')->num_rows();
			if($sd_check<1 || $sd_check==NULL)
				$this->db->insert('claim_note_preparation',array('claim_id' => $claim_id));
				
		}
		if($status == 2){
			$this->db->where('claim_id',$claim_id);
			$sd_check = $this->db->get('legal')->num_rows();
			if($sd_check<1||$sd_check==NULL)
				$this->db->insert('legal',array('claim_id' => $claim_id));
				
		}
		if($status == 3){
			$this->db->where('claim_id',$claim_id);
			$sd_check = $this->db->get('approve_recommend')->num_rows();
			if($sd_check<1||$sd_check==NULL)
				$this->db->insert('approve_recommend',array('claim_id' => $claim_id,'ar_status'=>'0'));
				
		}
		if($status == 4){
			$this->db->where('claim_id',$claim_id);
			$sd_check = $this->db->get('discharge_voucher_generation')->num_rows();
			if($sd_check<1||$sd_check==NULL)
				$this->db->insert('discharge_voucher_generation',array('claim_id' => $claim_id));
				
		}
		if($status == 5){
			$this->db->where('claim_id',$claim_id);
			$sd_check = $this->db->get('accounts')->num_rows();
			if($sd_check<1||$sd_check==NULL)
				$this->db->insert('accounts',array('claim_id' => $claim_id));
				
		}
		if($status == 6){
			$this->db->where('claim_id',$claim_id);
			$sd_check = $this->db->get('store')->num_rows();
			if($sd_check<1||$sd_check==NULL)
				$this->db->insert('store',array('claim_id' => $claim_id));
				
		}
		return $result;
	}
	public function generate_report($claim_id)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('surveyor_deportation', 'claim_registration.claim_id = surveyor_deportation.claim_id');
		$this->db->join('customer','claim_registration.customer_id = customer.cid');
		$this->db->where('surveyor_deportation.claim_id',$claim_id); 
		/* $this->db->where('registered',5); */
		$this->db->where('publish_bit',1);
		$result = $this->db->get();
		return $result;
	}
/* 	public function generate_entry_report($claim_id)
	{
	$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('surveyor_deportation', 'claim_registration.claim_id = surveyor_deportation.claim_id');
		$this->db->join('customer','claim_registration.customer_id = customer.cid');
		$this->db->where('surveyor_deportation.claim_id',$claim_id); 
		$this->db->where('publish_bit',1);
		$result = $this->db->get();
		return $result;
	} */
	
}