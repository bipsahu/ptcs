<?php
if(isset($result))
{
	if($result==1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Update Succesful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Update Failed!</b></div>";
}
if(isset($user_details)){
foreach($user_details as $row)
{
$user_id = $row['user_id'];
$user_name = $row['user_name'];
$user_position = $row['user_position'];
$user_address = $row['user_address'];
$user_mobile = $row['user_mobile'];
$user_email = $row['user_email'];
$user_phone_home = $row['user_phone_home'];
$user_phone_office = $row['user_phone_office'];
}
?>
<div class="container">
    <h3>User Edit Form</h3><br>
      <table class="table" id="table">
        <form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/cts_controller/edit_user_by_id/<?php echo $user_id?>">      
		 <tr>
              <td><label>E-mail</label></td>
              <td><input type="email" class="form-control" name="user_email" id="user_email" required value="<?php echo $user_email;?>"></td>
            </tr>
			  <tr id="error_msg_tr">
              <td colspan="2"><div class="error_msg"></div></td>
            </tr>
            <tr>
              <td><label>Name</label></td>
              <td><input type="text" class="form-control" name="user_name" required value="<?php echo $user_name;?>"></td>
            </tr>
            <tr>
              <td><label>Address</label></td>
              <td><input type="text" class="form-control" name="user_address" required value="<?php echo $user_address;?>"></td>
            </tr>
            <tr>
              <td><label>Position</label></td>
              <td><select name="user_position">
			  <?php if(isset($position_name)){
			  foreach($position_name->result() as $row)
			  { 
			  if($row->position_name == $user_position)
			  echo "<option value='".$row->position_name."' selected required>".$row->position_name."</option>";
			  else
			  echo "<option value='".$row->position_name."' required>".$row->position_name."</option>";
			  }
			  }
			  ?>
			  </select>
            </tr>
          
            <tr>
              <td><label>Mobile No.</label></td>
              <td><input type="text" class="form-control" name="user_mobile" value="<?php echo $user_mobile?>"></td>
            </tr>

            <tr>
              <td><label>Home Phone No.</label></td>
              <td><input type="text" class="form-control" name="user_phone_home" value="<?php echo $user_phone_home?>"></td>
            </tr>

            <tr>
              <td><label>Office Phone No.</label></td>
              <td><input type="text" class="form-control" name="user_phone_office" value="<?php echo $user_phone_office?>"></td>
            </tr>
			<tr>
			<td><label>Edit the Department</label></td>
			<td>
			<?php
			if(isset($department_list))
			$user_department_arr = array();
			foreach($department_of_user as $user_department_row)
			{
			$user_department_arr[] = $user_department_row['dep_id'];
			}
			foreach( $department_list as $row )
			{
			if(in_array($row->dep_id,$user_department_arr))
			echo "<input type='checkbox' name='dep_chk_group[]' value='".$row->dep_id."' checked/> ".$row->dep_name."&nbsp;&nbsp;";
			else
			echo "<input type='checkbox' name='dep_chk_group[]' value='".$row->dep_id."'/> ".$row->dep_name."&nbsp;&nbsp;";
			}
			?>
			</td>
			</tr>

            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary" id="submit">Submit</button></td>
            </tr>
          
        </form>
    </table>
	<?php } ?>
</div><!---end of container-->
</body>
</html>