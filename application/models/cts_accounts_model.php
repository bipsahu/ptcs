<?php 

Class cts_accounts_model extends CI_model 
{
	public function show_all_accounts_details()
	{	
	$this->db->group_by('claim_registration.claim_id');
	$this->db->select('*');
	$this->db->from('claim_registration');
	$this->db->join('surveyor_deportation', 'claim_registration.claim_id = surveyor_deportation.claim_id','left');
		$this->db->join('assigned_surveyors', 'claim_registration.claim_id = assigned_surveyors.claim_id','left');
		$this->db->join('claim_note_preparation', 'claim_registration.claim_id = claim_note_preparation.claim_id','left');
		$this->db->join('discharge_voucher_generation', 'claim_registration.claim_id = discharge_voucher_generation.claim_id','left');
		$this->db->join('legal', 'claim_registration.claim_id = legal.claim_id','left');$this->db->join('approve_recommend', 'claim_registration.claim_id = approve_recommend.claim_id','left');
		$this->db->join('insure', 'claim_registration.insure = insure.insure_name','left');
	$this->db->join('accounts', 'claim_registration.claim_id = accounts.claim_id');
	$this->db->order_by("claim_registration.claim_id", "desc");
	$this->db->where('status','5');
	//$this->db->or_where('status',1);
	$result = $this->db->get();
	
	//$this->db->where('status', 0);
	//$this->db->update('accounts',  array('status' => 1)); 
	
	return $result;
	}
	
	public function count_accounts_details()
	{	
	$this->db->group_by('claim_registration.claim_id');
	$this->db->select('*');
	$this->db->from('claim_registration');
	$this->db->where('status','5');
	//$this->db->or_where('status',1);
	$result = $this->db->get()->num_rows();
	
	//$this->db->where('status', 0);
	//$this->db->update('accounts',  array('status' => 1)); 
	
	return $result;
	}
	
	
	public function show_all_accounts_details_for_claim()
	{
		$result = $this->db->get('accounts');
		return $result->result_array();
	}
	public function cheque_ready($claim_id,$check_value){
		
		if($check_value == 1)
			$date = date('Y-m-d');
		else
			$date = NULL;
		$this->db->where('claim_id', $claim_id);
	$result = $this->db->update('accounts',array('cheque_ready'=>$check_value,'cheque_generated_date'=>$date));
	return $result;
		if($this->db->_error_number()==1062)
		{
		return 0;
		}
		else
		{
		return $result;
		}
	}
	
		
	public function insert_accounts_details($accounts_id)
	{
	if($this->input->post('cheque_ready')!=null)
	$data['collection_date'] = $this->input->post('collection_date');
	if($this->input->post('received_date')!=null)
	$data['received_date'] = $this->input->post('received_date');
	if($this->input->post('extraction_date')!=null)
	$data['extraction_date'] = $this->input->post('extraction_date');
	if($this->input->post('claim_submitted_by')!=null)
	$data['claim_submitted_by'] = $this->input->post('claim_submitted_by');
	if($this->input->post('flock_size')!=null)
	$data['flock_size'] = $this->input->post('flock_size');
	
	if($this->input->post('mortality')!=null)
	$data['mortality'] = $this->input->post('mortality');
	if($this->input->post('pcr_m_gene_result')!=null)
	$data['pcr_m_gene'] = $this->input->post('pcr_m_gene_result');
	if($this->input->post('pcr_h5_result')!=null)
	$data['pcr_h5'] = $this->input->post('pcr_h5_result');
	if($this->input->post('pcr_n1_result')!=null)
	$data['pcr_n1'] = $this->input->post('pcr_n1_result');
	if($this->input->post('pcr_n9_result')!=null)
	$data['pcr_n9'] = $this->input->post('pcr_n9_result');
	
	if($this->input->post('pcr_h9_result')!=null)
	$data['pcr_h9'] = $this->input->post('pcr_h9_result');
	
	if($this->input->post('pcr_h7_result')!=null)
	$data['pcr_h7'] = $this->input->post('pcr_h7_result');
	if($this->input->post('pcr_ibd_result')!=null)
	$data['pcr_ibd'] = $this->input->post('pcr_ibd_result');
	
	if($this->input->post('pcr_nd_result')!=null)
	$data['pcr_nd'] = $this->input->post('pcr_nd_result');
	
	if($this->input->post('others')!=null)
	$data['others'] = $this->input->post('others');
	
	if($this->input->post('remarks')!=null)
	$data['remarks'] = $this->input->post('remarks');
	
	if($this->input->post('ct_value')!=null)
	$data['ct_value'] = $this->input->post('ct_value');
	$data['tested_date'] = date('m/d/Y');
	/* $data = array(
	'collection_date' => $this->input->post('collection_date'),
	'received_date' => $this->input->post('received_date'),
	'extraction_date' => $this->input->post('extraction_date'),
	'claim_submitted_by' => $this->input->post('claim_submitted_by'),
	'flock_size' => $this->input->post('flock_size'),
	'mortality' => $this->input->post('mortality'),
	'pcr_m_gene' => $this->input->post('pcr_m_gene_result'),
	'pcr_h5' => $this->input->post('pcr_h5_result'),
	'pcr_n1' => $this->input->post('pcr_n1_result'),
	'pcr_n9' => $this->input->post('pcr_n9_result'),
	'pcr_h9' => $this->input->post('pcr_h9_result'),
	'pcr_h7' => $this->input->post('pcr_h7_result'),
	'pcr_ibd' => $this->input->post('pcr_ibd_result'),
	'pcr_nd' => $this->input->post('pcr_nd_result'),
	'others' => $this->input->post('others'),
	'remarks' => $this->input->post('remarks'),
	'ct_value' => $this->input->post('ct_value'),
	'tested_date' => date('m/d/Y'),
	); */
	$this->db->where('accounts_id', $accounts_id);
	$result = $this->db->update('accounts', $data);
	return $result;
		if($this->db->_error_number()==1062)
		{
		return 0;
		}
		else
		{
		return $result;
		}
	} 
	
	public function edit_accounts_by_id($accounts_id)
	{
	$data = array(
	'collection_date' => $this->input->post('collection_date'),
	'received_date' => $this->input->post('received_date'),
	'extraction_date' => $this->input->post('extraction_date'),
	'claim_submitted_by' => $this->input->post('claim_submitted_by'),
	'flock_size' => $this->input->post('flock_size'),
	'mortality' => $this->input->post('mortality'),
	'pcr_m_gene' => $this->input->post('pcr_m_gene_result'),
	'pcr_h5' => $this->input->post('pcr_h5_result'),
	'pcr_n1' => $this->input->post('pcr_n1_result'),
	'pcr_n9' => $this->input->post('pcr_n9_result'),
	'pcr_h9' => $this->input->post('pcr_h9_result'),
	'pcr_h7' => $this->input->post('pcr_h7_result'),
	'pcr_ibd' => $this->input->post('pcr_ibd_result'),
	'pcr_nd' => $this->input->post('pcr_nd_result'),
	'remarks' => $this->input->post('remarks'),
	'ct_value' => $this->input->post('ct_value'),
	'others' => $this->input->post('others')
	);
	$this->db->where('accounts_id', $accounts_id);
	$result = $this->db->update('accounts', $data);
	return $result;
	}
	
	public function get_details_of_accounts_by_id ($accounts_id)
	{
	$result = $this->db->get_where('accounts', array('accounts_id' => $accounts_id));
	return $result->result_array();
	}

	public function delete_accounts_by_id($accounts_id)
	{
	$result = $this->db->delete('accounts', array('accounts_id' => $accounts_id)); 
	return $result;
	}
	public function check_new_customer()
	{
		$this->db->group_by('claim_registration.claim_id');
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('accounts', 'claim_registration.claim_id = accounts.claim_id');
		$this->db->order_by("accounts_id", "desc"); 
		$this->db->where('status',0);
		$result = $this->db->get();
		$i = 0;
		$data_arr = array();
		foreach($result->result() as $row)
		{
			$data_arr[$i]["accounts_id"] = $row->accounts_id;
			$data_arr[$i]["labno"] = $row->labno;
			$data_arr[$i]["claim_id"] = $row->claim_id;
			$data_arr[$i]['customer_name'] = $row->customer_name;
			$data_arr[$i]['firm_name'] = $row->firm_name;
			$data_arr[$i]['animal'] = $row->animal;
			$data_arr[$i]['investigation_requested'] = $row->investigation_requested;
			$data_arr[$i]['age'] = $row->age;
			$data_arr[$i]['sex'] = $row->sex;
			$data_arr[$i]['claim_type'] = $row->claim_type;
			$data_arr[$i]['animal_type'] = $row->animal_type;
			$data_arr[$i]['no_of_claim'] = $row->no_of_claim;
			$data_arr[$i]['collection_date'] = $row->collection_date;
			$data_arr[$i]['received_date'] = $row->received_date;
			$data_arr[$i]['extraction_date'] = $row->extraction_date;
			$data_arr[$i]['claim_submitted_by'] = $row->claim_submitted_by;
			$data_arr[$i]['flock_size'] = $row->flock_size;
			$data_arr[$i]['mortality'] = $row->mortality;
			$this->db->where('claim_id',$row->claim_id);
			$this->db->update('accounts',array('status'=>1));
			$i++;
		}
		echo json_encode($data_arr);
	}
	public function set_accept_on($claim_id)
	{
	$this->db->where('claim_id',$claim_id);
	$result = $this->db->update('accounts',array('status' => 3));
	return $result;
	}
	public function search_pending_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('accounts', 'claim_registration.claim_id = accounts.claim_id');
		$this->db->order_by("accounts_id", "desc"); 
		$this->db->where('status',5);
		$this->db->where('publish_bit',0);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}
	public function set_publish_on($accounts_id)
	{
	$this->db->where('accounts_id',$accounts_id);
	$result = $this->db->update('accounts',array('publish_bit' => 1));
	return $result;
	}
	public function search_on_progress_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('accounts', 'claim_registration.claim_id = accounts.claim_id');
		$this->db->order_by("accounts_id", "desc"); 
		$this->db->where('status',3);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}
	public function set_completed_on($accounts_id)
	{
	$this->db->where('accounts_id',$accounts_id);
	$result = $this->db->update('accounts',array('status' => 4));
	return $result;
	}
	public function search_completed_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('accounts', 'claim_registration.claim_id = accounts.claim_id');
		$this->db->order_by("accounts_id", "desc"); 
		$this->db->where('status',4);
		$this->db->or_where('status',5);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}
	public function generate_report($claim_id)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('accounts', 'claim_registration.claim_id = accounts.claim_id');
		$this->db->join('customer','claim_registration.customer_id = customer.cid');
		$this->db->where('accounts.claim_id',$claim_id);
		/* $this->db->where('status',5); */
		$this->db->where('publish_bit',1);
		$result = $this->db->get();
		return $result;
	}
	/* public function generate_entry_report($customer_id)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('accounts', 'claim_registration.claim_id = accounts.claim_id');
		$this->db->join('customer','claim_registration.customer_id = customer.cid');
		$this->db->where('customer_id',$customer_id);
		$this->db->where('publish_bit',1);
		$result = $this->db->get();
		return $result;
	} */

}