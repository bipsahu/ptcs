<?php
// if(isset($result['data'][0]['project']))
// {
//     $project_id = $result['data'][0]['project'];
// }
// echo "<pre>";
// echo $project_id;die;
// print_r($projects->result_array());die;
// echo "hello";die;?>
<div class="container">
    <center><h1>Task Category Report </h1></center>
    <div id="print_btn" class="glyphicon glyphicon-print"></div>
    <div class="col-md-6 col-md-offset-3">
        <form class="form-horizontal">
            <div class="form-group">

                <select id="project" name="project" class="form-control">
                    <option value=" ">Select Project</option>
                    <?php

                    if (isset($projects)):
                        foreach ($projects->result_array() as $project) {
//                             if($project_id != '')
//                             {
// // die;
//                                 if($project['project_id'] == $project_id)
//                                 {
//                                     $stat = "selected";
//                                 }
//                                 else
//                                 $stat = '';
//                             }
//                             else
//                                 $stat = '';

                            echo "<option value='" . $project['project_id'] . "'>" . $project['project_name'] . "</option>";
                        }
                    endif;
                    ?>
                </select>
            </div>
        </form>
    </div>
    <table class="table table-bordered" id="taskDatatable">
        <caption></caption>
        <br/><br/>
        <strong id="totalTask" style="clear: both;float:left;font-weight: bold;font-size: 21px"></strong>
        <strong id="totalTaskwithtask" style="clear: both;float:left;font-weight: bold;font-size: 21px"></strong>
        <strong id="totalcomplete" style="clear: both;float:left;font-weight: bold;font-size: 21px;margin-bottom: 25px;"></strong>
        <br/><br/>
        <thead>
        <tr id="new_customer_table_row">
            <th>S.N</th>
            <th>Task Category</th>
            <th>Weightage</th>
            <!-- <th>Cost Allocated (NRs.)</th> -->
            <th>% Complete</th>
            <th>% Complete(Based on weightage)</th>
            <th>Task Details</th>
            <th>Status</th>
        </tr>
        </thead>
        <!--  -->
    </table>
</div>
<script type="text/javascript">
    $(document).ready(function () {
             // console.log(response);
// if(response.pass_data === '') {
//     alert('do nothing');
// }
// else {
//     alert('alert box');
    
// }
        $("#project").change(function () {

            //remove existing datatables
            // $('.tablebody').remove();
            $("#taskDatatable").DataTable().destroy();
            var project = $(this).val();
            // alert(project);
            $("#taskDatatable").find("caption").text("Project Name : " + $(this).find("option:selected").text());
            $("#taskDatatable").DataTable({
                "ajax": {
                    "url": '<?php echo site_url("cts_report_controller/task_category_report");?>',
                    "type": "POST",
                    "data": {"project": project}

                },
                "columns": [
                    {"data": "serial_no"},
                    {"data": "tast_cat_name"},
                    {"data": "task_category_weight"},
                    // {"data": "cost_allocated"},
                    {"data": "cat_com"},
                    {"data": "actual_cat_complete"},
                    {"data": "task_detail"},
                    {"data": "status"},

                ],

                "fnInitComplete": function(data) {
                    if(data.json.pass_data != '')
                    {
                        var display = [];
                        $(data.json.pass_data).each(function(index,value){
                            // console.log(value);
                            display.push(value.name);
                            // var display = value.name;

                        });
                        var displays = display.join(" and ");
                    console.log(displays);

                    alert('task is not assigned to '+ displays);
                    }
                    // alert('helo');
                    // if()
                    $("#totalTask").text("Total Task Category In This Project : " + data.json.tc_number);
                    $("#totalTaskwithtask").text("Total Task Category With Task : " + data.json.data.length);
                    $("#totalcomplete").text("Total Percentage Completed : " + data.json.overall_complete);
                }

            });

        });


        $('#print_btn').click(function () {
            $("#img_logo_anchor").attr('href', '#');
            window.print();
            $('#print_btn').show();
            $("#img_logo_anchor").attr('href', '<?php echo base_url();?>');
            $("#cts_logo_anchor").show();
        });
    });
</script>
<style type="text/css">
    input[type="search"] {
        margin-right: 35px;
    }

    #print_btn {
        position: absolute;
        margin-top: 65px;
        right: 20px;
    }
</style>
<style type="text/css" media="print">
    @page {
        size: auto;
        margin: 0mm;
    }

    body {
        font-weight: bold !important;
        font: Arial !important;
    }

    #cts_logo_anchor {
        display: none;
    }

    .select-report {
        display: none;
    }

    #head-icons {
        display: none;
    }

    #new_customer_tab_length {
        display: none;
    }

    #new_customer_tab_filter {
        display: none;
    }

    #new_customer_tab_info {
        display: none;
    }

    #new_customer_tab_paginate {
        display: none;
    }

    .footer-line {
        display: none;
    }

    #footer {
        display: none;
    }

    #print_btn {
        display: none;
    }
    #project,#task{
        display: none;
    }
    .main-head,.datetime{
        text-align: center;
    }
    table{
        width: 100% !important;
    }
    #taskDatatable_length,#taskDatatable_filter,#taskDatatable_info,#taskDatatable_paginate{
        display: none;
        display: none;
    }
    .main-head {
        font-size: 24px;
        width: 600px;
    }

    #cts_logo_anchor img {
        width: 140px;
    }

    .container h1 {
        font-size: 16px;
        margin-top: 0px;
    }
</style>
</body>
</html>