<?php
if(isset($roles_info))
{
$i=0;
$role = array();
foreach($roles_info->result_array() as $key=>$value)
{
$role[] = $value;
$module[] = $role[$i]['module_name'];
$i++;
}
}
?>
<div class="container">
	<div id="img-icon">
      <img src="<?php echo base_url();?>resource/images/department-icon.jpg" align="left"/>
    </div>
    <div id="breadcumb-text">
      <ol class="breadcrumb">
        <li><strong>Lab</strong></li>&nbsp;&nbsp;
        <li class="vertical-divider"></li>
        <li><a href="<?php echo base_url();?>index.php/lims_controller/add_department_form">Add New Lab</a></li>&nbsp;&nbsp;
        <li class="vertical-divider"></li>
        <li class="active">List Lab</li>
      </ol>
    </div>
    <hr>
<?php
if(!empty($result_indicator))
{
	if($result_indicator == 1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Deletion Successful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Deletion Failed!</b></div>";
}
?>
<table class="table table-striped" id="department_table">
<thead><th>Lab Name</th><th>Parent</th>
<?php
if(strtolower($this->session->userdata('user_position')) == "admin")
{
echo "<th>Edit</th>";
} 
elseif($role[2]['edit']){?>
<th>Edit</th>
<?php  } ?>
<?php if(strtolower($this->session->userdata('user_position')) == "admin")
{
echo "<th>Delete</th>";
} 
elseif($role[2]['delete']){?>
<th>Delete</th>
<?php  } ?>
</thead>
<?php
foreach($all_department_with_parent as $row){
?>
<tr>
<td><?php echo $row['child_name'];?></td>	
<td><?php echo $row['parent_name'];?></td>
<?php 
if(strtolower($this->session->userdata('user_position')) == "admin")
{
echo "<td><a href='".base_url()."index.php/lims_controller/edit_the_department_form/".$row['child_dep_id']."'><span class='glyphicon glyphicon-edit'></span></a></td>";
}
elseif($role[2]['edit'])
{
echo "<td><a href='".base_url()."index.php/lims_controller/edit_the_department_form/".$row['child_dep_id']."'><span class='glyphicon glyphicon-edit'></span></a></td>";
}
if(strtolower($this->session->userdata('user_position')) == "admin")
{
echo "<td><a href='".base_url()."index.php/lims_controller/delete_the_department/".$row['child_dep_id']."'><span class='glyphicon glyphicon-trash'></span></a></td>";
}
elseif($role[2]['delete'])
{
echo "<td><a href='".base_url()."index.php/lims_controller/delete_the_department/".$row['child_dep_id']."'><span class='glyphicon glyphicon-trash'></span></a></td>";
}
?>
</tr>
<?php
}
?> 

</table>
</div>
</body>
</html>