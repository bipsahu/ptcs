<?php 
$role= array();
$modules= array();
if(isset($roles_info))
{
$i=0;
foreach($roles_info->result_array() as $key=>$value)
{
$role[] = $value;
$module[] = $role[$i]['module_name'];
}
/* print_r($role);
echo "<br>";
print_r($module); */
}
?>

<div class="container table-small">
    
    <div id="breadcumb-text">
      <ul class="nav nav-tabs">
        <li class="tab_length"><a><strong>User</strong></a></li>
        <!-- <li class="vertical-divider"></li> -->
        <li class="active tab_length"><a href="">Add User</a></li>
        <!-- <li class="vertical-divider"></li> -->
    		<?php 
    		if(strtolower($this->session->userdata('user_position')) == "admin"  || $role[1]['edit'] ==1 || $role[1]['delete'] == 1){
    		?>
        <li class="tab_length"><a href="<?php echo base_url();?>index.php/cts_controller/show_all_user">List User</a></li>
		  <?php } ?>
      </ul>
    </div>
    <hr>
	  <?php

	if(isset($result))
	{
	if($result==1)
	{
	echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Successfully Registered!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Registration Failed!</b></div>";
	} 
  ?>


     
  <form method="post" role="form" enctype="multipart/form-data" id="user_add_form" action="<?php echo base_url();?>index.php/cts_controller/add_new_user">  
    <table class="table" id="table">   
			<tr>
        <td class="first_td"><label>E-mail</label></td>
        <td><input type="email" class="form-control" name="user_email" id="user_email" required></td>
			</tr>
			<tr id="error_msg_tr">
        <td></td><td><div class="error_msg"></div></td>
      </tr>
			<tr>
			<td></td><td>Email will be used as User Name</td>
			</tr>
			
            <tr>
              <td class="first_td"><label>Name</label></td>
              <td><input type="text" class="form-control" name="user_name" required></td>

            </tr>
		
          <tr>
              <td class="first_td"><label>Password</label></td>
              <td><input type="password" class="form-control" name="user_password" id="user_password" required></td>
            </tr>
			   <tr>
              <td class="first_td"><label>Confirm Password</label></td>
              <td><input type="password" class="form-control" name="confirm_password" id="confirm_password" required/></td>
            </tr>
            <tr>
              <td class="first_td"><label>Address</label></td>
              <td><input type="text" class="form-control" name="user_address"></td>
            </tr>
            <tr>
              <td class="first_td"><label>Position</label></td>
              <td> 
                <select name="user_position" class="form-control">
             		 <option>Select Position</option>
        						<?php 
        						if(isset($position_name))
        						foreach($position_name->result() as $row){
        							?>
        					  <option value="<?php echo $row->position_name ?>"> <?php echo $row->position_name;?> </option>
        						  <?php 
        							}
        							?>
        					</select>
        			</td>
            </tr>
            <tr>
              <td class="first_td"><label>Salary*</label></td>
              <td>
                <input type="number" id="grosssalary" class="form-control" name="grosssalary" placeholder="Gross Salary"  style="width:150px;">
                <!-- <br>OR -->
                <input type="text" id="hourlysalary" class="form-control" name="hourly_salary"  placeholder="Hourly Salary" style="width:150px;" required>
              </td>
            </tr>
            <tr>
              <td class="first_td"><label>Mobile No.</label></td>
              <td><input type="text" class="form-control" name="user_mobile"></td>
            </tr>
            <tr>
              <td class="first_td"><label>Home No.</label></td>
              <td><input type="text" class="form-control" name="user_phone_home"></td>
            </tr>

            <tr class="first_td">
              <td><label>Office No.</label></td>
              <td><input type="text" class="form-control" name="user_phone_office"></td>
            </tr>
			<tr>
			<td><label>Select the Department</label></td>
			<td><?php    
			if(isset($department_list))
			foreach( $department_list as $key=>$row )
			{
			echo "<input type='checkbox' id='dep_chk_".$key."' name='dep_chk_group[]' value='".$row->dep_id."'/> ".$row->dep_name."&nbsp;&nbsp;";
			}
			?></td>
			</tr>
             <tr id="approve_user_level">
              <td><label>User level: </label></td>
              <td><input type="checkbox" name="approve_user_level[0]"> level 1 (can recommend only)<br>
             	<input type="checkbox" name="approve_user_level[1]"> level 2 (can approve upto 1 lakh and recommend above)<br>
             	<input type="checkbox" name="approve_user_level[2]"> level 3 (can approve upto 2 lakh and recommend above)<br>
              	<input type="checkbox" name="approve_user_level[3]"> level 4 (can approve upto 5 lakh and recommend above)</td>
           </tr>            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary" id="submit" >Submit</button></td>
            </tr>
           </table>
        </form>
   
	</div>
</div><!---end of container-->

<div id="dialog-confirm" title="Password Donot Match">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>The Two Password Donot Match</p>
</div>
</body>
<script type="text/javascript">

$(document).ready(function(){
	$("#approve_user_level").hide();
	$("#dep_chk_3").click(function(){
	
		if($(this).is(":checked")){
			$("#approve_user_level").show();
			return true;
			}
		$("#approve_user_level").hide();
	});

  $("#grosssalary").keyup( function(){

    var gross = $('#grosssalary').val();
    var hour = gross/(30*9);
    // alert(hour);
    $('#hourlysalary').val(hour);
  });
});
</script>
</html>