<div class="container">
	<h3>Task Update Form</h3>
    <h4><?php echo $project_name.' | '.$task_name;?></h4>
    <br>
    <?php
    if (isset($success)) 
    {
        if ($success == "ok") 
        {
            echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Successfully Updated!</b></div>";
        } 
        else
            echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Update Failed!</b></div>";
    }
    ?>

	<form method="post" role="form" autocomplete="off" enctype="multipart/form-data"
          action="<?php echo base_url() . 'index.php/cts_task_controller/update_task_completion/' . $task_id.'/hour_status'; ?>"
          id="">
	    <div class="col-sm-12" style="margin-bottom: 40px;">
		    <table class="table table-striped">
	        	<thead>
		            <tr style="height: 55px;">
		                <td><strong><em>Users Selected</em></strong></td>
		                <td><strong><em>Assign Hours</em></strong></td>
		                <?php if($this->session->userdata('user_position') == 'Admin')
        				{?>
        

		                <td><strong><em>Salary Per Hrs</em></strong></td>
		                <td><strong><em>Estimated Cost</em></strong></td>
        				<?php } ?>
		                <td><strong><em>Completed Hours(hrs)</em></strong></td>
		                <?php if($this->session->userdata('user_position') == 'Admin')
        				{?>
		                <td><strong><em>Cost To Company</em></strong></td>
		                <?php } ?>
		            </tr>
		        </thead>   
	            <tbody style="text-align:center;">
		            <?php $total_estimated_cost = 0;
		            $total_completed_hour = 0;
		            $total_cost_to_company = 0;
		             foreach ($member as $key => $value) {
		            	# code...
		            ?>
		            <tr>
		                <td><strong><em><?php echo $value['member_name']; ?></em></strong></td>
		                <td><strong><em><?php echo $value['assigned_hour']; ?></em></strong></td>
		                <?php if($this->session->userdata('user_position') == 'Admin')
        				{?>
		                <td><strong><em><?php echo $value['hourly_salary']; ?></em></strong></td>
		                <td><strong><em><?php echo $value['assigned_hour'] * $value['hourly_salary']; ?></em></strong></td>
		                <?php } ?>
		                <?php $total_estimated_cost = $total_estimated_cost + $value['assigned_hour'] * $value['hourly_salary']; ?>
		                <td>
		                	<strong>
		                		<em>
		                			<input class="completed_hours" style="width:100px;" type="number" min="0" salary = <?php echo $value['hourly_salary']; ?> count = <?php echo $key;?> name="completed_hour[<?php echo $key;?>]" value="<?php echo $value['completed_hour']; ?>">
		                		</em>
		                	</strong>
		                </td>
		                <?php if($this->session->userdata('user_position') == 'Admin')
        				{?>
		                <td>
		                	<strong><em>
		                	<input id="ctc" count1 = <?php echo $key;?> name="total_cost[<?php echo $key;?>]" readonly value="<?php echo $value['hourly_salary'] * $value['completed_hour']; ?>">
		                	</em></strong>
		                </td>
		                <?php 
		            }
		                $total_completed_hour = $total_completed_hour + $value['completed_hour'];
		                $total_cost_to_company = $total_cost_to_company + $value['hourly_salary'] * $value['completed_hour'];
		                ?>
	            	<?php }?>
		            </tr>
		            <?php if($this->session->userdata('user_position') == 'Admin')
        				{?>
		            <tr>
		            	<td><h4>Total</h4></td>
		            	<td></td>
		            	<td></td>
		            	<td id="total_estimated_cost"><?php echo $total_estimated_cost; ?></td>
		            	<td id = 'total_completed_hour'> <?php echo $total_completed_hour; ?></td>
		            	<td id = 'total_cost_to_company'> <?php echo $total_cost_to_company; ?></td>
		            	
		            </tr>
		            <tr>
		            	<td><h4>Total Surplus/Deficit</h4></td>
		            	<td></td>
		            	<td></td>
		            	<td></td>
		            	<td></td>
		            	<td id="surplus_deficit"><?php echo $total_estimated_cost - $total_cost_to_company; ?></td>
		            </tr>
		            <?php } ?>
	            </tbody>
	        </table>
        <div class="" style="clear: both;">
        	
	    <table style="float:right;">
	        <tr>
	            <td colspan="2">
	                <button type="submit" class="btn btn-primary" id="submit">Update</button>
	            </td>
	            <td></td>
	        </tr>
		</table>
        </div>
        </div>
        <div style="clear:both;"></div>
    </form>

    <div class="col-sm-6">
	    <form method="post" role="form" autocomplete="off" enctype="multipart/form-data"
	          action="<?php echo base_url() . 'index.php/cts_task_controller/update_task_completion/' . $task_id.'/task_status'; ?>"
	          id="">
		        <table class="table table-striped">
		        	<thead>
				        <tr class="">
				            <td><strong><em>Indicators Selected</em></strong></td>
				            <td><strong><em>Assign Weightage(%)</em></strong></td>
				            <td><strong><em>Achievement 100% per <br/>indicator(%)</em></strong>
				            </td>
				        </tr>
		        	</thead>
		        	<tbody>
				        <?php $total_weightage = 0; $total_achieved = 0; 
				        foreach($task_detail as $key => $task_details){ ?>
				        <tr>
				        	<td>
				        		<?php echo $task_details['indicator_id']; ?></td>
				        	<td>
				        		<?php $total_weightage = $total_weightage + $task_details['weightage'];?>
				        		<input type="number" value="<?php echo $task_details['weightage'];?>" readonly>
				        	</td>
				        	<td>
					        	<?php $total_achieved = $total_achieved+($task_details['achieved'] * $task_details['weightage']/100);?>
					        	<input type="number" name="achieved[<?php echo $key ?>]" value="<?php echo $task_details['achieved'];?>" min="0" max="100" style="width:170px;">
				        	</td>
				        </tr>
						<?php } ?>

				        <tr class="">
				            <td><em><strong>Total: </strong></em></td>
				            <td>
				            	<?php echo $total_weightage.'%'; ?>
				            </td>
				            <td ><?php echo $total_achieved.'%'; ?></td>
				        </tr>
			        </tbody>
			    </table>

		   <table style="float:right;">
		        <tr>
		            <td colspan="2">
		                <button type="submit" class="btn btn-primary" id="submit">Update</button>
		            </td>
		            <td></td>
		        </tr>
			</table>
	        <!-- <div style="clear:both;"></div> -->
	    </form>
	</div>
	    <!-- <table class="table-bordered alert alert-warning" style="float: right; margin: -120px 171px 0px 0px;"> -->

    <div class="col-sm-6" style="float:right;">
    	<form method="post" role="form" autocomplete="off" enctype="multipart/form-data"
          action="<?php echo base_url() . 'index.php/cts_task_controller/insert_comment_task/' . $task_id; ?>"
          id="">
    	<!-- <table class="table-bordered alert alert-warning" style="margin-top: 50px;"> -->
	    	<table class="table table-striped"  id="department_table" >
	        	<thead>
			        <tr style="height: 55px;">
			            <td><strong><em>Date</em></strong></td>
			            <td><strong><em>Description</em></strong></td>
			           
			        </tr>
			    </thead>
	        	<tbody class="comment">
			        <?php 
			        if(!empty($comment_detail))
			        {
				        foreach ($comment_detail as $key => $comment_details) 
				        {?>
					        
					            <tr><td><em><strong><?php echo $comment_details['date']; ?> </strong></em></td>
					            <td><textarea readonly="" style="width:550px; height: 100px;"><?php echo $comment_details['description']; ?></textarea></td>
					        </tr>
				        <?php 
				     	} 
			     	}
			        else
			        {?>
				     	<tr>
				            <tr><td><em><strong><?php echo date('Y-m-d'); ?> </strong></em></td>
				            <td><textarea name = 'textarea[]'  style="width:550px; height: 100px;"></textarea></td>
				        </tr>
					<?php 
			         }
			         ?>	       
		        </tbody>
		    </table>
		    <table style="float:right;">
		        <tr>
		            <td colspan="2">
		                <text class="btn btn-primary" id="date">Add Comment</text>
		            </td>
		            <td style="margin-left:10px;"><input type="submit" class="btn btn-primary" style="margin-left:10px;"></td>
		        </tr>
			</table>
		</form>
    </div>
</div><!---end of container-->
<script>
    $(document).ready(function () {

    $('#date').click(function(){
// alert('hello');
       $('.comment').prepend("<tr><td><em><strong><?php echo date('Y-m-d'); ?> </strong></em></td><td><textarea name=\"textarea[]\"  style=\"width:550px; height: 100px;\"></textarea></td></tr>");
    });

    $('.completed_hours').on('keyup change',function(){
    	var total_completed_hour = 0;
    	var total_cost_to_company = 0;
    	var salary = 0;

	    $('.completed_hours').each(function(index,value){
	    	// console.log(index);
	    	var values = $(this).val();
	    	var name = $(this).attr('count');
	    	salary = $(this).attr('salary');
	    	var total_salary = values * salary;
	    	total_completed_hour = parseInt(total_completed_hour) + parseInt(values);
	    	total_cost_to_company = parseInt(total_cost_to_company) + parseInt(total_salary);
	    	// console.log(total_salary);
	    	// $('td[total_cost[name]]').val(values);
	    	$('[count1="' + name + '"]').val(total_salary);
	    	// $('#ctc').val(values);
	    	// console.log(values);
	    	// console.log($(this).val());
	    });

	    	var total_estimated_cost = $('#total_estimated_cost').text();
	    	// alert(total_estimated_cost);
	    	var surplus_deficit = parseInt(total_estimated_cost) - total_cost_to_company;
	    	$('#total_completed_hour').text(total_completed_hour);
	    	$('#total_cost_to_company').text(total_cost_to_company);
	    	$('#surplus_deficit').text(surplus_deficit);
    });

});


</script>
<style>
</style>
</body>
</html>
