<?php

/*
 * model class that manipulates indicator table
 * */

class cts_task_category_model extends CI_Model
{ 

    public function insert_task_category()
    {
        $data = array(
            'name' => $this->input->post('task_category')
        );
        // print_r($data);die;
        $result = $this->db->get_where('task_category', array('name' => $data['name']));
        if ($result->num_rows == 1) {
            return false;
        } else {
            $result = $this->db->insert('task_category', $data);
            return $result;
        }
    }

    public function getTaskCategory()
    {
        $result = $this->db->get('task_category');
        return $result->result_array();
    }

    public function get_task_category_by_id($taskid)
    {
        $result = $this->db->get_where('task_category', array('t_category_id' => $taskid));
        return $result->result_array();
    }

    public function editTaskCategory($taskId)
    {
            $data = array
            (
                'name' => $this->input->post('task_category'),
            );
            // echo $taskId;
        // print_r($data);die;


        $this->db->where('t_category_id', $taskId);
        $result = $this->db->update('task_category', $data);
        // $this->db->last_query();die;
        return $result;

    }

    public function delete_task_category_by_id($taskCategoryId)
    {
        // echo $taskCategoryId;die;
        $result = $this->db->delete('task_category', array('t_category_id' => $taskCategoryId));
        return $result;
    }

    /*
     * get task category
     * @params:none
     * @return: list of task category
     * */
    public function get_all_task_category()
    {
        $this->db->select('*');
        $this->db->from('task_category');
        // $this->db->join('indicators_type','indicators.indicator_type = indicators_type.id ');
        $result = $this->db->get();
        return $result->result_array();
    }

}