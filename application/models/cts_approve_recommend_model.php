<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class cts_approve_recommend_model extends CI_model
{	
	public function __construct(){
		parent::__construct();
		
		$this->load->library('email');
		}
		
	public function insert_approve_recommend_details($claim_id)
	{
	
		if(isset($claim_id)&&$claim_id!=NULL){
		if($this->input->post('approved_amount'))
			$data['approved_amount'] = $this->input->post('approved_amount');
			if($this->input->post('approve_recommend')!= null)
			{
				
				if($this->input->post('approve_remarks')){
					$data['ar_remarks']= $this->input->post('approve_remarks');
				}
				if($this->input->post('approve_recommend')=='1'){
				//transfer to other users
				
					$data['recommend_to'] = $this->input->post('dv_recommend');
					if($data['recommend_to']&&$data['recommend_to']!=NULL){
						
						$user_level_check = $this->cts_user_model->get_user_details($data['recommend_to']);
						$allowed_levels = json_decode($user_level_check[0]->user_level,'force_json_object');
						
						foreach($allowed_levels as $key=>$allowed){
								
								$data['ar_status'] = strval($key);
							}
						//send message
						
					if($this->input->post('approve_message')!=NULL){
							$asssignment_date = date('Y-m-d');
							$this->db->insert('assignment_table',array('assigned_date'=>$asssignment_date,'send_from'=>$this->session->userdata('user_id'),'send_to'=>$data['recommend_to'],'message'=>$this->input->post('approve_message'),'status'=>"1","check_status"=>"0"));
		}
						}
					}
					
				$data['ar_decision'] = $this->input->post('approve_recommend');
				
			}
		
			//$data['dv_received'] = $cn_status;
			
			 $this->db->select('*'); 
			 $this->db->where('claim_id',$claim_id);
			 $cn_count = $this->db->get('approve_recommend')->num_rows();
			 if($cn_count>=1){
			 
			 	 $this->db->where('claim_id',$claim_id);
				 $result = $this->db->update('approve_recommend', $data);
			 
			 }
			 else{
				 $data['claim_id'] = $claim_id;
				 $this->db->where('claim_id',$claim_id);
				$result = $this->db->insert('approve_recommend', $data);
			}
				if($this->db->_error_number()==1062)
				{
				return 0;
				}
				else
				{
				return $result;
				}
				
			}
	}
	
	
	public function get_all_approval_amount(){
		$this->db->select('*');
		$result = $this->db->get('approve_recommend_user_level')->result_array();
		return $result;
	}
	
	public function cheque_received($claim_id,$cn_status)
	{
		if($this->input->post('cheque_received')!= null)
		{
		$data['cheque_received'] = $this->input->post('cheque_received');
		}
		$data['cheque_received'] = $cn_status;
		
		 $this->db->select('*'); 
		 $this->db->where('claim_id',$claim_id);
		 $cn_count = $this->db->get('approve_recommend')->num_rows();
		 if($cn_count>=1){
		 $data['claim_id'] = $claim_id;
		  $this->db->where('claim_id',$claim_id);
		 $result = $this->db->update('approve_recommend', $data);
		 }
		 else
		$result = $this->db->insert('approve_recommend', $data);
			if($this->db->_error_number()==1062)
			{
			return 0;
			}
			else
			{
			return $result;
			}
	}
	
	public function insure_send_email(){
			
			$email_to = trim($this->input->post('email_address'));
			$subject = $this->input->post('email_subject');
			$body = $this->input->post('email_body');
			
			$this->email->from('info@shikharinsurance.com', 'Shikhar Insurance');
			$this->email->to($email_to); 
			//$this->email->cc('another@another-example.com'); 
			//$this->email->bcc('them@their-example.com'); 
			
			$this->email->subject($subject);
			$this->email->message($body);	
			
			$this->email->send();
			return '1';
	}
	
	public function show_all_approve_recommend_details($user_level_check)
	{
	if(isset($user_level_check) && $user_level_check!=NULL){
		$this->db->group_by('claim_registration.claim_id');
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('surveyor_deportation', 'claim_registration.claim_id = surveyor_deportation.claim_id','left');
		$this->db->join('assigned_surveyors', 'claim_registration.claim_id = assigned_surveyors.claim_id','left');
		$this->db->join('claim_note_preparation', 'claim_registration.claim_id = claim_note_preparation.claim_id','left');
		$this->db->join('legal', 'claim_registration.claim_id = legal.claim_id','left');
		$this->db->join('approve_recommend', 'claim_registration.claim_id = approve_recommend.claim_id','left');
		$this->db->join('insure', 'claim_registration.insure = insure.insure_name','left');
		$this->db->order_by("claim_registration.claim_id", "desc"); 
		$this->db->where('claim_registration.status','3');
			
			$allowed_levels = json_decode($user_level_check[0]->user_level,'force_json_object');
			$count = 1;
			if(is_array($allowed_levels))
			foreach($allowed_levels as $key=>$allowed){
			if($count==1)
				$this->db->where('approve_recommend.ar_status',strval($key));
			if($count>1)
				$this->db->or_where('approve_recommend.ar_status',strval($key));
			$count++;
			}
		
		$result = $this->db->get();
		//print_r($this->db->last_query());
		//$this->db->where('status', 1);
		//$this->db->update('claim_registration',  array('status' => 2)); 
		return $result;
		}
		else 
		return false;
	}
	
	public function count_approve_recommend_details()
	{
		$this->db->group_by('claim_registration.claim_id');
		$this->db->select('*');
		$this->db->from('claim_registration');
		
		$this->db->where('status','3');
		//$this->db->or_where('status',1);
		$result = $this->db->get()->num_rows();
		//$this->db->where('status', 1);
		//$this->db->update('claim_registration',  array('status' => 2)); 
		return $result;
	}
	
	public function count_ar_details($user_id)
	{
		
		
		//$this->db->or_where('status',1);
		$result = $this->db->get_where('user',array('user_id'=>$user_id))->result_array();
		
		$check_level = json_decode($result[0]['user_level']);
		if($check_level)
		$allowed_levels = get_object_vars($check_level);
		else
		 $allowed_levels = NULL;
		if(count($allowed_levels)==1){
			foreach($allowed_levels as $key=>$allowed){
			
				$this->db->where('ar_status',strval($key));
			}
		}
		elseif(count($allowed_levels)>1){
			foreach($allowed_levels as $key=>$allowed){
				$this->db->where('ar_status',$key);
			}
		}
		else{
			return '0';
		}
		$final_count = $this->db->get('approve_recommend')->num_rows();
		//$this->db->where('status', 1);
		//$this->db->update('claim_registration',  array('status' => 2)); 
		return $final_count;
	}
	
	public function show_all_approve_recommend_details_for_claim()
	{
		$result = $this->db->get('approve_recommend');
		return $result->result_array();
	
	}
	
	public function get_details_of_approve_recommend_by_id ($approve_recommend_id)
	{
	$result = $this->db->get_where('approve_recommend', array('approve_recommend_id' => $approve_recommend_id));
	return $result->result_array();
	}
	
	public function delete_approve_recommend_by_id($approve_recommend_id)
	{
	$result = $this->db->delete('approve_recommend', array('approve_recommend_id' => $approve_recommend_id)); 
	return $result;
	}
	
	public function edit_approve_recommend_by_id($approve_recommend_id)
	{
	
	$data = array(
	'egg_inoculation' => $this->input->post('egg_inoculation'),
	'plate_aggluination' => $this->input->post('pa_result'),
	'rapid_anitgen_test' => $this->input->post('rapid_anitgen_test'),
	'negri_body' =>$this->input->post('negri_body'),
	'penside_test' =>$this->input->post('penside_test'),
	'ha' => $this->input->post('ha'),
	'hi' => $this->input->post('hi'),
	'cft' => $this->input->post('cft_result'),
	'agid' => $this->input->post('agid_result'),
	'elisa' => $this->input->post('elisa_result'),
	'confirmative_diagnosis' => $this->input->post('cd_result'),
	'fat' => $this->input->post('fat')
	);
	$this->db->where('approve_recommend_id',$approve_recommend_id);
	$result = $this->db->update('approve_recommend', $data);
	return $result;
	}

	public function check_new_customer()
	{
		$this->db->group_by('claim_registration.claim_id');
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('approve_recommend', 'claim_registration.claim_id = approve_recommend.claim_id');
		$this->db->order_by("approve_recommend_id", "desc"); 
		$this->db->where('status',0);
		$result = $this->db->get();
		$i = 0;
		$data_arr = array();
		foreach($result->result() as $row)
		{
			$data_arr[$i]["approve_recommend_id"] = $row->approve_recommend_id;
			$data_arr[$i]["labno"] = $row->labno;
			$data_arr[$i]["claim_id"] = $row->claim_id;
			$data_arr[$i]['customer_name'] = $row->customer_name;
			$data_arr[$i]['firm_name'] = $row->firm_name;
			$data_arr[$i]['animal'] = $row->animal;
			$data_arr[$i]['claim_type'] = $row->claim_type;
			$data_arr[$i]['no_of_claim'] = $row->no_of_claim;
			$data_arr[$i]['age'] = $row->age;
			$data_arr[$i]['sex'] = $row->sex;
			$data_arr[$i]['animal_type'] = $row->animal_type;
			$data_arr[$i]['species_type'] = $row->species_type;
			$data_arr[$i]['investigation_requested'] = $row->investigation_requested;
			$data_arr[$i]['egg_inoculation'] = $row->egg_inoculation;
			$data_arr[$i]['pa_result'] = $row->plate_aggluination;
			$data_arr[$i]['ha'] = $row->ha;
			$data_arr[$i]['hi'] = $row->hi;
			$data_arr[$i]['cft_result'] = $row->cft;
			$data_arr[$i]['agid_result'] = $row->agid;
			$data_arr[$i]['elisa_result'] = $row->elisa;
			$data_arr[$i]['cd_result'] = $row->confirmative_diagnosis;
			$this->db->where('claim_id',$row->claim_id);
			$this->db->update('approve_recommend',array('status'=>1));
			$i++;
		}
		echo json_encode($data_arr);
	}

	public function set_accept_on($claim_id)
	{
	$this->db->where('claim_id',$claim_id);
	$result = $this->db->update('approve_recommend',array('status' => 3));
	return $result;
	}	
	public function search_pending_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('approve_recommend', 'claim_registration.claim_id = approve_recommend.claim_id');
		$this->db->order_by("approve_recommend_id", "desc"); 
		$this->db->where('status',5);
		$this->db->where('publish_bit',0);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}
	public function set_publish_on($approve_recommend_id)
	{
	$this->db->where('approve_recommend_id',$approve_recommend_id);
	$result = $this->db->update('approve_recommend',array('publish_bit' => 1));
	return $result;
	}
	public function search_on_progress_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('approve_recommend', 'claim_registration.claim_id = approve_recommend.claim_id');
		$this->db->order_by("approve_recommend_id", "desc"); 
		$this->db->where('status',3);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}
	public function set_completed_on($approve_recommend_id)
	{
	$this->db->where('approve_recommend_id',$approve_recommend_id);
	$result = $this->db->update('approve_recommend',array('status' => 4));
	return $result;
	}
	public function search_completed_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('approve_recommend', 'claim_registration.claim_id = approve_recommend.claim_id');
		$this->db->order_by("approve_recommend_id", "desc"); 
		$this->db->where('status',4);
		$this->db->or_where('status',5);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}

	public function generate_report($approve_recommend_id)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('approve_recommend', 'claim_registration.claim_id = approve_recommend.claim_id');
		$this->db->join('customer','claim_registration.customer_id = customer.cid');
		$this->db->where('approve_recommend_id',$approve_recommend_id);
		$result = $this->db->get();
		return $result;
	}
}