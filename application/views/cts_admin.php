<div class="container">
	<br>
	<nav class="navbar navbar-default" role="navigation">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">Menu</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href="#">Vio</a></li>
	        <li><a href="#">Sero</a></li>
	        <li><a href="#">Bio</a></li>  
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
		
	<br>
	<aside id="sidebar">
		<li><a href="#">Information</a></li>
		<li><a href="#">About</a></li>
	</aside>

	<ul class="products">
		<li>
  			<a href="#">
      			<img src="resource/images/a.jpg">
       			<center><h4>Vio</h4></center>
   			</a>
  		</li>
 		<li>
   			<a href="#">
       			<img src="resource/images/a.jpg">
       			<center><h4>Sero</h4></center>
   			</a>
   		</li>
   		<li>
		    <a href="#">
          		<img src="resource/images/a.jpg">
		        <center><h4>Bio</h4></center>
		    </a>
   		</li><!-- more list items -->
	</ul>
</div><!--end of container-->