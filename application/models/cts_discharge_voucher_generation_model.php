<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class cts_discharge_voucher_generation_model extends CI_model
{	

	public function __construct(){
		parent::__construct();
		
		$this->load->library('email');
		
	}
	public function insert_discharge_voucher_generation_details($claim_id,$cn_status)
	{
		if($this->input->post('dv_received')!= null)
		{
		$data['dv_received'] = $this->input->post('dv_received');
		}
		$data['dv_received'] = $cn_status;
		$data['claim_id'] = $claim_id;
		 $this->db->select('*'); 
		 $this->db->where('claim_id',$claim_id);
		 $cn_count = $this->db->get('discharge_voucher_generation')->num_rows();
		 if($cn_count>=1){
		 $this->db->where('claim_id', $claim_id);
		 $result = $this->db->update('discharge_voucher_generation', $data);
		 }
		 else
		$result = $this->db->insert('discharge_voucher_generation', $data);
			if($this->db->_error_number()==1062)
			{
			return 0;
			}
			else
			{
			return $result;
			}
	}
	
	public function insert_cheque_details($claim_id)
	{
		
		if($this->input->post('cheque_received_date')!= null)
		{
		$data['cheque_received_date'] = $this->input->post('cheque_received_date');
		}
		if($this->input->post('cheque_type')!= null)
		{
		$data['cheque_type'] = $this->input->post('cheque_type');
		}
				 
		 $this->db->where('claim_id',$claim_id);
		 $result = $this->db->update('discharge_voucher_generation', $data);
			if($this->db->_error_number()==1062)
			{
				return 0;
			}
			else
			{
			return $result;
			}
	}
	
	public function cheque_received($claim_id,$cn_status)
	{
		if($this->input->post('cheque_received')!= null)
		{
		$data['cheque_received'] = $this->input->post('cheque_received');
		}
		$data['cheque_received'] = $cn_status;
		$data['claim_id'] = $claim_id;
		 $this->db->select('*'); 
		 $this->db->where('claim_id',$claim_id);
		 $cn_count = $this->db->get('discharge_voucher_generation')->num_rows();
		 if($cn_count>=1)
		 $result = $this->db->update('discharge_voucher_generation', $data);
		 else
		$result = $this->db->insert('discharge_voucher_generation', $data);
			if($this->db->_error_number()==1062)
			{
			return 0;
			}
			else
			{
			return $result;
			}
	}
	
	public function insure_send_email(){
			
			$email_to = trim($this->input->post('email_address'));
			$subject = $this->input->post('email_subject');
			$body = $this->input->post('email_body');
			
			$this->email->from('info@shikharinsurance.com', 'Shikhar Insurance');
			$this->email->to($email_to); 
			//$this->email->cc('another@another-example.com'); 
			//$this->email->bcc('them@their-example.com'); 
			
			$this->email->subject($subject);
			$this->email->message($body);	
			
			$this->email->send();
			return '1';
	}
	
	public function show_all_discharge_voucher_generation_details()
	{
		$this->db->group_by('claim_registration.claim_id');
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('surveyor_deportation', 'claim_registration.claim_id = surveyor_deportation.claim_id','left');
		$this->db->join('assigned_surveyors', 'claim_registration.claim_id = assigned_surveyors.claim_id','left');
		$this->db->join('claim_note_preparation', 'claim_registration.claim_id = claim_note_preparation.claim_id','left');
		$this->db->join('discharge_voucher_generation', 'claim_registration.claim_id = discharge_voucher_generation.claim_id','left');
		$this->db->join('legal', 'claim_registration.claim_id = legal.claim_id','left');
		$this->db->join('approve_recommend', 'claim_registration.claim_id = approve_recommend.claim_id','left');
		$this->db->join('insure', 'claim_registration.insure = insure.insure_name','left');
		$this->db->order_by("claim_registration.claim_id", "desc"); 
		$this->db->where('status','4');
		//$this->db->or_where('status',1);
		$result = $this->db->get();
		//$this->db->where('status', 1);
		//$this->db->update('claim_registration',  array('status' => 2)); 
		return $result;
	}
	
	public function count_discharge_voucher_generation_details()
	{
		$this->db->group_by('claim_registration.claim_id');
		$this->db->select('*');
		$this->db->from('claim_registration');
		
		$this->db->where('status','4');
		//$this->db->or_where('status',1);
		$result = $this->db->get()->num_rows();
		//$this->db->where('status', 1);
		//$this->db->update('claim_registration',  array('status' => 2)); 
		return $result;
	}
	
	public function show_all_discharge_voucher_generation_details_for_claim()
	{
		$result = $this->db->get('discharge_voucher_generation');
		return $result->result_array();
	
	}
	
	public function get_details_of_discharge_voucher_generation_by_id ($discharge_voucher_generation_id)
	{
	$result = $this->db->get_where('discharge_voucher_generation', array('discharge_voucher_generation_id' => $discharge_voucher_generation_id));
	return $result->result_array();
	}
	
	public function delete_discharge_voucher_generation_by_id($discharge_voucher_generation_id)
	{
	$result = $this->db->delete('discharge_voucher_generation', array('discharge_voucher_generation_id' => $discharge_voucher_generation_id)); 
	return $result;
	}
	
	public function edit_discharge_voucher_generation_by_id($discharge_voucher_generation_id)
	{
	
	$data = array(
	'egg_inoculation' => $this->input->post('egg_inoculation'),
	'plate_aggluination' => $this->input->post('pa_result'),
	'rapid_anitgen_test' => $this->input->post('rapid_anitgen_test'),
	'negri_body' =>$this->input->post('negri_body'),
	'penside_test' =>$this->input->post('penside_test'),
	'ha' => $this->input->post('ha'),
	'hi' => $this->input->post('hi'),
	'cft' => $this->input->post('cft_result'),
	'agid' => $this->input->post('agid_result'),
	'elisa' => $this->input->post('elisa_result'),
	'confirmative_diagnosis' => $this->input->post('cd_result'),
	'fat' => $this->input->post('fat')
	);
	$this->db->where('discharge_voucher_generation_id',$discharge_voucher_generation_id);
	$result = $this->db->update('discharge_voucher_generation', $data);
	return $result;
	}

	public function check_new_customer()
	{
		$this->db->group_by('claim_registration.claim_id');
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('discharge_voucher_generation', 'claim_registration.claim_id = discharge_voucher_generation.claim_id');
		$this->db->order_by("discharge_voucher_generation_id", "desc"); 
		$this->db->where('status',0);
		$result = $this->db->get();
		$i = 0;
		$data_arr = array();
		foreach($result->result() as $row)
		{
			$data_arr[$i]["discharge_voucher_generation_id"] = $row->discharge_voucher_generation_id;
			$data_arr[$i]["labno"] = $row->labno;
			$data_arr[$i]["claim_id"] = $row->claim_id;
			$data_arr[$i]['customer_name'] = $row->customer_name;
			$data_arr[$i]['firm_name'] = $row->firm_name;
			$data_arr[$i]['animal'] = $row->animal;
			$data_arr[$i]['claim_type'] = $row->claim_type;
			$data_arr[$i]['no_of_claim'] = $row->no_of_claim;
			$data_arr[$i]['age'] = $row->age;
			$data_arr[$i]['sex'] = $row->sex;
			$data_arr[$i]['animal_type'] = $row->animal_type;
			$data_arr[$i]['species_type'] = $row->species_type;
			$data_arr[$i]['investigation_requested'] = $row->investigation_requested;
			$data_arr[$i]['egg_inoculation'] = $row->egg_inoculation;
			$data_arr[$i]['pa_result'] = $row->plate_aggluination;
			$data_arr[$i]['ha'] = $row->ha;
			$data_arr[$i]['hi'] = $row->hi;
			$data_arr[$i]['cft_result'] = $row->cft;
			$data_arr[$i]['agid_result'] = $row->agid;
			$data_arr[$i]['elisa_result'] = $row->elisa;
			$data_arr[$i]['cd_result'] = $row->confirmative_diagnosis;
			$this->db->where('claim_id',$row->claim_id);
			$this->db->update('discharge_voucher_generation',array('status'=>1));
			$i++;
		}
		echo json_encode($data_arr);
	}

	public function set_accept_on($claim_id)
	{
	$this->db->where('claim_id',$claim_id);
	$result = $this->db->update('discharge_voucher_generation',array('status' => 3));
	return $result;
	}	
	public function search_pending_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('discharge_voucher_generation', 'claim_registration.claim_id = discharge_voucher_generation.claim_id');
		$this->db->order_by("discharge_voucher_generation_id", "desc"); 
		$this->db->where('status',5);
		$this->db->where('publish_bit',0);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}
	public function set_publish_on($discharge_voucher_generation_id)
	{
	$this->db->where('discharge_voucher_generation_id',$discharge_voucher_generation_id);
	$result = $this->db->update('discharge_voucher_generation',array('publish_bit' => 1));
	return $result;
	}
	public function search_on_progress_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('discharge_voucher_generation', 'claim_registration.claim_id = discharge_voucher_generation.claim_id');
		$this->db->order_by("discharge_voucher_generation_id", "desc"); 
		$this->db->where('status',3);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}
	public function set_completed_on($discharge_voucher_generation_id)
	{
	$this->db->where('discharge_voucher_generation_id',$discharge_voucher_generation_id);
	$result = $this->db->update('discharge_voucher_generation',array('status' => 4));
	return $result;
	}
	public function search_completed_claim($offset)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('discharge_voucher_generation', 'claim_registration.claim_id = discharge_voucher_generation.claim_id');
		$this->db->order_by("discharge_voucher_generation_id", "desc"); 
		$this->db->where('status',4);
		$this->db->or_where('status',5);
		$data['count'] = $this->db->get()->num_rows();
		$query = $this->db->last_query();
		$offset = $offset*6;
		$data['result'] = $this->db->query($query.' LIMIT '.$offset.',6');
		return $data;
	}

	public function generate_report($discharge_voucher_generation_id)
	{
		$this->db->select('*');
		$this->db->from('claim_registration');
		$this->db->join('discharge_voucher_generation', 'claim_registration.claim_id = discharge_voucher_generation.claim_id');
		$this->db->join('customer','claim_registration.customer_id = customer.cid');
		$this->db->where('discharge_voucher_generation_id',$discharge_voucher_generation_id);
		$result = $this->db->get();
		return $result;
	}



}