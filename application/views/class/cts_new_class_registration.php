<div class="container">
<div class='alert alert-danger' role='alert' id="animal_indicator_alert"'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Animal Already Exists</b></div>
  <h3>Animal Registration Form</h3><br>
      <table class="table" id="table">

        <form method="post" role="form" enctype="multipart/form-data" id="animal_form">
			<tr>
              <td><label>Animal Name </label></td>
              <td><input type="text" class="form-control" name="animal_name"  required> </td>
            </tr>

            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary" id="animal_submit">Submit</button></td>
            </tr>
          
        </form>
    </table>
</div><!---end of container-->