<?php

class cts_investigation_model extends CI_Model 
{
	public function insert_investigation_details()
	{
	$lab_id = $this->input->post('lab_investigation');
	$data = array(
	'investigation_requested' => $this->input->post('investigation_requested')
	);
	$result = $this->db->get_where('investigation_requested',array('investigation_requested'=>$data['investigation_requested']));
		if($result->num_rows >= 1)
		{
		return false;
		}
		else
		{
		$result = $this->db->insert('investigation_requested', $data);
		$last_inserted_investigation_id = $this->db->insert_id();
		$this->db->insert('lab_investigation',array('lab_id'=>$lab_id,'investigation_id'=>$last_inserted_investigation_id));
		$this->db->select('*');
		$this->db->from('investigation_requested');
		$this->db->join('lab_investigation','lab_investigation.investigation_id = investigation_requested.investigation_requested_id');
		$this->db->join('department','lab_investigation.lab_id = department.dep_id');
		$this->db->where('investigation_requested_id',$last_inserted_investigation_id);
		$result = $this->db->get();
		 //$this->db->get_where('investigation_requested',array('investigation_requested_id'=>$last_inserted_investigation_id));
		return $result;
		}
	}
	public function add_investigation_details()
	{
		$lab_id = $this->input->post('lab_investigation');
		$data = array(
	'investigation_requested' => $this->input->post('investigation_requested')
	);
	$result = $this->db->get_where('investigation_requested',array('investigation_requested'=>$data['investigation_requested']));
		if($result->num_rows == 1)
		{
		return false;
		}
		else
		{
		$result = $this->db->insert('investigation_requested', $data);
		$last_inserted_investigation_id = $this->db->insert_id();
		$result = $this->db->insert('lab_investigation',array('lab_id'=>$lab_id,'investigation_id'=>$last_inserted_investigation_id));
		return $result;
	}
	}
	public function edit_investigation_requested_by_id($investigation_requested_id)
		{
		$data = array(
		'investigation_requested' => $this->input->post('investigation_requested')
		);
		$lab_id = $this->input->post('lab_investigation');
		
		$this->db->where('investigation_id', $investigation_requested_id);
		$result1 = $this->db->update('lab_investigation',array('lab_id'=>$lab_id));
		
		$result = $this->db->get_where('investigation_requested',array('investigation_requested'=>$data['investigation_requested']));
		if($result->num_rows >= 1)
		{
		return false;
		}
		else
		{
		$this->db->where('investigation_requested_id', $investigation_requested_id);
		$result = $this->db->update('investigation_requested', $data);
		echo $result1;
		exit;
		//return $result;
	}
	}
	public function get_lab_investigation_by_id($investigation_id)
	{
	$result = $this->db->get_where('lab_investigation',array('investigation_id'=>$investigation_id));
		foreach($result->result() as $row)
		{
		$lab_id = $row->lab_id;
		}
	return $lab_id;	
	}



	public function search_investigation_detail($key)

	{
		$result = $this->db->query("select * from investigation_requested where investigation_requested like '".$key."%'");
		
		$investigation_requests=array();
		foreach($result->result() as $row)
		{
			$investigation_requested= "";
			$investigation_requests []= $row->investigation_requested;
		}
		echo json_encode($investigation_requests);
	}
	
	public function show_all_investigation_requested($limit, $start)
	{
		$this->db->order_by("investigation_requested_id","asc");
		$this->db->limit($limit, $start);
		$query = $this->db->get('investigation_requested');
		return $query;

	
	}
	
	public function get_details_of_investigation_requested_by_id($investigation_requested_id)
	{
	$result = $this->db->get_where('investigation_requested', array('investigation_requested_id' => $investigation_requested_id));
	return $result->result_array();
	}
	
	public function delete_investigation_requested_by_id($investigation_requested_id)
	{
		$result = $this->db->delete('investigation_requested', array('investigation_requested_id' => $investigation_requested_id)); 
		return $result;
	}

	public function check_investigation()
	{
		$investigation_requested = $this->input->post('investigation_requested');
		$result = $this->db->get_where('investigation_requested',array('investigation_requested'=>$investigation_requested));
		if($result->num_rows()>=1)
		{
		return true;
		}
		else
		{
		return false;
		}
	}

		public function getInvestigationRequested()
	{
		$result = $this->db->get('investigation_requested');
		return $result->result();
	}
	public function get_investigation_name($investigation_id)
	{
	$result = $this->db->get_where('investigation_requested',array('investigation_requested_id'=>$investigation_id));
	return $result;
	}
	public function get_lab_investigaion()
	{
		$labid = $this->input->post('labid');
		$this->db->select('*');
		$this->db->from('lab_investigation');
		$this->db->join('investigation_requested', 'investigation_requested.investigation_requested_id = lab_investigation.investigation_id');
		$this->db->join('department', 'department.dep_id = lab_investigation.lab_id');
		$this->db->where('lab_id',$labid);
		$result = $this->db->get();
		return $result;
	}
	public function get_lab_investigation($lab_id)
	{
		$this->db->select('*');
		$this->db->from('lab_investigation');
		$this->db->join('investigation_requested', 'investigation_requested.investigation_requested_id = lab_investigation.investigation_id');
		$this->db->join('department', 'department.dep_id = lab_investigation.lab_id');
		$this->db->where('lab_id',$lab_id);
		$result = $this->db->get();
		return $result;
	}
	public function get_investigation_of_lab()
	{
		$this->db->select('*');
		$this->db->from('lab_investigation');
		$this->db->join('investigation_requested', 'investigation_requested.investigation_requested_id = lab_investigation.investigation_id');
		$this->db->join('department', 'department.dep_id = lab_investigation.lab_id');
		$result = $this->db->get();
		return $result;
		
	}

}