<?php
/* if(isset($result))
{
	if($result==1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Update Succesful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Update Failed!</b></div>";
}
if(isset($biology_details)){
foreach($biology_details as $row)
{
$biology_id = $row['biology_id'];
$collection_date = $row['collection_date'];
$received_date = $row['received_date'];
$extraction_date = $row['extraction_date'];
$rt_m_gene = $row['rt_m_gene'];
$rt_h5 = $row['rt_h5'];
$rt_n1 = $row['rt_n1'];
$rt_h9 = $row['rt_h9'];
$rt_h7 = $row['rt_h7'];
$cr_h9 = $row['cr_h9'];
$cr_h5 = $row['cr_h5'];
$others = $row['others'];
$remarks = $row['remarks'];
$tested_date = $row['tested_date'];
$approved_date = $row['approved_date'];
}
} */
?>

  <div class="container">
    <h3>Edit Biology Detail Form</h3><br>
	
      <table class="table" id="table">
        <form method="post" role="form" enctype="multipart/form-data" id="edit_biology_form">      
			
			            <tr>
              <td><label>Collection Date</label></td>
              <td><input type="text" class="form-control" name="collection_date" id="collection_dat" ></td>
            </tr>
            <tr>
              <td><label>Received Date</label></td>
              <td><input type="text" class="form-control" name="received_date" id="received_dat" value="<?php //echo $received_date;?>" ></td>
            </tr>
            <tr>
              <td><label>Extraction Date</label></td>
              <td><input type="text" class="form-control" name="extraction_date" id="extraction_dat" value="<?php //echo $extraction_date;?>" ></td>
            </tr>
          
		  <tr>
              <td><label>Sample Submitted By</label></td>
              <td><input type="text" class="form-control" name="sample_submitted_by" id="sample_submitted"></td>
            </tr>
            <tr>
              <td><label>Flock Size</label></td>
              <td><input type="number" class="form-control" name="flock_size" id="flock_siz"></td>
            </tr>
            <tr>
              <td><label>Mortality</label></td>
              <td><input type="number" class="form-control" name="mortality" id="mortalit"></td>
            </tr>


		   <tr>
              <td colspan ='2'><label><b>PCR Result </b> </label></td>
              
            </tr>
          <tr>
              <td><label>M Gene</label></td>
               <td><input type="radio" class="radio-inline" name="pcr_m_gene_result" value="positive" id="pcr_m_gene_positive">&nbsp;Positive</td>
               <td><input type="radio" class="radio-inline" name="pcr_m_gene_result" value="negative" id="pcr_m_gene_negative" >&nbsp;Negative</td>
            </tr>
            <tr>
              <td><label>H5</label></td>
              <td><input type="radio" class="radio-inline" name="pcr_h5_result" value="positive" id="pcr_h5_positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_h5_result" value="negative" id="pcr_h5_negative" >&nbsp;Negative</td>
            </tr>
			<tr>
              <td><label>N1</label></td>
              <td><input type="radio" class="radio-inline" name="pcr_n1_result" value="positive" id="pcr_n1_positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_n1_result" value="negative" id="pcr_n1_negative" >&nbsp;Negative</td>
            </tr> 
			<tr>
              <td><label>N9</label></td>
              <td><input type="radio" class="radio-inline" name="pcr_n9_result" value="positive" id="pcr_n9_positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_n9_result" value="negative" id="pcr_n9_negative" >&nbsp;Negative</td>
            </tr> 
			<tr>
              <td><label>H9</label></td>
              <td><input type="radio" class="radio-inline" name="pcr_h9_result" value="positive" id="pcr_h9_positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_h9_result" value="negative" id="pcr_h9_negative" >&nbsp;Negative</td>
            </tr>
			<tr>
              <td><label>H7 </label> </td>
             <td><input type="radio" class="radio-inline" name="pcr_h7_result" value="positive" id="pcr_h7_positive" >&nbsp;Positive</td>
             <td><input type="radio" class="radio-inline" name="pcr_h7_result" value="negative" id="pcr_h7_negative" >&nbsp;Negative</td>
            </tr>			
			
			<tr>
              <td><label>IBD</label></td>
              <td><input type="radio" class="radio-inline" name="pcr_ibd_result" value="positive" id="pcr_ibd_positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_ibd_result" value="negative" id="pcr_ibd_negative" >&nbsp;Negative</td>
            </tr>
			<tr>
              <td><label>ND</label></td>
              <td><input type="radio" class="radio-inline" name="pcr_nd_result" value="positive" id="pcr_nd_positive" >&nbsp;Positive</td>
              <td><input type="radio" class="radio-inline" name="pcr_nd_result" value="negative" id="pcr_nd_negative" >&nbsp;Negative</td>
            </tr>
			<tr>
              <td><label>CT Value of Positive</label></td>
              <td><input type="text" class="form-control" name="ct_value" id="ct_value" value="<?php //echo $others;?>" ></td>
            </tr>
			<tr>
              <td><label>Others</label></td>
              <td><input type="text" class="form-control" name="others" id="others" value="<?php //echo $others;?>" ></td>
            </tr>
			<tr>
              <td><label>Remarks</label></td>
              <td><textarea type="textarea" class="form-control" name="remarks" id="remarks" value="<?php //echo $remarks;?>"  ></textarea></td>
            </tr>
            
			
            <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary" id="submit" >Submit</button></td>
            </tr>
          
        </form>
    </table>
	</div>
</div><!---end of container-->
</body>
</html>