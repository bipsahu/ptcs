<?php
if(isset($roles_info))
{
$i=0;
$role = array();
foreach($roles_info->result_array() as $key=>$value)
{
$role[] = $value;
$module[] = $role[$i]['module_name'];
$i++;
}
}
?>
<div class="container">
<center><h1>All Customer</h1></center>
<?php
if(!empty($result_indicator))
{
	if($result_indicator == 1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Deletion Successful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Deletion Failed!</b></div>";
}
?>
<table class="table table-striped" id="table_show_all_customer">
<thead>
	<td>S.No.</td>
	<td>
		<ul style="margin-bottom: 0px; padding-left: 0px;">
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">First Name <span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu">
			    <li> <a href="<?php echo base_url()?>index.php/lims_customer_controller/show_all_customer"> Ascending Order </a> </li>
			    <li> <a href="<?php echo base_url()?>index.php/lims_customer_controller/show_all_customer_desc"> Descending Order </a> </li>
			</ul>
		</li>
		</ul>
	</td>
	<td>Middle Name</td>
	<td>
		<ul style="margin-bottom: 0px; padding-left: 0px;">
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">Last Name <span class="caret"></span></a>
		    <ul class="dropdown-menu" role="menu">
		        <li> <a href="<?php echo base_url()?>index.php/lims_customer_controller/show_all_customer_by_last_asc"> Ascending Order </a> </li>
		        <li> <a href="<?php echo base_url()?>index.php/lims_customer_controller/show_all_customer_desc"> Descending Order </a> </li>        
			</ul>
		</li>
		</ul>
	</td>
	<td>Zone</td>
	<td>
		<ul style="margin-bottom: 0px; padding-left: 0px;">
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">District<span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu">
			    <li> <a href="<?php echo base_url()?>index.php/lims_customer_controller/show_all_customer_by_district_asc"> Ascending Order </a> </li>
			    <li> <a href="<?php echo base_url()?>index.php/lims_customer_controller/show_all_customer_by_district_desc"> Descending Order </a> </li>
			</ul>
		</li>
		</ul>
	</td>
	<td>VDC/Municipality</td>
	<td>VDC/Municipality No.</td>
	<td>Street</td>
	<td>Mobile No.</td>
	<td>Phone</td>
	<td>Email</td>
	<?php 
		if(strtolower($this->session->userdata('user_position'))=="admin")
				{
				echo "<th>Edit</th>";
				}
				elseif( isset($role) || $role[3]['edit'])
				{
				echo "<th>Edit</th>";
				}
				?>
				<?php
				if(strtolower($this->session->userdata('user_position')) == "admin")
				{
				echo "<th>Delete</th>";
				}
				elseif( isset($role) || $role[3]['delete'])
				{
				echo "<th>Delete</th>";
				}


?>
</thead>

<?php 
$offset = $offset+1;
foreach($results->result() as $row){
?>
<tr>
<td><?php echo $offset;?></td>
<td><?php echo $row->customer_first_name;?></td>
<td><?php echo $row->customer_middle_name;?></td>
<td><?php echo $row->customer_last_name;?></td>
<td><?php echo $row->customer_zone;?></td>
<td><?php echo $row->customer_district;?></td>
<td><?php echo $row->customer_vdc_municipality;?></td>
<td><?php echo $row->customer_vdc_ward_no;?></td>
<td><?php echo $row->customer_street;?></td>
<td><?php echo $row->customer_mobile;?></td>
<td><?php echo $row->customer_phone;?></td>
<td><?php echo $row->customer_email;?></td>
<?php 
	if(strtolower($this->session->userdata('user_position'))=="admin")
	{
	echo "<td><a href='".base_url()."index.php/lims_customer_controller/edit_customer_by_id_form/".$row->cid."'><span class='glyphicon glyphicon-edit'></span></a></td>";
	}
	elseif( isset($role) ||  $role[3]['edit'])
	{
	echo "<td><a href='".base_url()."index.php/lims_customer_controller/edit_customer_by_id_form/".$row->cid."'><span class='glyphicon glyphicon-edit'></span></a></td>";
	}
	if(strtolower($this->session->userdata('user_position'))=="admin")
	{
	echo "<td><a href='#'><span class='glyphicon glyphicon-trash' alt='".$row->cid."'></span></a></td>";
	}
	elseif( isset($role) ||  $role[3]['delete'])
	{
	echo "<td><a href='#'><span class='glyphicon glyphicon-trash' alt='".$row->cid."'></span></a></td>";
	}
?>
</tr>
<?php
$offset++;
}
?>
<tr><td colspan="13"> <?php echo $links; ?></td></tr>
<?php if(strtolower($this->session->userdata('user_position'))=="admin" || isset($role) ||  $role[3]['add']){?>
<tr>
<td colspan="9"><a class="btn btn-primary" href="<?php echo base_url();?>index.php/lims_customer_controller/add_new_customer">Add New Customer</a>

<?php }?>


</td>
</tr>
</table>

</div>
<div id="dialog-confirm" title="Delete the Customer?">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

</body>
<script type="text/javascript">
$(document).ready(function(){
 $( "#dialog-confirm" ).hide();
$('.glyphicon-trash').click(function(){
var customer_id = $(this).attr('alt');
 $( "#dialog-confirm" ).dialog({
resizable: false,
height:160,
modal: true,
 show: {
effect: "blind",
duration: 300
},
hide: {
effect: "blind",
duration: 300
},
buttons: {
"Delete": function() {
window.location.assign('<?php echo base_url();?>'+'index.php/lims_customer_controller/delete_customer_by_id/'+customer_id);
},
Cancel: function() {
$( this ).dialog( "close" );
}
}
});
});
});
</script>
</html>