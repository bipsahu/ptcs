<?php
if(isset($result))
{
	if($result==1)
	{
		echo "<div class='alert alert-info' role='alert' id='register-alert'><div class='glyphicon glyphicon-ok'></div>&nbsp;<b>Update Succesful!</b></div>";
	}
	else
	echo "<div class='alert alert-danger' role='alert' id='register-alert'><div class='glyphicon glyphicon-remove'></div>&nbsp;<b>Update Failed!</b></div>";
}
if(isset($investigation_requested)){
foreach($investigation_requested as $row)
{
$investigation_requested_id = $row['investigation_requested_id'];
$investigation_requested = $row['investigation_requested'];
}
}
?>
<div class="container">
    <h3>Edit Investigation Requested Form</h3><br>
	<form method="post" role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/lims_investigation_controller/edit_investigation_requested_by_id/<?php echo $investigation_requested_id;?>">

           <table class="table" id="table">

        
			<tr><td><label>Select the Lab</label></td>
			<td><select name="lab_investigation" class="form-control">
			<?php
			foreach($departments as $row) {
			if($lab_id_in == $row->dep_id){
			?>
			<option selected value="<?php echo $row->dep_id;?>"><?php echo $row->dep_name;?></option>
			<?php }
			else
			{
			?>
			<option value="<?php echo $row->dep_id;?>"><?php echo $row->dep_name;?></option>
			<?php } 
			}?>
			</select>
			</td></tr>
			<tr>
              <td><label>Investigation Requested</label></td>
              <td><input type="text" class="form-control" name="investigation_requested" value="<?php echo $investigation_requested;?>" required></td>
			  <td></td>
            </tr> 
		
		  <tr>
        	   <td colspan="2"><button type="submit" name="submit" class="btn btn-primary">Submit</button></td>
			   <td></td>
            </tr>
          
        
    </table>
	</form>
</div><!---end of container-->
 </body>
 </html>
 
			

